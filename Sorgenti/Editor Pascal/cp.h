#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <io.h>
#define PERM 0644

int copy (char *sorg, char *dest)
{
	int nread, nwrite, infile, outfile;
	char buffer[BUFSIZ];
	infile=open(sorg,O_RDONLY);
	outfile=creat(dest,PERM);
	while(((nread=read(infile,buffer,BUFSIZ))>0))
	{
		nwrite=write(outfile,buffer,nread);
		if(nread != nwrite)
		{
			close(infile);
			close(outfile);
			return 1;
		}
	}
	close(infile);
	close(outfile);
	return 0;
}