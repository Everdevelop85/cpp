//---------------------------------------------------------------------------
#ifndef cppDataProjectH
#define cppDataProjectH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class TfrmDataProject : public TForm
{
__published:	// IDE-managed Components
        TLabel *Label1;
        TEdit *txtND;
        TLabel *Label2;
        TEdit *txtNP;
        TLabel *Label3;
        TEdit *txtDP;
        TButton *cmdBrowse;
        TButton *cmdOk;
        TButton *cmdCancel;
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall cmdOkClick(TObject *Sender);
        void __fastcall cmdOkExit(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall txtNPChange(TObject *Sender);
        void __fastcall txtNPKeyPress(TObject *Sender, char &Key);
        void __fastcall cmdBrowseClick(TObject *Sender);
        void __fastcall cmdCancelClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TfrmDataProject(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfrmDataProject *frmDataProject;
//---------------------------------------------------------------------------
#endif
