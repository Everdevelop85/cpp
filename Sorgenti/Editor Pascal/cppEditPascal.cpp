//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "cppEditPascal.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TfrmMain *frmMain;
PROJECT *Work;
ERROR_TYPE *Err;
//---------------------------------------------------------------------------
__fastcall TfrmMain::TfrmMain(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------

void Commenta(AnsiString text)
{
        int i=1;
        text.Insert("(*",0);
        text.Insert("*)",text.Length()+1);
        if(strstr(text.c_str(),"\r\n")==NULL)
        {
                frmMain->memEditor->ClearSelection();
                frmMain->memEditor->Lines->Insert(frmMain->memEditor->CaretPos.y,text);
                return;
        }
        while(i<text.Length())
        {
                if(text[i]=='\r')
                {
                        text.Insert("*)",i);
                        i+=2;
                }
                else
                {
                        if(text[i]=='\n')
                        {
                                text.Insert("(*",i+1);
                                i+=2;
                        }
                }
                i++;
        }
        frmMain->memEditor->ClearSelection();
        frmMain->memEditor->Lines->Insert(frmMain->memEditor->CaretPos.y,text);
        frmMain->memEditor->Lines->Delete(frmMain->memEditor->CaretPos.y);
}

void TogliCommenta(AnsiString text)
{
        int i=2;
        text.Delete(1,2);
        text.Delete(text.Length()-1,text.Length());
        if(strstr(text.c_str(),"\r\n")==NULL)
        {
                frmMain->memEditor->ClearSelection();
                frmMain->memEditor->Lines->Insert(frmMain->memEditor->CaretPos.y,text);
                return;
        }
        while(i<text.Length()-1)
        {
                if((text[i]=='(') && (text[i+1]=='*'))
                {
                        text.Delete(i,2);
                        i-=2;
                }
                else
                {
                        if((text[i]=='*') && (text[i+1]==')'))
                        {
                                text.Delete(i,2);
                                i-=2;
                        }
                }
                i++;
        }
        frmMain->memEditor->ClearSelection();
        frmMain->memEditor->Lines->Insert(frmMain->memEditor->CaretPos.y,text);
        frmMain->memEditor->Lines->Delete(frmMain->memEditor->CaretPos.y);
};

void SelectLInesError()
{
        int selst=0;
        int i=0;
        AnsiString s;
        for(i=0;i<Err->Location.x;i++)
        {
                s=frmMain->memEditor->Lines->Strings[i];
                selst+=s.Length();
        }
        frmMain->memEditor->SelStart=selst-2;
        frmMain->memEditor->SelLength=frmMain->memEditor->Lines->Strings[Err->Location.x].Length();
        frmMain->memEditor->SetFocus();
}

AnsiString GetName(char *p)
{
        AnsiString app;
        int i=strlen(p)-1;
        while(p[i]!='\\')
                i--;
        p+=(i+1);
        app=p;
        return app;
};

/*bool Controllo(char *l)
{

};*/
void SearchOnString(AnsiString str)
{
        int i,p;
        for(j=0;j<22;j++)
        {
                if((str.Pos(charText[i]))>0)
                {
                        p=frmMain->memEditor->Lines->Strings[i].Pos(charText[j])-1;
                }
        }
}

void remarkAll()
{
        int i,j=0,charCount=0,p;
        AnsiString app;
        bool trovato=false;
        for(i=0;i<frmMain->memEditor->Lines->Count;i++)
        {
                app=frmMain->memEditor->Lines->Strings[i];
                for(j=0;j<22;j++)
                {
                        if((app.Pos(charText[j]))>0)
                                trovato=true;
                }
                if(trovato)
                {
                        while((app.Pos(charText[j]))>0)
                        {
                                for(j=0;j<22;j++)
                                {
                                        if((app.Pos(charText[j]))>0)

                                        p=frmMain->memEditor->Lines->Strings[i].Pos(charText[j])-1;
                                        if(p>0)
                                        {
                                                if(((app[p-1]=='(')||(app[p-1]==' ')||(app[p-1]==':'))&&((app[(p+1)+(strlen(charText[j]))]==')')||(app[(p+1)+(strlen(charText[j]))]==' ')))
                                                {
                                                        frmMain->memEditor->SelStart=p+charCount;
                                                        frmMain->memEditor->SelLength=strlen(charText[j]);
                                                        frmMain->memEditor->SelAttributes->Style=frmMain->memEditor->SelAttributes->Style<<fsBold;
                                                        app=app.SubString(((p+strlen(charText[j]))+1),app.Length());
                                                }
                                        }
                                        else
                                        {
                                                frmMain->memEditor->SelStart=p+charCount;
                                                frmMain->memEditor->SelLength=strlen(charText[j]);
                                                frmMain->memEditor->SelAttributes->Style=frmMain->memEditor->SelAttributes->Style<<fsBold;
                                                app=app.SubString(((p+strlen(charText[j]))+1),app.Length());
                                        }
                                }
                        }
                }
                charCount+=frmMain->memEditor->Lines->Strings[i].Length()+2;
        }
};

void remark(char *line, int curX, int curY)
{
/*
        char *appL;
        while((appL=(strstr(line)))!=NULL)
        /*int selS=0,selL=0, LenWord;
        char Word[100];
        for(int i=0;i<curY-1;i++)
                selL+=frmMain->memEditor->Lines->Strings[i].Length();
        selL+=curX;
        LenWord=curX;
        while((LenWord>0) && ((line[LenWord]>='a') && (line[LenWord]<='z')))
                LenWord--;
        selS=selL-LenWord;
        if(selS==selL)
                selS=0;
        frmMain->memEditor->SelStart=selS;
        frmMain->memEditor->SelLength=selL;
        frmMain->memEditor->SelAttributes->Style=frmMain->memEditor->SelAttributes->Style<<fsBold;
        /*RichEdit1->SelAttributes->Style =
                       RichEdit1->SelAttributes->Style >> fsBold*/
        /*frmMain->memEditor->SelStart=selL;
        frmMain->memEditor->SelLength=1;
        frmMain->memEditor->SetFocus();*/
};


void __fastcall TfrmMain::FormConstrainedResize(TObject *Sender,
      int &MinWidth, int &MinHeight, int &MaxWidth, int &MaxHeight)
{
        memEditor->Width=frmMain->Width-10;
        lstViewError->Width=frmMain->Width-10;
        lstViewError->Top=frmMain->Height-(lstViewError->Height+73);
        memEditor->Height=lstViewError->Top-memEditor->Top;
        lstViewError->Columns->Items[0]->Width=(lstViewError->Width/3)-10;
        lstViewError->Columns->Items[1]->Width=(lstViewError->Width/3)-10;
        lstViewError->Columns->Items[2]->Width=(lstViewError->Width/3)-10;
        StatusBar->Panels->Items[0]->Width=(StatusBar->Width/4);
        StatusBar->Panels->Items[1]->Width=(StatusBar->Width/2)-10;
        
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::FormCreate(TObject *Sender)
{
        getcwd(DirComp,MAX);
        Work = new PROJECT(DirComp);
        AnsiString fileBMP;
        fileBMP=DirComp;
        fileBMP+="\\icons\\ImgList.bmp";
        strcpy(ColorFunction,DirComp);
        strcat(ColorFunction,"\\TXTCOLOR.EXE");
        Graphics::TBitmap *Bitmap = new Graphics::TBitmap();
        Bitmap->LoadFromFile(fileBMP);
        ImageList->Add(Bitmap,NULL);
        StatusBar->Panels->Items[2]->Text=DateToStr(Now());
        if(Work->ProjectEmpty())
        {
                mnuEditItem3->Enabled=false;
                mnuEditItem4->Enabled=false;
                mnuEditItem6->Enabled=false;
                mnuEditItem7->Enabled=false;
                cmdCut->Enabled=false;
                cmdCopy->Enabled=false;
                cmdCommenta->Enabled=false;
                cmdDelComm->Enabled=false;
                cmdRun->Enabled=false;
        }
        
        
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::mnuFileItem1Click(TObject *Sender)
{
        frmDataProject->Show();
        frmMain->Visible=false; 
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::mnuFileItem2Click(TObject *Sender)
{
        dlgOpen->InitialDir=Application->GetNamePath();
        dlgOpen->Execute();
        if(dlgOpen->FileName!="")
        {
                Work->SetProject(dlgOpen->FileName.c_str());
                frmMain->Caption="Pascal Editor - "+Work->GetFileProject();
        }
        Err = new ERROR_TYPE(Work->DirCompyler);
        if(!Work->ProjectEmpty())
        {
                mnuEditItem3->Enabled=true;
                mnuEditItem4->Enabled=true;
                mnuEditItem6->Enabled=true;
                mnuEditItem7->Enabled=true;
                cmdCut->Enabled=true;
                cmdCopy->Enabled=true;
                cmdCommenta->Enabled=true;
                cmdDelComm->Enabled=true;
                cmdRun->Enabled=true;
                StatusBar->Panels->Items[1]->Text=Work->GetInfoProject();
                remarkAll();
        }
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cmdRunClick(TObject *Sender)
{
        if(Work->ProjectEmpty())
        {
                if((MessageBox(frmMain->Handle,"E' necessario salvare il progetto prima di eseguire la compilazione. Salvarlo ?","Attenzione",MB_ICONEXCLAMATION+MB_YESNO))==ID_YES)
                        mnuFileItem4Click(Sender);        
        }
        else
        {
                lstViewError->Items->Clear();
                lstViewError->Repaint();
                if(Work->Compyle()==0)
                {
                        ShellExecute(0,"",Work->FileExe,"",Work->DirProject,1);
                        Err->ShowNoErrors();
                }
                else
                {
                        if(Err->ErrorControl())
                        {
                                Err->ShowErrors();
                                SelectLInesError();
                        }
                }
        }
}
//---------------------------------------------------------------------------


void __fastcall TfrmMain::mnuFileItem7Click(TObject *Sender)
{
        dlgPrint->Execute();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cmdSaveClick(TObject *Sender)
{
        mnuFileItem3Click(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::mnuEditItem3Click(TObject *Sender)
{
        memEditor->CutToClipboard();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::mnuEditItem4Click(TObject *Sender)
{
        memEditor->CopyToClipboard();        
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::mnuEditItem5Click(TObject *Sender)
{
        memEditor->PasteFromClipboard();         
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cmdPasteClick(TObject *Sender)
{
       mnuEditItem5Click(Sender);       
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cmdCommentaClick(TObject *Sender)
{
        mnuEditItem6Click(Sender);        
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cmdCutClick(TObject *Sender)
{
        mnuEditItem3Click(Sender);        
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cmdNewClick(TObject *Sender)
{
        mnuFileItem2Click(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::BitBtn1Click(TObject *Sender)
{
        mnuFileItem1Click(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::mnuFileItem3Click(TObject *Sender)
{
        if(memEditor->Modified)
        {
                if(Work->ProjectEmpty())
                        mnuFileItem4Click(Sender);
                else
                        //memEditor->Lines->SaveToFile(Work->FileProject);
                        Work->SaveOnFile(Work->FileProject);
                memEditor->Modified=false;        
        }
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cmdSaveAsClick(TObject *Sender)
{
        mnuFileItem4Click(Sender);
}
//---------------------------------------------------------------------------



void __fastcall TfrmMain::mnuEditItem1Click(TObject *Sender)
{
        if(memEditor->CanUndo)
                memEditor->Undo();
        else
                mnuEditItem1->Enabled=false; 
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::mnuEditItem2Click(TObject *Sender)
{
        memEditor->ClearUndo();        
}
//---------------------------------------------------------------------------



void __fastcall TfrmMain::FormClose(TObject *Sender, TCloseAction &Action)
{
        int flag;
        if(memEditor->Modified)
        {
                flag=MessageBox(NULL,"Salvare il file prima di chiuderlo?","Avviso",51);
                if(flag==ID_YES)
                {
                        if(Work->ProjectEmpty())
                                mnuFileItem4Click(Sender);
                        else
                                Work->SaveOnFile(Work->FileProject);
                }
                else
                {
                        if(flag==ID_CANCEL)
                                Action=caNone;
                }

        }

}
//---------------------------------------------------------------------------



void __fastcall TfrmMain::mnuOptionItem1Click(TObject *Sender)
{
        ShellExecute(0,"",ColorFunction,"",DirComp,1);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cmdColorClick(TObject *Sender)
{
        mnuOptionItem1Click(Sender);
}
//---------------------------------------------------------------------------





void __fastcall TfrmMain::mnuFileItem4Click(TObject *Sender)
{
        AnsiString name, title;
        dlgSave->Title="Salvataggio del progetto"; 
        do
        {
                dlgSave->Execute();
                name=dlgSave->FileName;
                if(name==NULL)
                        break;
                title=GetName(name.c_str());
                if((title.Length())>12)
                        MessageBox(frmMain->Handle,"Il nome scelto � troppo lungo","Avviso",MB_ICONEXCLAMATION+MB_OK);
        }
        while((title.Length()>12));
        if(name!="")
        {
                Work->SetProject(name.c_str());
                Err = new ERROR_TYPE(Work->DirProject);
                Work->SaveOnFile(Work->FileProject);
                frmMain->Caption = "Pascal Editor - "+Work->ToString()[1];
        }
}
//---------------------------------------------------------------------------



void __fastcall TfrmMain::lstViewErrorDblClick(TObject *Sender)
{
        SelectLInesError();        
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::memEditorChange(TObject *Sender)
{
        if(memEditor->Lines->Count<1)
        {
                mnuEditItem3->Enabled=false;
                mnuEditItem4->Enabled=false;
                mnuEditItem6->Enabled=false;
                mnuEditItem7->Enabled=false;
                cmdCut->Enabled=false;
                cmdCopy->Enabled=false;
                cmdCommenta->Enabled=false;
                cmdDelComm->Enabled=false;
                cmdRun->Enabled=false;
        }
        else
        {
                mnuEditItem3->Enabled=true;
                mnuEditItem4->Enabled=true;
                mnuEditItem6->Enabled=true;
                mnuEditItem7->Enabled=true;
                cmdCut->Enabled=true;
                cmdCopy->Enabled=true;
                cmdCommenta->Enabled=true;
                cmdDelComm->Enabled=true;
                cmdRun->Enabled=true;
        }
                          
}
//---------------------------------------------------------------------------


void __fastcall TfrmMain::TimerTimer(TObject *Sender)
{
        int x,y;
        x=memEditor->CaretPos.x+1;
        y=memEditor->CaretPos.y+1;
        StatusBar->Panels->Items[0]->Text="Riga: "+IntToStr(y)+"      Colonna: "+IntToStr(x); 
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::tmrDataTimer(TObject *Sender)
{
        StatusBar->Panels->Items[2]->Text=DateToStr(Now());
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cmdDelCommClick(TObject *Sender)
{
        mnuEditItem7Click(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::mnuEditItem6Click(TObject *Sender)
{
        Commenta(memEditor->SelText);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::mnuEditItem7Click(TObject *Sender)
{
        TogliCommenta(memEditor->SelText);
}
//---------------------------------------------------------------------------




void __fastcall TfrmMain::memEditorKeyPress(TObject *Sender, char &Key)
{
        if(Key==32)
                remark(memEditor->Lines->Strings[memEditor->CaretPos.y].c_str(),memEditor->CaretPos.x,memEditor->CaretPos.y);
}
//---------------------------------------------------------------------------




