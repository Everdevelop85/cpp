//---------------------------------------------------------------------------
#ifndef cppEditPascalH
#define cppEditPascalH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <Menus.hpp>
#include <ComCtrls.hpp>
#include <ToolWin.hpp>

#include "cppDataProject.h"
#include "cp.h"

#include <conio.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <dir.h>
#include <io.h>
#include <shellapi.h>
#include <shlobj.h>
#include <Buttons.hpp>
#include <Dialogs.hpp>
#include <ImgList.hpp>
#include <sys/types.h>

#include <Buttons.hpp>
#include <Dialogs.hpp>
#include <ImgList.hpp>

#define MAX 255

const char charText[22][10]={
                             "begin",
                             "end",
                             "or",
                             "and",
                             "if",
                             "else",
                             "then",
                             "for",
                             "to",
                             "downto",
                             "do",
                             "while",
                             "repeat",
                             "until",
                             "program",
                             "uses",
                             "var",
                             "integer",
                             "real",
                             "function",
                             "procedure",
                             "string"
                             };
//---------------------------------------------------------------------------
class TfrmMain : public TForm
{
__published:	// IDE-managed Components
        TMainMenu *mnuMain;
        TMenuItem *mnuFile;
        TMenuItem *mnuFileItem1;
        TMenuItem *mnuFileItem2;
        TMenuItem *N1;
        TMenuItem *mnuFileItem3;
        TMenuItem *mnuFileItem4;
        TToolBar *ToolBar1;
        TBitBtn *BitBtn1;
        TBitBtn *cmdNew;
        TSaveDialog *dlgSave;
        TOpenDialog *dlgOpen;
        TBitBtn *cmdRun;
        TPrintDialog *dlgPrint;
        TMenuItem *N2;
        TMenuItem *mnuFileItem5;
        TBitBtn *cmdSave;
        TBitBtn *cmdSaveAs;
        TImageList *ImageList;
        TToolButton *ToolButton2;
        TBitBtn *cmdCut;
        TBitBtn *cmdCopy;
        TBitBtn *cmdPaste;
        TMenuItem *mnuEdit;
        TMenuItem *mnuEditItem1;
        TMenuItem *mnuEditItem2;
        TMenuItem *N3;
        TMenuItem *mnuEditItem3;
        TMenuItem *mnuEditItem4;
        TMenuItem *mnuEditItem5;
        TToolButton *ToolButton3;
        TBitBtn *cmdCommenta;
        TBitBtn *cmdDelComm;
        TMenuItem *N4;
        TMenuItem *mnuEditItem6;
        TMenuItem *mnuEditItem7;
        TBitBtn *cmdColor;
        TToolButton *ToolButton4;
        TMenuItem *mnuOption;
        TMenuItem *mnuOptionItem1;
        TStatusBar *StatusBar;
        TTimer *Timer;
        TListView *lstViewError;
        TTimer *tmrData;
        TRichEdit *memEditor;
        void __fastcall FormConstrainedResize(TObject *Sender,
          int &MinWidth, int &MinHeight, int &MaxWidth, int &MaxHeight);
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall mnuFileItem1Click(TObject *Sender);
        void __fastcall mnuFileItem2Click(TObject *Sender);
        void __fastcall cmdRunClick(TObject *Sender);
        void __fastcall mnuFileItem7Click(TObject *Sender);
        void __fastcall cmdSaveClick(TObject *Sender);
        void __fastcall mnuEditItem3Click(TObject *Sender);
        void __fastcall mnuEditItem4Click(TObject *Sender);
        void __fastcall mnuEditItem5Click(TObject *Sender);
        void __fastcall cmdPasteClick(TObject *Sender);
        void __fastcall cmdCommentaClick(TObject *Sender);
        void __fastcall cmdCutClick(TObject *Sender);
        void __fastcall cmdNewClick(TObject *Sender);
        void __fastcall BitBtn1Click(TObject *Sender);
        void __fastcall mnuFileItem3Click(TObject *Sender);
        void __fastcall cmdSaveAsClick(TObject *Sender);
        void __fastcall mnuEditItem1Click(TObject *Sender);
        void __fastcall mnuEditItem2Click(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall mnuOptionItem1Click(TObject *Sender);
        void __fastcall cmdColorClick(TObject *Sender);
        void __fastcall mnuFileItem4Click(TObject *Sender);
        void __fastcall lstViewErrorDblClick(TObject *Sender);
        void __fastcall memEditorChange(TObject *Sender);
        void __fastcall TimerTimer(TObject *Sender);
        void __fastcall tmrDataTimer(TObject *Sender);
        void __fastcall cmdDelCommClick(TObject *Sender);
        void __fastcall mnuEditItem6Click(TObject *Sender);
        void __fastcall mnuEditItem7Click(TObject *Sender);
        void __fastcall memEditorKeyPress(TObject *Sender, char &Key);
private:	// User declarations
public:		// User declarations
        char compiler[MAX], DirComp[MAX], ColorFunction[MAX];
        __fastcall TfrmMain(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfrmMain *frmMain;
//---------------------------------------------------------------------------
#endif

class PROJECT
{

        public:
        char DevelopName[100];
        char NameProject[100];
        char FileProject[MAX];
        char FileExe[MAX];
        char DirProject[MAX];
        char DirCompyler[MAX];

        PROJECT(char *dc)
        {
                DevelopName[0]='\0';
                NameProject[0]='\0';
                FileProject[0]='\0';
                FileExe[0]='\0';
                DirProject[0]='\0';
                strcpy(DirCompyler,dc);
        };

        void SetProject(char *dn, char *np, char *dp)
        {
                strcpy(DevelopName, dn);
                strcpy(NameProject, np);
                strcpy(DirProject, dp);
                strcpy(FileProject,DirProject);
                strcat(FileProject,"\\");
                strcat(FileProject,NameProject);
                strcpy(FileExe,FileProject);
                strcat(FileProject,".pas");
                strcat(FileExe,".exe");
                mkdir(DirProject);
        };

        void SetProject(char *path)
        {
                FILE *f;
                char buffer[100];
                strcpy(FileProject,path);
                strcpy(DirProject,GetDir(FileProject));
                strcpy(NameProject,GetName(FileProject));
                strcpy(FileExe,DirProject);
                strcat(FileExe,"\\");
                strcat(FileExe,NameProject);
                strcat(FileExe,".exe");
                int i=0;
                if((f=fopen(FileProject,"rt"))==NULL)
                        return;
                while(fscanf(f,"%c",&(buffer[i]))>0 && buffer[i]!='\n')
                        i++;
                buffer[i]='\0';
                fclose(f);

                strcpy(DevelopName,GetNameDvl(buffer));
                OpenFromFile(FileProject);
        };

        void SetEditor()
        {
                char ttl[10][65]={"(*",
                          "(*------------------------------------------------------------*)",
                          "",
                          "PROGRAM ",
                          "uses crt;",
                          "VAR ",
                          "BEGIN",
                          "",
                          "REPEAT UNTIL KEYPRESSED;",
                          "END."};
                strcat(ttl[0],DevelopName);
                strcat(ttl[0],"  Data: ");
                strcat(ttl[0],DateToStr(Now()).c_str());
                strcat(ttl[0],"*)");
                strcat(ttl[3],NameProject);
                strcat(ttl[3],";");
                frmMain->memEditor->Lines->Clear();
                for(int i=0;i<10;i++)
                        frmMain->memEditor->Lines->Add(ttl[i]);
        };

        AnsiString *ToString()
        {
                AnsiString names[5];
                names[0]=DevelopName;
                names[1]=NameProject;
                names[2]=FileProject;
                names[3]=FileExe;
                names[4]=DirProject;
                return &(names[0]);
        };

        private: char *GetDir(char *s)
        {
                char p[MAX];
                strcpy(p,s);
                int l=strlen(p)-1;
                while(p[l]!='\\')
                        l--;
                p[l]='\0';
                return &(p[0]);
        };

        private: char *GetName(char *s)
        {
                char p[MAX];
                strcpy(p,s);
                int l=strlen(p)-1, ind, i;
                while(p[l]!='\\')
                        l--;
                ind=l+1;
                l=strlen(p);
                for(i=ind;i<l;i++)
                        p[i-ind]=p[i];
                p[i-ind]='\0';
                p[(strlen(p))-4]='\0';
                return &(p[0]);
        };

        private: char *GetNameDvl(char *s)
        {
                if((strstr(s,"Data:"))==NULL)
                        return "";
                int l,i=-1,j=i+1;
                s+=2;
                l=strlen(s);
                do
                {
                        i++;
                        j=i+1;
                }
                while(((s[i]!=' ') || (s[j]!=' ')) && (j<l));
                s[i]='\0';
                return s;
        };

        private: int CopyFile(char *sorg, char *dest)
        {
                char line[MAX];
                int f;
                if((f=open("copy.bat",O_CREAT|O_TRUNC|O_WRONLY,0644))<0)
                    return 1;
                strcpy(line,"COPY \"");
                strcat(line,sorg);
                strcat(line,"\" \"");
                strcat(line,dest);
                strcat(line,"\"\n");
                write(f,line,strlen(line));
                close(f);
                ShellExecute(0,NULL,"copy.bat",NULL,NULL,0);
                while((access(dest,0|1|4))!=0);
                return 0;
        }

        public: int SaveOnFile(char *name)
        {
                int f;
                if((f=open(name,O_TRUNC|O_WRONLY))<0)
                        return 1;
                for(int i=0;i<frmMain->memEditor->Lines->Count;i++)
                {
                        write(f,frmMain->memEditor->Lines->Strings[i].c_str(),frmMain->memEditor->Lines->Strings[i].Length());
                        write(f,"\n",1);
                }
                close(f);
                return 0;
        }

        public: int OpenFromFile(char *name)
        {
                int f;
                char buffer[MAX];
                int i=0;
                if((f=open(name,O_RDONLY))<0)
                        return 1;
                frmMain->memEditor->Clear();
                while((read(f,&(buffer[i]),1))>0)
                {
                        if(buffer[i]=='\n')
                        {
                                buffer[i]='\0';
                                frmMain->memEditor->Lines->Add(buffer);
                                i=0;
                        }
                        else
                                i++;
                }
                close(f);
                return 0;
        }
        public: int Compyle()
        {
                int i=0;
                FILE *f;
                char flTemp[100],flTempExe[100],cmd[MAX], appC[MAX];
                if((SaveOnFile(FileProject))!=0)
                        return 1;
                frmMain->memEditor->Modified=false;
                chdir(DirCompyler);

                if((remove(FileExe))==0)
                        while((access(FileExe,0))==0);

                if((remove("error.txt"))==0)
                        while((access("error.txt",0))==0);

                strcpy(flTemp,NameProject);
                strcpy(flTempExe,NameProject);
                strcat(flTemp,".pas");
                strcat(flTempExe,".exe");
                strcpy(cmd,"TPC ");
                strcat(cmd,flTemp);
                strcat(cmd,">> error.txt");
                if((copy(FileProject,flTemp))!=0)
                        return 1;
                while((access(flTemp,0|4))!=0);
                system(cmd);
                i=0;
                while(((access(flTempExe,0|4))!=0) && (i<1000))
                        i++;
                while((access("error.txt",0))!=0);
                if(i==1000)
                        return 1;
                if((CopyFile(flTempExe,FileExe))!=0)
                        return -1;
                if((remove(flTemp))==0)
                        while((access(flTemp,0))==0);
                if((remove(flTempExe))==0)
                        while((access(flTemp,0))==0);
                return 0;
        }

        bool ProjectEmpty()
        {
                if(FileProject[0]=='\0')
                        return true;
                return false;
        }

        AnsiString GetInfoProject()
        {
                AnsiString inf, nf, nd;
                nf=NameProject;
                nf+=".pas";
                nd=DevelopName;
                inf="Nome file: "+nf+"         Progettato da: "+nd;
                return inf;
        };

        AnsiString GetFileProject()
        {
                AnsiString fp;
                fp=NameProject;
                fp+=".pas";
                return fp;
        };
};

extern PACKAGE PROJECT *Work;


class ERROR_TYPE
{
        public:
        char FileError[MAX];
        char TypeError[100];
        TPoint Location;
        char Description[MAX];

        ERROR_TYPE(char dp[])
        {
                TypeError[0]='\0';
                Description[0]='\0';
                Location.x = -1;
                Location.y = -1;
                strcpy(FileError,dp);
                strcat(FileError,"\\error.txt");
        };

        AnsiString DeleteChar(char *b)
        {
                AnsiString s;
                int i=0;
                while((b[i]=='\t') || (b[i]=='\n') || (b[i]=='\r'))
                        i++;
                b+=i;
                s=b;
                return s;
        };

        bool ErrorControl()
        {
            FILE *f;
            char buffer[7][100], c, *p;
            int x,y;
            if((f=fopen(FileError,"r"))==NULL)
                return false;
            for(x=0;x<7;x++)
            {
                for(y=0;y<100;y++)
                        buffer[x][y]='\0';
            }
            x=y=0;
            while((fscanf(f,"%c",&c))!=EOF)
            {
               if(c=='\n')
               {
                        y=0;
                        x++;
               }
               else
               {
                       if(x>6)
                                break;
                       buffer[x][y]=c;
                       y++;
               }
            }
            for(x=0;x<7;x++)
            {
                p=strstr(buffer[x],"Error");
                if(p!=NULL)
                        break;
            }
            if(p==NULL)
                return false;
            strcpy(Description,(strstr(p,":"))+2);
            x=0;
            while(p[x]!=':')
                x++;
            p[x]='\0';
            strcpy(TypeError,p);
            Location.x=0;
            AnsiString s1,s2;
            s1=buffer[2];
            s1=s1.Trim();
            s2=DeleteChar(frmMain->memEditor->Lines->Strings[Location.x].c_str());
            s2=s2.Trim();
            while(s2!=s1)
            {
                Location.x++;
                s2=DeleteChar(frmMain->memEditor->Lines->Strings[Location.x].c_str());
                s2=s2.Trim();
            }
            Location.y=0;
            while(buffer[3][Location.y]!='^')
                Location.y++;
            fclose(f);
            Location.y+=1;
            Location.x+=1;
            return true;
        };

        void ShowErrors()
        {
                int a,b;
                a=Location.x;
                b=Location.y;
                frmMain->lstViewError->Items->Clear();
                TListItem  *ListItem;
                ListItem = frmMain->lstViewError->Items->Add();
                ListItem->Caption=TypeError;
                ListItem->SubItems->Add("Riga: "+IntToStr(a)+"     Colonna: "+IntToStr(b));
                ListItem->SubItems->Add(Description);
                ListItem->ImageIndex=0;
        };

        void ShowNoErrors()
        {
                frmMain->lstViewError->Items->Clear();
                TListItem  *ListItem;
                ListItem = frmMain->lstViewError->Items->Add();
                ListItem->Caption="Esecuzione";
                ListItem->SubItems->Add("-");
                ListItem->SubItems->Add("-");
                ListItem->ImageIndex=1;
        };

};

extern PACKAGE ERROR_TYPE *Err;


void BrowseFolder(char p[])
{
        BROWSEINFO bi;
	LPITEMIDLIST pidl;

  	bi.hwndOwner      = frmDataProject->Handle;
  	bi.pidlRoot       = NULL;
  	bi.pszDisplayName = p;
  	bi.lpszTitle      = "Seleziona la cartella che contiene\ni tuoi file musicali";
  	bi.ulFlags        = BIF_RETURNONLYFSDIRS;
  	bi.lpfn           = NULL;
  	bi.lParam         = 0;
  	bi.iImage         = 0;

  	pidl = SHBrowseForFolder(&bi);

        SHGetPathFromIDList(pidl, p);
};

