//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "cppDataProject.h"
#include "cppEditPascal.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TfrmDataProject *frmDataProject;
AnsiString path;
//---------------------------------------------------------------------------
__fastcall TfrmDataProject::TfrmDataProject(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TfrmDataProject::FormCreate(TObject *Sender)
{
        char p[MAX];
        getcwd(p,MAX);
        path=p;
        txtND->Text = "nessuno";
        txtNP->Text = "Prg1";
        txtDP->Text = path+"\\"+txtNP->Text;
        txtDP->SelStart=txtDP->Text.Length();
}
//---------------------------------------------------------------------------
void __fastcall TfrmDataProject::cmdOkClick(TObject *Sender)
{
        if(txtND->Text=="nessuno" || txtND->Text=="")
                MessageBox(NULL,"Attenzione! Non � possibile inserire campi nulli nel nome","Errore",MB_ICONERROR);
        else
        {
                Work->SetProject(txtND->Text.c_str(),txtNP->Text.c_str(),txtDP->Text.c_str());
                Err = new ERROR_TYPE(Work->DirCompyler);
                Work->SetEditor();
                frmMain->Caption = "Pascal Editor - "+Work->ToString()[1];
                frmMain->StatusBar->Panels->Items[1]->Text=Work->GetInfoProject();
                frmMain->mnuEditItem3->Enabled=true;
                frmMain->mnuEditItem4->Enabled=true;
                frmMain->mnuEditItem6->Enabled=true;
                frmMain->mnuEditItem7->Enabled=true;
                frmMain->cmdCut->Enabled=true;
                frmMain->cmdCopy->Enabled=true;
                frmMain->cmdCommenta->Enabled=true;
                frmMain->cmdDelComm->Enabled=true;
                frmMain->cmdRun->Enabled=true;
                frmMain->memEditor->Lines->SaveToFile(Work->FileProject);
                frmMain->memEditor->Modified=false;
                frmDataProject->Close();
        }

}
//---------------------------------------------------------------------------
void __fastcall TfrmDataProject::cmdOkExit(TObject *Sender)
{
        frmDataProject->Show();
        return;
}
//---------------------------------------------------------------------------

void __fastcall TfrmDataProject::FormClose(TObject *Sender,
      TCloseAction &Action)
{
       frmMain->Visible=true; 
}
//---------------------------------------------------------------------------


void __fastcall TfrmDataProject::txtNPChange(TObject *Sender)
{
        txtDP->Text = path+"\\"+txtNP->Text;
        txtDP->SelStart=txtDP->Text.Length();  
}
//---------------------------------------------------------------------------

void __fastcall TfrmDataProject::txtNPKeyPress(TObject *Sender, char &Key)
{
        if(Key==32)
                Key=0;        
}
//---------------------------------------------------------------------------

void __fastcall TfrmDataProject::cmdBrowseClick(TObject *Sender)
{
        char p[MAX];
        BrowseFolder(p);
        path=p;
        if(path!="")
        {
                if(path=="C:\\")
                        txtDP->Text = path+txtNP->Text;
                else
                        txtDP->Text = path+"\\"+txtNP->Text;
        }
}
//---------------------------------------------------------------------------


void __fastcall TfrmDataProject::cmdCancelClick(TObject *Sender)
{
        frmDataProject->Close();
}
//---------------------------------------------------------------------------






