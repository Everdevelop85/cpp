//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
USERES("bprEditPascal.res");
USEFORM("cppEditPascal.cpp", frmMain);
USEFORM("cppDataProject.cpp", frmDataProject);
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->CreateForm(__classid(TfrmMain), &frmMain);
                 Application->CreateForm(__classid(TfrmDataProject), &frmDataProject);
                 Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
