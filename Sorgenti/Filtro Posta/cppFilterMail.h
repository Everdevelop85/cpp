//---------------------------------------------------------------------------

#ifndef cppFilterMailH
#define cppFilterMailH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <NMpop3.hpp>
#include <Psock.hpp>
#include <ExtCtrls.hpp>
#include <ToolWin.hpp>
//---------------------------------------------------------------------------

const char script_word[9][20]={"format",
                               "cancel",
                               "delete",
                               "scan",
                               "{",
                               "}",
                               "for(",
                               "while(",
                               "do\n{"};
class TfrmMain : public TForm
{
__published:	// IDE-managed Components
        TStatusBar *StatusBar;
        TNMPOP3 *POP3;
        TButton *cmdConnect;
        TButton *cmdDisconnect;
        TButton *cmdControllo;
        TListBox *ListBox1;
        TPanel *Panel1;
        TMemo *memTextOfMessage;
        TSplitter *Splitter;
        TPanel *Panel2;
        TListView *lstMails;
        TToolBar *ToolBar1;
        void __fastcall POP3AuthenticationFailed(bool &Handled);
        void __fastcall POP3AuthenticationNeeded(bool &Handled);
        void __fastcall POP3Connect(TObject *Sender);
        void __fastcall POP3ConnectionFailed(TObject *Sender);
        void __fastcall POP3ConnectionRequired(bool &Handled);
        void __fastcall POP3DecodeEnd(TObject *Sender);
        void __fastcall POP3DecodeStart(AnsiString &FileName);
        void __fastcall POP3Disconnect(TObject *Sender);
        void __fastcall POP3Failure(TObject *Sender);
        void __fastcall POP3InvalidHost(bool &Handled);
        void __fastcall POP3PacketRecvd(TObject *Sender);
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall cmdConnectClick(TObject *Sender);
        void __fastcall cmdDisconnectClick(TObject *Sender);
        void __fastcall cmdControlloClick(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
private:	// User declarations
public:		// User declarations
        __fastcall TfrmMain(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfrmMain *frmMain;
//---------------------------------------------------------------------------
#endif
