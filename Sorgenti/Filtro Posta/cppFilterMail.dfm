object frmMain: TfrmMain
  Left = 202
  Top = 125
  Width = 744
  Height = 538
  Caption = 'frmMain'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter: TSplitter
    Left = 0
    Top = 313
    Width = 736
    Height = 3
    Cursor = crVSplit
    Align = alBottom
    Beveled = True
    ResizeStyle = rsNone
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 736
    Height = 313
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 6
    object lstMails: TListView
      Left = 0
      Top = 41
      Width = 736
      Height = 272
      Align = alClient
      Columns = <
        item
          Caption = 'Oggetto'
          Width = 200
        end
        item
          Caption = 'Destinatario'
          Width = 200
        end
        item
          Caption = 'Indirizzo'
          Width = 200
        end
        item
          Caption = 'Data di arrivo'
          Width = 80
        end
        item
          Caption = 'Ora di arrvio'
          Width = 80
        end>
      TabOrder = 0
      ViewStyle = vsReport
    end
    object ToolBar1: TToolBar
      Left = 0
      Top = 0
      Width = 736
      Height = 41
      Caption = 'ToolBar1'
      TabOrder = 1
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 316
    Width = 736
    Height = 174
    Align = alBottom
    TabOrder = 4
    object memTextOfMessage: TMemo
      Left = 1
      Top = 1
      Width = 734
      Height = 172
      Align = alClient
      ScrollBars = ssBoth
      TabOrder = 0
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 490
    Width = 736
    Height = 21
    Panels = <
      item
        Text = 'Status'
        Width = 300
      end
      item
        Width = 200
      end
      item
        Width = 50
      end>
    SimplePanel = False
  end
  object cmdConnect: TButton
    Left = 8
    Top = 8
    Width = 129
    Height = 25
    Caption = 'cmdConnect'
    TabOrder = 0
    OnClick = cmdConnectClick
  end
  object cmdDisconnect: TButton
    Left = 144
    Top = 8
    Width = 129
    Height = 25
    Caption = 'cmdDisconnect'
    TabOrder = 1
    OnClick = cmdDisconnectClick
  end
  object cmdControllo: TButton
    Left = 280
    Top = 8
    Width = 129
    Height = 25
    Caption = 'cmdControllo'
    TabOrder = 2
    OnClick = cmdControlloClick
  end
  object ListBox1: TListBox
    Left = 64
    Top = 112
    Width = 441
    Height = 153
    ItemHeight = 13
    TabOrder = 3
  end
  object POP3: TNMPOP3
    Port = 110
    ReportLevel = 0
    OnDisconnect = POP3Disconnect
    OnConnect = POP3Connect
    OnInvalidHost = POP3InvalidHost
    OnConnectionFailed = POP3ConnectionFailed
    OnConnectionRequired = POP3ConnectionRequired
    OnPacketRecvd = POP3PacketRecvd
    Parse = False
    DeleteOnRead = False
    OnAuthenticationNeeded = POP3AuthenticationNeeded
    OnAuthenticationFailed = POP3AuthenticationFailed
    OnFailure = POP3Failure
    OnDecodeStart = POP3DecodeStart
    OnDecodeEnd = POP3DecodeEnd
    Left = 32
    Top = 24
  end
end
