//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "cppFilterMail.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TfrmMain *frmMain;
TList *ThreadList;
//---------------------------------------------------------------------------
__fastcall TfrmMain::TfrmMain(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------

bool ControllingFroming(char *adress)
{
        int l=strlen(adress)-1;
        char *p;
        while(adress[l]!='.')
                l--;
        p=&(adress[l])+1;
        p[strlen(p)-1]='\0';
        if(((strcmp(p,"com"))==0)||((strcmp(p,"it"))==0))
                return true;
        return false;
}

bool matchForScriptLanguage(char *line)
{
        bool trovato=false;
        for(int i=0;i<9;i++)
        {
                if((strstr(line,script_word[i]))!=NULL)
                {
                        trovato=true;
                        break;
                }
        }
        return trovato;
}

bool ControllingBody(char *body)
{
        int l=strlen(body), ind=0;
        char buffer[1000];
        bool trovato=false;

//converti in minuscolo e controlla le lineee
        for(int i=0;i<l;i++)
        {
                if((body[i]>='A')&&(body[i]<='Z'))
                        buffer[i]=(char)body[i]-32;
                else
                        buffer[ind]=body[i];
                if((buffer[ind]=='\r')||(buffer[ind]=='\n')||(buffer[ind]=='\0'))
                {
                        buffer[ind]='\0';
                        while((body[i]=='\r')||(body[i]=='\n')||(body[i]=='\0'))
                                i++;
                        i--;
                        if(matchForScriptLanguage(buffer))
                        {
                                trovato=true;
                                break;
                        }
                        ind=-1;
                }
                ind++;
        }
        return !trovato;
}

class TControlThreadAdress : public TThread
{
        private:

        public:

                int ScanningMessage;
                TMailMessage *message;
                TControlThreadAdress(TMailMessage *ms): TThread(true)
                {

                        message= new TMailMessage(*ms);
                        frmMain->StatusBar->Panels->Items[1]->Text="Controllo provenienza messaggio terminta";
                        ScanningMessage=0;
                }

                virtual void __fastcall Execute()
                {

                        frmMain->StatusBar->Panels->Items[1]->Text="Controllo provenienza messaggio in corso...";
                        if((!ControllingFroming(message->From.c_str())))
                                ScanningMessage=1;
                }
};

class TControlThread : public TThread
{
        private:
                TControlThreadAdress *THA;
                TMailMessage *message;


        public:
                int ScanningMessage;
                TControlThread(TMailMessage *ms): TThread(true)
                {
                        message = new TMailMessage(*ms);
                        ScanningMessage=0;
                        THA = new TControlThreadAdress(message);
                }

                virtual void __fastcall Execute()
                {
                 //code
                        THA->Resume();
                        frmMain->StatusBar->Panels->Items[2]->Text="Controllo corpo del messaggio in corso...";
                        if(ControllingBody(message->Body->GetText()))
                                ScanningMessage=1;
                        while(THA->WaitFor()!=0);;
                        if((ScanningMessage==1)||(THA->ScanningMessage==1))
                                ScanningMessage=1;
                }
};

TControlThread** TCA;

class TControlStatusThreads : public TThread
{
        private:
               TControlThread **ControlTCA;
               //TNMPOP3 *pop;
        public:
                TControlStatusThreads(TControlThread **tca): TThread(true)
                {

                        ControlTCA=tca;

                        //pop=p;
                }

                virtual void __fastcall Execute()
                {

                        for(int i=0;i<frmMain->POP3->MailCount;i++)
                        {
                                while(ControlTCA[i]->WaitFor()!=0);;
                                if(ControlTCA[i]->ScanningMessage!=0)
                                        frmMain->POP3->DeleteMailMessage(i+1);

                        }
                        frmMain->POP3->Disconnect();
                        frmMain->StatusBar->Panels->Items[2]->Text="Controllo corpo terminato";
                        frmMain->ListBox1->Clear();
                        frmMain->POP3->Connect();
                        if(frmMain->POP3->MailCount>0)
                        {
                                for(int i=0;i<frmMain->POP3->MailCount;i++)
                                {
                                        frmMain->POP3->GetMailMessage(i+1);
                                        frmMain->ListBox1->Items->Add(IntToStr(i+1)+"   "+frmMain->POP3->MailMessage->From+"   "+frmMain->POP3->MailMessage->Subject);
                                }
                                frmMain->StatusBar->Panels->Items[1]->Text="Ci sono "+IntToStr(frmMain->POP3->MailCount)+" Messaggi";
                        }
                        else
                                frmMain->StatusBar->Panels->Items[1]->Text="Tutti i messaggi sono stati cancellati";
                        frmMain->POP3->Disconnect();
                }
}*TST;

void __fastcall TfrmMain::POP3AuthenticationFailed(bool &Handled)
{
        StatusBar->Panels->Items[0]->Text="Autenticazione Fallita";
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::POP3AuthenticationNeeded(bool &Handled)
{
        StatusBar->Panels->Items[0]->Text="Richiesta Autenticazione";
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::POP3Connect(TObject *Sender)
{
        StatusBar->Panels->Items[0]->Text="Connesso";
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::POP3ConnectionFailed(TObject *Sender)
{
        StatusBar->Panels->Items[0]->Text="Connessione Fallita";
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::POP3ConnectionRequired(bool &Handled)
{
        StatusBar->Panels->Items[0]->Text="Richiesta Connessione";
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::POP3DecodeEnd(TObject *Sender)
{
        StatusBar->Panels->Items[0]->Text="Fine Decodifica";
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::POP3DecodeStart(AnsiString &FileName)
{
        StatusBar->Panels->Items[0]->Text="Inizio Decodifica";
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::POP3Disconnect(TObject *Sender)
{
        StatusBar->Panels->Items[0]->Text="Disconnesso";
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::POP3Failure(TObject *Sender)
{
        StatusBar->Panels->Items[0]->Text="Errore";
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::POP3InvalidHost(bool &Handled)
{
        StatusBar->Panels->Items[0]->Text="Host non valido";
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::POP3PacketRecvd(TObject *Sender)
{
        StatusBar->Panels->Items[0]->Text="Pacchetto ricevuto";
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::FormCreate(TObject *Sender)
{
        POP3->Host="pop.tiscali.it";
        POP3->Port=110;
        POP3->TimeOut=60000;
        POP3->UserID="luc.zan";
        POP3->Password="davide";
        ThreadList = new TList();
}
//---------------------------------------------------------------------------
void __fastcall TfrmMain::cmdConnectClick(TObject *Sender)
{
        POP3->Connect();
        StatusBar->Panels->Items[1]->Text="Ci sono "+IntToStr(POP3->MailCount)+" Messaggi";
        if(POP3->MailCount==0)
                POP3->Disconnect();
}
//---------------------------------------------------------------------------
void __fastcall TfrmMain::cmdDisconnectClick(TObject *Sender)
{
        POP3->Disconnect();
}
//---------------------------------------------------------------------------
void __fastcall TfrmMain::cmdControlloClick(TObject *Sender)
{
//Thread Controllo
        TCA=(TControlThread**)calloc(POP3->MailCount,sizeof(TControlThread));
        if(POP3->MailCount>0)
        {
                for(int i=1;i<=POP3->MailCount;i++)
                {
                        POP3->GetMailMessage(i);
                        TCA[i-1] = new TControlThread(POP3->MailMessage);
                        TCA[i-1]->Resume();
                }
                TST = new TControlStatusThreads(TCA);
                TST->Resume();
        }
        else
        {
                StatusBar->Panels->Items[1]->Text="Non ci sono messaggi sul server";
                POP3->Disconnect();
        }
}
//---------------------------------------------------------------------------
void __fastcall TfrmMain::FormClose(TObject *Sender, TCloseAction &Action)
{
        if(POP3->Connected)
                POP3->Disconnect();
}
//---------------------------------------------------------------------------

