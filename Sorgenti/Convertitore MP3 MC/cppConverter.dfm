�
 TFRMMAIN 0l  TPF0TfrmMainfrmMainLeftGTop� WidthcHeight�CaptionfrmMainColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderOnClose	FormCloseOnCreate
FormCreatePixelsPerInch`
TextHeight TLabelLabel1LeftTop� WidthnHeightCaptionMusic Cassetta:Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel2Left�Top� Width1HeightCaptionLato A:Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel3Left@Top� Width1HeightCaptionLato B:Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel4LeftTopWidth� HeightCaptionGrafico Cassetta:Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel5LeftTop0Width;HeightCaptionLato A:Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel6Left�Top0Width;HeightCaptionLato B:Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  	TListView	ListViewALeftTopWidthQHeight� ColumnsCaption	Nome File
ImageIndexWidth�  	AlignmenttaRightJustifyCaptionDurata
ImageIndexWidthH  HotTrackStyles MultiSelect		RowSelect		PopupMenu	PopupMenuSmallImagesimgBmpSortTypestBothTabOrder 	ViewStylevsReportOnClickListViewAClick
OnDblClickListViewADblClick  TBitBtncmdAddLeft�TopWidthiHeightCaptionAggiungiTabOrderOnClickcmdAddClickKindbkYes  TBitBtn	cmdCancelLeft�Top(WidthiHeightCaptionEliminaTabOrderOnClickcmdCancelClickKindbkAbort  	TComboBoxcboMCLeftxTop� Width� Height
ItemHeightItems.StringsMusic-Cassetta 45 minutiMusic-Cassetta 60 minutiMusic-Cassetta 70 minutiMusic-Cassetta 74 minutiMusic-Cassetta 90 minuti TabOrderTextMusic-Cassetta 90 minutiOnChangecboMCChange  TStaticTextlblLatoALeft�Top� WidthaHeight	AlignmenttaCenterAutoSizeBorderStyle	sbsSunkenCaption00:00Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder  TStaticTextlblLatoBLeftwTop� WidthaHeight	AlignmenttaCenterAutoSizeBorderStyle	sbsSunkenCaption00:00Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder  TStaticTextlblLeftALeft�Top� WidthaHeight	AlignmenttaCenterAutoSizeBorderStyle	sbsSunkenCaption- 00:00TabOrder  TStaticTextlblLeftBLeftwTop� WidthaHeight	AlignmenttaCenterAutoSizeBorderStyle	sbsSunkenCaption- 00:00TabOrder  TProgressBarprgLatoALeftTopHWidth�HeightMin MaxdSmooth	TabOrder  TProgressBarprgLatoBLeft�TopHWidth�HeightMin MaxdSmooth	TabOrder	  TStaticTextlblFullTimeLeftTop� WidthYHeight	AlignmenttaCenterAutoSizeBorderStyle	sbsSunkenCaption00:00Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder
  TBitBtn	cmdOpenPLLeft�TopXWidthiHeightCaptionApri plylist...ModalResultTabOrderOnClickcmdOpenPLClick
Glyph.Data
�   �   BM�       v   (               �                       �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ���       wwwwww ����� ������p�����pp�����p�����pwwwwww{p������p������p����wwq��qww  TBitBtncmdLeftLeft`ToppWidth)Height!Caption�Font.CharsetSYMBOL_CHARSET
Font.ColorclTealFont.Height�	Font.NameWingdings 3
Font.Style 
ParentFontTabOrderOnClickcmdLeftClick  TBitBtncmdRightLeft`TopHWidth)Height!Caption�Font.CharsetSYMBOL_CHARSET
Font.ColorclTealFont.Height�	Font.NameWingdings 3
Font.Style 
ParentFontTabOrderOnClickcmdRightClick  TMediaPlayermmMP3Left�TopxWidth9HeightEnabledButtonsbtPlaybtStop VisibleButtonsbtPlaybtStop 
AutoEnable
AutoRewindParentShowHintShowHintTabOrderTabStopOnClick
mmMP3Click  	TListView	ListViewBLeft�TopWidthRHeight� ColumnsCaption	Nome File
ImageIndexWidth�  	AlignmenttaRightJustifyCaptionDurata
ImageIndexWidthH  HotTrackStyles 	RowSelect		PopupMenu	PopupMenuSmallImagesimgBmpSortTypestBothTabOrder	ViewStylevsReportOnClickListViewBClick
OnDblClickListViewBDblClick  TProgressBarprgBarLeft�Top� WidthiHeightMin MaxdTabOrderOnMouseDownprgBarMouseDown  TBitBtncmdSaveLeft�Top� WidthiHeightCaptionSalvaTabOrderOnClickcmdSaveClick  
TImageListimgBmpLeftTop  TOpenDialog
OpenDialogFilter>File plylist M3U (*.m3u)|*.m3u|File musicali MP3 (*.mp3)|*.mp3Left(Top  TTimerTimerEnabledInterval� OnTimer
TimerTimerLeftxTop  
TPopupMenu	PopupMenuLeftPTop 	TMenuItemmnuCopyCaptionCopia su...OnClickmnuCopyClick   TSaveDialogdlgSave
DefaultExt*.m3uFilterFile plylist M3U (*.m3u)|*.m3uLeft� Top   