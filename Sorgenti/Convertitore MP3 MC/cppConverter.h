//---------------------------------------------------------------------------
#ifndef cppConverterH
#define cppConverterH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <Dialogs.hpp>
#include <ExtCtrls.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <MPlayer.hpp>

#define MAX 256

#include <stdlib.h>
#include <stdio.h>
#include <conio.h>
#include <string.h>
#include <dir.h>
#include <fcntl.h>
#include <shlobj.h>
#include <io.h>

struct FILE_MM
{
   char name[150];
   char path[MAX];
   int duration;

   void operator =(FILE_MM &dato)
   {
      strcpy(name,dato.name);
      strcpy(path,dato.path);
      duration=dato.duration;
   }

};

typedef struct list_element
{
        FILE_MM value;
        struct list_element  *next;
}item;


typedef item*  list;

AnsiString strGetTime(int s);
AnsiString GetFileName(AnsiString st);
AnsiString GetFilePath(AnsiString s);
int GetSizeMC(AnsiString mc);
int OpenPlayList(char s[]);
void SetAutoLeftAB();

//---------------------------------------------------------------------------
class TfrmMain : public TForm
{
__published:	// IDE-managed Components
        TLabel *Label1;
        TLabel *Label2;
        TLabel *Label3;
        TLabel *Label4;
        TLabel *Label5;
        TLabel *Label6;
        TListView *ListViewA;
        TBitBtn *cmdAdd;
        TBitBtn *cmdCancel;
        TComboBox *cboMC;
        TStaticText *lblLatoA;
        TStaticText *lblLatoB;
        TStaticText *lblLeftA;
        TStaticText *lblLeftB;
        TProgressBar *prgLatoA;
        TProgressBar *prgLatoB;
        TStaticText *lblFullTime;
        TBitBtn *cmdOpenPL;
        TBitBtn *cmdLeft;
        TBitBtn *cmdRight;
        TMediaPlayer *mmMP3;
        TListView *ListViewB;
        TProgressBar *prgBar;
        TImageList *imgBmp;
        TOpenDialog *OpenDialog;
        TTimer *Timer;
        TPopupMenu *PopupMenu;
        TMenuItem *mnuCopy;
        TBitBtn *cmdSave;
        TSaveDialog *dlgSave;
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall cmdOpenPLClick(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall cboMCChange(TObject *Sender);
        void __fastcall ListViewAClick(TObject *Sender);
        void __fastcall ListViewADblClick(TObject *Sender);
        void __fastcall cmdCancelClick(TObject *Sender);
        void __fastcall ListViewBClick(TObject *Sender);
        void __fastcall ListViewBDblClick(TObject *Sender);
        void __fastcall cmdLeftClick(TObject *Sender);
        void __fastcall cmdRightClick(TObject *Sender);
        void __fastcall cmdAddClick(TObject *Sender);
        void __fastcall mmMP3Click(TObject *Sender, TMPBtnType Button,
          bool &DoDefault);
        void __fastcall TimerTimer(TObject *Sender);
        void __fastcall prgBarMouseDown(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
        void __fastcall mnuCopyClick(TObject *Sender);
        void __fastcall cmdSaveClick(TObject *Sender);

private:	// User declarations
public:		// User declarations
        int TempoTotale, TempoParziale;
        __fastcall TfrmMain(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfrmMain *frmMain;
//---------------------------------------------------------------------------
#endif

class ListaFILE
{
public:
        list root, l, end;

        ListaFILE()
        {
                root=NULL;
                end=NULL;
                l=root;
        };

        void Add(FILE_MM e)
        {

                list t;
                t=(list)malloc(sizeof(item));
                t->value=e;
                t->next=NULL;
                if(root==NULL)
                        root=t;
                else
                {
                        if(root->next==NULL)
                        {
                                root->next=t;
                                end=t;
                        }
                        else
                        {
                                end->next=t;
                                end=t;
                        }
                }
        };

        void FREE()
        {
                if(root==NULL)
                        return;
                l=root;
                while(root->next!=NULL)
                {
                        root=l->next;
                        free(l);
                        l=root;
                }
                free(l);
                free(root);
        };

        int Count()
        {
                l=root;
                int counter=1;
                if(l==NULL)
                        return 0;
                while(l->next!=NULL)
                {
                        counter++;
                        l=l->next;
                }
                return counter;
        };

        void Clear()
        {
                if(root==NULL)
                        return;
                l=root;
                while(root->next!=NULL)
                {
                        root=l->next;
                        free(l);
                        l=root;
                }
                free(l);
                root=NULL;
        };

        FILE_MM *GetMallocStruct()
        {
                if(root==NULL)
                        return NULL;
                FILE_MM *strc;
                strc=(FILE_MM *) malloc((sizeof(FILE_MM))*Count());
                l=root;
                int i=0;
                while (l!=NULL)
                {
                        strc[i]=l->value;
                        l=l->next;
                        i++;
                }
                return strc;
        };

        bool IsEmpty()
        {
                if(root==NULL)
                        return false;
                return true;
        };

        int CountTimeTracks()
        {
                l=root;
                int c=0;
                if(l==NULL)
                        return 0;
                while (l!=NULL)
                {
                        c+=l->value.duration;
                        l=l->next;
                }
                return c;
        };

        AnsiString GetTypeMC()
        {
                int sec=CountTimeTracks();
                if((sec>0)&&(sec<=2700))
                        return "Music-Cassetta 45 minuti";
                if((sec>2700)&&(sec<=3600))
                        return "Music-Cassetta 60 minuti";
                if((sec>3600)&&(sec<=4200))
                        return "Music-Cassetta 70 minuti";
                if((sec>4200)&&(sec<=4440))
                        return "Music-Cassetta 74 minuti";
                if((sec>4440)&&(sec<=5400))
                        return "Music-Cassetta 90 minuti";
                if(sec>5400)
                        return "Music-Cassetta 90 minuti";
                return "";
        };

        AnsiString GetFullPath(char *s)
        {
                l=root;
                bool trovato=false;
                AnsiString a,b;
                while(l!=NULL)
                {
                        if((strstr(l->value.name,s))!=NULL)
                        {
                                trovato=true;
                                break;
                        }
                        l=l->next; 
                }
                if(trovato)
                {
                        a=l->value.path;
                        b=l->value.name;
                        return a+"\\"+b;
                }
                return "";
        }

        FILE_MM Search(char *s)
        {
                l=root;
                bool trovato=false;
                while(l!=NULL)
                {
                        if((strstr(l->value.name,s))!=NULL)
                        {
                                trovato=true;
                                break;
                        }
                        l=l->next;
                }
                return l->value;
        };

        void Delete(char *e)
        {

                l=root;
                list l2;
                list_element *app;
                bool trovato=false;
                if((strcmp(root->value.name,e))==0)
                {
                        root=root->next;
                        free(l);
                        return;
                }
                while(l!=NULL)
                {

                        l2=l;
                        l=l->next;
                        if((strcmp(l->value.name,e))==0)
                        {
                                trovato=true;
                                break;
                        }
                }
                if(trovato)
                {
                    app=l->next;
                    free(l);
                    l2->next=app;
                }
        };

        void CopyOnDisk(char *d)
        {
                FILE *f;
                char line[255];
                if((f=fopen("copiaFile.bat","wt"))==NULL)
                        return;
                l=root;
                while(l!=NULL)
                {
                        strcpy(line,"COPY \"");
                        strcat(line,l->value.path);
                        strcat(line,"\\");
                        strcat(line,l->value.name);
                        strcat(line,"\" \"");
                        strcat(line,d);
                        strcat(line,l->value.name);
                        strcat(line,"\"");
                        fprintf(f,"%s\n",line);
                        l=l->next;
                }
                fclose(f);
        };

        int SaveOnPlayList(char *fn)
        {
                FILE *f;
                char fileMP3[MAX];
                bool nuovo=false;
                if((f=fopen(fn,"r+t"))==NULL)
                {
                        if((f=fopen(fn,"w"))==NULL)
                                return 0;
                        else
                                nuovo=true;
                }
                if(!nuovo)
                        fseek(f,0,SEEK_END);
                l=root;
                while(l!=NULL)
                {
                        strcpy(fileMP3,l->value.path);
                        strcat(fileMP3,"\\");
                        strcat(fileMP3,l->value.name);
                        fprintf(f,"%s\n",fileMP3);
                        l=l->next;
                }
                fclose(f);
                return 1;
        };

};
 