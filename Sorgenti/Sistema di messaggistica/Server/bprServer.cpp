//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
USERES("bprServer.res");
USEFORM("cppServer.cpp", frmServer);
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->CreateForm(__classid(TfrmServer), &frmServer);
                 Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
