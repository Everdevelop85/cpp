//---------------------------------------------------------------------------
#ifndef cppServerH
#define cppServerH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ScktComp.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TfrmServer : public TForm
{
__published:	// IDE-managed Components
        TMemo *MemoMsg;
        TEdit *txtMessaggio;
        TLabel *Label1;
        TServerSocket *ServerSocket;
        TLabel *lblConnessione;
        TTimer *tmrTempo;
        TStatusBar *StatusBar;
        TButton *cmdBG;
        TButton *cmdReset;
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall ServerSocketClientConnect(TObject *Sender,
          TCustomWinSocket *Socket);
        void __fastcall ServerSocketClientDisconnect(TObject *Sender,
          TCustomWinSocket *Socket);
        void __fastcall ServerSocketClientRead(TObject *Sender,
          TCustomWinSocket *Socket);
        void __fastcall ServerSocketAccept(TObject *Sender,
          TCustomWinSocket *Socket);
        void __fastcall txtMessaggioKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall tmrTempoTimer(TObject *Sender);
        void __fastcall cmdBGClick(TObject *Sender);
        void __fastcall cmdResetClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
        AnsiString NomeComputer, Messaggio;
        int button;
        __fastcall TfrmServer(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfrmServer *frmServer;
//---------------------------------------------------------------------------
#endif
