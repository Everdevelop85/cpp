//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "cppServer.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TfrmServer *frmServer;
//---------------------------------------------------------------------------
__fastcall TfrmServer::TfrmServer(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TfrmServer::FormCreate(TObject *Sender)
{
        NomeComputer="<Computer MICHELE>";
        ServerSocket->Active = true;
        MemoMsg->Lines->Add(NomeComputer+"  ATTIVO");
        tmrTempo->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TfrmServer::ServerSocketClientConnect(TObject *Sender,
      TCustomWinSocket *Socket)
{
        MemoMsg->Lines->Add("### Computer DAVIDE connesso ###");
}
//---------------------------------------------------------------------------

void __fastcall TfrmServer::ServerSocketClientDisconnect(TObject *Sender,
      TCustomWinSocket *Socket)
{
        MemoMsg->Lines->Add("### Computer DAVIDE disconnesso ###");
}
//---------------------------------------------------------------------------

void __fastcall TfrmServer::ServerSocketClientRead(TObject *Sender,
      TCustomWinSocket *Socket)
{
        button=MessageBox(0,"Messaggio ricevuto! Si desidera visualizzarlo?", "Sistema di Messaggistica", MB_YESNO);
        if (button==IDYES)
        {
                MemoMsg->Lines->Add(Socket->ReceiveText());
                frmServer->Visible = true;
        }
        else
        {
                if (button==IDNO)
                        Messaggio=Socket->ReceiveText();
        }
}
//---------------------------------------------------------------------------

void __fastcall TfrmServer::ServerSocketAccept(TObject *Sender,
      TCustomWinSocket *Socket)
{
        lblConnessione->Caption = "CONNESSIONE DA:   "+Socket->RemoteAddress;
}
//---------------------------------------------------------------------------

void __fastcall TfrmServer::txtMessaggioKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
  if (Key == VK_RETURN)
  {
     if (ServerSocket->Active)
     {
        if(txtMessaggio->Text!="")
        {
                Messaggio=NomeComputer+" "+txtMessaggio->Text;
                ServerSocket->Socket->Connections[0]->SendText(Messaggio);
                MemoMsg->Lines->Add(Messaggio);
                txtMessaggio->Text="";
        }
     }
  }
}
//---------------------------------------------------------------------------


void __fastcall TfrmServer::FormClose(TObject *Sender,
      TCloseAction &Action)
{
        tmrTempo->Enabled = false;
        ServerSocket->Active = false;
}
//---------------------------------------------------------------------------

void __fastcall TfrmServer::tmrTempoTimer(TObject *Sender)
{
        StatusBar->Panels->Items[0]->Text="Data odierna: "+Text=Now();
}
//---------------------------------------------------------------------------

void __fastcall TfrmServer::cmdBGClick(TObject *Sender)
{
        frmServer->Visible = false;        
}
//---------------------------------------------------------------------------

void __fastcall TfrmServer::cmdResetClick(TObject *Sender)
{
        MemoMsg->Clear();
}
//---------------------------------------------------------------------------

