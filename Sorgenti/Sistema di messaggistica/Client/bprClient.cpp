//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
USERES("bprClient.res");
USEFORM("cppClient.cpp", frmClient);
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->CreateForm(__classid(TfrmClient), &frmClient);
                 Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
