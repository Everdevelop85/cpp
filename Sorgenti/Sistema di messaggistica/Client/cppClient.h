//---------------------------------------------------------------------------
#ifndef cppClientH
#define cppClientH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <ScktComp.hpp>
//---------------------------------------------------------------------------
class TfrmClient : public TForm
{
__published:	// IDE-managed Components
        TMemo *MemoMsg;
        TEdit *Edit1;
        TGroupBox *GroupBox1;
        TButton *cmdStop;
        TButton *cmdStart;
        TLabel *Label1;
        TLabel *lblCnn;
        TLabel *Label2;
        TLabel *Label3;
        TButton *cmdSetup;
        TButton *cmdEsci;
        TClientSocket *ClientSocket;
        TTimer *tmrControllo;
        TLabel *Label4;
private:	// User declarations
public:		// User declarations
        __fastcall TfrmClient(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfrmClient *frmClient;
//---------------------------------------------------------------------------
#endif
