object frmMain: TfrmMain
  Left = 192
  Top = 114
  Width = 696
  Height = 480
  Caption = 'frmMain'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 432
    Top = 112
    Width = 161
    Height = 121
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object ListBox: TListBox
    Left = 168
    Top = 104
    Width = 217
    Height = 273
    ItemHeight = 13
    TabOrder = 1
  end
  object ADOConnection: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=promemoria.mdb;Pers' +
      'ist Security Info=False'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 120
    Top = 40
  end
  object ADOTable1: TADOTable
    Connection = ADOConnection
    Left = 136
    Top = 72
  end
  object ADOQuery: TADOQuery
    Active = True
    Connection = ADOConnection
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'SELECT (Format(PRM.Data,"dddd dd mmmm yyyy")) AS Data, PRM.Ora, ' +
        'PRM.testo'
      'FROM PRM'
      'ORDER BY PRM.Data, PRM.Ora;')
    Left = 200
    Top = 64
  end
end
