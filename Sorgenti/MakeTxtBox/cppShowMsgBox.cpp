//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "cppShowMsgBox.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TfrmMain *frmMain;
OptionButton *OB;
//---------------------------------------------------------------------------
__fastcall TfrmMain::TfrmMain(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------


char *GetStringOfText(char *t)
{
        char appT[255];
        int i=0,j=0;
        if(strstr(t,"\r\n")==NULL)
                return t;
        while(t[i]!='\0')
        {
                if(t[i]=='\r')
                {
                        appT[j]='\\';
                        appT[j+1]='r';
                        j+=2;
                        i++;
                        if(t[i]=='\n')
                        {
                                appT[j]='\\';
                                appT[j+1]='n';
                                j+=2;
                                i++;
                        }
                }
                appT[j]=t[i];
                i++;
                j++;
        }
        appT[j]='\0';
        return &(appT[0]);
}



void __fastcall TfrmMain::cmdAvClick(TObject *Sender)
{
        imgIcon->Picture->LoadFromFile("img\\1.bmp");
        FlagIcon=FlagsIcons[0];
        NameFlagIcon=NameFlagIcons[0];
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cmdCritClick(TObject *Sender)
{
        imgIcon->Picture->LoadFromFile("img\\2.bmp");
        FlagIcon=FlagsIcons[1];
        NameFlagIcon=NameFlagIcons[1];
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cmdInfClick(TObject *Sender)
{
        imgIcon->Picture->LoadFromFile("img\\59.bmp");
        FlagIcon=FlagsIcons[3];
        NameFlagIcon=NameFlagIcons[3];
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cmdHelpClick(TObject *Sender)
{
        imgIcon->Picture->LoadFromFile("img\\58.bmp");
        FlagIcon=FlagsIcons[2];
        NameFlagIcon=NameFlagIcons[2];
}
//---------------------------------------------------------------------------



void __fastcall TfrmMain::txtNumButtKeyPress(TObject *Sender, char &Key)
{
        if(((Key<49) || (Key>51)) && (Key!=8))
                Key=0;
}
//---------------------------------------------------------------------------



void __fastcall TfrmMain::FormCreate(TObject *Sender)
{
        OB = new OptionButton();
        for(int i=0;i<6;i++)
                cboComb->Items->Add(OB->Combinations[i]);
        cboComb->Text=cboComb->Items->Strings[0];
        FlagIcon=-1;
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::txtTitleChange(TObject *Sender)
{
        lblTitle->Caption=txtTitle->Text;
}
//---------------------------------------------------------------------------


void __fastcall TfrmMain::memTextChange(TObject *Sender)
{
        if(memText->Lines->Count<=1)
        {
                lblText1->Caption=memText->Lines->Strings[0];
                lblText2->Caption="";
        }
        else
                lblText2->Caption=memText->Lines->Strings[1];
}
//---------------------------------------------------------------------------







void __fastcall TfrmMain::cboCombChange(TObject *Sender)
{
        int n;
        n=cboComb->ItemIndex;
        if(n==0)
        {
                imgB1->Visible=false;
                imgB3->Visible=false;
                imgB2->Visible=true;
                imgB1->Left=112;
                imgB2->Left=192;
                imgB3->Left=272;
        }
        else
        {
                if((n==2) || (n==3))
                {
                        imgB1->Visible=true;
                        imgB3->Visible=true;
                        imgB2->Visible=true;
                        imgB1->Left=112;
                        imgB2->Left=192;
                        imgB3->Left=272;
                }
                else
                {
                        imgB1->Visible=true;
                        imgB2->Visible=true;
                        imgB3->Visible=false;
                        imgB1->Left=154;
                        imgB2->Left=242;
                        imgB3->Left=272;
                }
        }
        switch(n)
        {
                case 0:
                        imgB1->Picture=NULL;
                        imgB2->Picture->LoadFromFile("img\\ok.bmp");
                        imgB3->Picture=NULL;
                break;

                case 1:
                        imgB1->Picture->LoadFromFile("img\\ok.bmp");
                        imgB2->Picture->LoadFromFile("img\\annulla.bmp");
                        imgB3->Picture=NULL;
                break;

                case 2:
                        imgB1->Picture->LoadFromFile("img\\interrompi.bmp");
                        imgB2->Picture->LoadFromFile("img\\riprova.bmp");
                        imgB3->Picture->LoadFromFile("img\\ignora.bmp");
                break;

                case 3:
                        imgB1->Picture->LoadFromFile("img\\si.bmp");
                        imgB2->Picture->LoadFromFile("img\\no.bmp");
                        imgB3->Picture->LoadFromFile("img\\annulla.bmp");
                break;

                case 4:
                        imgB1->Picture->LoadFromFile("img\\si.bmp");
                        imgB2->Picture->LoadFromFile("img\\no.bmp");
                        imgB3->Picture=NULL;
                break;

                case 5:
                        imgB1->Picture->LoadFromFile("img\\riprova.bmp");
                        imgB2->Picture->LoadFromFile("img\\annulla.bmp");
                        imgB3->Picture=NULL;
                break;
        };
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cboCombClick(TObject *Sender)
{
        cboCombChange(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cmdProvaMsgClick(TObject *Sender)
{
        if(FlagIcon>=0)
                MessageBox(frmMain->Handle,memText->Text.c_str(),txtTitle->Text.c_str(),FlagIcon+OB->getFlag(cboComb->Text.c_str()));
        else
                MessageBox(frmMain->Handle,memText->Text.c_str(),txtTitle->Text.c_str(),OB->getFlag(cboComb->Text.c_str()));
}

//---------------------------------------------------------------------------


void __fastcall TfrmMain::cmdNoneClick(TObject *Sender)
{
        imgIcon->Picture=NULL;
        FlagIcon=-1;        
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cmdCopyClick(TObject *Sender)
{
        char msg[255];
        strcpy(msg,"MessageBox(frmMain->Handle,\"");
        strcat(msg,GetStringOfText(memText->Text.c_str()) );
        strcat(msg,"\",\"");
        strcat(msg,txtTitle->Text.c_str());
        strcat(msg,"\",");
        if(FlagIcon>=0)
        {
                strcat(msg,NameFlagIcon.c_str());
                strcat(msg,"+");
        }
        strcat(msg,OB->getNameFlag(cboComb->Text.c_str()).c_str());
        strcat(msg,");");
        txtApp->Text=msg;
        txtApp->SelectAll();
        txtApp->CopyToClipboard();  
}
//---------------------------------------------------------------------------











