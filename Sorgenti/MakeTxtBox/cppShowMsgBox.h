//---------------------------------------------------------------------------
#ifndef cppShowMsgBoxH
#define cppShowMsgBoxH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Buttons.hpp>
#include <ExtCtrls.hpp>
#include <Graphics.hpp>
#include <ComCtrls.hpp>

#include <conio.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

const int FlagsIcons[4]={MB_ICONEXCLAMATION,MB_ICONERROR,MB_ICONQUESTION,MB_ICONINFORMATION};

const char NameFlagIcons[4][20]={"MB_ICONEXCLAMATION",
                                 "MB_ICONERROR",
                                 "MB_ICONQUESTION",
                                 "MB_ICONINFORMATION"};
//---------------------------------------------------------------------------
class TfrmMain : public TForm
{
__published:	// IDE-managed Components
        TGroupBox *GroupBox1;
        TLabel *Label1;
        TLabel *Label2;
        TEdit *txtTitle;
        TGroupBox *GroupBox2;
        TGroupBox *GroupBox3;
        TBitBtn *cmdAv;
        TBitBtn *cmdCrit;
        TBitBtn *cmdInf;
        TImage *Image1;
        TImage *imgIcon;
        TLabel *lblText2;
        TImage *imgB1;
        TImage *imgB2;
        TImage *imgB3;
        TLabel *lblTitle;
        TBitBtn *cmdHelp;
        TGroupBox *GroupBox4;
        TLabel *lblText1;
        TMemo *memText;
        TComboBox *cboComb;
        TBitBtn *cmdNone;
        TBitBtn *cmdProvaMsg;
        TEdit *txtApp;
        TBitBtn *cmdCopy;
        void __fastcall cmdAvClick(TObject *Sender);
        void __fastcall cmdCritClick(TObject *Sender);
        void __fastcall cmdInfClick(TObject *Sender);
        void __fastcall cmdHelpClick(TObject *Sender);
        void __fastcall txtNumButtKeyPress(TObject *Sender, char &Key);
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall txtTitleChange(TObject *Sender);
        void __fastcall memTextChange(TObject *Sender);
        void __fastcall cboCombChange(TObject *Sender);
        void __fastcall cboCombClick(TObject *Sender);
        void __fastcall cmdProvaMsgClick(TObject *Sender);
        void __fastcall cmdNoneClick(TObject *Sender);
        void __fastcall cmdCopyClick(TObject *Sender);
private:	// User declarations
public:		// User declarations

        AnsiString NameFlagIcon;
        int FlagIcon;
        __fastcall TfrmMain(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfrmMain *frmMain;
//---------------------------------------------------------------------------
#endif

class OptionButton
{
        public:
        char Combinations[6][26];
        char NameFlag[6][26];
        int Flags[6];

        OptionButton()
        {
                strcpy(Combinations[0],"OK");
                strcpy(Combinations[1],"OK-ANNULLA");
                strcpy(Combinations[2],"INTERROMPI-RIPROVA-IGNORA");
                strcpy(Combinations[3],"SI-NO-ANNULLA");
                strcpy(Combinations[4],"SI-NO");
                strcpy(Combinations[5],"RIPROVA-ANNULLA");

                strcpy(NameFlag[0],"MB_OK");
                strcpy(NameFlag[1],"MB_OKCANCEL");
                strcpy(NameFlag[2],"MB_ABORTRETRYIGNORE");
                strcpy(NameFlag[3],"MB_YESNOCANCEL");
                strcpy(NameFlag[4],"MB_YESNO");
                strcpy(NameFlag[5],"MB_RETRYCANCEL");

                Flags[0]=MB_OK;
                Flags[1]=MB_OKCANCEL;
                Flags[2]=MB_ABORTRETRYIGNORE;
                Flags[3]=MB_YESNOCANCEL;
                Flags[4]=MB_YESNO;
                Flags[5]=MB_RETRYCANCEL;
        };

        AnsiString getNameFlag(char *c)
        {
                AnsiString cb;
                int i=0;
                while(strcmp(Combinations[i],c)!=0)
                        i++;
                cb=NameFlag[i];
                return cb;
        };

        int getFlag(char *c)
        {
                int i=0;
                while(strcmp(Combinations[i],c)!=0)
                        i++;
                return Flags[i];
        };

};
