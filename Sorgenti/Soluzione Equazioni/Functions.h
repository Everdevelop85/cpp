//---------------------------------------------------------------------------

#ifndef FunctionsH
#define FunctionsH
//---------------------------------------------------------------------------
#endif

#include "cppEquation.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <fcntl.h>
#include <Dialogs.hpp>
#include <sys\stat.h>
#include <io.h>

class FRACTAL
{
        public:
                int num;
                int den;

//COSTRUCTOR
        FRACTAL(void)
        {
                num=0;
                den=1;
        }

        FRACTAL(const FRACTAL &fr)
        {
                num=fr.num;
                den=fr.den;
                Semplifica();
        }

        FRACTAL(int n, int d)
        {
                if(den<0)
                {
                        num*=-1;
                        den=abs(den);
                }
                Semplifica();
        }

        FRACTAL(char *fr)
        {
                int i=0,l;
                char n[5],d[5];
                if((strchr(fr,'/'))==NULL)
                {
                        num=atoi(fr);
                        den=1;
                        return;
                }                
                while(fr[i]!='/')
                {
                        n[i]=fr[i];
                        i++;
                }
                n[i]='\0';
                i++;
                l=i;
                while(fr[i]!='\0')
                {
                        d[i-l]=fr[i];
                        i++;
                }
                d[i-l]='\0';
                num=atoi(n);
                if(atoi(d)<0)
                {
                        num*=-1;
                        den=abs(atoi(d));
                }
                else
                        den=atoi(d);
                Semplifica();
        }

//METHODS
        public: void Semplifica()
        {
                int *n,divisor,sign=1;
                if(num>den)
                        n=&den;
                else
                        n=&num;
                if(*n<0)
                {
                        sign=-1;
                        *n=(*n)*-1;
                }
                for(divisor=*n;divisor>1;divisor--)
                {
                        while((((den%divisor)==0)&&((num%divisor)==0))&&(divisor>=2))
                        {
                                den/=divisor;
                                num/=divisor;
                                divisor=*n;
                        }
                }
                num*=sign;
        }

        public: void Print()
        {
                if((den==1)||(num==0))
                        printf("%d",num);
                else
                        printf("%d/%d",num,den);
        }

        float toFloat()
        {
                return (float) num/den;
        }
//OPERATOR SAME TYPE
        void operator=(const FRACTAL &fr)
        {
                num=fr.num;
                den=fr.den;
        }

        void operator=(const int &n)
        {
                num=n;
                den=1;
        }

        void operator=(const char *fr)
        {
                int i=0,l;
                char n[5],d[5];
                if((strchr(fr,'/'))==NULL)
                {
                        num=atoi(fr);
                        den=1;
                        return;
                }
                else
                while(fr[i]!='/')
                {
                        n[i]=fr[i];
                        i++;
                }
                n[i]='\0';
                i++;
                l=i;
                while(fr[i]!='\0')
                {
                        d[i-l]=fr[i];
                        i++;
                }
                d[i-l]='\0';
                num=atoi(n);
                if(atoi(d)<0)
                {
                        num*=-1;
                        den=abs(atoi(d));
                }
                else
                        den=atoi(d);
                Semplifica();
        }

        friend bool operator==(const FRACTAL &f1, const FRACTAL &f2)
        {
                FRACTAL p1(f1),p2(f2);
                if((p1.num==p2.num)&&(p1.den==p2.den))
                        return true;
                return false;
        }

        friend bool operator<(const FRACTAL &f1, const FRACTAL &f2)
        {
                float fr1=0.00, fr2=0.00;
                fr1=(float) (f1.num/f1.den);
                fr2=(float) (f2.num/f2.den);
                if(fr1<fr2)
                        return true;
                return false;
        }

        friend bool operator==(const FRACTAL &f1, const int &n)
        {
                FRACTAL app(n,1), p1(f1);
                if(p1==app)
                        return true;
                return false;
        }


        friend FRACTAL operator+(const FRACTAL &f1, const FRACTAL &f2)
        {
                FRACTAL ris;
                ris.den=f1.den*f2.den;
                ris.num=((ris.den/f1.den)*f1.num)+((ris.den/f2.den)*f2.num);
                ris.Semplifica();
                return ris;
        }

        friend FRACTAL operator+=(const FRACTAL &f1, const FRACTAL &f2)
        {
                FRACTAL ris;
                ris.den=f1.den*f2.den;
                ris.num=((ris.den/f1.den)*f1.num)+((ris.den/f2.den)*f2.num);
                ris.Semplifica();
                return ris;
        }

        friend FRACTAL operator-(const FRACTAL &f1, const FRACTAL &f2)
        {
                FRACTAL ris;
                ris.den=f1.num*f2.den;
                ris.num=(((ris.den/f1.den)*f1.num)-((ris.den/f2.den)*f2.num));
                ris.Semplifica();
                return ris;
        };

        friend FRACTAL operator*(const FRACTAL &f1, const FRACTAL &f2)
        {
                FRACTAL ris;
                ris.num=f1.num*f2.num;
                ris.den=f1.den*f2.den;
                ris.Semplifica();
                return ris;
        };

        friend FRACTAL operator/(const FRACTAL &f1, const FRACTAL &f2)
        {
                FRACTAL ris;
                ris.num=f1.num*f2.den;
                ris.den=f1.den*f2.num;
                ris.Semplifica();
                return ris;
        };

        bool operator!=(const FRACTAL &fr)
        {
                if((num!=fr.num)&&((den!=fr.den)))
                        return true;
                return false;
        };

//OPERTORS AND CONSTANT
        bool operator!=(const int &n)
        {
                if(den!=1)
                        return true;
                else
                {
                        if(num!=n)
                                return true;
                        return false;
                }
        };

        bool operator>(const int &n)
        {
                float fr,nm;
                nm=(float) n;
                fr=(float) num/den;
                if(fr>nm)
                        return true;
                return false;
        };

        friend FRACTAL operator^(const FRACTAL &f1, const int &n)
        {
                FRACTAL ris(1,1),Rfr(f1);
                int app,exp=n;
                if(exp<0)
                {
                        app=Rfr.den;
                        Rfr.den=Rfr.num;
                        Rfr.num=app;
                        exp=abs(exp);
                }
                if(exp>0)
                {
                        ris.num=pow(Rfr.num,exp);
                        ris.den=pow(Rfr.den,exp);
                        ris.Semplifica();
                }
                return ris;
        };

        friend FRACTAL operator+(const FRACTAL &f1, const int &n)
        {
                FRACTAL ris;
                ris.den=f1.den;
                ris.num=(ris.den*n)+f1.num;
                ris.Semplifica();
                return ris;
        }

        friend FRACTAL operator-(const FRACTAL &f1, const int &n)
        {
                FRACTAL ris;
                ris.den=f1.den;
                ris.num=(f1.num-(f1.den*n));
                ris.Semplifica();
                return ris;
        };

        friend FRACTAL operator*(const FRACTAL &f1, const int &n)
        {
                FRACTAL ris;
                ris.num=f1.num*n;
                ris.den=f1.den;
                ris.Semplifica();
                return ris;
        };

        friend FRACTAL operator/(const FRACTAL &f1, const int &n)
        {
                FRACTAL ris;
                ris.num=f1.num;
                ris.den=f1.den*n;
                ris.Semplifica();
                return ris;
        };

        AnsiString toString()
        {
                this->Semplifica();
                if(this->den==1)
                        return IntToStr(this->num);
                return IntToStr(this->num)+"/"+IntToStr(this->den);
        }
};




class MATRICE
{

        public: int nl, nc;
                FRACTAL **matrix;
                AnsiString NameM;

        MATRICE(void)
        {
                nl=0;
                nc=0;
                NameM="";
        };

        MATRICE(int m, int n)
        {
                int i,j;
                nl=m;
                nc=n;
                matrix=(FRACTAL **)malloc(sizeof(FRACTAL)*nl);
                for(i=0;i<nl;i++)
                        matrix[i]=(FRACTAL *)malloc(sizeof(FRACTAL)*nc);
                for(i=0;i<nl;i++)
                {
                        for(j=0;j<nc;j++)
                                matrix[i][j]=0;
                }
        };

        MATRICE(char *d)
        {
                char num[6];
                int i=0,l,j;
                while((d[i]!='x')&&(d[i]!='X'))
                {
                        num[i]=d[i];
                        i++;
                }
                num[i]='\0';
                nl=atoi(num);
                l=++i;
                while(d[i]!='\0')
                {
                        num[i-l]=d[i];
                        i++;
                }
                num[i]='\0';
                nc=atoi(num);
                matrix=(FRACTAL **)malloc(sizeof(FRACTAL)*nl);
                for(i=0;i<nl;i++)
                        matrix[i]=(FRACTAL *)malloc(sizeof(FRACTAL)*nc);
                for(i=0;i<nl;i++)
                {
                        for(j=0;j<nc;j++)
                                matrix[i][j]=0;
                }
        }

        void SetMatrix(int m, int n)
        {
                int i,j;
                nl=m;
                nc=n;
                matrix=(FRACTAL **)malloc(sizeof(FRACTAL)*nl);
                for(i=0;i<nl;i++)
                        matrix[i]=(FRACTAL *)malloc(sizeof(FRACTAL)*nc);
                for(i=0;i<nl;i++)
                {
                        for(j=0;j<nc;j++)
                                matrix[i][j]=0;
                }
        }

        public: MATRICE Minor(int l, int c)
        {
                MATRICE M(nl-1,nc-1);
                int i,j,im=-1,jm=-1;
                for(i=0;i<nl;i++)
                {
                        if(i!=l)
                        {
                                im++;
                                for(j=0;j<nc;j++)
                                {
                                        if(j!=c)
                                        {
                                                jm++;
                                                M.matrix[im][jm]=matrix[i][j];
                                        }

                                }
                                jm=-1;
                        }
                }
                return M;
        };

        public: FRACTAL DET()
        {
                int i,j,exp;
                FRACTAL det,app;
                MATRICE M(nl-1,nc-1);
                switch(nl)
                {
                        case 2:
                                det=((matrix[0][1]*matrix[1][0])*(-1))+(matrix[0][0]*matrix[1][1]);
                        break;
                        case 3:
                                det=((matrix[0][2]*matrix[1][1]*matrix[2][0])+(matrix[0][0]*matrix[1][2]*matrix[2][1])+(matrix[0][1]*matrix[1][0]*matrix[2][2]))*(-1);
                                det=det+((matrix[0][0]*matrix[1][1]*matrix[2][2])+(matrix[0][1]*matrix[1][2]*matrix[2][0])+(matrix[0][2]*matrix[1][0]*matrix[2][1]));
                        break;

                        default:
                                for(j=0;j<nc;j++)
                                {
                                        if(matrix[0][j]!=0)
                                        {
                                                M=Minor(0,j);
                                                det=det+(matrix[0][j]*(M.DET()*(pow(-1,(1+(j+1))))));
                                        }
                                }
                        break;
                };
                return det;
        }

        public: MATRICE COMP()
        {
                MATRICE M(nl-1,nc-1);
                MATRICE CM(nl,nc);
                int i,j;
                for(i=0;i<nl;i++)
                {
                        for(j=0;j<nc;j++)
                        {
                                M=this->Minor(i,j);
                                CM.matrix[i][j]=M.DET()*(pow(-1,((i+1)+(j+1))));
                        }
                }
                return CM;
        };

        void operator=(const MATRICE &M)
        {
                int i,j;
                for(i=0;i<nl;i++)
                {
                        for(j=0;j<nc;j++)
                                matrix[i][j]=M.matrix[i][j];
                }
        };

        MATRICE operator*(const MATRICE &M)
        {
                MATRICE ris(nl,M.nc);
                int k,i,j;
                FRACTAL app1,app2;
                for(i=0;i<ris.nl;i++)
                        for(j=0;j<ris.nc;j++)
                        {
                                ris.matrix[i][j]=0;
                                for(k=0;k<nc;k++)
                                {
                                        app1=matrix[i][k]*M.matrix[k][j];
                                        ris.matrix[i][j]=ris.matrix[i][j]+app1;
                                }
                };
                return ris;
        };

        MATRICE operator+(const MATRICE &M)
        {
                MATRICE ris(nl,M.nc);
                int i,j;
                for(i=0;i<nl;i++)
                {
                        for(j=0;j<nc;j++)
                                ris.matrix[i][j]=matrix[i][j]+M.matrix[i][j];
                }
                return ris;
        };

        MATRICE operator-(const MATRICE &M)
        {
                MATRICE ris(nl,M.nc);
                int i,j;
                for(i=0;i<nl;i++)
                {
                        for(j=0;j<nc;j++)
                                ris.matrix[i][j]=matrix[i][j]-M.matrix[i][j];
                }
                return ris;
        };

        MATRICE operator*(const FRACTAL &sc)
        {
                MATRICE ris(nl,nc);
                int i,j;
                for(i=0;i<ris.nl;i++)
                {
                        for(j=0;j<ris.nc;j++)
                                ris.matrix[i][j]=matrix[i][j]*sc;
                }
                return ris;
        };

        MATRICE operator+(const FRACTAL &sc)
        {
                MATRICE ris(nl,nc);
                FRACTAL app;
                int i,j;
                for(i=0;i<ris.nl;i++)
                {
                        for(j=0;j<ris.nc;j++)
                                ris.matrix[i][j]=matrix[i][j]+sc;
                }
                return ris;
        };

        MATRICE operator-(const FRACTAL &sc)
        {
                MATRICE ris(nl,nc);
                FRACTAL app;
                int i,j;
                for(i=0;i<ris.nl;i++)
                {
                        for(j=0;j<ris.nc;j++)
                                ris.matrix[i][j]=matrix[i][j]-sc;
                }
                return ris;
        };

        public: MATRICE Trsposta()
        {
                MATRICE ris(nc,nl);
                int i,j,app;
                for(i=0;i<nc;i++)
                {
                        for(j=0;j<nl;j++)
                                ris.matrix[i][j]=matrix[j][i];
                }
                free(matrix);
                app=nc;
                nc=nl;
                nl=app;
                matrix=(FRACTAL **)malloc(sizeof(FRACTAL)*nl);
                for(i=0;i<nl;i++)
                        matrix[i]=(FRACTAL *)malloc(sizeof(FRACTAL)*nc);
                return ris;
        }

        /*int Rango()
        {
                int indOr;
                int min;
                if(nc<=nl)
                        min=nc;
                else
                        min=nl;
                for(indOr=0;indOr<min;indOr++)
                {

                }
        }*/
};

class MONOMIO_X
{
        public:
                FRACTAL coeff;
                int grad;

        MONOMIO_X(void)
        {
                grad=0;
                coeff=0;
        }

        MONOMIO_X(FRACTAL c, int g)
        {
                coeff=c;
                grad=g;
        }

        MONOMIO_X(char *c, int g)
        {
                coeff=c;
                grad=g;
        }

//METHODS
        AnsiString toString()
        {
                AnsiString ret;
                if(coeff.num>=0)
                        ret="+";
                //else
                        //ret="-";
                if(grad!=0)
                        ret=ret+coeff.toString()+"X^"+IntToStr(grad);
                else
                        ret=ret+coeff.toString();
                return ret;
        }

//OPERATOR SAME TYPE
        void operator=(const MONOMIO_X &mx)
        {
                coeff=mx.coeff;
                grad=mx.grad;
        }

        void operator=(const int &n)
        {
                coeff=n;
                grad=0;
        }

        void operator=(char *mx)
        {
                char c[20]="\0",g[20]="\0", *app;
                int i=0;
                if(strstr(mx,"X")!=NULL)
                {
                        while(mx[i]!='X')
                        {
                                c[i]=mx[i];
                                i++;
                        }
                        c[i]='\0';
                        if(strlen(c)>0)
                        {
                                if(strlen(c)==1)
                                {
                                        if(c[0]=='+')
                                                coeff=1;
                                        if(c[0]=='-')
                                                coeff=-1;
                                }
                                coeff=c;
                        }
                        else
                                coeff=1;
                        app=strstr(mx,"^");
                        if(app!=NULL)
                        {
                                app+=1;
                                grad=atoi(app);
                        }
                        else
                                grad=1;
                }
                else
                {
                        coeff=mx;
                        grad=0;
                }
        }

        float GenerareMNWithX(FRACTAL num)
        {
                FRACTAL app(num^grad);
                return coeff.toFloat()*app.toFloat();
        }

//OPERATORS
        /*friend bool operator==(const FRACTAL &f1, const FRACTAL &f2)
        {
                FRACTAL p1(f1),p2(f2);
                if((p1.num==p2.num)&&(p1.den==p2.den))
                        return true;
                return false;
        }

        friend bool operator==(const FRACTAL &f1, const int &n)
        {
                FRACTAL app(n,1), p1(f1);
                if(p1==app)
                        return true;
                return false;
        }*/


        friend MONOMIO_X operator+(const MONOMIO_X &mx1, const MONOMIO_X &mx2)
        {
                MONOMIO_X ris(0,0);
                if(mx1.grad==mx2.grad)
                {
                        ris.coeff=mx1.coeff+mx2.coeff;
                        ris.grad=mx1.grad;
                }
                return ris;
        }

        /*friend FRACTAL operator+=(const FRACTAL &f1, const FRACTAL &f2)
        {
                FRACTAL ris;
                ris.den=f1.den*f2.den;
                ris.num=((ris.den/f1.den)*f1.num)+((ris.den/f2.den)*f2.num);
                ris.Semplifica();
                return ris;
        }*/

        friend MONOMIO_X operator-(const MONOMIO_X &mx1, const MONOMIO_X &mx2)
        {
                MONOMIO_X ris(0,0);
                if(mx1.grad==mx2.grad)
                {
                        ris.coeff=mx1.coeff-mx2.coeff;
                        ris.grad=mx1.grad;
                }
                return ris;
        };

        friend MONOMIO_X operator*(const MONOMIO_X &mx1, const MONOMIO_X &mx2)
        {
                MONOMIO_X ris;
                ris.coeff=mx1.coeff*mx2.coeff;
                ris.grad=mx1.grad+mx2.grad;
                return ris;
        };

        friend MONOMIO_X operator/(const MONOMIO_X &mx1, const MONOMIO_X &mx2)
        {
                MONOMIO_X ris;
                ris.coeff=mx1.coeff/mx2.coeff;
                ris.grad=mx1.grad-mx2.grad;
                return ris;
        };
};

typedef struct list_element
{
        MONOMIO_X value;
        struct list_element  *next;
}item;
typedef item*  list;


class POLINOMIO_X
{
public:
        list root, tail;

        POLINOMIO_X()
        {
                root=NULL;
                tail=root;
        };

        void Add(MONOMIO_X e)
        {
                list t;
                t=(list)malloc(sizeof(item));
                t->value=e;
                if(root==NULL)
                {
                        t->next=root;
                        root=t;
                        tail=root;
                }
                else
                {
                        t->next=tail->next;
                        tail->next=t;
                        //root->next=t
                        tail=t;
                        //
                        //tail=t;
                }
        }

        int Count()
        {
                list l;
                l=root;
                int i=0;
                while(l!=NULL)
                {
                        i++;
                        l=l->next;
                }
                return i;
        }

        void Clear()
        {
                list l;
                while(root!=NULL)
                {
                        l=root;
                        root=root->next;
                        free(l);
                }
        }

        void AutoComplete()
        {
                list l, d;
                l=root;
                d=l->next;
                while(d!=NULL)
                {
                        if((d->value.grad-l->value.grad)>1)
                        {
                                list t;
                                t=(list)malloc(sizeof(item));
                                t->value.grad=l->value.grad-1;
                                l->value.coeff=0;
                                t->next=l->next;
                                l->next=t;
                                l=root;
                        }
                        l=l->next;
                        d=l->next;
                }
        }


        POLINOMIO_X* ConvertCoeffToInt()
        {
                POLINOMIO_X *pi;
                pi= new POLINOMIO_X();
                MONOMIO_X el;
                MONOMIO_X *strc;
                strc=getStruct();
                int c=Count();
                for(int i=0;i<c;i++)
                {
                        if(strc[i].coeff.den!=1)
                        {
                                for(int j=0;j<c;j++)
                                        strc[j].coeff=strc[j].coeff*strc[i].coeff.den;
                        }
                }
                for(int i=0;i<c;i++)
                        pi->Add(strc[i]);
                return pi;
        }

        MONOMIO_X *getStruct()
        {
                MONOMIO_X *vett;
                list l;
                int i=0;
                vett=(MONOMIO_X *)malloc((sizeof(MONOMIO_X))*this->Count());
                l=root;
                while(l!=NULL)
                {
                        vett[i]=l->value;
                        i++;
                        l=l->next;
                }
                return vett;
        }

        AnsiString toString()
        {
                list l;
                AnsiString ret;
                ret="";
                l=root;
                while(l!=NULL)
                {
                        ret=ret+l->value.toString();
                        l=l->next;
                }
                return ret;
        }

        float getSolutionWith(FRACTAL num)
        {
                float s=0.0;
                list l;
                l=root;
                while(l!=NULL)
                {
                        s+=l->value.GenerareMNWithX(num);
                        l=l->next;
                }
                return s;
        }

        MONOMIO_X getMonomio(int index)
        {
                int i=1;
                list l;
                l=root;
                while(i!=index)
                {
                        l=l->next;
                        i++;
                }
                return l->value;
        }

        float getCoeffMonomio(int index)
        {
                int i=1;
                list l;
                l=root;
                while(i!=index)
                {
                        l=l->next;
                        i++;
                }
                return l->value.coeff.toFloat();
        }

        /*FRACTAL *Solution(int *NS)
        {

        }*/
};

int SaveData(const char *p);
int OpenData(const char *p);
AnsiString RemovingSpaces(AnsiString s);
void getColLinesM(int *col, int *lin, char *ptr);
void getElementsM(char *ptr, MATRICE *M);
void PrintMatrix(MATRICE *M);
void GetPnX(char *Px, POLINOMIO_X *pdx);
void OrderPdX(POLINOMIO_X *pdx);
float *getSolutions(int &NS, POLINOMIO_X *pdx);
POLINOMIO_X **getScomposition(int nSol, float *VettSolutions);

