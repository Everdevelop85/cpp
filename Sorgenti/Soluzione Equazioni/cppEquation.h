//---------------------------------------------------------------------------

#ifndef cppEquationH
#define cppEquationH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <Buttons.hpp>
#include <ToolWin.hpp>
#include <Dialogs.hpp>
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

class TfrmMain : public TForm
{
__published:	// IDE-managed Components
        TPageControl *PageControl;
        TTabSheet *TabSheet1;
        TPanel *Panel1;
        TPanel *Panel2;
        TEdit *txtEquazione;
        TButton *cmdInsert;
        TPanel *Panel3;
        TPanel *Panel4;
        TRichEdit *rtfViewEq;
        TTabSheet *TabSheet2;
        TPanel *Panel5;
        TPanel *Panel6;
        TEdit *txtMatrix;
        TButton *cmdInsertMatrix;
        TPanel *panViewM;
        TPanel *Panel7;
        TToolBar *ToolBar1;
        TSpeedButton *SpeedButton1;
        TSpeedButton *cmdSave;
        TButton *cmdSolution;
        TGroupBox *grbMatrixOp;
        TButton *cmdSum;
        TButton *cmdMul;
        TButton *cmdDif;
        TGroupBox *grbMatrixCalc;
        TButton *cmdDet;
        TButton *cmdComp;
        TButton *cmdInv;
        TButton *cmdTras;
        TButton *cmdMul2;
        TButton *cmdDif2;
        TMemo *memTextMatrix;
        TSaveDialog *SaveDialog;
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall cmdInsertMatrixClick(TObject *Sender);
        void __fastcall SpeedButton1Click(TObject *Sender);
        void __fastcall cmdSolutionClick(TObject *Sender);
        void __fastcall cmdDetClick(TObject *Sender);
        void __fastcall cmdCompClick(TObject *Sender);
        void __fastcall cmdInvClick(TObject *Sender);
        void __fastcall cmdTrasClick(TObject *Sender);
        void __fastcall cmdSumClick(TObject *Sender);
        void __fastcall cmdMulClick(TObject *Sender);
        void __fastcall cmdMul2Click(TObject *Sender);
        void __fastcall cmdDifClick(TObject *Sender);
        void __fastcall cmdDif2Click(TObject *Sender);
        void __fastcall cmdSaveClick(TObject *Sender);
        void __fastcall cmdInsertClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TfrmMain(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfrmMain *frmMain;
//---------------------------------------------------------------------------
#endif


