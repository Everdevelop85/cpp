//---------------------------------------------------------------------------


#pragma hdrstop

#include "Functions.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

#define RD_RW   06	//Check for read and write permission
#define RD_     04	//Check for read permission
#define WR_     02	//Check for write permission
#define EXEC    01	//Execute (ignored)
#define EXISTS  00	//Check for existence of file


int SaveData(const char *p)
{
        int f;
        if((access(p,EXISTS))==0)
        {
                switch(MessageBox(frmMain->Handle,"Il file esiste gi�, sovrascrivelo ?","Attenzione",MB_ICONEXCLAMATION+MB_YESNOCANCEL))
                {
                        case IDYES:
                                if((f=open(p,O_CREAT|O_TRUNC,S_IREAD|S_IWRITE))<0)
                                        return 1;
                        break;

                        case IDNO:
                                if((f=open(p,O_APPEND,S_IREAD|S_IWRITE))<0)
                                        return 2;

                        default:
                                return 3;
                };
        }
        write(f,frmMain->txtMatrix->Text.c_str(),frmMain->txtMatrix->Text.Length());
        write(f,"\n",1);
        for(int i=0;i<frmMain->memTextMatrix->Lines->Count;i++)
        {
                write(f,frmMain->memTextMatrix->Lines->Strings[i].c_str(),frmMain->memTextMatrix->Lines->Strings[i].Length());
                write(f,"\n",1);
        }
        close(f);
        return 0;
}

int OpenData(const char *p)
{
        int f,i=0;
        char buffer[100];
        if((f=open(p,O_RDONLY))<0)
                return 1;
        frmMain->memTextMatrix->Clear();
        while(((read(f,&(buffer[i]),1))>0)&&(buffer[i]!='\n'))
                i++;
        buffer[i]='\0';
        frmMain->txtMatrix->Text=buffer[i];
        i=0;
        while((read(f,&(buffer[i]),1))>0)
        {
                if(buffer[i]=='\n')
                {
                        buffer[i]='\0';
                        frmMain->memTextMatrix->Lines->Add(buffer);
                        i=0;
                }
                i++;
        }
        close(f);
        return 0;
}

AnsiString RemovingSpaces(AnsiString s)
{
        AnsiString app=s;
        while(app.Pos(" ")!=0)
                app.Delete(app.Pos(" "),1);
        return app;
}

void getColLinesM(int *col, int *lin, char *ptr)
{
        int i=0;
        *col=1;
        *lin=1;
        while(ptr[i]!=';')
        {
                if(ptr[i]==',')
                        (*col)++;
                i++;
        }
        i=0;
        while(ptr[i]!='\0')
        {
                if(ptr[i]==';')
                        (*lin)++;
                i++;
        }
}

void getElementsM(char *ptr, MATRICE *M)
{
        char num[20];
        int ind=0,i=0;
        for(int l=0;l<M->nl;l++)
        {
                for(int c=0;c<M->nc;c++)
                {
                        while(((ptr[i]>='0')&&(ptr[i]<='9'))||(ptr[i]=='-')||(ptr[i]=='/'))
                        {
                                num[ind]=ptr[i];
                                i++;
                                ind++;
                        }
                        num[ind]='\0';
                        M->matrix[l][c]=num;
                        ind=0;
                        i++;
                }
        }
}

void PrintMatrix(MATRICE *M)
{
        AnsiString line;
        line="";
        frmMain->memTextMatrix->Lines->Add("Matrice "+M->NameM+":");
        for(int i=0;i<M->nl;i++)
        {
                for(int j=0;j<M->nc;j++)
                        line+=M->matrix[i][j].toString()+"\t";
                frmMain->memTextMatrix->Lines->Add(line);
                frmMain->memTextMatrix->Lines->Add("");
                line="";
        }
        frmMain->memTextMatrix->Lines->Add("");
}

void GetPnX(char *Px, POLINOMIO_X *pdx)
{
        int i=0, ind;
        char app[50];
        MONOMIO_X element;
        while(Px[i]!='\0')
        {
                ind=0;
                do
                {
                        app[ind]=Px[i];
                        ind++;
                        i++;
                }
                while((Px[i]!='+')&&(Px[i]!='-')&&(Px[i]!='\0'));
                app[ind]='\0';
                element=app;
                pdx->Add(element);
        }
}

void OrderPdX(POLINOMIO_X *pdx)
{
        MONOMIO_X *v;
        MONOMIO_X app;
        v=pdx->getStruct();
        int l=pdx->Count();
        for(int i=0;i<l-1;i++)
        {
                for(int j=i+1;j<l;j++)
                {
                        if(v[i].grad<v[j].grad)
                        {
                                app=v[j];
                                v[j]=v[i];
                                v[i]=app;
                        }
                }
        }
        pdx->Clear();
        for(int i=0;i<l;i++)
                pdx->Add(v[i]);
        free(v);
}

int *getDivisor(int num)
{
        int *div, i=0, count=num;
        div=(int*)malloc(num);
        while(count>=1)
        {
                if((num%count)==0)
                {
                        div[i]=count;
                        count--;
                        i++;
                }
        }
        div[i]=-1;
        return div;
}




float *getSolutions(int &NS, POLINOMIO_X *pdx)
{
        float *sl=NULL;
        NS=pdx->root->value.grad;
        switch(NS)
        {
                case 2:
                        float delta, a,b,c;
                        a=pdx->getCoeffMonomio(1);
                        b=pdx->getCoeffMonomio(2);
                        c=pdx->getCoeffMonomio(3);
                        delta=pow(b,2)-(4*a*c);
                        if(delta>=0)
                        {
                                sl=(float *)malloc(NS);
                                sl[0]=((-1*b)+(sqrt(delta)))/(2*a);
                                sl[1]=((-1*b)-(sqrt(delta)))/(2*a);
                        }
                break;

                case 3:
                        /*POLINOMIO_X *pd;
                        pd=pdx->ConvertCoeffToInt();
                        MONOMIO_X *pn=pdx->getStruct();
                        int num_mon=pdx->Count();
                        int tn=pn[num_mon-1].coeff.num;
                        int *div, i=0;
                        div=getDivisor(tn);
                        bool trovato=false;
                        while(div[i]>0)
                        {
                                if(pd->getSolutionWith(div[i] ==0)
                                {
                                        trovato=true;
                                        break;
                                }
                                i++;
                        }
                        if(trovato)
                        {
                                pd->Clear();

                        }
                        free(div);
                        pd->Clear();
                        free(pd);
                        free(pn); */
                break;
        };
        return sl;
}

POLINOMIO_X **getScomposition(int nSol, float *VettSolutions)
{

        POLINOMIO_X **scomposition;
        MONOMIO_X value;
        scomposition=(POLINOMIO_X**)malloc(sizeof(POLINOMIO_X)*nSol);
        for(int i=0;i<nSol;i++)
        {
                scomposition[i]  = new POLINOMIO_X();
                value="X";
                scomposition[i]->Add(value);
                value=(VettSolutions[i]*(-1));
                scomposition[i]->Add(value);
        }
        return scomposition;
}

