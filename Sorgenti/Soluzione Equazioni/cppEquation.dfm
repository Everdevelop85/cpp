object frmMain: TfrmMain
  Left = 250
  Top = 182
  Width = 730
  Height = 576
  Caption = 'Equation'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl: TPageControl
    Left = 0
    Top = 0
    Width = 722
    Height = 549
    ActivePage = TabSheet1
    Align = alClient
    TabIndex = 0
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'TabSheet1'
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 714
        Height = 521
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Panel2: TPanel
          Left = 0
          Top = 0
          Width = 714
          Height = 49
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object txtEquazione: TEdit
            Left = 0
            Top = 8
            Width = 521
            Height = 24
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Courier New'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
          end
          object cmdInsert: TButton
            Left = 528
            Top = 8
            Width = 81
            Height = 25
            Caption = 'Inserisci'
            TabOrder = 1
            OnClick = cmdInsertClick
          end
        end
        object Panel3: TPanel
          Left = 0
          Top = 453
          Width = 714
          Height = 68
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 1
          object cmdSolution: TButton
            Left = 8
            Top = 8
            Width = 129
            Height = 25
            Caption = 'cmdSolution'
            TabOrder = 0
            OnClick = cmdSolutionClick
          end
        end
        object Panel4: TPanel
          Left = 0
          Top = 49
          Width = 714
          Height = 404
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 2
          object rtfViewEq: TRichEdit
            Left = 0
            Top = 0
            Width = 714
            Height = 404
            Align = alClient
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Courier New'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
          end
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'TabSheet2'
      ImageIndex = 1
      object Panel5: TPanel
        Left = 0
        Top = 0
        Width = 714
        Height = 521
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Panel6: TPanel
          Left = 0
          Top = 0
          Width = 714
          Height = 41
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object txtMatrix: TEdit
            Left = 0
            Top = 8
            Width = 521
            Height = 24
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Courier New'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
          end
          object cmdInsertMatrix: TButton
            Left = 528
            Top = 8
            Width = 81
            Height = 25
            Caption = 'Inserisci'
            TabOrder = 1
            OnClick = cmdInsertMatrixClick
          end
        end
        object panViewM: TPanel
          Left = 0
          Top = 41
          Width = 714
          Height = 358
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object ToolBar1: TToolBar
            Left = 0
            Top = 0
            Width = 714
            Height = 48
            BorderWidth = 2
            ButtonHeight = 33
            Caption = 'ToolBar'
            TabOrder = 0
            DesignSize = (
              706
              38)
            object SpeedButton1: TSpeedButton
              Left = 0
              Top = 2
              Width = 65
              Height = 33
              AllowAllUp = True
              Anchors = [akLeft]
              Caption = 'Cancella'
              OnClick = SpeedButton1Click
            end
            object cmdSave: TSpeedButton
              Left = 65
              Top = 2
              Width = 64
              Height = 33
              AllowAllUp = True
              Anchors = [akLeft, akTop, akRight, akBottom]
              BiDiMode = bdLeftToRight
              Caption = 'Salva'
              ParentBiDiMode = False
              OnClick = cmdSaveClick
            end
          end
          object memTextMatrix: TMemo
            Left = 0
            Top = 48
            Width = 714
            Height = 310
            Align = alClient
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'Courier New'
            Font.Style = []
            ParentFont = False
            ScrollBars = ssVertical
            TabOrder = 1
          end
        end
        object Panel7: TPanel
          Left = 0
          Top = 399
          Width = 714
          Height = 122
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 2
          object grbMatrixOp: TGroupBox
            Left = 369
            Top = 0
            Width = 345
            Height = 122
            Align = alClient
            Caption = 'Operazioni sulle matrici'
            TabOrder = 0
            object cmdSum: TButton
              Left = 8
              Top = 24
              Width = 161
              Height = 25
              TabOrder = 0
              OnClick = cmdSumClick
            end
            object cmdMul: TButton
              Left = 8
              Top = 56
              Width = 161
              Height = 25
              TabOrder = 1
              OnClick = cmdMulClick
            end
            object cmdDif: TButton
              Left = 8
              Top = 88
              Width = 161
              Height = 25
              TabOrder = 2
              OnClick = cmdDifClick
            end
            object cmdMul2: TButton
              Left = 176
              Top = 24
              Width = 161
              Height = 25
              TabOrder = 3
              OnClick = cmdMul2Click
            end
            object cmdDif2: TButton
              Left = 176
              Top = 56
              Width = 161
              Height = 25
              TabOrder = 4
              OnClick = cmdDif2Click
            end
          end
          object grbMatrixCalc: TGroupBox
            Left = 0
            Top = 0
            Width = 369
            Height = 122
            Align = alLeft
            Caption = 'Calcolo sulla matrice'
            TabOrder = 1
            object cmdDet: TButton
              Left = 8
              Top = 24
              Width = 161
              Height = 25
              Caption = 'DETERMINANTE'
              TabOrder = 0
              OnClick = cmdDetClick
            end
            object cmdComp: TButton
              Left = 8
              Top = 56
              Width = 161
              Height = 25
              Caption = 'Matrice COMPLEMENTARE'
              TabOrder = 1
              OnClick = cmdCompClick
            end
            object cmdInv: TButton
              Left = 8
              Top = 88
              Width = 161
              Height = 25
              Caption = 'INVERSA'
              TabOrder = 2
              OnClick = cmdInvClick
            end
            object cmdTras: TButton
              Left = 176
              Top = 24
              Width = 161
              Height = 25
              Caption = 'TRASPOSTA'
              TabOrder = 3
              OnClick = cmdTrasClick
            end
          end
        end
      end
    end
  end
  object SaveDialog: TSaveDialog
    Filter = 'File di testo standard (*.txt)|*.txt'
    InitialDir = 'C:\Documents and Setings\All Users'
    Title = 'Salvataggio del file'
    Left = 160
    Top = 72
  end
end
