//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "cppEquation.h"
#include "Functions.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"


MATRICE A,B;
POLINOMIO_X *PdX;
TfrmMain *frmMain;

//TLabel **MatrA, **MatrB;
AnsiString NameMatrA, NameMatrB;
//---------------------------------------------------------------------------
__fastcall TfrmMain::TfrmMain(TComponent* Owner)
        : TForm(Owner)
{
}


//---------------------------------------------------------------------------
void __fastcall TfrmMain::FormCreate(TObject *Sender)
{
        PageControl->Pages[0]->Caption="Equazioni";
        PageControl->Pages[1]->Caption="Matrici";
        PdX = new POLINOMIO_X();
        grbMatrixOp->Visible=false;
        grbMatrixCalc->Align=alClient;
}
//---------------------------------------------------------------------------
//A=[12/2, -4/3, 34; 1, 2, 3; 45, 7/2, 21, -2/7]|  B=[1,2,3,4;5,6,7,8;9,10,11,12]
//A=[12/2,-4/3,5,34 ;1,2,3,5;45,7/2,21,-2/7]
void __fastcall TfrmMain::cmdInsertMatrixClick(TObject *Sender)
{
        memTextMatrix->Clear();
        AnsiString TrueFormat;
        TrueFormat=RemovingSpaces(txtMatrix->Text);
        if(TrueFormat.Pos("|")>0)
        {
                AnsiString strParam[2];
                strParam[0]=TrueFormat.SubString(1,TrueFormat.Pos("|")-1);
                strParam[1]=TrueFormat.SubString(TrueFormat.Pos("|")+1,TrueFormat.Length()-TrueFormat.Pos("|"));
                int c,l;
                getColLinesM(&c,&l,strParam[0].c_str());
                A.SetMatrix(l,c);
                A.NameM=strParam[0].SubString(0,strParam[0].Pos("=")-1);
                getElementsM(strParam[0].SubString(strParam[0].Pos("[")+1,strParam[0].Pos("]")).c_str(),&A);
                getColLinesM(&c,&l,strParam[1].c_str());
                B.SetMatrix(l,c);
                B.NameM=strParam[1].SubString(0,strParam[1].Pos("=")-1);
                getElementsM(strParam[1].SubString(strParam[1].Pos("[")+1,strParam[1].Pos("]")).c_str(),&B);
                PrintMatrix(&A);
                PrintMatrix(&B);
                cmdSum->Caption="Calcola "+A.NameM+"+"+B.NameM;
                cmdMul->Caption="Calcola "+A.NameM+"X"+B.NameM;
                cmdDif->Caption="Calcola "+A.NameM+"-"+B.NameM;
                cmdMul2->Caption="Calcola "+B.NameM+"X"+A.NameM;
                cmdDif2->Caption="Calcola "+B.NameM+"-"+A.NameM;
                grbMatrixOp->Visible=true;
                grbMatrixCalc->Align=alLeft;
                grbMatrixCalc->Width=frmMain->Width/2; 
        }
        else
        {
                int c,l;
                getColLinesM(&c,&l,TrueFormat.c_str());
                A.SetMatrix(l,c);
                A.NameM=TrueFormat.SubString(0,TrueFormat.Pos("=")-1);
                getElementsM(TrueFormat.SubString(TrueFormat.Pos("[")+1,TrueFormat.Pos("]")).c_str(),&A);
                PrintMatrix(&A);
                grbMatrixOp->Visible=false;
                grbMatrixCalc->Align=alClient;
        }
}
//---------------------------------------------------------------------------


void __fastcall TfrmMain::SpeedButton1Click(TObject *Sender)
{
        memTextMatrix->Clear();       
}
//---------------------------------------------------------------------------


void __fastcall TfrmMain::cmdSolutionClick(TObject *Sender)
{
        int NumeroSoluzioni;
        POLINOMIO_X **scomp;
        float *VectSol;
        if((VectSol=getSolutions(NumeroSoluzioni, PdX))!=NULL)
        {
                scomp=getScomposition(NumeroSoluzioni,VectSol);
                AnsiString result;
                rtfViewEq->Lines->Add("");
                result="SOLUZIONI:  ";
                for(int i=0; i<NumeroSoluzioni;i++)
                        result.cat_printf("  x%d = %.3f",(i+1),VectSol[i]);
                rtfViewEq->Lines->Add(result);
                result="";
                for(int i=0; i<NumeroSoluzioni;i++)
                        result+="("+scomp[i]->toString()+")";
                rtfViewEq->Lines->Add("SCOMPOSIZIONE:  "+result);
        }
        else
                MessageBox(frmMain->Handle,"l'equazione inserita non presenta soluzioni","Attenzione",MB_ICONEXCLAMATION+MB_OK);

}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cmdDetClick(TObject *Sender)
{
        if(A.nc==A.nl)
        {
                memTextMatrix->Lines->Add("DET "+A.NameM+" = "+A.DET().toString());
                memTextMatrix->Lines->Add("");
                memTextMatrix->Lines->Add("");
        }
        else
                MessageBox(frmMain->Handle,"Impossibile calcolare il DETERMINANTE della matrice data","Errore",MB_ICONERROR+MB_OK);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cmdCompClick(TObject *Sender)
{
        if(A.nc==A.nl)
        {
                MATRICE CM(A.nl,A.nc);
                CM.NameM="COMP "+A.NameM;
                CM=A.COMP();
                PrintMatrix(&CM);
        }
        else
                MessageBox(frmMain->Handle,"Impossibile calcolare la matrice complementare della matrice data","Errore",MB_ICONERROR+MB_OK);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cmdInvClick(TObject *Sender)
{
        if(A.nc==A.nl)
        {
                if(A.DET()!=0)
                {
                        MATRICE MI(A.nl,A.nc);
                        MI.NameM="INV "+A.NameM;
                        MATRICE CM(A.nl,A.nc);
                        CM=A.COMP();
                        CM=CM.Trsposta();
                        MI=CM*(A.DET()^-1);
                        PrintMatrix(&MI);
                }

        }
        else
                MessageBox(frmMain->Handle,"Impossibile calcolare l'inversa della matrice data","Errore",MB_ICONERROR+MB_OK);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cmdTrasClick(TObject *Sender)
{
        MATRICE TR(A.Trsposta());
        TR.NameM="TRAS "+A.NameM;
        PrintMatrix(&TR);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cmdSumClick(TObject *Sender)
{
        if((A.nl==B.nl)&&(A.nc==B.nc))
        {
                MATRICE C(A.nl,A.nc);
                C=A+B;
                C.NameM=A.NameM+" + "+B.NameM;
                PrintMatrix(&C);
        }
        else
                MessageBox(frmMain->Handle,"Impossibile eseguire la somma","Attenzione",MB_ICONEXCLAMATION+MB_OK);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cmdMulClick(TObject *Sender)
{
        if((A.nc==B.nl))
        {
                MATRICE C(A.nl,B.nc);
                C=A*B;
                C.NameM=A.NameM+" X "+B.NameM;
                PrintMatrix(&C);
        }
        else
                MessageBox(frmMain->Handle,"Impossibile eseguire il prodotto","Attenzione",MB_ICONEXCLAMATION+MB_OK);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cmdMul2Click(TObject *Sender)
{
        if((B.nc==A.nl))
        {
                MATRICE C(B.nl,A.nc);
                C=B*A;
                C.NameM=B.NameM+" X "+A.NameM;
                PrintMatrix(&C);
        }
        else
                MessageBox(frmMain->Handle,"Impossibile eseguire il prodotto","Attenzione",MB_ICONEXCLAMATION+MB_OK);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cmdDifClick(TObject *Sender)
{
        if((A.nl==B.nl)&&(A.nc==B.nc))
        {
                MATRICE C(A.nl,A.nc);
                C=A-B;
                C.NameM=A.NameM+" - "+B.NameM;
                PrintMatrix(&C);
        }
        else
                MessageBox(frmMain->Handle,"Impossibile eseguire la differenza","Attenzione",MB_ICONEXCLAMATION+MB_OK);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cmdDif2Click(TObject *Sender)
{
        if((A.nl==B.nl)&&(A.nc==B.nc))
        {
                MATRICE C;
                C=B-A;
                C.NameM=B.NameM+" - "+A.NameM;
                PrintMatrix(&C);
        }
        else
                MessageBox(frmMain->Handle,"Impossibile eseguire la differenza","Attenzione",MB_ICONEXCLAMATION+MB_OK);
}
//---------------------------------------------------------------------------


void __fastcall TfrmMain::cmdSaveClick(TObject *Sender)
{
        SaveDialog->Execute();
        if(SaveDialog->FileName=="")
                return;
        int ret=SaveData(SaveDialog->FileName.c_str());
        switch(ret)
        {
                case 2:
                        cmdSaveClick(Sender);
                break;

                case 1:
                        MessageBox(frmMain->Handle,"Errore nella creazione del file","Errore",MB_ICONERROR+MB_OK);
                break;
        };
}
//---------------------------------------------------------------------------


void __fastcall TfrmMain::cmdInsertClick(TObject *Sender)
{
        GetPnX(txtEquazione->Text.c_str(), PdX);
        OrderPdX(PdX);
        rtfViewEq->Lines->Add(PdX->toString());
}
//---------------------------------------------------------------------------

