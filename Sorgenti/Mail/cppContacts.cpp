//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "cppContacts.h"
#include "cppMail.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TfrmContacts *frmContacts;
//---------------------------------------------------------------------------
__fastcall TfrmContacts::TfrmContacts(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------



void __fastcall TfrmContacts::cmdOkClick(TObject *Sender)
{
        if((txtN->Text!="")&&(txtA->Text!=""))
        {
                if(AddContact()==0)
                {
                        ViewList();
                        ViewCbo();
                        txtN->Text="";
                        txtA->Text="";
                }
        }
}
//---------------------------------------------------------------------------

void __fastcall TfrmContacts::FormCreate(TObject *Sender)
{
        txtN->Text="";
        txtA->Text="";
}
//---------------------------------------------------------------------------

