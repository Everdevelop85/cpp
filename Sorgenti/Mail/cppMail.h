//---------------------------------------------------------------------------

#ifndef cppMailH
#define cppMailH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <NMsmtp.hpp>
#include <Psock.hpp>
#include <ComCtrls.hpp>
#include <Dialogs.hpp>
//---------------------------------------------------------------------------
#include <conio.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <io.h>
#include <dir.h>
#include <sys\stat.h>

struct CONTACT
{
        int Index;
        char Name[100];
        char Adress[100];
};

int ViewList();
int ViewCbo();
int AddContact();

class TfrmMail : public TForm
{
__published:	// IDE-managed Components
        TNMSMTP *smtpHost;
        TMemo *txtText;
        TLabel *Label1;
        TComboBox *cboTo;
        TGroupBox *GroupBox1;
        TLabel *Label2;
        TEdit *txtName;
        TLabel *Label3;
        TEdit *txtAdress;
        TLabel *Label4;
        TEdit *txtDate;
        TLabel *Label5;
        TEdit *txtHour;
        TGroupBox *fraCommand;
        TButton *cmdSend;
        TButton *cmdNewContact;
        TButton *cmdGetContact;
        TButton *cmdExit;
        TButton *cmdSysTray;
        TStatusBar *stsBar;
        TEdit *txtSubject;
        TLabel *Label6;
        TButton *cmdAllegato;
        TOpenDialog *OpenDialog;
        void __fastcall cmdSendClick(TObject *Sender);
        void __fastcall smtpHostConnect(TObject *Sender);
        void __fastcall smtpHostConnectionFailed(TObject *Sender);
        void __fastcall smtpHostDisconnect(TObject *Sender);
        void __fastcall smtpHostFailure(TObject *Sender);
        void __fastcall smtpHostPacketSent(TObject *Sender);
        void __fastcall smtpHostSendStart(TObject *Sender);
        void __fastcall smtpHostSuccess(TObject *Sender);
        void __fastcall smtpHostAuthenticationFailed(bool &Handled);
        void __fastcall smtpHostConnectionRequired(bool &Handled);
        void __fastcall smtpHostInvalidHost(bool &Handled);
        void __fastcall smtpHostStatus(TComponent *Sender,
          AnsiString Status);
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall cmdAllegatoClick(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall cmdExitClick(TObject *Sender);
        void __fastcall cmdNewContactClick(TObject *Sender);
        void __fastcall smtpHostAttachmentNotFound(AnsiString Filename);
        void __fastcall smtpHostEncodeEnd(AnsiString Filename);
        void __fastcall smtpHostEncodeStart(AnsiString Filename);
        void __fastcall smtpHostHeaderIncomplete(bool &handled,
          int hiType);
        void __fastcall smtpHostHostResolved(TComponent *Sender);
        void __fastcall smtpHostMailListReturn(AnsiString MailAddress);
        void __fastcall smtpHostRecipientNotFound(AnsiString Recipient);
private:	// User declarations
public:		// User declarations

        char DBContacts[255];

        __fastcall TfrmMail(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfrmMail *frmMail;
//---------------------------------------------------------------------------
#endif
