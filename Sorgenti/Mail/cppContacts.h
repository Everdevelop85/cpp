//---------------------------------------------------------------------------

#ifndef cppContactsH
#define cppContactsH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Grids.hpp>
#include <ValEdit.hpp>
//---------------------------------------------------------------------------
class TfrmContacts : public TForm
{
__published:	// IDE-managed Components
        TLabel *Label1;
        TEdit *txtN;
        TLabel *Label2;
        TEdit *txtA;
        TButton *cmdOk;
        TListBox *lstAdress;
        void __fastcall cmdOkClick(TObject *Sender);
        void __fastcall FormCreate(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TfrmContacts(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfrmContacts *frmContacts;
//---------------------------------------------------------------------------
#endif
