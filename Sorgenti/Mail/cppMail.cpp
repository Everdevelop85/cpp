//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "cppMail.h"
#include "cppContacts.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TfrmMail *frmMail;
AnsiString attachement=NULL;
//---------------------------------------------------------------------------
__fastcall TfrmMail::TfrmMail(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------

//============================================================================
//FUNZIONI
//============================================================================
int ViewList()
{
        FILE *f;
        CONTACT dato;
        char app[200];
        if((f=fopen(frmMail->DBContacts,"rb"))==NULL)
                return 1;
        frmContacts->lstAdress->Clear();
        while((fread(&dato,sizeof(CONTACT),1,f))>0)
        {
                strcpy(app,dato.Name);
                strcat(app,"     ");
                strcat(app,dato.Adress);
                strcat(app,"     ");
                itoa(dato.Index,&(app[strlen(app)-1]),10);
                frmContacts->lstAdress->Items->Add(app);
        }
        fclose(f);
        return 0;
}

int ViewCbo()
{
        FILE *f;
        CONTACT dato;
        if((f=fopen(frmMail->DBContacts,"rb"))==NULL)
                return 1;
        frmMail->cboTo->Clear();
        while((fread(&dato,sizeof(CONTACT),1,f))>0)
                frmMail->cboTo->Items->Add(dato.Adress);
        frmMail->cboTo->Text=frmMail->cboTo->Items->Strings[0];
        fclose(f);
        return 0;
}

int AddContact()
{
        FILE *f;
        CONTACT dato, d;
        if((access(frmMail->DBContacts,0))!=0)
        {
                if((f=fopen(frmMail->DBContacts,"wb"))==NULL)
                        return 1;
                dato.Index=1;
        }
        else
        {
                if((f=fopen(frmMail->DBContacts,"r+"))==NULL)
                        return 1;
                fseek(f,0,SEEK_END);
                fseek(f,(ftell(f)-sizeof(CONTACT)),SEEK_SET);
                fread(&d,sizeof(CONTACT),1,f);
                dato.Index=d.Index+1;
                fseek(f,0,SEEK_END);
        }
        strcpy(dato.Name,frmContacts->txtN->Text.c_str());
        strcpy(dato.Adress,frmContacts->txtA->Text.c_str());
        fwrite(&dato,sizeof(CONTACT),1,f);
        fclose(f);
        return 0;
}

/*AnsiString getAdress(char *element)
{
        FILE *f;
        CONTACT dato;
        AnsiString app;
        app="";
        if((f=fopen(frmMail->DBContacts,"rb"))==NULL)
                return 1;
        while((fread(&dato,sizeof(CONTACT),1,f))>0)
        {

                if((strstr(element,dato.Adress))!=NULL)
                        break;
        }
        app=dato.Adress;
        fclose(f);
        return app;
}*/

//============================================================================
//============================================================================

void __fastcall TfrmMail::cmdSendClick(TObject *Sender)
{
        smtpHost->PostMessageA->FromAddress=txtAdress->Text;
        smtpHost->PostMessageA->FromName=txtName->Text;
        smtpHost->PostMessageA->ToAddress->Add(cboTo->Text);
        if(attachement!="0")
                smtpHost->PostMessageA->Attachments->Add(attachement);
        smtpHost->PostMessageA->Body->AddStrings(txtText->Lines);
        smtpHost->PostMessageA->Subject=txtSubject->Text;
        smtpHost->SendMail();

}
//---------------------------------------------------------------------------
void __fastcall TfrmMail::smtpHostConnect(TObject *Sender)
{
        stsBar->Panels->Items[0]->Text="Connesso";
}
//---------------------------------------------------------------------------
void __fastcall TfrmMail::smtpHostConnectionFailed(TObject *Sender)
{
        stsBar->Panels->Items[0]->Text="Connessione fallita";
}
//---------------------------------------------------------------------------
void __fastcall TfrmMail::smtpHostDisconnect(TObject *Sender)
{
        stsBar->Panels->Items[0]->Text="Disconnesso";
}
//---------------------------------------------------------------------------
void __fastcall TfrmMail::smtpHostFailure(TObject *Sender)
{
        stsBar->Panels->Items[0]->Text="Errore";
}
//---------------------------------------------------------------------------
void __fastcall TfrmMail::smtpHostPacketSent(TObject *Sender)
{
        stsBar->Panels->Items[0]->Text="Invio pacchetto";
}
//---------------------------------------------------------------------------
void __fastcall TfrmMail::smtpHostSendStart(TObject *Sender)
{
        stsBar->Panels->Items[0]->Text="Invio in corso...";
}
//---------------------------------------------------------------------------
void __fastcall TfrmMail::smtpHostSuccess(TObject *Sender)
{
        stsBar->Panels->Items[0]->Text="Operazione riuscita";
}
//---------------------------------------------------------------------------
void __fastcall TfrmMail::smtpHostAuthenticationFailed(bool &Handled)
{
        stsBar->Panels->Items[0]->Text="Autenticazione fallita";
}
//---------------------------------------------------------------------------
void __fastcall TfrmMail::smtpHostConnectionRequired(bool &Handled)
{
        stsBar->Panels->Items[0]->Text="Richiesta connessione";        
}
//---------------------------------------------------------------------------
void __fastcall TfrmMail::smtpHostInvalidHost(bool &Handled)
{
        stsBar->Panels->Items[0]->Text="Host non valido";        
}
//---------------------------------------------------------------------------
void __fastcall TfrmMail::smtpHostStatus(TComponent *Sender,
      AnsiString Status)
{
        stsBar->Panels->Items[0]->Text=Status;        
}
//---------------------------------------------------------------------------
void __fastcall TfrmMail::FormCreate(TObject *Sender)
{
        smtpHost->Host="out.alice.it";
        smtpHost->Connect();
        txtDate->Text=Date();
        txtHour->Text=Time();
        getcwd(DBContacts,255);
        strcat(DBContacts,"\\DBContacts.dat");
        ViewCbo();
}
//---------------------------------------------------------------------------
void __fastcall TfrmMail::cmdAllegatoClick(TObject *Sender)
{
        OpenDialog->Execute();
        attachement=OpenDialog->FileName;
}
//---------------------------------------------------------------------------

void __fastcall TfrmMail::FormClose(TObject *Sender, TCloseAction &Action)
{
        if(smtpHost->Connected)
                smtpHost->Disconnect();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMail::cmdExitClick(TObject *Sender)
{
        frmMail->Close();        
}
//---------------------------------------------------------------------------

void __fastcall TfrmMail::cmdNewContactClick(TObject *Sender)
{
        ViewList();
        frmContacts->Show();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMail::smtpHostAttachmentNotFound(AnsiString Filename)
{
        stsBar->Panels->Items[0]->Text="File da allegare non trovato";
}
//---------------------------------------------------------------------------

void __fastcall TfrmMail::smtpHostEncodeEnd(AnsiString Filename)
{
        stsBar->Panels->Items[0]->Text="Fine codifica";        
}
//---------------------------------------------------------------------------

void __fastcall TfrmMail::smtpHostEncodeStart(AnsiString Filename)
{
        stsBar->Panels->Items[0]->Text="Inizio codifica";        
}
//---------------------------------------------------------------------------

void __fastcall TfrmMail::smtpHostHeaderIncomplete(bool &handled,
      int hiType)
{
        stsBar->Panels->Items[0]->Text="Header di messagio incompleto";        
}
//---------------------------------------------------------------------------

void __fastcall TfrmMail::smtpHostHostResolved(TComponent *Sender)
{
        stsBar->Panels->Items[0]->Text="Impossibile connettersi";
}
//---------------------------------------------------------------------------

void __fastcall TfrmMail::smtpHostMailListReturn(AnsiString MailAddress)
{
        stsBar->Panels->Items[0]->Text="MailList Return";
}
//---------------------------------------------------------------------------

void __fastcall TfrmMail::smtpHostRecipientNotFound(AnsiString Recipient)
{
        stsBar->Panels->Items[0]->Text="Recipient Not Found";
}
//---------------------------------------------------------------------------






