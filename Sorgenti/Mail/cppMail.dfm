object frmMail: TfrmMail
  Left = 381
  Top = 162
  Width = 569
  Height = 476
  Caption = 'frmMail'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 232
    Width = 10
    Height = 13
    Caption = 'A:'
  end
  object txtText: TMemo
    Left = 8
    Top = 8
    Width = 401
    Height = 217
    TabOrder = 0
  end
  object cboTo: TComboBox
    Left = 24
    Top = 232
    Width = 385
    Height = 21
    ItemHeight = 13
    TabOrder = 1
  end
  object GroupBox1: TGroupBox
    Left = 8
    Top = 264
    Width = 401
    Height = 161
    Caption = 'Informazioni'
    TabOrder = 2
    object Label2: TLabel
      Left = 8
      Top = 24
      Width = 28
      Height = 13
      Caption = 'Nome'
    end
    object Label3: TLabel
      Left = 8
      Top = 48
      Width = 41
      Height = 13
      Caption = 'Indirizzo:'
    end
    object Label4: TLabel
      Left = 8
      Top = 104
      Width = 26
      Height = 13
      Caption = 'Data:'
    end
    object Label5: TLabel
      Left = 8
      Top = 128
      Width = 20
      Height = 13
      Caption = 'Ora:'
    end
    object Label6: TLabel
      Left = 8
      Top = 72
      Width = 41
      Height = 13
      Caption = 'Oggetto:'
    end
    object txtName: TEdit
      Left = 56
      Top = 24
      Width = 337
      Height = 21
      TabOrder = 0
    end
    object txtAdress: TEdit
      Left = 56
      Top = 48
      Width = 337
      Height = 21
      TabOrder = 1
    end
    object txtDate: TEdit
      Left = 56
      Top = 104
      Width = 161
      Height = 21
      TabOrder = 2
    end
    object txtHour: TEdit
      Left = 56
      Top = 128
      Width = 161
      Height = 21
      TabOrder = 3
    end
    object txtSubject: TEdit
      Left = 56
      Top = 72
      Width = 337
      Height = 21
      TabOrder = 4
    end
  end
  object fraCommand: TGroupBox
    Left = 416
    Top = 8
    Width = 137
    Height = 417
    Caption = 'Comandi'
    TabOrder = 3
    object cmdSend: TButton
      Left = 8
      Top = 64
      Width = 121
      Height = 25
      Caption = 'Invia'
      TabOrder = 0
      OnClick = cmdSendClick
    end
    object cmdNewContact: TButton
      Left = 8
      Top = 224
      Width = 121
      Height = 25
      Caption = 'Nuovo Contatto'
      TabOrder = 1
      OnClick = cmdNewContactClick
    end
    object cmdGetContact: TButton
      Left = 8
      Top = 256
      Width = 121
      Height = 25
      Caption = 'Invia a contatto...'
      TabOrder = 2
    end
    object cmdExit: TButton
      Left = 8
      Top = 160
      Width = 121
      Height = 25
      Caption = 'Esci'
      TabOrder = 3
      OnClick = cmdExitClick
    end
    object cmdSysTray: TButton
      Left = 8
      Top = 128
      Width = 121
      Height = 25
      Caption = 'Background'
      TabOrder = 4
    end
    object cmdAllegato: TButton
      Left = 8
      Top = 32
      Width = 121
      Height = 25
      Caption = 'Allegato'
      TabOrder = 5
      OnClick = cmdAllegatoClick
    end
  end
  object stsBar: TStatusBar
    Left = 0
    Top = 432
    Width = 561
    Height = 17
    Panels = <
      item
        Width = 100
      end>
    SimplePanel = False
  end
  object smtpHost: TNMSMTP
    Port = 25
    ReportLevel = 0
    OnDisconnect = smtpHostDisconnect
    OnConnect = smtpHostConnect
    OnInvalidHost = smtpHostInvalidHost
    OnHostResolved = smtpHostHostResolved
    OnStatus = smtpHostStatus
    OnConnectionFailed = smtpHostConnectionFailed
    OnPacketSent = smtpHostPacketSent
    OnConnectionRequired = smtpHostConnectionRequired
    UserID = '1'
    EncodeType = uuMime
    ClearParams = False
    SubType = mtPlain
    Charset = 'us-ascii'
    OnRecipientNotFound = smtpHostRecipientNotFound
    OnHeaderIncomplete = smtpHostHeaderIncomplete
    OnSendStart = smtpHostSendStart
    OnSuccess = smtpHostSuccess
    OnFailure = smtpHostFailure
    OnEncodeStart = smtpHostEncodeStart
    OnEncodeEnd = smtpHostEncodeEnd
    OnMailListReturn = smtpHostMailListReturn
    OnAttachmentNotFound = smtpHostAttachmentNotFound
    OnAuthenticationFailed = smtpHostAuthenticationFailed
    Left = 328
  end
  object OpenDialog: TOpenDialog
    Filter = 'Tutti i file|*.*'
    Title = 'Apri il file da inviare'
    Left = 296
  end
end
