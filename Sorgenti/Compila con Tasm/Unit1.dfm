object frmMain: TfrmMain
  Left = 285
  Top = 227
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'Compile assembly'
  ClientHeight = 322
  ClientWidth = 560
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 273
    Height = 281
    Caption = 'Files'
    TabOrder = 0
    object lstDir: TDirectoryListBox
      Left = 8
      Top = 41
      Width = 257
      Height = 136
      ItemHeight = 16
      TabOrder = 0
      OnChange = lstDirChange
    end
    object lstFile: TFileListBox
      Left = 8
      Top = 176
      Width = 257
      Height = 97
      ItemHeight = 13
      Mask = '*.asm'
      TabOrder = 1
      OnChange = lstFileChange
      OnDblClick = lstFileDblClick
    end
    object cboDrive: TDriveComboBox
      Left = 8
      Top = 16
      Width = 257
      Height = 19
      TabOrder = 2
      OnChange = cboDriveChange
    end
  end
  object GroupBox2: TGroupBox
    Left = 280
    Top = 0
    Width = 273
    Height = 281
    Caption = 'Compiles Informations'
    TabOrder = 1
    object memInfoComp: TMemo
      Left = 8
      Top = 16
      Width = 257
      Height = 257
      ReadOnly = True
      ScrollBars = ssVertical
      TabOrder = 0
    end
  end
  object cmdCompyle: TButton
    Left = 0
    Top = 288
    Width = 137
    Height = 25
    Caption = 'Compyle'
    TabOrder = 2
    OnClick = cmdCompyleClick
  end
  object rdbOpt1: TRadioButton
    Left = 144
    Top = 288
    Width = 97
    Height = 25
    Caption = 'Create EXE file'
    TabOrder = 3
    OnClick = rdbOpt1Click
  end
  object rdbOpt2: TRadioButton
    Left = 248
    Top = 288
    Width = 97
    Height = 25
    Caption = 'Create COM file'
    Checked = True
    TabOrder = 4
    TabStop = True
    OnClick = rdbOpt2Click
  end
end
