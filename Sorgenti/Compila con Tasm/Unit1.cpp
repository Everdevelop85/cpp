//---------------------------------------------------------------------------

#include <vcl.h>
#include <conio.h>
#include <stdlib.h>
#include <stdio.h>
#include <dir.h>
#include <io.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TfrmMain *frmMain;
char FilePath[30], FileASM[255], FileOBJ[255], FileEXEC[255];
//---------------------------------------------------------------------------
__fastcall TfrmMain::TfrmMain(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------


int SaveDirOnFile(char *fn, char *dir)
{
        FILE *f;
        if((f=fopen(fn,"w+"))==NULL)
                return -1;
        fwrite(dir,strlen(dir),1,f);
        fclose(f);
        return 0;
}

AnsiString OpenDirFromFile(char *fn)
{
        FILE *f;
        AnsiString app="";
        char buffer[1024];
        int nread;
        if((f=fopen(fn,"rt"))==NULL)
                return app;
        nread=fread(buffer,sizeof(char),1024,f);
        buffer[nread]='\0';
        app=buffer;
        fclose(f);
        return app;
}

void CreateFileNames(char *fn)
{
        char app[255];
        strcpy(app,fn);
        strcpy(FileASM,fn);
        app[(strlen(fn)-4)]='\0';
        strcpy(FileOBJ,app);
        strcat(FileOBJ,".obj");
        strcpy(FileEXEC,app);
        if(frmMain->rdbOpt1->Checked)
                strcat(FileEXEC,".exe");
        else
                strcat(FileEXEC,".com");
}

int ceckErrors()
{
        FILE *f;
        char buffer[255];
        int i;
        bool ceck=false;
        if((f=fopen("errors.txt","rt"))==NULL)
                return -1;
        i=0;
        while((buffer[i]=(char)fgetc(f))!=EOF)
        {
                if((buffer[i]=='\r')||(buffer[i]=='\n')||(buffer[i]=='\0'))
                {
                        buffer[i]='\0';
                        if(strstr(buffer,"**Error**")!=NULL)
                        {
                                ceck=true;
                                break;
                        }
                        i=-1;
                }
                i++;
        }
        fclose(f);
        if(ceck)
                return -2;
        return 0;
}


int CreateOBJfile()
{
        chdir("C:\\assebler");
        char command[255];
        strcpy(command,"TASM ");
        strcat(command,FileASM);
        strcat(command," > errors.txt");
        system(command);
        while(!((access("errors.txt",0)==0)&&(access("errors.txt",1)==0)&&(access("errors.txt",4)==0)));;
        if(ceckErrors()<0)
                return -1;
        return 0;
}

int CreateExecutable()
{
        char command[255];
        if(frmMain->rdbOpt1->Checked)
                strcpy(command,"TLINK ");
        else
                strcpy(command,"TLINK /t ");
        strcat(command,FileOBJ);
        system(command);
        while(!((access(FileEXEC,0)==0)&&(access(FileEXEC,1)==0)&&(access(FileEXEC,4)==0)));;
        return 0;
}

class TExecutionThread : public TThread
{
        private:
                char command[255];
        public:
                bool IsActivated;
                TExecutionThread(): TThread(true)
                {
                        IsActivated=false;
                }

                int CreateBatFile()
                {
                        FILE *f;
                        if((f=fopen("C:\\Temp\\Execution.bat","wt"))==NULL)
                                return -1;
                        char command_line[255];
                        command_line[0]='"';
                        getcwd(&(command_line[1]),254);
                        strcat(command_line,"\\");
                        strcat(command_line,FileEXEC);
                        strcat(command_line,"\"\n\rPAUSE");
                        fwrite(command_line,strlen(command_line),1,f);
                        fclose(f);
                        return 0;
                }

                virtual void __fastcall Execute()
                {
                        if(CreateBatFile()==0)
                        {
                                 while(!(((access("C:\\Temp\\Execution.bat",0))==0)&&((access("C:\\Temp\\Execution.bat",1))==0)&&((access("C:\\Temp\\Execution.bat",6))==0)));;
                                //ShellExecute(frmMain->Handle,"open",FileEXEC,NULL,".",SW_RESTORE);
                                ShellExecute(frmMain->Handle,"open","C:\\Temp\\Execution.bat",NULL,NULL,SW_SHOW);
                                IsActivated=true;
                        }
                        else
                                IsActivated=false;
                }
}*TExec;



void __fastcall TfrmMain::rdbOpt2Click(TObject *Sender)
{
        rdbOpt1->Checked=false;
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::rdbOpt1Click(TObject *Sender)
{
        rdbOpt2->Checked=false;
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::lstDirChange(TObject *Sender)
{
        lstFile->Directory=lstDir->Directory;
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::FormCreate(TObject *Sender)
{
        strcpy(FilePath,"C:\\Temp\\FileInfo.txt");
        AnsiString app=OpenDirFromFile(FilePath);
        if(app!="")
        {
                lstDir->Directory=app;
                lstFile->Directory=app;
        }
        lstDir->Refresh();
        lstFile->Refresh();
        TExec = new TExecutionThread();
}
//---------------------------------------------------------------------------
void __fastcall TfrmMain::FormClose(TObject *Sender, TCloseAction &Action)
{
        if(SaveDirOnFile(FilePath,lstFile->Directory.c_str())!=0)
                exit(1);
        if(TExec->IsActivated)
                TExec->Terminate();
        TExec->Free();
        free(TExec);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cmdCompyleClick(TObject *Sender)
{
        AnsiString file;
        file=FileOBJ;
        memInfoComp->Lines->Add("Creating obj..."+file);
        if(CreateOBJfile()==0)
        {
                file=FileEXEC;
                memInfoComp->Lines->Add("Creating executable..."+file);
                if(CreateExecutable()==0)
                {
                        memInfoComp->Lines->Add("WorkFinisched");
                        if(TExec->IsActivated)
                                TExec->Terminate();
                        TExec->Execute();
                        //ShellExecute(frmMain->Handle,NULL,"TLINK.EXE",FileOBJ,".",0);
                }
        }
        else
                memInfoComp->Lines->Add("***Error***  on "+file);
        memInfoComp->Lines->Add("");
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::lstFileDblClick(TObject *Sender)
{
        ShellExecute(frmMain->Handle,"open","c:\\WINDOWS\\notepad.exe",FileASM,NULL,SW_SHOW);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::lstFileChange(TObject *Sender)
{
        if(lstFile->ItemIndex>=0)
                CreateFileNames(lstFile->Items->Strings[lstFile->ItemIndex].c_str());
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cboDriveChange(TObject *Sender)
{
        AnsiString app="";
        app.printf("%c",cboDrive->Drive);
        app=app+":\\";
        lstDir->Directory=app;
        lstDir->Refresh();
}
//---------------------------------------------------------------------------

