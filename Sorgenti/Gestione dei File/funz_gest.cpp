//---------------------------------------------------------------------------
#pragma hdrstop

#include "funz_gest.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

void naviga(char pathname[100], TFILE *pfile)
{
  char ast[100];
  TFILE *nuovo, *prec;
  int finito;


     strcpy(pathname, ast);
     strcat(ast, "\\*.*");
     finito=findfirst(ast, &pfile->ff, FA_DIREC);

         if(!finito)
         {
             nuovo=new(TFILE);
             strcpy(pathname, nuovo->ff.ff_name);
             nuovo->ff.ff_attrib=0x10;
             nuovo->sorella=NULL;
             nuovo->figlia=NULL;
             pfile->figlia=nuovo;
             prec=nuovo;
         }

            while(!finito)
            {
              if(nuovo->ff.ff_attrib==0x10)
              {
                strcat(pathname, "\\");
                strcat(pathname, nuovo->ff.ff_name);
                naviga(pathname, nuovo);
              }
             finito=findnext(&nuovo->ff);

                if(!finito)
                {
                   nuovo=new(TFILE);
                   strcpy(pathname, nuovo->ff.ff_name);
                   nuovo->ff.ff_attrib=0x10;
                   nuovo->sorella=NULL;
                   nuovo->figlia=NULL;
                   prec->sorella=nuovo;
                   prec=nuovo;
                }
            }

}

void visualizza(TFILE *pfile)
{


       if((pfile->sorella==NULL) && (pfile->figlia==NULL))

         return;

       else
       {
         chdir(pfile->ff.ff_name);
         puts(pfile->ff.ff_name);
         getch();
         visualizza(pfile);
       }
}         
