//---------------------------------------------------------------------------
#ifndef funz_gestH
#define funz_gestH
//---------------------------------------------------------------------------
#endif
#include <condefs.h>
#include <iostream.h>
#include <conio.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <dir.h>
#include <dos.h>

struct TFILE
{
  ffblk ff;
  TFILE *sorella, *figlia;
};

void naviga(char pathname[40], TFILE *pfile);
void visualizza(TFILE *pfile);
