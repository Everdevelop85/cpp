#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int menu()
{
    int m;
    printf("1. CONVERSIONE DA DECIMALE A BINARIO\n");
    printf("2. CONVERSIONE DA BINARIO A DECIMALE\n");
    printf("3. ESCI\n\n");
    printf("Digita il numero del menu e premi \"[INVIO]\"\n");
    printf("Numero --> ");
    scanf("%1d",&m);
    return m;
}

int main(void)
{
    int mn,i,num;
    do
    {
        system("cls");
        mn=menu();
        switch(mn)
        {
            case 1:
                    int vett[256],r;
                    system("cls");
                    printf("Inserisci il numero da convertire: ");
                    scanf("%d",&num);
                    r=num;
                    i=0;
                    do
                    {
                        vett[i]=r%2;
                        r/=2;
                        i++;
                    }
                    while(r>0);
                    system("cls");
                    printf("%d --> ",num);
                    for(int a=i-1;a>=0;a--)
                        printf("%d",vett[a]);
                    printf("\n\n");
                    system("pause");
            break;

            case 2:
                    char binary[256];
                    system("cls");
                    printf("Inserire il numero binario\n");
                    scanf("%s",binary);
                    i=0;
                    num=0;
                    while(binary[i]!='\0')
                        i++;
                    i--;
                    for(int a=i;a>=0;a--)
                    {
                        if(binary[a]=='1')
                                num+=(pow(2,i-a));
                    }
                    system("cls");
                    printf("%s --> %d",binary,num);
                    printf("\n\n");
                    system("pause");
            break;
        };
    }
    while(mn!=3);
    return 0;
}
