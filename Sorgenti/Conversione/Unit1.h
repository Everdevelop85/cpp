//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
#include <conio.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
class TfrmMain : public TForm
{
__published:	// IDE-managed Components
        TGroupBox *GroupBox1;
        TLabel *Label1;
        TLabel *Label2;
        TEdit *txtNDec;
        TEdit *txtNBin;
        TEdit *txtConvert;
        TCheckBox *chbCn2;
        TGroupBox *GroupBox2;
        TLabel *Label3;
        TEdit *txtCharacter;
        TStaticText *lblDecimal;
        TStaticText *lblBinary;
        TEdit *txtHex;
        void __fastcall txtNDecKeyPress(TObject *Sender, char &Key);
        void __fastcall txtNBinKeyPress(TObject *Sender, char &Key);
        void __fastcall txtCharacterKeyPress(TObject *Sender, char &Key);
private:	// User declarations
public:		// User declarations
        __fastcall TfrmMain(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfrmMain *frmMain;
//---------------------------------------------------------------------------
#endif
