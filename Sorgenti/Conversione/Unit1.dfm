object frmMain: TfrmMain
  Left = 195
  Top = 195
  Width = 466
  Height = 315
  Caption = 'Conversione'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 8
    Top = 8
    Width = 441
    Height = 121
    Caption = 'Conversione numeri'
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 24
      Width = 85
      Height = 13
      Caption = 'Numero decimale:'
    end
    object Label2: TLabel
      Left = 8
      Top = 48
      Width = 74
      Height = 13
      Caption = 'Numero binario:'
    end
    object txtNDec: TEdit
      Left = 96
      Top = 24
      Width = 337
      Height = 21
      TabOrder = 0
      OnKeyPress = txtNDecKeyPress
    end
    object txtNBin: TEdit
      Left = 96
      Top = 48
      Width = 337
      Height = 21
      TabOrder = 1
      OnKeyPress = txtNBinKeyPress
    end
    object txtConvert: TEdit
      Left = 8
      Top = 88
      Width = 305
      Height = 21
      TabOrder = 2
    end
    object chbCn2: TCheckBox
      Left = 320
      Top = 88
      Width = 113
      Height = 17
      Caption = 'Complemento a 2'
      TabOrder = 3
    end
  end
  object GroupBox2: TGroupBox
    Left = 8
    Top = 136
    Width = 441
    Height = 137
    Caption = 'Conversione caratteri'
    TabOrder = 1
    object Label3: TLabel
      Left = 8
      Top = 24
      Width = 46
      Height = 13
      Caption = 'Carattere:'
    end
    object txtCharacter: TEdit
      Left = 56
      Top = 24
      Width = 33
      Height = 21
      MaxLength = 1
      TabOrder = 0
      OnKeyPress = txtCharacterKeyPress
    end
    object lblDecimal: TStaticText
      Left = 8
      Top = 56
      Width = 425
      Height = 17
      AutoSize = False
      BevelKind = bkTile
      TabOrder = 1
    end
    object lblBinary: TStaticText
      Left = 8
      Top = 80
      Width = 425
      Height = 17
      AutoSize = False
      BevelKind = bkTile
      TabOrder = 2
    end
    object txtHex: TEdit
      Left = 8
      Top = 104
      Width = 425
      Height = 21
      TabOrder = 3
      OnKeyPress = txtNDecKeyPress
    end
  end
end
