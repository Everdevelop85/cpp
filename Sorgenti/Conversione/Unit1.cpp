//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TfrmMain *frmMain;
//---------------------------------------------------------------------------

int *DecToBin(int num, int *nC)
{
        int n=0, i, *vett;
        int app=num;
        while((app/2)>0)
        {
                n++;
                app=app/2;
        }
        n++;
        *nC=n;
        vett=(int *)calloc(n,sizeof(int));
        app=num;
        i=n-1;
        while((app/2)>0)
        {
                vett[i]=app%2;
                app=app/2;
                i--;
        }
        vett[i]=1;
        return vett;
}

int BinToDec(int v[], int n)
{
        int ret=0;
        for(int i=n-1;i>=0;i--)
                ret+=(v[i]*(pow(2,(n-i-1))));
        return ret;
}

int *VettRet(char *s)
{
        int *v, l=strlen(s);
        v=(int *)calloc(l,sizeof(int));
        for(int i=0;i<l;i++)
                v[i]=(int)s[i]-48;
        return v;
}

int *DecToBinC2(int num, int *nC)
{
        int *vett, n, app;
        vett=DecToBin(num,&n);
        for(int i=0;i<n;i++)
        {
                if(vett[i]==0)
                        vett[i]=1;
                else
                        vett[i]=0;
        }
        app=BinToDec(vett,n);
        app+=1;
        free(vett);
        vett=DecToBin(app,&n);
        *nC=n;
        return vett;
}

int BinC2ToDec(int v[], int nc)
{
        int *vett, *appV, n, app;
        app=BinToDec(v,nc);
        app-=1;
        vett=(int *)calloc(nc,sizeof(int));
        for(int i=0;i<nc;i++)
                vett[i]=0;
        appV=DecToBin(app,&n);
        app=nc;
        for(int i=n-1;i>=0;i--)
        {
                vett[app-1]=appV[n];
                app--;
        }
        for(int i=0;i<nc;i++)
        {
                if(vett[i]==0)
                        vett[i]=1;
                else
                        vett[i]=0;
        }
        free(appV);
        return BinToDec(vett,nc);
}

AnsiString BinToHex(int bin[], int n)
{
        AnsiString app;
        char HexNum[10];
        int buff[4],ind, indH=0, newBin[16];
        ind=15;
        for(int i=n-1;i>=0;i--)
        {
                newBin[ind]=bin[i];
                ind--;
        }
        for(int i=0;i<=ind;i++)
                newBin[i]=0;
        ind=0;
        for(int i=0;i<16;i++)
        {
                buff[ind]=newBin[i];
                if(ind==3)
                {
                        int app=BinToDec(buff,4);
                        if((app>=10)&&(app<=15))
                                HexNum[indH]=(char)(app+55);
                        else
                                HexNum[indH]=(char)(app+48);
                        indH++;
                        ind=-1;
                }
                ind++;
        }
        HexNum[indH]='\0';
        app=HexNum;
        return app;

}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
__fastcall TfrmMain::TfrmMain(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::txtNDecKeyPress(TObject *Sender, char &Key)
{
        if(Key==VK_RETURN)
        {
                int *Bin, nC;
                char *st;
                if(chbCn2->Checked)
                        Bin=DecToBinC2(txtNDec->Text.ToInt(),&nC);
                else
                        Bin=DecToBin(txtNDec->Text.ToInt(),&nC);
                st=(char *)calloc((nC+1),sizeof(char));
                for(int i=0;i<nC;i++)
                st[i]=(char)Bin[i]+48;
                st[nC]='\0';
                txtConvert->Text=st;
                free(st);
                free(Bin);
        }
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::txtNBinKeyPress(TObject *Sender, char &Key)
{
        if(Key==VK_RETURN)
        {
                int *Bin, nC;
                nC=txtNBin->Text.Length();
                Bin=VettRet(txtNBin->Text.c_str());
                if(chbCn2->Checked)
                        txtConvert->Text=IntToStr(BinC2ToDec(Bin,nC));
                else
                        txtConvert->Text=IntToStr(BinToDec(Bin,nC));
                txtHex->Text=BinToHex(Bin,nC)+"h";
                free(Bin);
        }
}
//---------------------------------------------------------------------------


void __fastcall TfrmMain::txtCharacterKeyPress(TObject *Sender, char &Key)
{
        int *Bin, nC;
        char *st;
        lblDecimal->Caption=(int)Key;
        Bin=DecToBin(lblDecimal->Caption.ToInt(),&nC);
        st=(char *)calloc((nC+1),sizeof(char));
        for(int i=0;i<nC;i++)
                st[i]=(char)Bin[i]+48;
        st[nC]='\0';
        lblBinary->Caption=st;
        free(st);
        txtHex->Text=BinToHex(Bin,nC)+"h";
        free(Bin);
}
//---------------------------------------------------------------------------



