#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

class FRACTAL
{
        public:
                int num;
                int den;

//COSTRUCTOR
        FRACTAL(void)
        {
                num=0;
                den=1;
        }

        FRACTAL(const FRACTAL &fr)
        {
                num=fr.num;
                den=fr.den;
                Semplifica();
        }

        FRACTAL(int n, int d)
        {
                if(den<0)
                {
                        num*=-1;
                        den=abs(den);
                }
                Semplifica();
        }
        FRACTAL(char *fr)
        {
                int i=0,l;
                char n[5],d[5];
                if((strchr(fr,'/'))==NULL)
                {
                        num=atoi(fr);
                        den=1;
                        return;
                }                
                while(fr[i]!='/')
                {
                        n[i]=fr[i];
                        i++;
                }
                n[i]='\0';
                i++;
                l=i;
                while(fr[i]!='\0')
                {
                        d[i-l]=fr[i];
                        i++;
                }
                d[i-l]='\0';
                num=atoi(n);
                if(atoi(d)<0)
                {
                        num*=-1;
                        den=abs(atoi(d));
                }
                else
                        den=atoi(d);
                Semplifica();
        }

//METHODS
        public: void Semplifica()
        {
                int *n,divisor,sign=1;
                if(num>den)
                        n=&den;
                else
                        n=&num;
                if(*n<0)
                {
                        sign=-1;
                        *n*=-1;
                }
                for(divisor=*n;divisor>1;divisor--)
                {
                        while((((den%divisor)==0)&&((num%divisor)==0))&&(divisor>=2))
                        {
                                den/=divisor;
                                num/=divisor;
                                divisor=*n;
                        }
                }
                num*=sign;
        }

        public: void Print()
        {
                if((den==1)||(num==0))
                        printf("%d",num);
                else
                        printf("%d/%d",num,den);
        }

//OPERATOR SAME TYPE
        void operator=(const FRACTAL &fr)
        {
                num=fr.num;
                den=fr.den;
        }

        void operator=(const int &n)
        {
                num=n;
                den=1;
        }

        void operator=(const char *fr)
        {
                int i=0,l;
                char n[5],d[5];
                if((strchr(fr,'/'))==NULL)
                {
                        num=atoi(fr);
                        den=1;
                        return;
                }
                else
                while(fr[i]!='/')
                {
                        n[i]=fr[i];
                        i++;
                }
                n[i]='\0';
                i++;
                l=i;
                while(fr[i]!='\0')
                {
                        d[i-l]=fr[i];
                        i++;
                }
                d[i-l]='\0';
                num=atoi(n);
                if(atoi(d)<0)
                {
                        num*=-1;
                        den=abs(atoi(d));
                }
                else
                        den=atoi(d);
                Semplifica();
        }

        friend FRACTAL operator+(const FRACTAL &f1, const FRACTAL &f2)
        {
                FRACTAL ris;
                ris.den=f1.den*f2.den;
                ris.num=((ris.den/f1.den)*f1.num)+((ris.den/f2.den)*f2.num);
                ris.Semplifica();
                return ris;
        }

        friend FRACTAL operator+=(const FRACTAL &f1, const FRACTAL &f2)
        {
                FRACTAL ris;
                ris.den=f1.den*f2.den;
                ris.num=((ris.den/f1.den)*f1.num)+((ris.den/f2.den)*f2.num);
                ris.Semplifica();
                return ris;
        }

        friend FRACTAL operator-(const FRACTAL &f1, const FRACTAL &f2)
        {
                FRACTAL ris;
                ris.den=f1.num*f2.den;
                ris.num=(((ris.den/f1.den)*f1.num)-((ris.den/f2.den)*f2.num));
                ris.Semplifica();
                return ris;
        };

        friend FRACTAL operator*(const FRACTAL &f1, const FRACTAL &f2)
        {
                FRACTAL ris;
                ris.num=f1.num*f2.num;
                ris.den=f1.den*f2.den;
                ris.Semplifica();
                return ris;
        };

        friend FRACTAL operator/(const FRACTAL &f1, const FRACTAL &f2)
        {
                FRACTAL ris;
                ris.num=f1.num*f2.den;
                ris.den=f1.den*f2.num;
                ris.Semplifica();
                return ris;
        };

        bool operator!=(const FRACTAL &fr)
        {
                if((num!=fr.num)&&((den!=fr.den)))
                        return true;
                return false;
        };

//OPERTORS AND CONSTANT
        bool operator!=(const int &n)
        {
                if(den!=1)
                        return true;
                else
                {
                        if(num!=n)
                                return true;
                        return false;
                }
        };

        friend FRACTAL operator^(const FRACTAL &f1, const int &n)
        {
                FRACTAL ris(1,1),Rfr(f1);
                int app,exp=n;
                if(exp<0)
                {
                        app=Rfr.den;
                        Rfr.den=Rfr.num;
                        Rfr.num=app;
                        exp=abs(exp);
                }
                if(exp>0)
                {
                        ris.num=pow(Rfr.num,exp);
                        ris.den=pow(Rfr.den,exp);
                        ris.Semplifica();
                }
                return ris;
        };

        friend FRACTAL operator+(const FRACTAL &f1, const int &n)
        {
                FRACTAL ris;
                ris.den=f1.den;
                ris.num=(ris.den*n)+f1.num;
                ris.Semplifica();
                return ris;
        }

        friend FRACTAL operator-(const FRACTAL &f1, const int &n)
        {
                FRACTAL ris;
                ris.den=f1.den;
                ris.num=(f1.num-(f1.den*n));
                ris.Semplifica();
                return ris;
        };

        friend FRACTAL operator*(const FRACTAL &f1, const int &n)
        {
                FRACTAL ris;
                ris.num=f1.num*n;
                ris.den=f1.den;
                ris.Semplifica();
                return ris;
        };

        friend FRACTAL operator/(const FRACTAL &f1, const int &n)
        {
                FRACTAL ris;
                ris.num=f1.num;
                ris.den=f1.den*n;
                ris.Semplifica();
                return ris;
        };
};

class MATRICE
{

        public: int nl, nc;
                FRACTAL **matrix;

        MATRICE(int m, int n)
        {
                int i,j;
                nl=m;
                nc=n;
                matrix=(FRACTAL **)malloc(sizeof(FRACTAL)*nl);
                for(i=0;i<nl;i++)
                        matrix[i]=(FRACTAL *)malloc(sizeof(FRACTAL)*nc);
                for(i=0;i<nl;i++)
                {
                        for(j=0;j<nc;j++)
                                matrix[i][j]=0;
                }
        };
        MATRICE(char *d)
        {
                char num[6];
                int i=0,l,j;
                while((d[i]!='x')&&(d[i]!='X'))
                {
                        num[i]=d[i];
                        i++;
                }
                num[i]='\0';
                nl=atoi(num);
                l=++i;
                while(d[i]!='\0')
                {
                        num[i-l]=d[i];
                        i++;
                }
                num[i]='\0';
                nc=atoi(num);
                matrix=(FRACTAL **)malloc(sizeof(FRACTAL)*nl);
                for(i=0;i<nl;i++)
                        matrix[i]=(FRACTAL *)malloc(sizeof(FRACTAL)*nc);
                for(i=0;i<nl;i++)
                {
                        for(j=0;j<nc;j++)
                                matrix[i][j]=0;
                }
        }

        public: void Print()
        {
                int i,j,n;
                unsigned int zero;
                for(i=0;i<nl;i++)
                {
                        for(j=0;j<nc;j++)
                        {
                                printf("\t");
                                matrix[i][j].Print();
                        }
                        printf("\n\n");
                }
        };

        public: MATRICE Minor(int l, int c)
        {
                MATRICE M(nl-1,nc-1);
                int i,j,im=-1,jm=-1;
                for(i=0;i<nl;i++)
                {
                        if(i!=l)
                        {
                                im++;
                                for(j=0;j<nc;j++)
                                {
                                        if(j!=c)
                                        {
                                                jm++;
                                                M.matrix[im][jm]=matrix[i][j];
                                        }

                                }
                                jm=-1;
                        }
                }
                return M;
        };

        public: FRACTAL DET()
        {
                int i,j,exp;
                FRACTAL det,app;
                MATRICE M(nl-1,nc-1);
                switch(nl)
                {
                        case 2:
                                det=((matrix[0][1]*matrix[1][0])*(-1))+(matrix[0][0]*matrix[1][1]);
                        break;
                        case 3:
                                det=((matrix[0][2]*matrix[1][1]*matrix[2][0])+(matrix[0][0]*matrix[1][2]*matrix[2][1])+(matrix[0][1]*matrix[1][0]*matrix[2][2]))*(-1);
                                det=det+((matrix[0][0]*matrix[1][1]*matrix[2][2])+(matrix[0][1]*matrix[1][2]*matrix[2][0])+(matrix[0][2]*matrix[1][0]*matrix[2][1]));
                        break;

                        default:
                                for(j=0;j<nc;j++)
                                {
                                        if(matrix[0][j]!=0)
                                        {
                                                M=Minor(0,j);
                                                det=det+(matrix[0][j]*(M.DET()*(pow(-1,(1+(j+1))))));
                                        }
                                }
                        break;
                };
                return det;
        }

        public: MATRICE COMP()
        {
                MATRICE M(nl-1,nc-1);
                MATRICE CM(nl,nc);
                int i,j;
                for(i=0;i<nl;i++)
                {
                        for(j=0;j<nc;j++)
                        {
                                M=this->Minor(i,j);
                                CM.matrix[i][j]=M.DET()*(pow(-1,((i+1)+(j+1))));
                        }
                }
                return CM;
        };

        void operator=(const MATRICE &M)
        {
                int i,j;
                for(i=0;i<nl;i++)
                {
                        for(j=0;j<nc;j++)
                                matrix[i][j]=M.matrix[i][j];
                }
        };

        MATRICE operator*(const MATRICE &M)
        {
                MATRICE ris(nl,M.nc);
                int k,i,j;
                FRACTAL app1,app2;
                for(i=0;i<ris.nl;i++)
                        for(j=0;j<ris.nc;j++)
                        {
                                ris.matrix[i][j]=0;
                                for(k=0;k<nc;k++)
                                {
                                        app1=matrix[i][k]*M.matrix[k][j];
                                        ris.matrix[i][j]=ris.matrix[i][j]+app1;
                                }
                };
                return ris;
        };

        MATRICE operator+(const MATRICE &M)
        {
                MATRICE ris(nl,M.nc);
                int i,j;
                for(i=0;i<nl;i++)
                {
                        for(j=0;j<nc;j++)
                                ris.matrix[i][j]=matrix[i][j]+M.matrix[i][j];
                }
                return ris;
        };

        MATRICE operator-(const MATRICE &M)
        {
                MATRICE ris(nl,M.nc);
                int i,j;
                for(i=0;i<nl;i++)
                {
                        for(j=0;j<nc;j++)
                                ris.matrix[i][j]=matrix[i][j]-M.matrix[i][j];
                }
                return ris;
        };

        MATRICE operator*(const FRACTAL &sc)
        {
                MATRICE ris(nl,nc);
                int i,j;
                for(i=0;i<ris.nl;i++)
                {
                        for(j=0;j<ris.nc;j++)
                                ris.matrix[i][j]=matrix[i][j]*sc;
                }
                return ris;
        };

        MATRICE operator+(const FRACTAL &sc)
        {
                MATRICE ris(nl,nc);
                FRACTAL app;
                int i,j;
                for(i=0;i<ris.nl;i++)
                {
                        for(j=0;j<ris.nc;j++)
                                ris.matrix[i][j]=matrix[i][j]+sc;
                }
                return ris;
        };

        MATRICE operator-(const FRACTAL &sc)
        {
                MATRICE ris(nl,nc);
                FRACTAL app;
                int i,j;
                for(i=0;i<ris.nl;i++)
                {
                        for(j=0;j<ris.nc;j++)
                                ris.matrix[i][j]=matrix[i][j]-sc;
                }
                return ris;
        };

        public: MATRICE Trsposta()
        {
                MATRICE ris(nc,nl);
                int i,j,app;
                for(i=0;i<nc;i++)
                {
                        for(j=0;j<nl;j++)
                                ris.matrix[i][j]=matrix[j][i];
                }
                free(matrix);
                app=nc;
                nc=nl;
                nl=app;
                matrix=(FRACTAL **)malloc(sizeof(FRACTAL)*nl);
                for(i=0;i<nl;i++)
                        matrix[i]=(FRACTAL *)malloc(sizeof(FRACTAL)*nc);
                return ris;
        }

        /*int Rango()
        {
                int indOr;
                int min;
                if(nc<=nl)
                        min=nc;
                else
                        min=nl;
                for(indOr=0;indOr<min;indOr++)
                {

                }
        }*/
};

typedef char element[12];

int menu()
{
        int n;
        system("CLS");
        printf("1. CAMBIA I VALORI DELLA MATRICE\n");
        printf("2. CALCOLA IL DETERMIANTE\n");
        printf("3. CALCOLA LA MATRICE COMPLEMENTARE\n");
        printf("4. CALCOLA LA MATRICE INVERSA\n");
        printf("5. ESCI\n");
        printf("Scegli: ");
        scanf("%d",&n);
        return n;
}

int main(int argc, char *argv[])
{
        int i,j,m=0;
        char el[10];
                if(argc==2)
                {
                        system("CLS");
                        MATRICE M(argv[1]);
                        printf("Matrice:\n\n");
                        for(i=0;i<M.nl;i++)
                        {
                                for(j=0;j<M.nc;j++)
                                {
                                        printf("[%d][%d] = ",i+1,j+1);
                                        scanf("%s",el);
                                        M.matrix[i][j]=el;
                                }
                        }
                        system("CLS");
                        printf("Matrice:\n\n");
                        M.Print();
                        system("PAUSE");
                        do
                        {
                                system("CLS");
                                m=menu();
                                system("CLS");
                                        if(m==1)
                                        {
                                                int riga,colonna;
                                                char val[12];
                                                do
                                                {
                                                        printf("Riga: ");
                                                        scanf("%d",&riga);
                                                }
                                                while((riga<1)||(riga>M.nl));
                                                do
                                                {
                                                        printf("Colonna: ");
                                                        scanf("%d",&colonna);
                                                }
                                                while((colonna<1)||(colonna>M.nc));
                                                printf("\nVALORE: ");
                                                scanf("%s",val);
                                                M.matrix[riga-1][colonna-1]=val;
                                                system("CLS");
                                                printf("Matrice:\n\n");
                                                M.Print();
                                                system("PAUSE");
                                        }
                                        if(m==2)
                                        {
                                                if(M.nc==M.nl)
                                                {
                                                        printf("Determinante:\n\n");
                                                        M.Print();
                                                        printf("DetM: ");
                                                        M.DET().Print();
                                                        printf("\n\n");
                                                }
                                                else
                                                        printf("Impossibile calcolare il determinante\n\n");
                                                system("PAUSE");
                                        }
                                        if(m==3)
                                        {
                                                if(M.nc==M.nl)
                                                {
                                                        MATRICE CM(M.nl,M.nc);
                                                        CM=M.COMP();
                                                        printf("Matrice complementare:\n\n");
                                                        CM.Print();
                                                        system("PAUSE");
                                                }
                                        }
                                        if(m==4)
                                        {
                                                if(M.nc==M.nl)
                                                {
                                                        if(M.DET()!=0)
                                                        {
                                                                MATRICE MI(M.nl,M.nc);
                                                                MATRICE V(M.nl,M.nc);
                                                                MATRICE CM(M.nl,M.nc);
                                                                CM=M.COMP();
                                                                CM=CM.Trsposta();
                                                                MI=CM*(M.DET()^-1);
                                                                printf("Inversa:\n\n");
                                                                MI.Print();
                                                                printf("Verifica:\n\n");
                                                                V=M*MI;
                                                                V.Print();
                                                        }
                                                        else
                                                                printf("Impossibile calcolare l\'Inversa\n\n");
                                                }
                                                else
                                                        printf("Impossibile calcolare l\'Inversa\n\n");
                                                system("PAUSE");
                                        }
                        }
                        while(m!=5);
                }

                if(argc==4)
                {
                        system("CLS");
                        if((strchr(argv[1],'x')!=NULL)||(strchr(argv[1],'X')!=NULL))
                        {
                                MATRICE A(argv[1]);
                                printf("Matrice 1:\n\n");
                                for(i=0;i<A.nl;i++)
                                {
                                        for(j=0;j<A.nc;j++)
                                        {
                                                printf("[%d][%d] = ",i+1,j+1);
                                                scanf("%s",el);
                                                A.matrix[i][j]=el;
                                        }
                                }
                                printf("\n");
                                if((strchr(argv[3],'x')!=NULL)||(strchr(argv[3],'X')!=NULL))
                                {
                                        MATRICE B(argv[3]);
                                        printf("Matrice 2:\n\n");
                                        for(i=0;i<B.nl;i++)
                                        {
                                                for(j=0;j<B.nc;j++)
                                                {
                                                        printf("[%d][%d] = ",i+1,j+1);
                                                        scanf("%s",el);
                                                        B.matrix[i][j]=el;
                                                }
                                        }
                                        system("CLS");
                                        printf("Matrice 1:\n\n");
                                        A.Print();
                                        printf("Matrice 2:\n\n");
                                        B.Print();
                                        system("PAUSE");
                                        system("CLS");
                                        switch(argv[2][0])
                                        {
                                                case '+':
                                                        if((A.nc==B.nc)&&(A.nl==B.nl))
                                                        {
                                                                MATRICE R(A.nl,A.nc);
                                                                R=A+B;
                                                                printf("RISULTATO:\n\n");
                                                                R.Print();
                                                        }
                                                        else
                                                                printf("Impossibile eseguire la somma\n\n");
                                                break;
                                                case '*':
                                                        if(A.nc==B.nl)
                                                        {
                                                                MATRICE R(A.nl,B.nc);
                                                                R=A*B;
                                                                printf("RISULTATO:\n\n");
                                                                R.Print();
                                                        }
                                                        else
                                                                printf("Impossibile eseguire la somma\n\n");
                                                break;
                                                case '-':
                                                        if((A.nc==B.nc)&&(A.nl==B.nl))
                                                        {
                                                                MATRICE R(A.nl,A.nc);
                                                                R=A-B;
                                                                printf("RISULTATO:\n\n");
                                                                R.Print();
                                                        }
                                                        else
                                                                printf("Impossibile eseguire la differenza\n\n");

                                                break;
                                        };
                                        system("PAUSE");
                                }
                                else
                                {
                                        FRACTAL scalare(argv[3]);
                                        MATRICE R(A.nl,A.nc);
                                        switch(argv[2][0])
                                        {
                                                case '+':
                                                        R=A+scalare;
                                                break;
                                                case '*':
                                                        R=A*scalare;
                                                break;
                                                case '-':
                                                        R=A-scalare;
                                                break;
                                        };
                                        system("CLS");
                                        printf("RISULTATO:\n\n");
                                        R.Print();
                                        system("PAUSE");
                                }
                        }
                        else
                        {
                                FRACTAL scalare(argv[1]);
                                MATRICE A(argv[3]);
                                MATRICE R(A.nl,A.nc);
                                printf("Matrice:\n\n");
                                for(i=0;i<A.nl;i++)
                                {
                                        for(j=0;j<A.nc;j++)
                                        {
                                                printf("[%d][%d] = ",i+1,j+1);
                                                scanf("%s",el);
                                                A.matrix[i][j]=el;
                                        }
                                }
                                switch(argv[2][0])
                                {
                                        case '+':
                                                R=A+scalare;
                                        break;
                                        case '*':
                                                R=A*scalare;
                                        break;
                                        case '-':
                                                R=A-scalare;
                                        break;
                                };
                                system("CLS");
                                printf("RISULTATO:\n\n");
                                R.Print();
                                system("PAUSE");
                        }
                }
                if(argc==1)
                {
                        system("CLS");
                        printf("SINTASSI COMANDO: 1. matr [<MATRICE 1>,<numero>,<frazione>] <segno> [<MATRICE 2>,<numero>,<frazione>]\n");
                        printf("                  2. matr [<MATRICE>]\n");
                        printf("                  3. matr <NULL>\n\n");
                        printf("1. [<MATRICE 1>,<numero>]: Si prevede l'inserimento della dimensione\n");
                        printf("                           della matrice come: \"3x3\" oppure \"5X4\".\n");
                        printf("                           o di un numero.\n");
                        printf("               <frazione>: Si prevede una frazione come: \"-2/3\" o \"4/5\"\n\n");
                        printf("         <segno>: Segno dell'operazione da eseguire: \'+\', \'-\', \'*\'.\n\n");
                        printf("2.   [<MATRICE>]: Si prevede una sola matrice: \" C:\\> matr 3x3 \"\n\n");
                        printf("3.        <NULL>: Help.\n\n\n\n\n\n\n\t\t\t");
                        system("PAUSE");
                }
        return 0;
}
 