#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

class MATRICE
{
        public: int nl, nc;
                float **matrix;

        MATRICE(int m, int n)
        {
                int i,j;
                nl=m;
                nc=n;
                matrix=(float **)malloc(sizeof(float)*nl);
                for(i=0;i<nl;i++)
                        matrix[i]=(float *)malloc(sizeof(float)*nc);
                for(i=0;i<nl;i++)
                {
                        for(j=0;j<nc;j++)
                                matrix[i][j]=0;
                }
        };
        MATRICE(char *d)
        {
                char num[6];
                int i=0,l,j;
                while((d[i]!='x')&&(d[i]!='X'))
                {
                        num[i]=d[i];
                        i++;
                }
                num[i]='\0';
                nl=atoi(num);
                l=++i;
                while(d[i]!='\0')
                {
                        num[i-l]=d[i];
                        i++;
                }
                num[i]='\0';
                nc=atoi(num);
                matrix=(float **)malloc(sizeof(float)*nl);
                for(i=0;i<nl;i++)
                        matrix[i]=(float *)malloc(sizeof(float)*nc);
                for(i=0;i<nl;i++)
                {
                        for(j=0;j<nc;j++)
                                matrix[i][j]=0;
                }
        }

        public: void Print()
        {
                int i,j,n;
                unsigned int zero;
                for(i=0;i<nl;i++)
                {
                        for(j=0;j<nc;j++)
                        {
                                if(matrix[i][j]!=0)
                                {
                                        n=matrix[i][j]*10;
                                        if(n==0)
                                                printf("%7d",n);
                                        else
                                        {
                                                if((n%10)==0)
                                                        printf("%7.0f",matrix[i][j]);
                                                else
                                                        printf("%7.1f",matrix[i][j]);
                                        }
                                }
                                else
                                        printf("%7d",matrix[i][j]);
                        }
                        printf("\n\n");
                }
        };

        public: MATRICE Minor(int l, int c)
        {
                MATRICE M(nl-1,nc-1);
                int i,j,im=-1,jm=-1;
                for(i=0;i<nl;i++)
                {
                        if(i!=l)
                        {
                                im++;
                                for(j=0;j<nc;j++)
                                {
                                        if(j!=c)
                                        {
                                                jm++;
                                                M.matrix[im][jm]=matrix[i][j];
                                        }

                                }
                                jm=-1;
                        }
                }
                return M;
        };

        public: float DET()
        {
                int i,j;
                float det=0.0;
                MATRICE M(nl-1,nc-1);
                switch(nl)
                {
                        case 2:
                                det=((-1)*(matrix[0][1]*matrix[1][0]))+(matrix[0][0]*matrix[1][1]);
                        break;
                        case 3:
                                det=((-1)*((matrix[0][2]*matrix[1][1]*matrix[2][0])+(matrix[0][0]*matrix[1][2]*matrix[2][1])+(matrix[0][1]*matrix[1][0]*matrix[2][2])));
                                det+=((matrix[0][0]*matrix[1][1]*matrix[2][2])+(matrix[0][1]*matrix[1][2]*matrix[2][0])+(matrix[0][2]*matrix[1][0]*matrix[2][1]));
                        break;
                        default:
                                for(j=0;j<nc;j++)
                                {
                                        if(matrix[0][j]!=0)
                                        {
                                                M=this->Minor(0,j);
                                                det+=matrix[0][j]*(pow(-1,(1+(j+1)))*M.DET());
                                        }
                                }
                        break;
                };
                return det;
        };

        public: MATRICE COMP()
        {
                MATRICE M(nl-1,nc-1);
                MATRICE CM(nl,nc);
                int i,j;
                for(i=0;i<nl;i++)
                {
                        for(j=0;j<nc;j++)
                        {
                                M=this->Minor(i,j);
                                CM.matrix[i][j]=(pow(-1,((i+1)+(j+1))))*M.DET();
                        }
                }
                return CM;
        };

        void operator=(const MATRICE &M)
        {
                int i,j;
                for(i=0;i<nl;i++)
                {
                        for(j=0;j<nc;j++)
                                matrix[i][j]=M.matrix[i][j];
                }
        };

        MATRICE operator*(const MATRICE &M)
        {
                MATRICE ris(nl,M.nc);
                int k,i,j;
                for(i=0;i<ris.nl;i++)
                        for(j=0;j<ris.nc;j++)
                        {
                                ris.matrix[i][j]=0;
                                for(k=0;k<nc;k++)
                                        ris.matrix[i][j]+=(matrix[i][k]*M.matrix[k][j]);
                };
                return ris;
        };

        MATRICE operator+(const MATRICE &M)
        {
                MATRICE ris(nl,M.nc);
                int i,j;
                for(i=0;i<nl;i++)
                {
                        for(j=0;j<nc;j++)
                                ris.matrix[i][j]=matrix[i][j]+M.matrix[i][j];
                }
                return ris;
        };

        MATRICE operator-(const MATRICE &M)
        {
                MATRICE ris(nl,M.nc);
                int i,j;
                for(i=0;i<nl;i++)
                {
                        for(j=0;j<nc;j++)
                                ris.matrix[i][j]=matrix[i][j]-M.matrix[i][j];
                }
                return ris;
        };

        MATRICE operator*(const float &sc)
        {
                MATRICE ris(nl,nc);
                int i,j;
                for(i=0;i<ris.nl;i++)
                {
                        for(j=0;j<ris.nc;j++)
                                ris.matrix[i][j]=matrix[i][j]*sc;
                }
                return ris;
        };

        MATRICE operator+(const float &sc)
        {
                MATRICE ris(nl,nc);
                int i,j;
                for(i=0;i<ris.nl;i++)
                {
                        for(j=0;j<ris.nc;j++)
                                ris.matrix[i][j]=matrix[i][j]+sc;
                }
                return ris;
        };

        MATRICE operator-(const float &sc)
        {
                MATRICE ris(nl,nc);
                int i,j;
                for(i=0;i<ris.nl;i++)
                {
                        for(j=0;j<ris.nc;j++)
                                ris.matrix[i][j]=matrix[i][j]-sc;
                }
                return ris;
        };

        public: MATRICE Trsposta()
        {
                MATRICE ris(nc,nl);
                int i,j,app;
                for(i=0;i<nc;i++)
                {
                        for(j=0;j<nl;j++)
                                ris.matrix[i][j]=matrix[j][i];
                }
                free(matrix);
                app=nc;
                nc=nl;
                nl=app;
                matrix=(float **)malloc(sizeof(float)*nl);
                for(i=0;i<nl;i++)
                        matrix[i]=(float *)malloc(sizeof(float)*nc);
                return ris;
        }
};


int menu()
{
        int n;
        system("CLS");
        printf("1. CAMBIA I VALORI DELLA MATRICE\n");
        printf("2. CALCOLA IL DETERMIANTE\n");
        printf("3. CALCOLA LA MATRICE COMPLEMENTARE\n");
        printf("4. CALCOLA LA MATRICE INVERSA\n");
        printf("5. ESCI\n");
        printf("Scegli: ");
        scanf("%d",&n);
        return n;
}

int main(int argc, char *argv[])
{
        int i,j,m=0;

                if(argc==2)
                {
                        system("CLS");
                        MATRICE M(argv[1]);
                        printf("Matrice:\n\n");
                        for(i=0;i<M.nl;i++)
                        {
                                for(j=0;j<M.nc;j++)
                                {
                                        printf("[%d][%d] = ",i+1,j+1);
                                        scanf("%f",&(M.matrix[i][j]));
                                }
                        }
                        system("CLS");
                        printf("Matrice:\n\n");
                        M.Print();
                        system("PAUSE");
                        do
                        {
                                system("CLS");
                                m=menu();
                                system("CLS");
                                        if(m==1)
                                        {
                                                int riga,colonna;
                                                do
                                                {
                                                        printf("Riga: ");
                                                        scanf("%d",&riga);
                                                }
                                                while((riga<1)||(riga>M.nl));
                                                do
                                                {
                                                        printf("Colonna: ");
                                                        scanf("%d",&colonna);
                                                }
                                                while((colonna<1)||(colonna>M.nc));
                                                printf("\nVALORE: ");
                                                scanf("%f",&(M.matrix[riga-1][colonna-1]));
                                                system("CLS");
                                                printf("Matrice:\n\n");
                                                M.Print();
                                                system("PAUSE");
                                        }
                                        if(m==2)
                                        {
                                                if(M.nc==M.nl)
                                                {
                                                        printf("Determinante:\n\n");
                                                        M.Print();
                                                        printf("DetA: %.2f\n\n",M.DET());
                                                }
                                                else
                                                        printf("Impossibile calcolare il determinante\n\n");
                                                system("PAUSE");
                                        }
                                        if(m==3)
                                        {
                                                if(M.nc==M.nl)
                                                {
                                                        MATRICE CM(M.nl,M.nc);
                                                        CM=M.COMP();
                                                        printf("Matrice complementare:\n\n");
                                                        CM.Print();
                                                        system("PAUSE");
                                                }
                                        }
                                        if(m==4)
                                        {
                                                if(M.nc==M.nl)
                                                {
                                                        if(M.DET()!=0)
                                                        {
                                                                MATRICE MI(M.nl,M.nc);
                                                                MATRICE V(M.nl,M.nc);
                                                                MATRICE CM(M.nl,M.nc);
                                                                CM=M.COMP();
                                                                CM=CM.Trsposta();
                                                                MI=CM*(pow(M.DET(),-1));
                                                                printf("Inversa:\n\n");
                                                                MI.Print();
                                                                printf("Verifica:\n\n");
                                                                V=M*MI;
                                                                V.Print();
                                                        }
                                                        else
                                                                printf("Impossibile calcolare l\'Inversa\n\n");
                                                }
                                                else
                                                        printf("Impossibile calcolare l\'Inversa\n\n");
                                                system("PAUSE");
                                        }
                        }
                        while(m!=5);
                }

                if(argc==4)
                {
                        system("CLS");
                        if((strchr(argv[1],'x')!=NULL)||(strchr(argv[1],'X')!=NULL))
                        {
                                MATRICE A(argv[1]);
                                printf("Matrice 1:\n\n");
                                for(i=0;i<A.nl;i++)
                                {
                                        for(j=0;j<A.nc;j++)
                                        {
                                                printf("[%d][%d] = ",i+1,j+1);
                                                scanf("%f",&(A.matrix[i][j]));
                                        }
                                }
                                printf("\n");
                                if((strchr(argv[3],'x')!=NULL)||(strchr(argv[3],'X')!=NULL))
                                {
                                        MATRICE B(argv[3]);
                                        printf("Matrice 2:\n\n");
                                        for(i=0;i<B.nl;i++)
                                        {
                                                for(j=0;j<B.nc;j++)
                                                {
                                                        printf("[%d][%d] = ",i+1,j+1);
                                                        scanf("%f",&(B.matrix[i][j]));
                                                }
                                        }
                                        system("CLS");
                                        printf("Matrice 1:\n\n");
                                        A.Print();
                                        printf("Matrice 2:\n\n");
                                        B.Print();
                                        system("PAUSE");
                                        system("CLS");
                                        switch(argv[2][0])
                                        {
                                                case '+':
                                                        if((A.nc==B.nc)&&(A.nl==B.nl))
                                                        {
                                                                MATRICE R(A.nl,A.nc);
                                                                R=A+B;
                                                                printf("RISULTATO:\n\n");
                                                                R.Print();
                                                        }
                                                        else
                                                                printf("Impossibile eseguire la somma\n\n");
                                                break;
                                                case '*':
                                                        if(A.nc==B.nl)
                                                        {
                                                                MATRICE R(A.nl,A.nc);
                                                                R=A*B;
                                                                printf("RISULTATO:\n\n");
                                                                R.Print();
                                                        }
                                                        else
                                                                printf("Impossibile eseguire la somma\n\n");
                                                break;
                                                case '-':
                                                        if((A.nc==B.nc)&&(A.nl==B.nl))
                                                        {
                                                                MATRICE R(A.nl,A.nc);
                                                                R=A-B;
                                                                printf("RISULTATO:\n\n");
                                                                R.Print();
                                                        }
                                                        else
                                                                printf("Impossibile eseguire la differenza\n\n");

                                                break;
                                        };
                                        system("PAUSE");
                                }
                                else
                                {
                                        float scalare=atoi(argv[3]);
                                        MATRICE R(A.nl,A.nc);
                                        switch(argv[2][0])
                                        {
                                                case '+':
                                                        R=A+scalare;
                                                break;
                                                case '*':
                                                        R=A*scalare;
                                                break;
                                                case '-':
                                                        R=A-scalare;
                                                break;
                                        };
                                        system("CLS");
                                        printf("RISULTATO:\n\n");
                                        R.Print();
                                        system("PAUSE");
                                }
                        }
                        else
                        {
                                float scalare=atoi(argv[1]);
                                MATRICE A(argv[3]);
                                MATRICE R(A.nl,A.nc);
                                printf("Matrice 1:\n\n");
                                for(i=0;i<A.nl;i++)
                                {
                                        for(j=0;j<A.nc;j++)
                                        {
                                                printf("[%d][%d] = ",i+1,j+1);
                                                scanf("%f",&(A.matrix[i][j]));
                                        }
                                }
                                switch(argv[2][0])
                                {
                                        case '+':
                                                R=A+scalare;
                                        break;
                                        case '*':
                                                R=A*scalare;
                                        break;
                                        case '-':
                                                R=A-scalare;
                                        break;
                                };
                                system("CLS");
                                printf("RISULTATO:\n\n");
                                R.Print();
                                system("PAUSE");
                        }
                }
                if(argc==1)
                {
                        printf("\n");
                        printf("SINTASSI COMANDO: 1. matr [<MATRICE 1>,<numero>] <segno> [<MATRICE 2>,<numero>]\n");
                        printf("                  2. matr [<MATRICE>]\n");
                        printf("                  3. matr <NULL>\n\n");
                        printf("1. [<MATRICE 1>,<numero>]: Si prevede l'inserimento della dimensione\n");
                        printf("                           della matrice come: \"3x3\" oppure \"5X4\".\n");
                        printf("                           o di un numero.\n");
                        printf("         <segno>: Segno dell'operazione da eseguire: \'+\', \'-\', \'*\'.\n\n");
                        printf("2.   [<MATRICE>]: Si prevede una sola matrice.\n\n");
                        printf("3.        <NULL>: Help.\n");
                }
        return 0;
}
 