
#pragma hdrstop
#include <condefs.h>
#include <iostream.h>
#include <conio.h>
#include <stdlib.h>
#include <stdio.h>

//---------------------------------------------------------------------------
#pragma argsused

void APERTURA_FILE(char p[])
{
        FILE *f;
        char c;
        if((f=fopen(p,"r"))==NULL)
                exit(1);
        clrscr();
        while(fscanf(f,"%c",&c)!=EOF)
            printf("%c",c);
        fclose(f);
}

int main(int argc, char* argv[])
{
        if(argc==2)
        {
                APERTURA_FILE(argv[1]);
                printf("\n\n\n");
                system("PAUSE");
        }
        return 0;
}
 