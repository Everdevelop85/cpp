; EditorPIC Compyler of Zanasi Michele   10/04/2008 10.58.42
;
        PROCESSOR       16F84A
        RADIX           DEC
        INCLUDE         "P16F84A.INC"
        ERRORLEVEL      -302
        __CONFIG        0x3FF1

	ORG	0Ch
count	RES	2
ncicl	RES	1

	ORG	0h
	BSF	STATUS,RP0
        MOVLW   B'00000000'
        MOVWF   TRISA
        MOVLW   B'00000000'
        MOVWF   TRISB
        BCF     STATUS,RP0
	
	CALL	MainLoop
stop	MOVLW	B'10000000'
	MOVWF	PORTB
	GOTO	stop	

;Ritardo FirstCicle
cnt
	MOVLW	0FFh
	MOVWF	count
	MOVWF	count+1
ciclo	
	DECFSZ	count
	GOTO	ciclo
	
	DECFSZ	count+1
	GOTO	ciclo

	RETURN

;Delay finished
delay
	MOVLW	02h
	MOVWF	ncicl
clcnt
	CALL	cnt
	DECFSZ	ncicl
	GOTO	clcnt
	RETURN


;Main function
	
MainLoop
	MOVLW	B'01111111'
	MOVWF	PORTB
	CALL	delay
	MOVLW	B'01111010'
	MOVWF	PORTB
	CALL	delay
	MOVLW	B'00111100'
	MOVWF	PORTB
	CALL	delay
	MOVLW	B'01100011'
	MOVWF	PORTB
	CALL	delay
	MOVLW	B'00000111'
	MOVWF	PORTB
	CALL	delay
	MOVLW	B'00000011'
	MOVWF	PORTB
	CALL	delay
	MOVLW	B'00000000'
	MOVWF	PORTB
	RETURN
	
	END
