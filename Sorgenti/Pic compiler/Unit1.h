//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H

//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
//---------------------------------------------------------------------------

#include <conio.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <Menus.hpp>
#include <ExtCtrls.hpp>
#include <ToolWin.hpp>
#include <Buttons.hpp>
#include <Dialogs.hpp>
#include <sys/stat.h>
#include <io.h>
#include <dir.h>

#define MAX 255

class TfrmEditor : public TForm
{
__published:	// IDE-managed Components
        TMainMenu *MainMenu;
        TMenuItem *mnuFile;
        TMenuItem *mnuFileItem1;
        TMenuItem *mnuFileItem2;
        TMenuItem *mnuFileItem3;
        TMenuItem *mnuFileItem4;
        TMenuItem *mnuFileItem5;
        TMenuItem *Modifica1;
        TMenuItem *mnuEditItem1;
        TMenuItem *mnuEditItem2;
        TMenuItem *mnuEditItem3;
        TPanel *Panel1;
        TPanel *Panel2;
        TPanel *Panel3;
        TStatusBar *stsBar;
        TRichEdit *rtfEditor;
        TMemo *memErrors;
        TToolBar *ToolBar1;
        TBitBtn *cmdSaveAs;
        TTimer *tmrSaveResque;
        TOpenDialog *OpenDialog;
        TSplitter *Splitter1;
        TMenuItem *mnuEditItem4;
        TMenuItem *mnuEditItem5;
        TMenuItem *mnuEditItem6;
        TMenuItem *mnuFileItem6;
        TBitBtn *cmdSave;
        TBitBtn *cmdOpen;
        TBitBtn *cmdNew;
        TSplitter *Splitter2;
        TBitBtn *cmdRun;
        TSplitter *Splitter3;
        TBitBtn *cmdPutComment;
        TBitBtn *cmdCancComment;
        TSplitter *Splitter4;
        TBitBtn *cmdCopy;
        TBitBtn *cmdCut;
        TBitBtn *cmdPaste;
        TSaveDialog *SaveDialog;
        TMemo *memApp;
        TTimer *tmrControlComp;
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall tmrSaveResqueTimer(TObject *Sender);
        void __fastcall mnuFileItem1Click(TObject *Sender);
        void __fastcall mnuFileItem2Click(TObject *Sender);
        void __fastcall mnuFileItem6Click(TObject *Sender);
        void __fastcall rtfEditorChange(TObject *Sender);
        void __fastcall mnuFileItem3Click(TObject *Sender);
        void __fastcall mnuFileItem5Click(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall cmdNewClick(TObject *Sender);
        void __fastcall cmdOpenClick(TObject *Sender);
        void __fastcall cmdSaveClick(TObject *Sender);
        void __fastcall cmdSaveAsClick(TObject *Sender);
        void __fastcall cmdCopyClick(TObject *Sender);
        void __fastcall cmdCutClick(TObject *Sender);
        void __fastcall cmdPasteClick(TObject *Sender);
        void __fastcall cmdPutCommentClick(TObject *Sender);
        void __fastcall cmdCancCommentClick(TObject *Sender);
        void __fastcall mnuEditItem1Click(TObject *Sender);
        void __fastcall mnuEditItem2Click(TObject *Sender);
        void __fastcall mnuEditItem3Click(TObject *Sender);
        void __fastcall mnuEditItem5Click(TObject *Sender);
        void __fastcall mnuEditItem6Click(TObject *Sender);
        void __fastcall cmdRunClick(TObject *Sender);
        void __fastcall tmrControlCompTimer(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TfrmEditor(TComponent* Owner);

        char WorkDir[MAX],
             ProjectDir[MAX],
             ProjectPath[MAX],
             BackUPFile[MAX],
             ErrorList[MAX],
             HexFile[MAX],
             CompylerProg[MAX],
             ProjectName[20];
};
//---------------------------------------------------------------------------
extern PACKAGE TfrmEditor *frmEditor;
//---------------------------------------------------------------------------
#endif
