//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TfrmEditor *frmEditor;
bool Edited;
//===========================================================================
//FUNCTIONS
//===========================================================================
void ResetTimer(TTimer *tmr)
{
        tmr->Enabled=false;
        tmr->Enabled=true;
}

int getIndexPrj()
{
        char number[5], path[MAX];
        struct ffblk ff;
        int i=0, num;
        strcpy(path,frmEditor->ProjectDir);
        strcat(path,"\\*.asm");
        if((findfirst(path,&ff,FA_ARCH))==0)
        {
                while((findnext(&ff))==0);;
        }
        if(strlen(ff.ff_name)<=0)
                return 0;
        for(i=3;ff.ff_name[i]!='.';i++)
                number[i-3]=ff.ff_name[i];
        number[i-3]='\0';
        return atoi(number);
}

void CreatePath()
{
        getcwd(frmEditor->WorkDir,MAX);
        strcpy(frmEditor->ProjectPath,frmEditor->WorkDir);
        strcpy(frmEditor->BackUPFile,frmEditor->WorkDir);
        strcpy(frmEditor->ProjectDir,frmEditor->WorkDir);
        strcpy(frmEditor->ErrorList,frmEditor->WorkDir);
        strcpy(frmEditor->HexFile,frmEditor->ProjectDir);
        strcpy(frmEditor->CompylerProg,frmEditor->WorkDir);
        strcpy(frmEditor->ProjectName,"Prj");
        strcat(frmEditor->BackUPFile,"\\Prj~");
        int l=strlen(frmEditor->BackUPFile), n;
        n=getIndexPrj()+1;
        itoa(n,&(frmEditor->BackUPFile[l]),10);
        itoa(n,&(frmEditor->ProjectName[3]),10);
        strcat(frmEditor->ProjectName,".asm");
        strcat(frmEditor->BackUPFile,".asm");
        strcat(frmEditor->ProjectPath,"\\");
        strcat(frmEditor->HexFile,"\\");
        strcat(frmEditor->ErrorList,"\\");
        strcat(frmEditor->HexFile,frmEditor->ProjectName);
        strcat(frmEditor->ErrorList,frmEditor->ProjectName);
        strcat(frmEditor->ProjectPath,frmEditor->ProjectName);
        frmEditor->HexFile[strlen(frmEditor->HexFile)-4]='\0';
        frmEditor->ErrorList[strlen(frmEditor->ErrorList)-4]='\0';
        strcat(frmEditor->HexFile,".HEX");
        strcat(frmEditor->ErrorList,".ERR");
        strcat(frmEditor->CompylerProg,"\\ASM\\MPASMWIN.EXE");
}

void ChangeNameProject(char *newFn)
{
        int mark=strlen(newFn)-1, i;
        while(newFn[mark]!='\\')
                mark--;
        for(i=0;i<mark;i++)
                frmEditor->ProjectDir[i]=newFn[i];
        frmEditor->ProjectDir[i]='\0';
        for(i=mark+1;newFn[i]!='\0';i++)
                frmEditor->ProjectName[i-(mark+1)]=newFn[i];
        frmEditor->ProjectName[i-(mark+1)]='\0';
        strcpy(frmEditor->ProjectPath,frmEditor->ProjectDir);
        strcpy(frmEditor->BackUPFile,frmEditor->ProjectDir);
        strcpy(frmEditor->HexFile,frmEditor->ProjectDir);
        strcpy(frmEditor->ErrorList,frmEditor->ProjectDir);
        strcat(frmEditor->ProjectPath,"\\");
        strcat(frmEditor->BackUPFile,"\\bck~");
        strcat(frmEditor->BackUPFile,frmEditor->ProjectName);
        strcat(frmEditor->ProjectPath,frmEditor->ProjectName);
        strcat(frmEditor->HexFile,"\\");
        strcat(frmEditor->ErrorList,"\\");
        strcat(frmEditor->HexFile,frmEditor->ProjectName);
        strcat(frmEditor->ErrorList,frmEditor->ProjectName);
        frmEditor->HexFile[strlen(frmEditor->HexFile)-4]='\0';
        frmEditor->ErrorList[strlen(frmEditor->ErrorList)-4]='\0';
        strcat(frmEditor->HexFile,".HEX");
        strcat(frmEditor->ErrorList,".ERR");
        chdir(frmEditor->WorkDir);
}

int SaveOnFile(char *FileName)
{
        int hndFile;
        AnsiString FirstLine;
        if((hndFile=open(FileName,O_CREAT|O_TRUNC|O_RDWR,S_IREAD|S_IWRITE))<0)
                return 1;
        FirstLine="; EditorPIC Compyler of Zanasi Michele   "+Now()+"\n";
        if((write(hndFile,FirstLine.c_str(),FirstLine.Length()))<0)
                return 2;
        write(hndFile,";\n",2);
        for(int i=0;i<frmEditor->rtfEditor->Lines->Count;i++)
        {
                if((write(hndFile,frmEditor->rtfEditor->Lines->Strings[i].c_str(),frmEditor->rtfEditor->Lines->Strings[i].Length()))<0)
                        return 2;
                write(hndFile,"\n",1);
        }
        close(hndFile);
        return 0;
}

int OpenFromFile()
{
        int hndFile, nline=0;
        if((access(frmEditor->ProjectPath,00|06))!=0)
                return 1;
        if((hndFile=open(frmEditor->ProjectPath,O_RDONLY,S_IREAD|S_IWRITE))<0)
                return 2;
        char buffer[MAX];
        int i=0;
        frmEditor->rtfEditor->Clear();
        while((read(hndFile,&(buffer[i]),1))>0)
        {
                if(buffer[i]=='\n')
                {
                        buffer[i]='\0';
                        nline++;
                        if((nline>2)||(buffer[0]!=';'))
                                frmEditor->rtfEditor->Lines->Add(buffer);
                        i=-1;
                }
                i++;
        }
        buffer[i]='\0';
        frmEditor->rtfEditor->Lines->Add(buffer);
        close(hndFile);
        return 0;
}

bool CheckFile(char *FileName)
{
        int naccess=0;
        while(((access(FileName,00|01|06))!=0)&&(naccess<30000))
                naccess++;
        if(((access(FileName,00))!=0)&&(naccess==30000))
                return false;
        return true;
}

//===========================================================================
__fastcall TfrmEditor::TfrmEditor(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------

//Error,Message[<number>]/n� spaces/<ProjectPath> <riga> : <descrizione>
class TErrorList : public TThread
{
        private:
                AnsiString message;
        public:
                bool Terminated;
                TErrorList(): TThread(true)
                {
                        Terminated = false;
                }

                void ClearLine(char *line)
                {
                        int ind=0, space=1, l=strlen(frmEditor->ProjectPath), i;
                        char *app;
                        while(line[ind]!=' ')
                                ind++;
                        line[ind]='\t';
                        ind++;
                        while(line[space+ind]==' ')
                                space++;
                        l+=space;
                        for(i=0;line[i+ind+l]!='\0';i++)
                                line[i+ind]=line[i+ind+l];
                        line[i+ind]='\0';
                }

                int OpenErrorFile()
                {
                        int hndFile,i=0;
                        char buffer[MAX];
                        if((hndFile=open(frmEditor->ErrorList,O_RDONLY,S_IREAD|S_IWRITE))<0)
                                return 1;
                        while((read(hndFile,&(buffer[i]),1))>0)
                        {
                                if(buffer[i]=='\n')
                                {
                                        buffer[i]='\0';
                                        ClearLine(buffer);
                                        frmEditor->memErrors->Lines->Add(buffer);
                                        i=-1;
                                }
                                i++;
                        }
                        buffer[i]='\0';
                        frmEditor->memErrors->Lines->Add(buffer);
                        close(hndFile);
                        return 0;
                }

                virtual void __fastcall Execute()
                {
                        if(OpenErrorFile()!=0)
                                frmEditor->memErrors->Lines->Add("Impossibile aprire ErrorList");
                        Terminated = true;
                }
}*TEL;


class TControlComp: public TThread
{
        private:
                AnsiString message;
        public:
                bool Terminated;
                TControlComp(TErrorList *TEL): TThread(true)
                {
                        Terminated = false;
                }

                virtual void __fastcall Execute()
                {
                        if(CheckFile(frmEditor->HexFile))
                        {
                                message="File ";
                                message.cat_printf("%s",frmEditor->ProjectName);
                                message+=" COMPILATO!!!";
                                frmEditor->memErrors->Lines->Add(message);
                                frmEditor->memErrors->Lines->Add("---------------------------------------------------------------");
                                frmEditor->memErrors->Lines->Add("");
                                if(CheckFile(frmEditor->ErrorList))
                                {
                                        frmEditor->memErrors->Lines->Add("Warnings:");
                                        frmEditor->memErrors->Lines->Add("---------------------------------------------------------------");
                                }
                        }
                        else
                        {
                                frmEditor->memErrors->Lines->Add("Errors:");
                                frmEditor->memErrors->Lines->Add("---------------------------------------------------------------");
                        }
                        TEL->Resume();
                        Terminated = true;
                }
}*TCC;

class TCompiler : public TThread
{
        private:
                AnsiString message;
        public:
                bool Terminated;
                TCompiler(): TThread(true)
                {
                        Terminated = false;
                }

                virtual void __fastcall Execute()
                {
                //Check file asm
                        remove(frmEditor->ErrorList);
                        if(access(frmEditor->HexFile,00)==0)
                        {
                                if(remove(frmEditor->HexFile)!=0)
                                {
                                        message="Impossibile rimuovere ";
                                        message.cat_printf("%s",frmEditor->HexFile);
                                        frmEditor->memErrors->Lines->Add(message);
                                        this->Terminate();
                                        return;
                                }
                        }
                        if(!CheckFile(frmEditor->ProjectPath))
                        {
                                message="File ";
                                message.cat_printf("%s",frmEditor->ProjectName);
                                message+=" non pronto per la compilazione";
                                frmEditor->memErrors->Lines->Add(message);
                                frmEditor->memErrors->Lines->Add("");
                                this->Terminate();
                                return;
                       }
                       frmEditor->memErrors->Lines->Add("Ok per compilazione");
                //Compilazione
                        //TSE->Resume();
                        frmEditor->tmrControlComp->Enabled=true;
                        chdir(frmEditor->ProjectDir);
                        frmEditor->memErrors->Lines->Add("Compilazione...");
                        frmEditor->memErrors->Lines->Add("");
                        ShellExecute(frmEditor->Handle,NULL,frmEditor->CompylerProg,frmEditor->ProjectName,NULL,0);
                        chdir(frmEditor->WorkDir);
                        Terminated = true;
                }
}*TComp;

//---------------------------------------------------------------------------
void __fastcall TfrmEditor::FormCreate(TObject *Sender)
{
        CreatePath();
        tmrSaveResque->Enabled=true;
        tmrControlComp->Enabled=false;
        stsBar->Panels->Items[0]->Text="Stato: ";
        stsBar->Panels->Items[0]->Text.cat_printf("%s",ProjectName);
        stsBar->Panels->Items[1]->Text=Now();
        mnuFileItem3->Enabled = false;
}
//---------------------------------------------------------------------------
void __fastcall TfrmEditor::tmrSaveResqueTimer(TObject *Sender)
{
        if(rtfEditor->Text!="")
        {
                if(SaveOnFile(frmEditor->BackUPFile)!=0)
                {
                        stsBar->Panels->Items[0]->Text.sprintf("Stato: Errore file %s",frmEditor->BackUPFile);
                        MessageBox(frmEditor->Handle,"Errore nel salvataggio del file di backup, il backup non sar� disponibile","Errore",MB_ICONERROR+MB_OK);
                        tmrSaveResque->Enabled=false;
                }
        }
}
//---------------------------------------------------------------------------

void __fastcall TfrmEditor::mnuFileItem1Click(TObject *Sender)
{
        OpenDialog->Execute();
        if(OpenDialog->FileName=="")
                return;
        ChangeNameProject(OpenDialog->FileName.c_str());
        if(OpenFromFile()!=0)
                MessageBox(frmEditor->Handle,"Errore di apertura del file","Errore",MB_ICONERROR+MB_OK);
        ResetTimer(tmrSaveResque);
        Edited=false;
        stsBar->Panels->Items[0]->Text="Stato: ";
        stsBar->Panels->Items[0]->Text.cat_printf("%s",ProjectName);
}
//---------------------------------------------------------------------------

void __fastcall TfrmEditor::mnuFileItem2Click(TObject *Sender)
{
        if(SaveOnFile(frmEditor->ProjectPath)!=0)
                MessageBox(frmEditor->Handle,"Errore nel salvataggio del file","Errore",MB_ICONERROR+MB_OK);
        ResetTimer(tmrSaveResque);
        Edited=false;
}
//---------------------------------------------------------------------------

void __fastcall TfrmEditor::mnuFileItem6Click(TObject *Sender)
{
        if(Edited)
        {
                if((MessageBox(frmEditor->Handle,"Salvare le modifica del file attuale?","Salva",MB_ICONEXCLAMATION+MB_YESNO))==IDYES)
                {
                        if(SaveOnFile(frmEditor->ProjectPath)!=0)
                                MessageBox(frmEditor->Handle,"Errore nel salvataggio del file","Errore",MB_ICONERROR+MB_OK);
                }
        }
        rtfEditor->Clear();
        memErrors->Clear();
        CreatePath();
        ResetTimer(tmrSaveResque);
        Edited=false;
        stsBar->Panels->Items[0]->Text="Stato: ";
        stsBar->Panels->Items[0]->Text.cat_printf("%s",ProjectName);
}
//---------------------------------------------------------------------------

void __fastcall TfrmEditor::rtfEditorChange(TObject *Sender)
{
        if(Edited)
        {
                mnuFileItem3->Enabled = true;
                cmdSave->Enabled = true;
                return;
        }
        mnuFileItem3->Enabled = false;
        cmdSave->Enabled = false;
        Edited=true;

}
//---------------------------------------------------------------------------

void __fastcall TfrmEditor::mnuFileItem3Click(TObject *Sender)
{
        SaveDialog->Execute();
        if(SaveDialog->FileName=="")
                return;
        ChangeNameProject(SaveDialog->FileName.c_str());
        if(SaveOnFile(frmEditor->ProjectPath)!=0)
        {
                MessageBox(frmEditor->Handle,"Errore nel salvataggio del file","Errore",MB_ICONERROR+MB_OK);
                return;
        }
        ResetTimer(tmrSaveResque);
        Edited=false;
        stsBar->Panels->Items[0]->Text="Stato: ";
        stsBar->Panels->Items[0]->Text.cat_printf("%s",ProjectName);
}
//---------------------------------------------------------------------------

void __fastcall TfrmEditor::mnuFileItem5Click(TObject *Sender)
{
        tmrSaveResque->Enabled=false;
        frmEditor->Close();
}
//---------------------------------------------------------------------------

void __fastcall TfrmEditor::FormClose(TObject *Sender,
      TCloseAction &Action)
{
        tmrSaveResque->Enabled=false;
        if(TComp!=NULL)
        {
                if(TComp->Terminated)
                {
                        TComp->Free();
                        free(TComp);
                }
        }
        if(TCC!=NULL)
        {
                if(TCC->Terminated)
                {
                        TCC->Free();
                        free(TCC);
                }
        }
        if(TEL!=NULL)
        {
                if(TEL->Terminated)
                {
                        TEL->Free();
                        free(TComp);
                }
        }
}
//---------------------------------------------------------------------------

void __fastcall TfrmEditor::cmdNewClick(TObject *Sender)
{
        mnuFileItem6Click(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TfrmEditor::cmdOpenClick(TObject *Sender)
{
        mnuFileItem1Click(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TfrmEditor::cmdSaveClick(TObject *Sender)
{
        mnuFileItem2Click(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TfrmEditor::cmdSaveAsClick(TObject *Sender)
{
        mnuFileItem3Click(Sender);
}
//---------------------------------------------------------------------------


void __fastcall TfrmEditor::cmdCopyClick(TObject *Sender)
{
        mnuEditItem1Click(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TfrmEditor::cmdCutClick(TObject *Sender)
{
        mnuEditItem2Click(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TfrmEditor::cmdPasteClick(TObject *Sender)
{
        mnuEditItem3Click(Sender);
}
//---------------------------------------------------------------------------


void __fastcall TfrmEditor::cmdPutCommentClick(TObject *Sender)
{
        mnuEditItem5Click(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TfrmEditor::cmdCancCommentClick(TObject *Sender)
{
        mnuEditItem6Click(Sender);
}
//---------------------------------------------------------------------------


void __fastcall TfrmEditor::mnuEditItem1Click(TObject *Sender)
{
        if(rtfEditor->SelLength>0)
           rtfEditor->CopyToClipboard();
        if(memErrors->SelLength>0)
           memErrors->CopyToClipboard();
}
//---------------------------------------------------------------------------

void __fastcall TfrmEditor::mnuEditItem2Click(TObject *Sender)
{
        if(rtfEditor->SelLength>0)
           rtfEditor->CutToClipboard();
        rtfEditor->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TfrmEditor::mnuEditItem3Click(TObject *Sender)
{
        rtfEditor->PasteFromClipboard();
        rtfEditor->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TfrmEditor::mnuEditItem5Click(TObject *Sender)
{
        if(rtfEditor->SelLength<=0)
                return;
        memApp->Clear();
        rtfEditor->CutToClipboard();
        memApp->PasteFromClipboard();
        for(int i=0;i<memApp->Lines->Count;i++)
                memApp->Lines->Strings[i]=";"+memApp->Lines->Strings[i];
        memApp->SelectAll();
        memApp->CopyToClipboard();
        rtfEditor->SetFocus();
        rtfEditor->PasteFromClipboard();
        rtfEditor->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TfrmEditor::mnuEditItem6Click(TObject *Sender)
{
        if(rtfEditor->SelLength<=0)
                return;
        memApp->Clear();
        rtfEditor->CutToClipboard();
        memApp->PasteFromClipboard();
        for(int i=0;i<memApp->Lines->Count;i++)
                memApp->Lines->Strings[i]=memApp->Lines->Strings[i].Delete(1,1);
        memApp->SelectAll();
        memApp->CopyToClipboard();
        rtfEditor->SetFocus();
        rtfEditor->PasteFromClipboard();
        rtfEditor->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TfrmEditor::cmdRunClick(TObject *Sender)
{
        if(Edited)
        {
                if(SaveOnFile(frmEditor->ProjectPath)!=0)
                {
                        MessageBox(frmEditor->Handle,"Errore nel salvataggio del file","Errore",MB_ICONERROR+MB_OK);
                        return;
                }
        }
        TEL = new TErrorList();
        TComp = new TCompiler();
        TCC = new TControlComp(TEL);
        TComp->Resume();
}
//---------------------------------------------------------------------------



void __fastcall TfrmEditor::tmrControlCompTimer(TObject *Sender)
{
        if((access(frmEditor->HexFile,00)==0)||(access(frmEditor->ErrorList,00)==0))
        {
                TCC->Resume();
                tmrControlComp->Enabled=false;
        }
}
//---------------------------------------------------------------------------

