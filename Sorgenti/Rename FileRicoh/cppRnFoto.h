//---------------------------------------------------------------------------
#ifndef cppRnFotoH
#define cppRnFotoH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <FileCtrl.hpp>
#include <ExtCtrls.hpp>
#include <jpeg.hpp>
//---------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <iostream.h>
#include <string.h>
#include <dir.h>

class TfrmMain : public TForm
{
__published:	// IDE-managed Components
        TPanel *Panel1;
        TPanel *panPicture;
        TImage *ImgPicture;
        TSplitter *Splitter1;
        TPanel *Panel2;
        TPanel *Panel3;
        TGroupBox *GroupBox1;
        TLabel *Label1;
        TLabel *Label2;
        TLabel *Label3;
        TEdit *txtName;
        TEdit *txtNP;
        TEdit *txtIndN;
        TSplitter *Splitter2;
        TPanel *Panel4;
        TPanel *Panel5;
        TFileListBox *lstImg;
        TSplitter *Splitter3;
        TPanel *Panel6;
        TPanel *Panel7;
        TDirectoryListBox *lstDir;
        TDriveComboBox *drvFile;
        TButton *cmdRename;
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall lstDirChange(TObject *Sender);
        void __fastcall cmdRenameClick(TObject *Sender);
        void __fastcall drvFileClick(TObject *Sender);
        void __fastcall lstImgClick(TObject *Sender);
        void __fastcall Splitter3Moved(TObject *Sender);
        void __fastcall Splitter3Paint(TObject *Sender);
private:	// User declarations
public:		// User declarations
        bool NumerazioneFn;
        __fastcall TfrmMain(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfrmMain *frmMain;
//---------------------------------------------------------------------------
#endif
