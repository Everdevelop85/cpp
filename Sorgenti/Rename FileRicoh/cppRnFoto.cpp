//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "cppRnFoto.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TfrmMain *frmMain;
//---------------------------------------------------------------------------
__fastcall TfrmMain::TfrmMain(TComponent* Owner)
        : TForm(Owner)
{

}

int RenemeFileIMG(char *dr)
{
        char oldN[255];
        AnsiString newN;
        int indF=StrToInt(frmMain->txtIndN->Text), ps=StrToInt(frmMain->txtNP->Text), f;
        for(f=0;f<frmMain->lstImg->Items->Count;f++)
        {
                if(frmMain->lstImg->Selected[f])
                {
                        strcpy(oldN,dr);
                        strcat(oldN,"\\");
                        strcat(oldN,frmMain->lstImg->Items->Strings[f].c_str());
                        newN=dr;
                        newN.Insert("\\",newN.Length()+1);
                        newN.cat_printf(frmMain->txtName->Text.c_str(),indF);
                        if(newN.Pos("jpg")<=0)
                                newN.Insert(".jpg",newN.Length()+1);
                        if(rename(oldN,newN.c_str())!=0)
                                return 1;
                        indF=indF+ps;
                }
        }
        return 0;
}

void CenterPicture()
{
        frmMain->ImgPicture->Left=(frmMain->panPicture->Width/2)-(frmMain->ImgPicture->Width/2);
        frmMain->ImgPicture->Top=(frmMain->panPicture->Height/2)-(frmMain->ImgPicture->Height/2);
}

void ResizePicture()
{
        float X=frmMain->ImgPicture->Width,Y=frmMain->ImgPicture->Height;
        if((X>=frmMain->panPicture->Width)||(Y>=frmMain->panPicture->Height))
        {
                frmMain->ImgPicture->AutoSize=false;
                frmMain->ImgPicture->Stretch=true;
                do
                {
                        X/=1.5;
                        Y/=1.5;
                }
                while((X>frmMain->panPicture->Width)||(Y>frmMain->panPicture->Height));
        }
        else
        {
                CenterPicture();
                return;
        }
        frmMain->ImgPicture->Width=X;
        frmMain->ImgPicture->Height=Y;
        CenterPicture();
}
//---------------------------------------------------------------------------
void __fastcall TfrmMain::FormCreate(TObject *Sender)
{
        drvFile->Drive='c';
        lstDir->Directory="c:\\";
}
//---------------------------------------------------------------------------
void __fastcall TfrmMain::lstDirChange(TObject *Sender)
{
        lstImg->Clear();
        lstImg->Directory=lstDir->Directory;
}
//---------------------------------------------------------------------------
void __fastcall TfrmMain::cmdRenameClick(TObject *Sender)
{
        if(RenemeFileIMG(lstDir->Directory.c_str())!=0)
        {
                MessageBox(frmMain->Handle,"Errore nella funzione rename(char *oldname, char *newname)","Errore in Rename",MB_ICONERROR+MB_OK);
                exit(1);
        }
        char mess[255];
        strcpy(mess,"Rinomina dei file completata.\r\n");
        strcat(mess,IntToStr(lstImg->SelCount).c_str());
        strcat(mess," sono stati rinominati");
        MessageBox(frmMain->Handle,mess,"Avviso",MB_ICONINFORMATION+MB_OK);
        AnsiString app;
        app=lstDir->Directory;
        lstDir->Directory="C:\\";
        lstDir->Directory=app;
        lstImg->Selected[0]=true;
}
//---------------------------------------------------------------------------



void __fastcall TfrmMain::drvFileClick(TObject *Sender)
{
        AnsiString drv=drvFile->Drive;
        lstDir->Directory=drv+":\\";
}
//---------------------------------------------------------------------------


void __fastcall TfrmMain::lstImgClick(TObject *Sender)
{
        ImgPicture->Visible=false;
        frmMain->ImgPicture->AutoSize=true;
        ImgPicture->Stretch=false;
        ImgPicture->Picture->LoadFromFile(lstDir->Directory+"\\"+lstImg->Items->Strings[lstImg->ItemIndex]);
        ResizePicture();
        ImgPicture->Visible=true;
}
//---------------------------------------------------------------------------



void __fastcall TfrmMain::Splitter3Moved(TObject *Sender)
{
        drvFile->Width=Panel7->Width;
}
//---------------------------------------------------------------------------




void __fastcall TfrmMain::Splitter3Paint(TObject *Sender)
{
        drvFile->Width=Panel7->Width;        
}
//---------------------------------------------------------------------------

