object Form1: TForm1
  Left = 283
  Top = 184
  Width = 696
  Height = 480
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 360
    Top = 56
    Width = 105
    Height = 49
    Caption = 'Button1'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 112
    Top = 200
    Width = 105
    Height = 41
    Caption = 'Button2'
    TabOrder = 1
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 224
    Top = 200
    Width = 105
    Height = 41
    Caption = 'Button3'
    TabOrder = 2
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 336
    Top = 200
    Width = 105
    Height = 41
    Caption = 'Button4'
    TabOrder = 3
    OnClick = Button4Click
  end
  object TrayIcon1: TTrayIcon
    Hide = True
    RestoreOn = imDoubleClick
    PopupMenuOn = imNone
    Icons = ImageList
    Left = 136
    Top = 48
  end
  object ImageList: TImageList
    Left = 248
    Top = 96
  end
end
