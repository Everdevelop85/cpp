//---------------------------------------------------------------------------

#include <vcl.h>
#include <shellapi.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "trayicon"
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormCreate(TObject *Sender)
{
        //TrayIcon1->Visible=false;
        Graphics::TBitmap *Bitmap1 = new Graphics::TBitmap();
        Graphics::TBitmap *Bitmap2 = new Graphics::TBitmap();
        Graphics::TBitmap *Bitmap3 = new Graphics::TBitmap();
        Bitmap1->LoadFromFile("Clock.bmp");
        Bitmap2->LoadFromFile("Clock2.bmp");
        Bitmap3->LoadFromFile("Clock3.bmp");
        ImageList->Add(Bitmap1,Bitmap1);
        ImageList->Add(Bitmap2,Bitmap2);
        ImageList->Add(Bitmap3,Bitmap3);
        //TrayIcon1->Icons->Height=48;
        //TrayIcon1->Icons->Width=48;
        TrayIcon1->IconIndex=0;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{
        TrayIcon1->Visible=true;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button2Click(TObject *Sender)
{
        TrayIcon1->Icons->Width=18;
        TrayIcon1->Icons->Height=18;
        TrayIcon1->IconIndex=0;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button3Click(TObject *Sender)
{
        TrayIcon1->Icons->Width=16;
        TrayIcon1->Icons->Height=16;
        TrayIcon1->IconIndex=1;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button4Click(TObject *Sender)
{
        TrayIcon1->Icons->Width=20;
        TrayIcon1->Icons->Height=20;
        TrayIcon1->IconIndex=2;
}
//---------------------------------------------------------------------------

