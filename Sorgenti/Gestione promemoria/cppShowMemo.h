//---------------------------------------------------------------------------

#ifndef cppShowMemoH
#define cppShowMemoH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class TfrmShowMemo : public TForm
{
__published:	// IDE-managed Components
        TMemo *txtText;
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
private:	// User declarations
public:		// User declarations
        __fastcall TfrmShowMemo(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfrmShowMemo *frmShowMemo;
//---------------------------------------------------------------------------
#endif
