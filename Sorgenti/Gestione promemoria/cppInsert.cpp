//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "cppInsert.h"
#include "cppAlarm.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TfrmInsert *frmInsert;
//---------------------------------------------------------------------------
__fastcall TfrmInsert::TfrmInsert(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TfrmInsert::cmdUpDownClick(TObject *Sender,
      TUDBtnType Button)
{
        HourClass h;
        h=txtOraAllarme->Text;
        if(Button==btNext)
                h.minuti+=1;
        else
                h.minuti-=1;
        if(h.minuti>59)
        {
                h.ore++;
                h.minuti=0;
        }
        if(h.minuti<0)
        {
                h.ore--;
                h.minuti=59;
        }
        if(h.ore>23)
                h.ore=0;
        if(h.ore<0)
                h.ore=23;
        txtOraAllarme->Text=h.getString();
}
//---------------------------------------------------------------------------
void __fastcall TfrmInsert::cmdCancelClick(TObject *Sender)
{
        frmInsert->Close();
}
//---------------------------------------------------------------------------
void __fastcall TfrmInsert::FormCreate(TObject *Sender)
{
        mntCalendar->Date=Now();
        txtOraAllarme->Text=FormatDateTime("hh:mm",Now());
}
//---------------------------------------------------------------------------
void __fastcall TfrmInsert::cmdOkClick(TObject *Sender)
{
        PROMEMORIA dato;
        dato.datalarm=FormatDateTime("dd/mm/yyyy",mntCalendar->Date);
        dato.oralarm=txtOraAllarme->Text;
        strcpy(dato.Testo,txtTextAlarm->Text.c_str());
        frmMain->DB_ALARM->Add(dato);
        if(frmMain->tmrControlAlarm->Enabled)
                frmMain->tmrControlAlarm->Enabled=false;
        if((frmMain->DB_ALARMTODAY=frmMain->DB_ALARM->getAlarmToday())==NULL)
                frmMain->tmrControlAlarm->Enabled=false;
        else
                frmMain->tmrControlAlarm->Enabled=true;
        frmInsert->Close();
        Visualizza();
}
//---------------------------------------------------------------------------

