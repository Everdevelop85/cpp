object frmMain: TfrmMain
  Left = 351
  Top = 219
  Width = 696
  Height = 409
  Caption = 'w'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object panTime: TPanel
    Left = 0
    Top = 0
    Width = 688
    Height = 33
    Align = alTop
    Caption = 'Gioved'#236' 12 Marzo 2007 - 18:30'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Arial Black'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
  end
  object Panel1: TPanel
    Left = 0
    Top = 33
    Width = 688
    Height = 322
    Align = alClient
    TabOrder = 1
    object Splitter1: TSplitter
      Left = 337
      Top = 1
      Width = 9
      Height = 320
      Cursor = crHSplit
      Beveled = True
    end
    object Panel2: TPanel
      Left = 1
      Top = 1
      Width = 336
      Height = 320
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
      object txtProMemo: TMemo
        Left = 0
        Top = 0
        Width = 336
        Height = 320
        Align = alClient
        BevelOuter = bvNone
        BorderStyle = bsNone
        Color = clNavy
        Font.Charset = ANSI_CHARSET
        Font.Color = clYellow
        Font.Height = -21
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
      end
    end
    object Panel3: TPanel
      Left = 346
      Top = 1
      Width = 341
      Height = 320
      Align = alClient
      TabOrder = 1
      object lstViewAlarm: TListView
        Left = 1
        Top = 1
        Width = 339
        Height = 318
        Align = alClient
        BevelInner = bvSpace
        BorderStyle = bsNone
        Columns = <
          item
            Caption = 'Indice'
          end
          item
            Caption = 'Data'
            Width = 150
          end
          item
            Caption = 'Ora'
          end>
        ColumnClick = False
        GridLines = True
        HideSelection = False
        ReadOnly = True
        RowSelect = True
        TabOrder = 0
        ViewStyle = vsReport
        OnClick = lstViewAlarmClick
      end
    end
  end
  object MainMenu: TMainMenu
    Top = 8
    object mnuPromemoria: TMenuItem
      Caption = 'Promemoria'
      object mnuPromemoriaItem1: TMenuItem
        Caption = 'Inserisci nuovo promemoria'
        OnClick = mnuPromemoriaItem1Click
      end
      object mnuPromemoriaItem2: TMenuItem
        Caption = 'Modifica promemoria...'
        OnClick = mnuPromemoriaItem2Click
      end
      object mnuPromemoriaItem3: TMenuItem
        Caption = 'Elimina promemoria'
      end
      object mnuPromemoriaItem4: TMenuItem
        Caption = '-'
      end
      object mnuPromemoriaItem5: TMenuItem
        Caption = 'Esci'
      end
    end
  end
  object tmrControlAlarm: TTimer
    Interval = 5000
    OnTimer = tmrControlAlarmTimer
    Left = 96
    Top = 64
  end
  object tmrData: TTimer
    OnTimer = tmrDataTimer
    Left = 136
    Top = 64
  end
end
