object frmInsert: TfrmInsert
  Left = 449
  Top = 99
  BorderStyle = bsDialog
  Caption = 'frmInsert'
  ClientHeight = 549
  ClientWidth = 409
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Pitch = fpFixed
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 352
    Width = 101
    Height = 13
    Caption = 'Testo del promemoria'
  end
  object Label2: TLabel
    Left = 8
    Top = 520
    Width = 119
    Height = 13
    Caption = 'Ora a cui fissare l'#39'allarme:'
  end
  object mntCalendar: TMonthCalendar
    Left = 0
    Top = 0
    Width = 409
    Height = 345
    Date = 39061.7325289352
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -27
    Font.Name = 'Times New Roman'
    Font.Pitch = fpFixed
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    WeekNumbers = True
  end
  object txtTextAlarm: TMemo
    Left = 0
    Top = 368
    Width = 401
    Height = 137
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Pitch = fpFixed
    Font.Style = []
    ParentFont = False
    ScrollBars = ssVertical
    TabOrder = 1
  end
  object cmdUpDown: TUpDown
    Left = 192
    Top = 515
    Width = 25
    Height = 26
    Min = -32768
    Max = 32767
    Position = 0
    TabOrder = 2
    Wrap = False
    OnClick = cmdUpDownClick
  end
  object txtOraAllarme: TEdit
    Left = 136
    Top = 515
    Width = 57
    Height = 28
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Pitch = fpFixed
    Font.Style = [fsBold]
    MaxLength = 5
    ParentFont = False
    ReadOnly = True
    TabOrder = 3
    Text = '00:00'
  end
  object cmdOk: TButton
    Left = 248
    Top = 516
    Width = 73
    Height = 25
    Caption = 'OK'
    TabOrder = 4
    OnClick = cmdOkClick
  end
  object cmdCancel: TButton
    Left = 328
    Top = 516
    Width = 73
    Height = 25
    Caption = 'Annulla'
    TabOrder = 5
    OnClick = cmdCancelClick
  end
end
