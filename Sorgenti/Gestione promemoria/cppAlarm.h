//---------------------------------------------------------------------------

#ifndef cppAlarmH
#define cppAlarmH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
#include "MagicLibrary.h"
//#include "AlarmClass.h"
#include "DataClass.h"
#include "HourClass.h"
#include <ExtCtrls.hpp>
#include <Menus.hpp>
#include <Graphics.hpp>
#include <ComCtrls.hpp>

struct PROMEMORIA
{
        char Testo[1024];
        DataClass datalarm;
        HourClass oralarm;

        PROMEMORIA(void)
        {
                this->datalarm="00/00/0000";
                this->oralarm="24:60";
                this->Testo[0]='\0';
        };

        PROMEMORIA(char *str)
        {
                AnsiString obj=str;
                this->datalarm=obj.SubString(1,10);
                this->oralarm=obj.SubString(12,5);
                strcpy(this->Testo,obj.SubString(18,obj.Length()).c_str());
        };

        void operator=(PROMEMORIA &obj)
        {
                this->datalarm=obj.datalarm;
                this->oralarm=obj.oralarm;
                strcpy(this->Testo,obj.Testo);
        };
        /*void friend operator=(const PROMEMORIA &obj1, const PROMEMORIA &obj2)
        {
                obj1.datalarm=obj2.datalarm;
                obj1.oralarm=obj2.oralarm;
                strcpy(obj1.Testo,obj2.Testo);
        };*/

        bool operator>(PROMEMORIA &obj)
        {
                if(this->datalarm>obj.datalarm)
                        return true;
                else
                {
                        if(this->datalarm==obj.datalarm)
                        {
                                if(this->oralarm>obj.oralarm)
                                        return true;
                                return false;
                        }
                }
                return false;
        };

        bool operator<(PROMEMORIA &obj)
        {
                if(this->datalarm<obj.datalarm)
                        return true;
                else
                {
                        if(this->datalarm==obj.datalarm)
                        {
                                if(this->oralarm<obj.oralarm)
                                        return true;
                                return false;
                        }
                }
                return false;
        };

        bool operator==(PROMEMORIA &obj)
        {
                if((this->datalarm==obj.datalarm)&&(this->oralarm==obj.oralarm))
                        return true;
                return false;
        };
};
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
typedef struct list_element
{
        int index;
        PROMEMORIA value;
        struct list_element  *next;
}item;


typedef item*  list;

class ListaALARM
{
public:
        list root, last, l;

        ListaALARM()
        {
                root=NULL;
                last=NULL;
        }

        void Add(PROMEMORIA e)
        {
/*                list t;
                t=(list)malloc(sizeof(item));
                t->value=e;
                t->next=root;
                root=t;*/
                if(root==NULL)
                {
                        root=(list)malloc(sizeof(item));
                        root->value=e;
                        root->next=NULL;
                        root->index=0;
                        last=root;
                }
                else
                {
                        list t;
                        t=(list)malloc(sizeof(item));
                        t->value=e;
                        t->index=last->index+1;
                        t->next=NULL;
                        last->next=t;
                        last=t;
                }

        };

        int Count()
        {
                if(this->isEmpty())
                        return 0;
                l=root;
                int counter=1;
                while (l->next!=NULL)
                {
                        counter++;
                        l=l->next;
                }
                return counter;
        };

        void Clear()
        {
                FREE();
                root=NULL;
                last=NULL;
                l=NULL;
        }

        void FREE()
        {
                l=root;
                do
                {
                        l=l->next;
                        root->next=NULL;
                        free(root);
                        root=l;
                }
                while (l->next!=NULL);
                free(last);
        }

        bool isEmpty()
        {
                if(root==NULL)
                        return true;
                return false;
        }

        int writeOnFile(char *fileName)
        {
                FILE *f;
                if((f=fopen(fileName,"wb"))==NULL)
                        return -1;
                l=root;
                while(l->next!=NULL)
                {
                        fwrite(&(l->value),sizeof(PROMEMORIA),1,f);
                        l=l->next;
                }
                fwrite(&(last->value),sizeof(PROMEMORIA),1,f);
                fclose(f);
                return 0;
        }

        int openFromFile(char *fileName)
        {
                FILE *f;
                PROMEMORIA dato;
                if((f=fopen(fileName,"rb"))==NULL)
                        return access(fileName,04);
                if(!this->isEmpty())
                        this->Clear();
                while((fread(&dato,sizeof(PROMEMORIA),1,f))>0)
                        this->Add(dato);
                fclose(f);
                return 0;
        }

        ListaALARM* getAlarmToday()
        {
                if(this->isEmpty())
                        return NULL;
                bool trovato=false;
                DataClass today;
                ListaALARM *lstToday;
                lstToday = new ListaALARM();
                today=FormatDateTime("dd/mm/yyyy",Now());
                l=root;
                while(l->next!=NULL)
                {
                        if(l->value.datalarm==today)
                        {
                                trovato=true;
                                lstToday->Add(l->value);
                        }
                        l=l->next;
                }
                if(last->value.datalarm==today)
                {
                        trovato=true;
                        lstToday->Add(last->value);
                }
                if(!trovato)
                {
                        free(lstToday);
                        lstToday=NULL;
                }
                return lstToday;
        }

        PROMEMORIA* getMallocStruct()
        {
                if(this->isEmpty())
                        return NULL;
                PROMEMORIA *strc;
                strc=(PROMEMORIA *)malloc(sizeof(PROMEMORIA)*this->Count());
                l=root;
                int i=0;
                while(l->next!=NULL)
                {
                        strc[i]=l->value;
                        l=l->next;
                        i++;
                }
                strc[i]=last->value;
                return strc;
        }

        PROMEMORIA getMemo(int index)
        {
                PROMEMORIA strc;
                bool trovato=false;
                l=root;
                if(last->index==index)
                        strc=last->value;
                while(l->next!=NULL)
                {
                        if(l->index==index)
                        {
                                trovato=true;
                                break;
                        }
                        l=l->next;
                }
                if(trovato)
                        strc=l->value;
                return strc;
        }

        AnsiString getTextMemo(DataClass data, HourClass ora)
        {
                l=root;
                AnsiString app;
                bool trovato=true;
                while(l!=NULL)
                {
                        if((l->value.datalarm==data)&&(l->value.oralarm==ora))
                        {
                                trovato=true;
                                app=l->value.Testo;
                                break;
                        }
                        l=l->next;
                }
                if((last->value.datalarm==data)&&(last->value.oralarm==ora))
                {
                        trovato=true;
                        app=last->value.Testo;
                }
                if(trovato)
                        return app;
                return "";
        }

        void Delete(DataClass data, HourClass ora)
        {
                list a;
                a=root;
                l=a->next;
                while(l->next!=NULL)
                {
                        if((a->value.datalarm==data)&&(a->value.oralarm==ora))
                        {
                                a->next=l->next;
                                l->next=NULL;
                                free(l);
                                break;
                        }
                        a=a->next;
                        l=a->next;
                }
                if((l->value.datalarm==data)&&(l->value.oralarm==ora))
                {
                        a->next=NULL;
                        free(l);
                        last=a;
                }
        }

        PROMEMORIA* ControlMemo(AnsiString now_h)
        {
                bool trovato=true;
                l=root;
                PROMEMORIA *prm=NULL;
                HourClass now_hour;
                now_hour=now_h;
                while(l->next!=NULL)
                {
                        if(l->value.oralarm==now_hour)
                        {
                                trovato=true;
                                break;
                        }
                }
                if(last->value.oralarm==now_hour)
                {
                        prm = new PROMEMORIA(l->value);
                        return prm;
                }
                if(trovato)
                        prm = new PROMEMORIA(l->value);
                return prm;
        }
};
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
void Visualizza();


class TfrmMain : public TForm
{
__published:	// IDE-managed Components
        TPanel *panTime;
        TMainMenu *MainMenu;
        TMenuItem *mnuPromemoria;
        TMenuItem *mnuPromemoriaItem1;
        TMenuItem *mnuPromemoriaItem2;
        TMenuItem *mnuPromemoriaItem3;
        TMenuItem *mnuPromemoriaItem4;
        TMenuItem *mnuPromemoriaItem5;
        TPanel *Panel1;
        TPanel *Panel2;
        TSplitter *Splitter1;
        TPanel *Panel3;
        TListView *lstViewAlarm;
        TMemo *txtProMemo;
        TTimer *tmrControlAlarm;
        TTimer *tmrData;
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall mnuPromemoriaItem1Click(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall lstViewAlarmClick(TObject *Sender);
        void __fastcall tmrControlAlarmTimer(TObject *Sender);
        void __fastcall tmrDataTimer(TObject *Sender);
        void __fastcall mnuPromemoriaItem2Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
        ListaALARM *DB_ALARM, *DB_ALARMTODAY;
        char pathFileDB[MAX];
        __fastcall TfrmMain(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfrmMain *frmMain;
//---------------------------------------------------------------------------
#endif
