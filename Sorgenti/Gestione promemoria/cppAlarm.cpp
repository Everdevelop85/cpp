//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "cppAlarm.h"
#include "cppInsert.h"
#include "cppShowMemo.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TfrmMain *frmMain;
//---------------------------------------------------------------------------
__fastcall TfrmMain::TfrmMain(TComponent* Owner)
        : TForm(Owner)
{
        DB_ALARM = new ListaALARM();
}

AnsiString formattigtext(char *t)
{
        AnsiString app;
        t[0]-=32;
        int i=0;
        while(t[i]!=' ')
                i++;
        t[i+4]-=32;
        app=t;
        return app;
}

void Visualizza()
{
        int n=frmMain->DB_ALARM->Count();
        PROMEMORIA *strc=frmMain->DB_ALARM->getMallocStruct();
        TListItem  *ListItem;
        frmMain->lstViewAlarm->Clear();
        int ind=1;
        for(int i=0;i<n;i++)
        {
                ListItem = frmMain->lstViewAlarm->Items->Add();
                ListItem->Caption = IntToStr(ind);
                ListItem->SubItems->Add(strc[i].datalarm.getString());
                ListItem->SubItems->Add(strc[i].oralarm.getString());
                ind++;
        }
        free(strc);
}
//---------------------------------------------------------------------------
void __fastcall TfrmMain::FormCreate(TObject *Sender)
{
        panTime->Caption=formattigtext(FormatDateTime("dddd dd mmmm yyyy - hh:nn",Now()).c_str());
        getcwd(pathFileDB,MAX);
        strcat(pathFileDB,"\\PROMEMORIA_DB.DAT");
        if((DB_ALARM->openFromFile(pathFileDB))<0)
                creat(pathFileDB,0644);
        if((DB_ALARMTODAY=DB_ALARM->getAlarmToday())==NULL)
                tmrControlAlarm->Enabled=false;
        else
                tmrControlAlarm->Enabled=true;
        Visualizza();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::mnuPromemoriaItem1Click(TObject *Sender)
{
        frmInsert->Show();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::FormClose(TObject *Sender, TCloseAction &Action)
{
        if(!DB_ALARM->isEmpty())
        {
                if(DB_ALARM->writeOnFile(pathFileDB)!=0)
                        MessageBox(frmMain->Handle,"Attenzione! Errore nell'inserimeto dei promemoria su file","Errore",MB_ICONERROR+MB_OK);
                DB_ALARM->FREE();
        }
}
//---------------------------------------------------------------------------


void __fastcall TfrmMain::lstViewAlarmClick(TObject *Sender)
{
        DataClass d(lstViewAlarm->ItemFocused->SubItems->Strings[0]);
        HourClass h(lstViewAlarm->ItemFocused->SubItems->Strings[1]);
        frmMain->txtProMemo->Text=DB_ALARM->getTextMemo(d,h);
}
//---------------------------------------------------------------------------


void __fastcall TfrmMain::tmrControlAlarmTimer(TObject *Sender)
{
        if(DB_ALARMTODAY==NULL)
        {
                tmrControlAlarm->Enabled=false;
                return;
        }
        if(DB_ALARMTODAY->isEmpty())
        {
                tmrControlAlarm->Enabled=false;
                return;
        }
        PROMEMORIA *alm;
        alm=DB_ALARMTODAY->ControlMemo(FormatDateTime("hh:mm",Now()));
        if(alm!=NULL)
        {
                AnsiString app;
                app=alm->datalarm.getString()+"  "+alm->oralarm.getString();
                app.Insert("\n\n",app.Length()+1);
                app.Insert(alm->Testo,app.Length()+1);
                frmShowMemo->txtText->Lines->Text=app;
                frmShowMemo->Show();
                tmrControlAlarm->Enabled=false;
        }
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::tmrDataTimer(TObject *Sender)
{
        panTime->Caption=formattigtext(FormatDateTime("dddd dd mmmm yyyy - hh:nn:ss",Now()).c_str());        
}
//---------------------------------------------------------------------------


void __fastcall TfrmMain::mnuPromemoriaItem2Click(TObject *Sender)
{
        PROMEMORIA app=DB_ALARM->getMemo(StrToInt(lstViewAlarm->ItemFocused->Caption));
        frmInsert->txtOraAllarme->Text=app.oralarm.getString();
        frmInsert->mntCalendar->Date=app.datalarm.getString();
        frmInsert->txtTextAlarm->Text=app.Testo;
        frmInsert->Show();
}
//---------------------------------------------------------------------------

