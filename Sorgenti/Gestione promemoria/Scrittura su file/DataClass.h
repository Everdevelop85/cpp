class DataClass
{
        public:
                int giorno;
                int mese;
                int anno;

        DataClass(void)
        {
                this->giorno=0;
                this->mese=0;
                this->anno=0;
        }

        void operator=(DataClass &obj)
        {
                this->giorno=obj.giorno;
                this->mese=obj.mese;
                this->anno=obj.anno;
        }

        void operator=(const char *s)
        {
                char gg[3],mm[3],aaaa[10];
                gg[0]=s[0];
                gg[1]=s[1];
                gg[2]='\0';
                mm[0]=s[3];
                mm[1]=s[4];
                mm[2]='\0';
                int i=6;
                while(s[i]!='\0')
                {
                        aaaa[i-6]=s[i];
                        i++;
                }
                aaaa[i-6]='\0';
                this->giorno=atoi(gg);
                this->mese=atoi(mm);
                this->anno=atoi(aaaa);
        }

        /*void operator=(AnsiString &str)
        {
                this->giorno=StrToInt(str.SubString(0,1));
                this->mese=StrToInt(str.SubString(3,4));
                this->anno=StrToInt(str.SubString(6,str.Length()));
        }*/

        bool operator==(DataClass &obj)
        {
                if((this->giorno==obj.giorno)&&(this->mese==obj.mese)&&(this->anno==obj.anno))
                        return true;
                return false;
        }

        bool operator>(DataClass &obj)
        {
                bool confirm=false;
                if(this->anno>obj.anno)
                        confirm=true;
                if(this->anno<obj.anno)
                        confirm=false;
                if(this->anno==obj.anno)
                {
                        if(this->mese>obj.mese)
                                confirm=true;
                        if(this->mese<obj.mese)
                                confirm=false;
                        if(this->mese==obj.mese)
                        {
                                if(this->giorno>obj.giorno)
                                        confirm=true;
                                if(this->giorno<=obj.giorno)
                                        confirm=false;
                        }
                }
                return confirm;
        }

        bool operator<(DataClass &obj)
        {
                bool confirm=false;
                if(this->anno<obj.anno)
                        confirm=true;
                if(this->anno>obj.anno)
                        confirm=false;
                if(this->anno==obj.anno)
                {
                        if(this->mese<obj.mese)
                                confirm=true;
                        if(this->mese>obj.mese)
                                confirm=false;
                        if(this->mese==obj.mese)
                        {
                                if(this->giorno<obj.giorno)
                                        confirm=true;
                                if(this->giorno>=obj.giorno)
                                        confirm=false;
                        }
                }
                return confirm;
        }

        /*AnsiString getString()
        {
                AnsiString g,m;
                if(this->giorno<10)
                        g="0"+IntToStr(this->giorno);
                else
                        g=IntToStr(this->giorno);
                if(this->mese<10)
                        m="0"+IntToStr(this->mese);
                else
                        m=IntToStr(this->mese);
                return g+"/"+m+"/"+this->anno;
        }*/
};
