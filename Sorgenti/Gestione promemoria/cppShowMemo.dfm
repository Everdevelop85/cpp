object frmShowMemo: TfrmShowMemo
  Left = 640
  Top = 339
  BorderStyle = bsDialog
  Caption = 'frmShowMemo'
  ClientHeight = 303
  ClientWidth = 353
  Color = cl3DLight
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object txtText: TMemo
    Left = 0
    Top = 0
    Width = 353
    Height = 303
    Align = alClient
    BevelInner = bvNone
    BevelOuter = bvNone
    BorderStyle = bsNone
    Color = clOlive
    Font.Charset = ANSI_CHARSET
    Font.Color = clYellow
    Font.Height = -21
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
  end
end
