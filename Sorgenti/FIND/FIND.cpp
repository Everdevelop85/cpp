//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

//---------------------------------------------------------------------------

#pragma argsused

#include <stdlib.h>
#include <conio.h>
#include <stdio.h>
#include <string.h>
#include <dir.h>
#include <dos.h>
#define MAX 255

//-----------------------------------------------------------------------------
//Nodi
//-----------------------------------------------------------------------------
struct FILE_SYS
{
   char name[150];
   char path[MAX];

   void operator =(FILE_SYS &dato)
   {
      strcpy(name,dato.name);
      strcpy(path,dato.path);
   }

};


typedef struct list_element
{
        FILE_SYS value;
        struct list_element  *next;
}item;
typedef item*  list;

//Classe Database
//-----------------------------------------------------------------------------
class ListFile
{
public:
        list root;

//-----------------------------------------------------------------------------
//Primitive
//-----------------------------------------------------------------------------

        ListFile()
        {
                root=NULL;
        };
        void Add(FILE_SYS e)
        {
                list t;
                t=(list)malloc(sizeof(item));
                t->value=e;
                t->next=root;
                root=t;
        };

        void Clear()
        {
                if(root==NULL)
                        return;
                list l;
                l=root;
                while(l!=NULL)
                {
                        l=l->next;
                        free(root);
                        root=l;

                }
                if(root!=NULL)
                        free(root);
        };

        int Count()
        {
                list l;
                l=root;
                int counter=0;
                while(l!=NULL)
                {
                        counter++;
                        l=l->next;
                }
                return counter;
        };

        bool IsEmpty()
        {
                if(root==NULL)
                        return true;
                return false;
        };
//-----------------------------------------------------------------------------
//Metodi
//-----------------------------------------------------------------------------
        void Print()
        {
                list l;
                l=root;
                while(l!=NULL)
                {
                        printf("%s\t->\t%s\n",l->value.name,l->value.path);
                        l=l->next;
                }
        }
}*listaF;

char *nameLCase(char n[])
{
        int i=0;
        char lcn[MAX];
        while(n[i]!='\0')
        {
                if((n[i]>='A')&&(n[i]<='Z'))
                        lcn[i]=(char)(n[i]-31);
                else
                        lcn[i]=n[i];
                i++;
        }
        lcn[i]='\0';
        return &(lcn[0]);
}

int esplora(char *p, char *w, ListFile *l)
{
        struct ffblk ff;
        char pth[MAX],ext[MAX];
        FILE_SYS dato;
        strcpy(ext,p);
        strcat(ext,"\\*.*");
        if((findfirst(ext,&ff,FA_ARCH|FA_DIREC|FA_RDONLY|FA_HIDDEN))!=0)
                return 0;
        do
        {
                if(ff.ff_name[0]!='.')
                {
                        gotoxy(2,2);printf("Controllo: %s\\%s\n",p,ff.ff_name);
                        if((strstr(ff.ff_name,w))!=NULL)
                        {
                                strcpy(dato.name,ff.ff_name);
                                strcpy(dato.path,p);
                                l->Add(dato);
                        }
                }

        }
        while((findnext(&ff))==0);
        strcpy(pth,p);
        if((findfirst(ext,&ff,FA_ARCH|FA_DIREC|FA_RDONLY|FA_HIDDEN))!=0)
                return 0;
        do
        {
                if((strchr(ff.ff_name,'.'))==NULL)
                {
                        strcpy(ext,pth);
                        strcat(ext,"\\");
                        strcat(ext,ff.ff_name);
                        esplora(ext,w,l);
                }
        }
        while((findnext(&ff))==0);
        return 0;
};


class TLOGOThread : public TThread
{
        public:
                int time;
                bool finished;
                TLOGOThread(): TThread(true)
                {
                        time=1000000;
                        printf("\n   Ricerca in corso...");
                        Rectangle();
                }

void Rectangle()
{
        gotoxy(3,3);
        printf("%c",218);
        for(int i=1;i<74;i++)
                printf("%c",196);
        printf("%c",191);
        gotoxy(77,4);
        printf("%c",179);
        gotoxy(3,4);
        printf("%c",179);
        gotoxy(3,5);
        printf("%c",192);
        for(int i=1;i<74;i++)
                printf("%c",196);
        gotoxy(77,5);
        printf("%c",217);
}

void Scroll()
{
        gotoxy(4,4);
        for(int i=0;i<time;i++);;
        printf("%c",176);
        for(int i=0;i<time;i++);;
        gotoxy(4,4);
        printf("%c",177);
        gotoxy(5,4);
        printf("%c",176);
        for(int i=0;i<time;i++);;
        gotoxy(4,4);
        printf("%c",219);
        gotoxy(5,4);
        printf("%c",177);
        gotoxy(6,4);
        printf("%c",176);
        for(int i=0;i<time;i++);;
        gotoxy(4,4);
        printf("%c",177);
        gotoxy(5,4);
        printf("%c",219);
        gotoxy(6,4);
        printf("%c",177);
        gotoxy(7,4);
        printf("%c",176);
        for(int i=0;i<time;i++);;
        gotoxy(4,4);
        printf("%c",176);
        gotoxy(5,4);
        printf("%c",177);
        gotoxy(6,4);
        printf("%c",219);
        gotoxy(7,4);
        printf("%c",177);
        gotoxy(8,4);
        printf("%c",176);
        for(int i=0;i<time;i++);;
        for(int i=4;i<=71;i++)
        {
                gotoxy(i,4);
                printf(" %c%c%c%c%c",176,177,219,177,176);
                for(int i=0;i<time;i++);;
        }
        gotoxy(72,4);
        printf(" %c",176);
        gotoxy(74,4);
        printf("%c",177);
        gotoxy(75,4);
        printf("%c",219);
        gotoxy(76,4);
        printf("%c",177);
        for(int i=0;i<time;i++);;
        gotoxy(72,4);
        printf("  %c",176);
        gotoxy(75,4);
        printf("%c",177);
        gotoxy(76,4);
        printf("%c",219);
        for(int i=0;i<time;i++);;
        gotoxy(72,4);
        printf("   %c",176);
        gotoxy(76,4);
        printf("%c",177);
        for(int i=0;i<time;i++);;
        gotoxy(72,4);
        printf("    %c",176);
        for(int i=0;i<time;i++);;
        gotoxy(72,4);
        printf("     ",176);

}
                virtual void __fastcall Execute()
                {
                        while(true)
                        {
                                Scroll();
                        }
                }
};

class TSearchThread : public TThread
{
        public:
                char arv1[255], arv2[255];
                ListFile *listaF;
                bool finished;
                TSearchThread(char *arg1, char *arg2): TThread(true)
                {

                        strcpy(arv1,arg1);
                        strcpy(arv2,arg2);
                        listaF = new ListFile();

                }

                virtual void __fastcall Execute()
                {
                        finished=false;
                        //TLOGOThread *thL = new TLOGOThread();
                        //thL->Resume();
                        system("cls");
                        if(!(esplora(arv1,arv2,listaF)))
                        {
                          //      thL->Suspend();
                           //     thL->Terminate();
                                system("cls");                           
                                listaF->Print();
                                printf("\n\nTrovati %d risultati per \"%s\"\n\n",listaF->Count(),arv2);
                                listaF->Clear();
                        }
                        else
                        {
                             //   thL->Terminate();
                                printf("Errore nell\' epslorazione\n");
                        }
                        Finished();
                }

                virtual void __fastcall Finished()
                {
                        finished=true;
                        this->Terminate();
                }

};

//============================================================================
// PROGRAM MAIN
//============================================================================

ListFile* getPaths()
{
        struct ffblk ff;
        ListFile *l = new ListFile();
        FILE_SYS el;
        strcpy(el.name,"\\");
        strcpy(el.path,"C:");
        l->Add(el);
        findfirst("C:\\Documents and Settings\\*.*",&ff,FA_DIREC);
        do
        {
                if(ff.ff_name[0]!='.')
                {
                        if(((strcmp(ff.ff_name,"All Users"))!=0)&&
                           ((strcmp(ff.ff_name,"Default User"))!=0)&&
                           ((strcmp(ff.ff_name,"LocalService"))!=0)&&
                           ((strcmp(ff.ff_name,"NetworkService"))!=0))
                        {
                                strcpy(el.name,ff.ff_name);
                                strcpy(el.path,"C:\\Documents and Settings");
                                l->Add(el);
                        }
                }

        }
        while((findnext(&ff))==0);
        strcpy(el.name,"Documenti");
        strcpy(el.path,l->root->value.path);
        strcat(el.path,"\\");
        strcat(el.path,l->root->value.name);
        l->Add(el);
        return l;
}

int menu(ListFile *l)
{
        int i=1, c;
        list ls;
        ls=l->root;
        printf("\n\tCominciare la ricerca dai seguenti percorsi:");
        while(ls!=NULL)
        {
                printf("\n\t%d. %s\\%s",i,ls->value.path,ls->value.name);
                ls=ls->next;
                i++;
        }
        printf("\n\t4. Sceli tu il percorso");
        printf("\n\t5. Esci\n\n");
        printf("SCELTA --> ");
        scanf("%d",&c);
        return c;
}

char* getString()
{
        char app[1000];
        int c, i=0;
        while((c=getch())!=13)
        {
                if(c==8)
                {
                        printf("%c %c",8,8);
                        i--;
                }
                else
                {
                        app[i]=(char)c;
                        printf("%c",c);
                        i++;
                }
        }
        app[i]='\0';
        return &(app[0]);
}

int main(int argc, char *argv[])
{
        char par1[255], par2[255];
	if(argc!=3)
	{
                if(argc==2)
                {
                        ListFile *l= getPaths();
                        int m=menu(l);
                        switch(m)
                        {
                                case 1:
                                        strcpy(par1,l->root->value.path);
                                        strcat(par1,"\\");
                                        strcat(par1,l->root->value.name);
                                break;

                                case 2:
                                        l->root=l->root->next;
                                        strcpy(par1,l->root->value.path);
                                        strcat(par1,"\\");
                                        strcat(par1,l->root->value.name);
                                break;

                                case 3:
                                        for(int i=0;i<2;i++)
                                                l->root=l->root->next;
                                        strcpy(par1,l->root->value.path);
                                        strcat(par1,"\\");
                                        strcat(par1,l->root->value.name);
                                break;

                                case 4:
                                        printf("\n\nInserire il percorso:\n\n  -->");
                                        strcpy(par1,getString());
                                break;

                                case 5: exit(0);
                        }
                        l->Clear();
                        free(l);
                        strcpy(par2,argv[1]);
                }
                else
                {
        		printf("Errore nel numero di parametri\n");
                		return 1;
                }

	}
        else
        {
                strcpy(par1,argv[1]);
                strcpy(par2,argv[2]);
        }
        if(par1[(strlen(par1))-1]=='\\')
                par1[(strlen(par1))-1]='\0';
        TSearchThread *thd = new TSearchThread(par1,par2);
        thd->Resume();
        system("pause");
	return 0;
}
//============================================================================
