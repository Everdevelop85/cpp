object Form1: TForm1
  Left = 192
  Top = 114
  Width = 696
  Height = 480
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Scaled = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 200
    Top = 32
    Width = 441
    Height = 313
    DataSource = DataSource1
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object ADOConnection1: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=michele;Persist Security Info=True;' +
      'User ID=sa;Initial Catalog=provaCSharp;Data Source=ZEUS\SQL2005;' +
      'Use Procedure for Prepare=1;Auto Translate=True;Packet Size=4096' +
      ';Workstation ID=APOLLO;Use Encryption for Data=False;Tag with co' +
      'lumn collation when possible=False'
    Provider = 'SQLOLEDB.1'
    Left = 112
    Top = 200
  end
  object ADOTable1: TADOTable
    Active = True
    Connection = ADOConnection1
    CursorType = ctStatic
    TableName = 'tab1'
    Left = 112
    Top = 160
  end
  object DataSource1: TDataSource
    DataSet = ADOQuery1
    Left = 136
    Top = 40
  end
  object ADOQuery1: TADOQuery
    Active = True
    Connection = ADOConnection1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT nome,tipo'
      'FROM tab1 JOIN tab2 ON tab1.id=tab2.id'
      'ORDER BY nome')
    Left = 48
    Top = 288
  end
end
