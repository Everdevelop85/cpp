#include <stdlib.h>
#include <stdio.h>
#include <string.h>

class TIME
{
        public:
                int hh;
                int mm;
                int ss;

        TIME(void)
        {
                hh=0;
                mm=0;
                ss=0;
        };

        TIME(int h, int m, int s)
        {
                hh=h;
                mm=m;
                ss=s;
        };

        TIME(int s)
        {
                hh=s/3600;
                mm=(s%3600)/60;
                ss=(s%3600)%60;
        };

        const int GetSecond()
        {
                return (hh*3600)+(mm*60)+ss;
        }

        void operator=(const char *t)
        {
                int i=0,l;
                char h[5],m[5],s[5];
                while(t[i]!=':')
                {
                        h[i]=t[i];
                        i++;
                }
                h[i]='\0';
                i++;
                l=i;
                while((t[i]!=':')&&(t[i]!='.'))
                {
                        m[i-l]=t[i];
                        i++;
                }
                m[i]='\0';
                i++;
                l=i;
                while(t[i]!='\0')
                {
                        s[i-l]=t[i];
                        i++;
                }
                s[i]='\0';
                hh=atoi(h);
                mm=atoi(m);
                ss=atoi(s);
        }
        void operator=(const TIME t)
        {
                hh=t.hh;
                mm=t.mm;
                ss=t.ss;
        }
        friend TIME operator+(TIME &t1, TIME &t2)
        {
                TIME ris(t1.GetSecond()+t2.GetSecond());
                return ris;
        };
        friend TIME operator*(TIME &t1, TIME &t2)
        {
                TIME ris(t1.GetSecond()*t2.GetSecond());
                return ris;
        };
        friend TIME operator/(TIME &t1, TIME &t2)
        {
                TIME ris(t1.GetSecond()/t2.GetSecond());
                return ris;
        };
        friend TIME operator*(TIME &t1, const float &sc)
        {
                TIME ris(t1.GetSecond()*sc);
                return ris;
        };
        friend TIME operator/(TIME &t1, const float &sc)
        {
                TIME ris(t1.GetSecond()/sc);
                return ris;
        };
        friend int printf(char *arg, TIME &value)
        {
                char app[255], *rest;
                int i=0,l;
                while(arg[i]!='\0')
                {
                        if(i>0)
                        {
                                if((arg[i]=='T')&&(arg[i-1]=='%'))
                                {
                                        l=i-1;
                                        break;
                                }
                        }
                        app[i]=arg[i];
                        i++;
                }
                app[l]='\0';
                if(value.hh<10)
                        strcat(app,"0%d:");
                else
                        strcat(app,"%d:");
                if(value.mm<10)
                        strcat(app,"0%d.");
                else
                        strcat(app,"%d.");
                if(value.ss<10)
                        strcat(app,"0%d");
                else
                        strcat(app,"%d");
                if((rest=strstr(arg,"%T"))!=NULL)
                        rest+=2;
                strcat(app,rest);
                return printf(app,value.hh,value.mm,value.ss);
        };

};

int menu()
{
        int s;
        printf("1. CALCOLA TEMPI E INFORMAZIONI DELLA CORSA\n");
        printf("2. CALCOLATRICE DI TEMPI\n");
        printf("3. ESCI\n\n");
        printf("Scelta: ");
        scanf("%d",&s);
        return s;
}

int main(void)
{
        int m;
        char tempo[12];
        do
        {
                system("CLS");
                m=menu();
                system("CLS");
                switch(m)
                {
                        case 1:
                                float km,v,app;
                                printf("Tempo della corsa: ");
                                scanf("%s",tempo);
                                printf("Chilometri percorsi: ");
                                scanf("%f",&km);
                                TIME t,tk;
                                t=tempo;
                                system("CLS");
                                tk=t/km;
                                printf("INFORMAZIONI CORSA\n\n");
                                printf("\t%.2f Km in ",km);
                                printf("%T\n\n",t);
                                printf("\tTempo al chilometro: %T\n\n",tk);
                                app=tk.GetSecond();
                                v=3600/app;
                                printf("\tVelocitÓ media: %.2f Km/h\n\n\n\t\t",v);
                                system("PAUSE");
                        break;
                };
        }
        while(m!=3);
        return 0;
}
 