/*
#include <dir.h>
#include <dos.h>
#include <math.h>
#include <io.h>
*/
//-----------------------------------------------------------------------------
//Costanti
//-----------------------------------------------------------------------------
#define BYTE_MODE 0
#define KBYTE_MODE 1
#define MBYTE_MODE 2
//-----------------------------------------------------------------------------



//=============================================================================
//CLASSE
//=============================================================================
class FileOnSystem
{
        public:
                AnsiString name;
                AnsiString path;
                AnsiString FullPath;

        void operator =(char *fn)
        {
                int ln=strlen(fn)-1;
                FullPath=fn;
                name=fn;
                this->path=name;
                while(fn[ln]!='\\')
                        ln--;
                path=path.SubString(1,ln);
                name=name.SubString(ln+2,name.Length());
        };

        void operator =(AnsiString fn)
        {
                int ln=fn.Length()-1;
                while(fn[ln]!='\\')
                        ln--;
                FullPath=fn;
                name=fn.SubString(ln+1,fn.Length()) ;
                path=fn;
                path.SetLength(ln);
        };

        private: void IntToBin(int n, int bin[])
        {
                int i=0;
                while((n>0)||(i<16))
                {
                        bin[i]=(n%2);
                        n/=2;
                        i++;
                }
        };

        public: bool FileExist()
        {
                if((access(FullPath.c_str(),0))==0)
                        return true;
                return false;
        };

        public: AnsiString GetNameWithouthExtenction()
        {
		return name.SubString(1,name.Pos(".")-1);
        };

        public: AnsiString GetLastModifyData()
        {
                AnsiString data="";
                int bin[16],g=0,m=0,a=0,i;
                struct ffblk ff;
                if((findfirst(this->FullPath.c_str(),&ff,FA_ARCH))==0)
                {
                        IntToBin(ff.ff_fdate,bin);
                        for(i=0;i<5;i++)
                                g+=((pow(2,i))*bin[i]);
                        for(i=5;i<9;i++)
                                m+=((pow(2,i-5))*bin[i]);
                        for(i=9;i<16;i++)
                                a+=((pow(2,i-9))*bin[i]);
                        a+=1980;
                        if(g<10)
                                data+="0"+IntToStr(g)+"\\";
                        else
                                data+=IntToStr(g)+"\\";
                        if(m<10)
                                data+="0"+IntToStr(m)+"\\";
                        else
                                data+=IntToStr(m)+"\\";
                        data+=IntToStr(a);
                }
                return data;
        };

        public: AnsiString GetLastModifyOur()
        {
                AnsiString ora="";
                int bin[16],h=0,m=0,s=0,i;
                struct ffblk ff;
                if((findfirst(this->FullPath.c_str(),&ff,FA_ARCH))==0)
                {
                        IntToBin(ff.ff_ftime,bin);
                        for(i=0;i<5;i++)
                                s+=((pow(2,i))*bin[i]);
                        for(i=5;i<12;i++)
                                m+=((pow(2,i-5))*bin[i]);
                        for(i=11;i<16;i++)
                                h+=((pow(2,i-11))*bin[i]);
                        if(h<10)
                                ora+="0"+IntToStr(h)+":";
                        else
                                ora+=IntToStr(h)+":";
                        if(m<10)
                                ora+="0"+IntToStr(m)+".";
                        else
                                ora+=IntToStr(m)+".";
                        if(s<10)
                                ora+="0"+IntToStr(s);
                        else
                                ora+=IntToStr(s);
                }
                return ora;
        };

        float FileSize(const int mode)
        {
                float ret=0.0;
                struct ffblk ff;
                if((findfirst(this->FullPath.c_str(),&ff,FA_ARCH))!=0)
                        return -1;
                switch(mode)
                {
                        case BYTE_MODE: ret=ff.ff_fsize; break;
                        case KBYTE_MODE:
                                ret=ff.ff_fsize;
                                ret/=1024;
                        break;
                        case MBYTE_MODE:
                                ret=ff.ff_fsize;
                                ret/=1048576;
                        break;
                };
                return ret;
        };
};
//=============================================================================

