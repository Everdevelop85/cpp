object frmMain: TfrmMain
  Left = 173
  Top = 120
  Width = 711
  Height = 583
  Caption = 'Ricerca nei files'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 0
    Top = 343
    Width = 703
    Height = 8
    Cursor = crVSplit
    Align = alBottom
    Beveled = True
  end
  object Panel1: TPanel
    Left = 0
    Top = 351
    Width = 703
    Height = 205
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object lstFilesRecover: TListView
      Left = 0
      Top = 0
      Width = 703
      Height = 205
      Align = alClient
      Columns = <
        item
          Caption = 'Nome file'
          Width = 200
        end
        item
          Caption = 'Percorso'
          Width = 250
        end
        item
          Caption = 'Dimensione'
          Width = 80
        end
        item
          Caption = 'Ultima modifica'
          Width = 200
        end>
      FlatScrollBars = True
      PopupMenu = PopupMenu
      TabOrder = 0
      ViewStyle = vsReport
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 703
    Height = 343
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Splitter2: TSplitter
      Left = 312
      Top = 0
      Width = 8
      Height = 343
      Cursor = crHSplit
      Beveled = True
      OnMoved = Splitter2Moved
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 312
      Height = 343
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
      object Splitter3: TSplitter
        Left = 0
        Top = 214
        Width = 312
        Height = 9
        Cursor = crVSplit
        Align = alBottom
        Beveled = True
      end
      object Panel5: TPanel
        Left = 0
        Top = 223
        Width = 312
        Height = 120
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 0
        object lstFiles: TFileListBox
          Left = 0
          Top = 0
          Width = 312
          Height = 120
          Align = alClient
          ItemHeight = 13
          MultiSelect = True
          PopupMenu = PopupMenu
          TabOrder = 0
        end
      end
      object Panel6: TPanel
        Left = 0
        Top = 0
        Width = 312
        Height = 214
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object lstDir: TDirectoryListBox
          Left = 0
          Top = 25
          Width = 312
          Height = 189
          Align = alClient
          ItemHeight = 16
          TabOrder = 0
          OnChange = lstDirChange
        end
        object Panel7: TPanel
          Left = 0
          Top = 0
          Width = 312
          Height = 25
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object cboDrive: TDriveComboBox
            Left = -1
            Top = 0
            Width = 313
            Height = 19
            AutoDropDown = True
            TabOrder = 0
            OnChange = cboDriveChange
          end
        end
      end
    end
    object Panel4: TPanel
      Left = 320
      Top = 0
      Width = 383
      Height = 343
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 383
        Height = 129
        Align = alTop
        Caption = 'Parametri di ricerca'
        TabOrder = 0
        object txtFileName: TLabeledEdit
          Left = 16
          Top = 40
          Width = 193
          Height = 21
          EditLabel.Width = 47
          EditLabel.Height = 13
          EditLabel.Caption = 'Nome file:'
          LabelPosition = lpAbove
          LabelSpacing = 3
          TabOrder = 0
        end
        object txtTextInside: TLabeledEdit
          Left = 16
          Top = 88
          Width = 353
          Height = 21
          EditLabel.Width = 148
          EditLabel.Height = 13
          EditLabel.Caption = 'Parole o frase all'#39'interno del file:'
          LabelPosition = lpAbove
          LabelSpacing = 3
          TabOrder = 1
        end
      end
      object GroupBox2: TGroupBox
        Left = 0
        Top = 129
        Width = 383
        Height = 166
        Align = alClient
        Caption = 'Specifiche'
        TabOrder = 1
        object ckbRecSearch: TCheckBox
          Left = 16
          Top = 24
          Width = 145
          Height = 17
          Caption = 'Ricerca nelle sottocartelle'
          TabOrder = 0
        end
        object ckbSintax: TCheckBox
          Left = 16
          Top = 48
          Width = 113
          Height = 17
          Caption = 'Nome file corretto'
          TabOrder = 1
        end
        object ckbLUCase: TCheckBox
          Left = 16
          Top = 72
          Width = 193
          Height = 17
          Caption = 'Distingui tra maiuscole e minuscole'
          TabOrder = 2
        end
        object ckbInExtenction: TCheckBox
          Left = 16
          Top = 96
          Width = 113
          Height = 17
          Caption = 'Includi l'#39'estensione'
          TabOrder = 3
        end
        object Animate: TAnimate
          Left = 264
          Top = 64
          Width = 80
          Height = 50
          Active = False
          CommonAVI = aviFindFolder
          StopFrame = 29
        end
      end
      object Panel8: TPanel
        Left = 0
        Top = 295
        Width = 383
        Height = 48
        Align = alBottom
        TabOrder = 2
        object cmdRicerca: TButton
          Left = 256
          Top = 8
          Width = 113
          Height = 33
          Caption = 'Inizia la ricerca'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          OnClick = cmdRicercaClick
        end
        object Button1: TButton
          Left = 136
          Top = 7
          Width = 113
          Height = 33
          Caption = 'Interrompi'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
        end
      end
    end
  end
  object PopupMenu: TPopupMenu
    Left = 312
    Top = 416
    object cmdExplorer: TMenuItem
      Caption = 'Esplora...'
    end
    object Apricon1: TMenuItem
      Caption = 'Apricon'
      object Blocconote1: TMenuItem
        Caption = 'Blocco note'
      end
      object Altro1: TMenuItem
        Caption = 'Altro...'
      end
    end
    object N1: TMenuItem
      Caption = '-'
    end
  end
end
