//---------------------------------------------------------------------------

#ifndef cppSearchOnFilesH
#define cppSearchOnFilesH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <FileCtrl.hpp>
#include <ComCtrls.hpp>
#include <Menus.hpp>
//---------------------------------------------------------------------------
#define MAX 1024
//---------------------------------------------------------------------------
#include <dir.h>
#include <dos.h>
#include <io.h>
#include <fcntl.h>
#include <sys\stat.h>
#include <math.h>
#include "FileOnSystem.h"
//---------------------------------------------------------------------------
class TfrmMain : public TForm
{
__published:	// IDE-managed Components
        TPanel *Panel1;
        TSplitter *Splitter1;
        TPanel *Panel2;
        TPanel *Panel3;
        TSplitter *Splitter2;
        TPanel *Panel4;
        TPanel *Panel5;
        TSplitter *Splitter3;
        TPanel *Panel6;
        TDirectoryListBox *lstDir;
        TPanel *Panel7;
        TDriveComboBox *cboDrive;
        TFileListBox *lstFiles;
        TListView *lstFilesRecover;
        TGroupBox *GroupBox1;
        TGroupBox *GroupBox2;
        TPanel *Panel8;
        TLabeledEdit *txtFileName;
        TLabeledEdit *txtTextInside;
        TCheckBox *ckbRecSearch;
        TCheckBox *ckbSintax;
        TCheckBox *ckbLUCase;
        TCheckBox *ckbInExtenction;
        TButton *cmdRicerca;
        TButton *Button1;
        TPopupMenu *PopupMenu;
        TMenuItem *cmdExplorer;
        TMenuItem *Apricon1;
        TMenuItem *Blocconote1;
        TMenuItem *Altro1;
        TMenuItem *N1;
        TAnimate *Animate;
        void __fastcall Splitter2Moved(TObject *Sender);
        void __fastcall cboDriveChange(TObject *Sender);
        void __fastcall lstDirChange(TObject *Sender);
        void __fastcall cmdRicercaClick(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
private:	// User declarations
public:		// User declarations
        //FileOnSystem *MATCHFOR;
        __fastcall TfrmMain(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfrmMain *frmMain;
//---------------------------------------------------------------------------
#endif

class TSearchThread : public TThread
{
        private:
        protected:
                void __fastcall Execute();
        public:
                __fastcall TSearchThread(bool CreateSuspended);
};

typedef struct list_element
{
        int index;
        FileOnSystem value;
        struct list_element  *next;
}item;


typedef item*  list;

class ListaFILE
{
public:
        list root, l, tail;

        ListaFILE()
        {
                root->index=-1;
                root->next=NULL;
        };

        void Add(FileOnSystem element)
        {
                if(root->index<0)
                {
                        root=(list)malloc(sizeof(item));
                        root->value=element;
                        root->index=0;
                        root->next=NULL;
                        tail=root;
                }
                else
                {
                        list t;
                        t=(list)malloc(sizeof(item));
                        t->value=element;
                        t->index=root->index+1;
                        t->next=NULL;
                        tail->next=t;
                        tail=t;
                }
        };

        int Count()
        {
                l=root;
                int counter=1;
                if(root->index<0)
                        return 0;
                while (l->next!=NULL)
                {
                        counter++;
                        l=l->next;
                }
                return counter;
        };

        void Clear()
        {
                while (root->next!=NULL)
                {
                        free(root);
                        root=root->next;
                }
                tail=root;
                root->next=NULL;
                root->index=-1;
        };

        void FREE()
        {
                while (root->next!=NULL)
                {
                        free(root);
                        root=root->next;
                }
                free(root);
        }

        bool isEmpty()
        {
                if(root->index<0)
                        return true;
                return false;
        }

        FileOnSystem GetFile(int index)
        {
                if(root->index==index)
                        return root->value;
                if(tail->index==index)
                        return tail->value;
                l=root->next;
                while (l->next!=NULL)
                {
                        if(l->index==index)
                                break;
                        l=l->next;
                }
                return l->value;
        }

        FileOnSystem *GetFiles()
        {
                FileOnSystem *strc;
                strc=(FileOnSystem *) malloc((sizeof(FileOnSystem))*Count());
                l=root;
                int i=0;
                while (l->next!=NULL)
                {
                        strc[i]=l->value;
                        l=l->next;
                        i++;
                }
                strc[i]=l->value;
                return strc;
        }

/*private:

        char *GetExtenction(char *s)
        {
                char ext[5];
                int l=strlen(s);
                while(s[l-1]!='.')
                        l--;
                s+=l;
                return s;
        }

        char *GetFileName(char *s)
        {
                char ext[5];
                int l=strlen(s);
                while(s[l-1]!='\\')
                        l--;
                s+=l;
                return s;
        }

        char *GetPath(char *s)
        {
                char ext[5];
                int l=strlen(s);
                while(s[l-1]!='\\')
                        l--;
                s-=l;
                return s;
        }

        char *GetSize(long sz)
        {
                float szmb=0.0;
                szmb=sz/1048576;
                char app2[20];
                //app=FloatToStr(szmb).c_str();

        }*/

};
