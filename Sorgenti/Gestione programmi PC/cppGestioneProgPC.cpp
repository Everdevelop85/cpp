//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "cppGestioneProgPC.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TfrmMain *frmMain;
//---------------------------------------------------------------------------
__fastcall TfrmMain::TfrmMain(TComponent* Owner)
        : TForm(Owner)
{
        Lista = new ListaFILE();
}
//---------------------------------------------------------------------------

int GetIndexImg(char ext[])
{
        if((strcmp(ext,"EXE"))==0)
                return 0;
        else
        {
                if((strcmp(ext,"ZIP"))==0)
                        return 1;
                else
                {
                        if((strcmp(ext,"RAR"))==0)
                                return 2;
                        else
                        {
                                if((strcmp(ext,"NRG"))==0)
                                        return 3;
                                else
                                {
                                        if((strcmp(ext,"ISO"))==0)
                                                return 4;
                                }
                        }
                }
        }
        return 5;
}

AnsiString GetExt(char file[])
{
        int l=(strlen(file))-3, i=0;
        char estenction[4];
        for(i=0;i<3;i++)
                estenction[i]=file[l+i];
        estenction[i]='\0';
        return UpperCase(estenction);
}

long GetFileSize(char f[])
{
        struct ffblk ff;
        findfirst(f,&ff,0);
        return ff.ff_fsize/1024;
}

AnsiString GetFileSize(long s)
{
        float size;
        AnsiString app;
        app=s;
        //size=s;
        //size=s/1024.0;
        //app=FloatToStrF(size,3,10,0);
        return app+"  KB";
}

/*AnsiString GetFileSize(long s)
{
        AnsiString app;
        char conv[20],rev[20];
        int len, ind=0, p=1, l=0;
        app=s;
        strcpy(conv,app.c_str());
        len=strlen(conv);
        for(int i=len-1;i>-1;i--)
        {
            ind++;
            l++;
            rev[(len-i-p)]=conv[i];
            if(ind==3)
            {
                rev[l]='\0';
                l++;
                strcat(rev,"\'");
                ind=0;
                p--;
            }
        }
        rev[l]='\0';
        len=strlen(rev);
        for(int i=0;i<len;i++)
                conv[i]=rev[(len-i-1)];
        conv[len]='\0';
        app=conv;
        return app+" Kb";

}*/

AnsiString MakePath(char s1[], char s2[], char s3[])
{
  getcwd(s1,MAX);
  strcat(s1,"\\");
  getcwd(s2,MAX);
  strcat(s2,"\\");
  getcwd(s3,MAX);
  strcat(s3,"\\");
  strcat(s1,"DataBasePRG.dat");
  strcat(s2,"DataBaseP.dat");
  strcat(s3,"Temp.dat");
  return "*.exe;*.zip;*.rar;*.nrg;*.iso";
}

void BrowseFolder(char p[])
{
        BROWSEINFO bi;
	LPITEMIDLIST pidl;

  	bi.hwndOwner      = frmMain->Handle;
  	bi.pidlRoot       = NULL;
  	bi.pszDisplayName = p;
  	bi.lpszTitle      = "Seleziona la cartella che contiene\ni tuoi programmi";
  	bi.ulFlags        = BIF_RETURNONLYFSDIRS;
  	bi.lpfn           = NULL;
  	bi.lParam         = 0;
  	bi.iImage         = 0;

  	pidl = SHBrowseForFolder(&bi);

        SHGetPathFromIDList(pidl, p);
}

void ControlloPATH(STR_PATH *P, FILE *temp)
{
   STR_PATH pathT;
   bool trovato=false;
   fseek(temp,0,SEEK_SET);
   while(trovato || (fread(&pathT,sizeof(STR_PATH),1,temp)>0))
   {
       if(strcmp(*(P),pathT)==0)
       {
        trovato=true;
        break;
       }
   }
     if(trovato)
     {
        int d;
        d=strlen(*P);
        while((*P)[d]!='\\')
          d--;
        (*P)[d]='\0';
        ControlloPATH(P, temp);
     }
}

void Exploration(STR_PATH *P, FILE *file, FILE *temp)
{
    int i;
    frmMain->lstDir->Directory=*P;
    if(frmMain->lstFile->Items->Count>0)
    {
      fwrite(P,sizeof(STR_PATH),1,file);
      frmMain->RecordCount=frmMain->RecordCount+frmMain->lstFile->Items->Count;
    }
    if(frmMain->lstDir->Items->Count>frmMain->lstDir->ItemIndex)
    {
       for(i=frmMain->lstDir->ItemIndex+1;i<frmMain->lstDir->Items->Count;i++)
       {
          strcat(*(P),"\\");
          strcat(*(P),frmMain->lstDir->Items->Strings[i].c_str());
          Exploration(P,file,temp);
       }
       fwrite(P,sizeof(STR_PATH),1,temp);
       ControlloPATH(P,temp);
       frmMain->lstDir->Directory=*P;
    }
    else
    {
       fwrite(P,sizeof(STR_PATH),1,temp);
       ControlloPATH(P,temp);
       frmMain->lstDir->Directory=*P;
    }
}

int MakeFile(char p[], char pd[])
{
   FILE *f;
   FILE *fdir;
   INF_FILE datoPRG;
   STR_PATH datoS;
   int ind=0;
   if((f=fopen(p,"wb"))==NULL)
   {
      MessageBox(frmMain->Handle,"Errore di apertura nel file: void MakeFile(char p[], char pd[])","Attenzione",49);
      exit(1);
   }
   else
   {
      if((fdir=fopen(pd,"rb"))==NULL)
      {
         fclose(f);
         MessageBox(frmMain->Handle,"Errore di apertura nel file:","Attenzione",49);
         exit(1);
      }
   }
   while((fread(&datoS,sizeof(STR_PATH),1,fdir))>0)
   {
        frmMain->lstDir->Directory=datoS;
        for(int i=0;i<frmMain->lstFile->Items->Count;i++)
            Lista->Add((frmMain->lstFile->Directory+"\\"+frmMain->lstFile->Items->Strings[i]).c_str());
   }
   fclose(fdir);
   fclose(f);
   return ind;
}

void ShowFile(char s[])
{
   FILE *f;
   FILE_PRG dato;
   if((f=fopen(s,"rb"))==NULL)
        exit(1);
   TListItem  *ListItem;
   frmMain->lsvProgrammi->Items->Clear();  
   while((fread(&dato,sizeof(FILE_PRG),1,f))>0)
   {
        ListItem = frmMain->lsvProgrammi->Items->Add();
        ListItem->Caption = dato.nome;
        ListItem->SubItems->Add(dato.path);
        ListItem->SubItems->Add(dato.tipo);
        ListItem->SubItems->Add(GetFileSize(dato.size));
        ListItem->ImageIndex=GetIndexImg(dato.tipo);
   }
   fclose(f);
}

//---------------------------------------------------------------------------
void __fastcall TfrmMain::FormCreate(TObject *Sender)
{
        Graphics::TBitmap *Bitmap32 = new Graphics::TBitmap();
        Graphics::TBitmap *Bitmap16 = new Graphics::TBitmap();
        Bitmap32->LoadFromFile("Icons\\Icons32.bmp");
        Bitmap16->LoadFromFile("Icons\\Icons16.bmp");
        imlIcons32->Add(Bitmap32,NULL);
        imlIcons16->Add(Bitmap16,NULL);
        lsvProgrammi->SmallImages=imlIcons16;
        lsvProgrammi->LargeImages=imlIcons32;
        lsvProgrammi->ViewStyle=vsReport;
        lstFile->Mask=MakePath(pathDataBase,pathDataDir,pathTemp);
        ShowFile(pathDataBase);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::Button1Click(TObject *Sender)
{
  STR_PATH app;
  FILE *file, *temp;
  if((file=fopen(pathDataDir,"wb"))==NULL)
    exit(1);
  if((temp=fopen(pathTemp,"wb"))!=NULL)
  {
    fclose(temp);
    if((temp=fopen(pathTemp,"r+b"))==NULL)
    {
      fclose(file);
      exit(1);
    }
  }
  else
  {
   fclose(file);
   exit(1);
  }
  BrowseFolder(app);
  if(&(app)[0]!='\0')
  {
    Exploration(&app,file,temp);
    fclose(temp);
    fclose(file);
    remove(pathTemp);
    RecordCount=MakeFile(pathDataBase,pathDataDir);
    ShowFile(pathDataBase);
  }
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::lstDirChange(TObject *Sender)
{
        lstFile->Directory=lstDir->Directory;          
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::Button2Click(TObject *Sender)
{
        lsvProgrammi->ViewStyle=vsIcon;
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::lsvProgrammiColumnClick(TObject *Sender,
      TListColumn *Column)
{
        if(Column->Index==0)
                lsvProgrammi->SortType=stText;
        if(Column->Index==1)
                lsvProgrammi->SortType=stData;
        if(Column->Index==2)
                lsvProgrammi->SortType=stBoth;

        lsvProgrammi->Refresh();
}
//---------------------------------------------------------------------------

