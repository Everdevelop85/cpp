//---------------------------------------------------------------------------
#ifndef cppGestioneProgPCH
#define cppGestioneProgPCH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <ImgList.hpp>
//---------------------------------------------------------------------------
#include <stdlib.h>
#include <stdio.h>
#include <conio.h>
#include <string.h>
#include <shlobj.h>
#include <shellapi.h>
#include <dir.h>
#include <FileCtrl.hpp>
#include <fcntl.h>
#include <io.h>
#include <FileCtrl.hpp>
#define MAX 256
//---------------------------------------------------------------------------
struct INF_FILE
{
        char name[200];
        char path[MAX];
        char ext[5];
        float size;

   void operator = (INF_FILE &dato)
   {
      strcpy(name,dato.name);
      strcpy(path,dato.path);
      strcpy(ext,dato.ext);
      size=dato.size;
   }
};

typedef struct list_element
{
        int index;
        INF_FILE value;
        struct list_element  *next;
}item;

typedef item*  list;



typedef char STR_PATH[MAX];

int GetIndexImg(char ext[]);
AnsiString GetExt(char file[]);
AnsiString MakePath(char s1[], char s2[], char s3[]);
int MakeFile(char p[], char pd[]);
long GetFileSize(char f[]);
AnsiString GetFileSize(long s);

class TfrmMain : public TForm
{
__published:	// IDE-managed Components
        TListView *lsvProgrammi;
        TImageList *imlIcons32;
        TImageList *imlIcons16;
        TFileListBox *lstFile;
        TDirectoryListBox *lstDir;
        TButton *Button1;
        TButton *Button2;
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall Button1Click(TObject *Sender);
        void __fastcall lstDirChange(TObject *Sender);
        void __fastcall Button2Click(TObject *Sender);
        void __fastcall lsvProgrammiColumnClick(TObject *Sender,
          TListColumn *Column);
private:	// User declarations
public:		// User declarations
        char pathDataBase[MAX],pathDataDir[MAX],pathTemp[MAX];
        int RecordCount;

        __fastcall TfrmMain(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfrmMain *frmMain;
//---------------------------------------------------------------------------
#endif

/*#include <iostream.h>
#include <conio.h>
#include <dir.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX 256*/

class ListaFILE
{
public:
        list root, l;

        ListaFILE()
        {
                root->index=-1;
                root->next=NULL;
        };

        void Add(char fileN[])
        {
                struct ffblk ff;
                list t;
                t=(list)malloc(sizeof(item));
                strcpy(ff.ff_name,fileN);
                strcpy(t->value.name,GetFileName(fileN));
                strcpy(t->value.path,GetPath(fileN));
                strcpy(t->value.ext,GetExtenction(fileN));
                t->value.size=GetSize(ff.ff_fsize);
                t->next=root;
                t->index=root->index+1;
                root=t;
        };

        void Add(INF_FILE element)
        {
                struct ffblk ff;
                list t;
                t=(list)malloc(sizeof(item));
                t->value=element;
                t->next=root;
                t->index=root->index+1;
                root=t;
        };

        int Count()
        {
                l=root;
                int counter=0;
                while (l->next!=NULL)
                {
                        counter++;
                        l=l->next;
                }
                return counter;
        };

        void FREE()
        {
                while (root->next!=NULL)
                {
                        free(root);
                        root=root->next;
                }
                free(root);
        }

        INF_FILE *GetMallocStruct()
        {
                INF_FILE *strc;
                strc=(INF_FILE *) malloc((sizeof(INF_FILE))*Count());
                l=root;
                int i=0;
                while (l->next!=NULL)
                {
                        strc[i]=l->value;
                        l=l->next;
                        i++;
                }
                return strc;
        }

        int WriteOnFile(char *FileName)
        {
                FILE *f;
                if((f=fopen(FileName,"wb"))==NULL)
                        return -1;
                l=root;
                while (l->next!=NULL)
                {
                        fwrite(&(l->value),sizeof(INF_FILE),1,f);
                        l=l->next;
                }
                fclose(f);
                return 0;
        }

        int OpenFromFile(char *FileName)
        {
                FILE *f;
                INF_FILE dato;
                FREE();
                if((f=fopen(FileName,"rb"))==NULL)
                        return -1;
                l=root;
                while ((fread(&dato,sizeof(INF_FILE),1,f))>0)
                        Add(dato);
                fclose(f);
                return 0;
        }

        void ViewListItem(TListItem  *ListItem)
        {
                l=root;
                while (l->next!=NULL)
                {
                        fwrite(&(l->value),sizeof(INF_FILE),1,f);
                        l=l->next;
                }
                while((fread(&dato,sizeof(FILE_PRG),1,f))>0)
                ListItem = frmMain->lsvProgrammi->Items->Add();
                ListItem->Caption = dato.nome;
                ListItem->SubItems->Add(dato.path);
                ListItem->SubItems->Add(dato.tipo);
                ListItem->SubItems->Add(GetFileSize(dato.size));
                ListItem->ImageIndex=GetIndexImg(dato.tipo);
        }

private:

        char *GetExtenction(char *s)
        {
                char ext[5];
                int l=strlen(s);
                while(s[l-1]!='.')
                        l--;
                s+=l;
                return s;
        }

        char *GetFileName(char *s)
        {
                char ext[5];
                int l=strlen(s);
                while(s[l-1]!='\\')
                        l--;
                s+=l;
                return s;
        }

        char *GetPath(char *s)
        {
                char ext[5];
                int l=strlen(s);
                while(s[l-1]!='\\')
                        l--;
                s-=l;
                return s;
        }

        /*char *GetSize(long sz)
        {
                float szmb=0.0;
                char app[100];
                int i=0;
                szmb=sz/1048576;
                strcpy(app,FloatToStr(szmb).c_str());
                while(app[i]!='.')
                        i++;
                i+2;
                app[i]='\0';
                return app;
        }*/
        float GetSize(long sz)
        {
                return sz/1048576;
        }
};
ListaFILE *Lista;
