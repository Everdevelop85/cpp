//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormCreate(TObject *Sender)
{
 Graphics::TBitmap *Bitmap1 = new Graphics::TBitmap();
 Bitmap1->LoadFromFile("Icons16.bmp");
 ImageList1->Add(Bitmap1,NULL);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button1Click(TObject *Sender)
{
 Graphics::TBitmap *Bitmap1 = new Graphics::TBitmap();
 Bitmap1->LoadFromFile("Icons32.bmp");
 ImageList1->Clear();
 ImageList1->Width=32;
 ImageList1->Height=32;
 ImageList1->Add(Bitmap1,NULL);
        ListView1->SmallImages=NULL;
        ListView1->LargeImages=ImageList1;
        ListView1->ViewStyle=vsIcon;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button2Click(TObject *Sender)
{
 Graphics::TBitmap *Bitmap1 = new Graphics::TBitmap();
 Bitmap1->LoadFromFile("Icons16.bmp");
 ImageList1->Clear();
 ImageList1->Width=16;
 ImageList1->Height=16;
 ImageList1->Add(Bitmap1,NULL);
        ListView1->LargeImages=NULL;
        ListView1->SmallImages=ImageList1;
        ListView1->ViewStyle=vsReport;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button3Click(TObject *Sender)
{
        /*TListItem *lista;
        lista = new TListItem();
        lista.Caption="prova inseroimento";
        lista.ImageIndex=4;
        lista.Index=ListView1->Items->Count-1;
        ListView1->Items->*/
        TListItem  *ListItem;
  for (int i = 0; i <6 ; i++)
  {
    ListItem = ListView1->Items->Add();
    ListItem->Caption = "prova "+IntToStr(i);
    ListItem->ImageIndex=5;
  }

}
//---------------------------------------------------------------------------
