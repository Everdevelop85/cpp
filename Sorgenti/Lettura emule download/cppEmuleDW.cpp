//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "cppEmuleDW.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TfrmEmuleDW *frmEmuleDW;
//---------------------------------------------------------------------------
__fastcall TfrmEmuleDW::TfrmEmuleDW(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TfrmEmuleDW::tmrControlTimer(TObject *Sender)
{
        lblTimeInfo->Caption=FormatDateTime("dddd d mmmm yyyy   hh:mm", Now());
}
//---------------------------------------------------------------------------
void __fastcall TfrmEmuleDW::FormCreate(TObject *Sender)
{
        lblTimeInfo->Caption=FormatDateTime("dddd d mmmm yyyy   hh:mm", Now());
        if(pop3->Connected)
                pop3->Disconnect();
        pop3->UserID="luc.zan";
        pop3->Host="pop.tiscali.it";
        pop3->Password="davide";
        pop3->Port=110;
        //pop3->Connect();
}
//---------------------------------------------------------------------------
void __fastcall TfrmEmuleDW::pop3AuthenticationFailed(bool &Handled)
{
        stbPOP->Panels->Items[0]->Text="Stato connessione: AUTENTICAZIONE FALLITA";        
}
//---------------------------------------------------------------------------

void __fastcall TfrmEmuleDW::pop3AuthenticationNeeded(bool &Handled)
{
        stbPOP->Panels->Items[0]->Text="Stato connessione: RICHIESTA AUTENTICAZIONE";
}
//---------------------------------------------------------------------------

void __fastcall TfrmEmuleDW::pop3Connect(TObject *Sender)
{
        stbPOP->Panels->Items[0]->Text="Stato connessione: CONNESSO";        
}
//---------------------------------------------------------------------------

void __fastcall TfrmEmuleDW::pop3ConnectionFailed(TObject *Sender)
{
        stbPOP->Panels->Items[0]->Text="Stato connessione: CONNESSIONE FALLITA";        
}
//---------------------------------------------------------------------------

void __fastcall TfrmEmuleDW::pop3ConnectionRequired(bool &Handled)
{
        stbPOP->Panels->Items[0]->Text="Stato connessione: RICHISTA CONNESSIONE";        
}
//---------------------------------------------------------------------------

void __fastcall TfrmEmuleDW::pop3DecodeEnd(TObject *Sender)
{
        stbPOP->Panels->Items[0]->Text="Stato connessione: FINE DECODIFICA";
}
//---------------------------------------------------------------------------

void __fastcall TfrmEmuleDW::pop3DecodeStart(AnsiString &FileName)
{
        stbPOP->Panels->Items[0]->Text="Stato connessione: INIZIO DECODIFICA \""+FileName+"\"";
}
//---------------------------------------------------------------------------

void __fastcall TfrmEmuleDW::pop3Disconnect(TObject *Sender)
{
        stbPOP->Panels->Items[0]->Text="Stato connessione: DISCONNESSO";        
}
//---------------------------------------------------------------------------

void __fastcall TfrmEmuleDW::pop3Failure(TObject *Sender)
{
        stbPOP->Panels->Items[0]->Text="Stato connessione: ERRORE";        
}
//---------------------------------------------------------------------------

void __fastcall TfrmEmuleDW::pop3InvalidHost(bool &Handled)
{
        stbPOP->Panels->Items[0]->Text="Stato connessione: HOST NON VALIDO";        
}
//---------------------------------------------------------------------------

void __fastcall TfrmEmuleDW::pop3PacketRecvd(TObject *Sender)
{
        stbPOP->Panels->Items[0]->Text="Stato connessione: PACCHETTO RICEVUTO";        
}
//---------------------------------------------------------------------------

void __fastcall TfrmEmuleDW::FormClose(TObject *Sender,
      TCloseAction &Action)
{
        if(pop3->Connected)
                pop3->Disconnect();        
}
//---------------------------------------------------------------------------


