//---------------------------------------------------------------------------

#ifndef cppEmuleDWH
#define cppEmuleDWH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <ComCtrls.hpp>
#include <NMpop3.hpp>
#include <Psock.hpp>
//---------------------------------------------------------------------------
class TfrmEmuleDW : public TForm
{
__published:	// IDE-managed Components
        TStaticText *lblTimeInfo;
        TTimer *tmrControl;
        TListView *lstView;
        TNMPOP3 *pop3;
        TStatusBar *stbPOP;
        void __fastcall tmrControlTimer(TObject *Sender);
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall pop3AuthenticationFailed(bool &Handled);
        void __fastcall pop3AuthenticationNeeded(bool &Handled);
        void __fastcall pop3Connect(TObject *Sender);
        void __fastcall pop3ConnectionFailed(TObject *Sender);
        void __fastcall pop3ConnectionRequired(bool &Handled);
        void __fastcall pop3DecodeEnd(TObject *Sender);
        void __fastcall pop3DecodeStart(AnsiString &FileName);
        void __fastcall pop3Disconnect(TObject *Sender);
        void __fastcall pop3Failure(TObject *Sender);
        void __fastcall pop3InvalidHost(bool &Handled);
        void __fastcall pop3PacketRecvd(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
private:	// User declarations
public:		// User declarations
        __fastcall TfrmEmuleDW(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfrmEmuleDW *frmEmuleDW;
//---------------------------------------------------------------------------
#endif

class DATA
{
        public:
                int giorno;
                int mese;
                int anno;

        public: DATA(int g, int m, int a)
        {
                giorno=g;
                mese=m;
                anno=a;
        }

        DATA& operator= (const char *s)
        {
                char gg[3],mm[3],aaaa[5];
                gg[0]=s[0];
                gg[1]=s[1];
                gg[2]='\0';
                mm[0]=s[3];
                mm[1]=s[4];
                mm[2]='\0';
                aaaa[0]=s[6];
                aaaa[1]=s[7];
                aaaa[2]=s[8];
                aaaa[3]=s[9];
                aaaa[4]='\0';
                this->giorno=atoi(gg);
                this->mese=atoi(mm);
                this->anno=atoi(aaaa);
                return *this;
        }

        public: AnsiString getData()
        {
                return IntToStr(giorno)+"/"+IntToStr(mese)+"/"+IntToStr(anno);
        }

};

class ORA
{
        public:
                int ore;
                int minuti;
                int secondi;

        ORA(int o, int m, int s)
        {
                ore=o;
                minuti=m;
                secondi=s;
        }

        ORA& operator= (const char s[])
        {
                char hh[3],mm[3],ss[3];
                hh[0]=s[0];
                hh[1]=s[1];
                hh[2]='\0';
                mm[0]=s[3];
                mm[1]=s[4];
                mm[2]='\0';
                ss[0]=s[6];
                ss[1]=s[7];
                ss[2]='\0';
                this->ore=atoi(hh);
                this->minuti=atoi(mm);
                this->secondi=atoi(ss);
                return *this;
        }

        public: AnsiString getOra()
        {
                return IntToStr(ore)+"/"+IntToStr(minuti)+"/"+IntToStr(secondi);
        }
};

class eMuleFILE
{
        public:
                char name[255];
                char type[5];
                DATA *dt_arrivo;
                ORA *or_arrivo;

        public: eMuleFILE(char *file, char *d, char *h)
        {
                int i=0;
                while(file[i]!='.')
                {
                        name[i]=file[i];
                        i++;
                }
                name[i]='\0';
                type[0]=file[i];
                type[1]=file[i+1];
                type[2]=file[i+2];
                type[3]='\0';
                dt_arrivo = new DATA(0,0,0);
                or_arrivo = new ORA(0,0,0);
                *dt_arrivo=d;
                *or_arrivo=h;
        }

        bool operator== (const eMuleFILE &emf)
        {
                if(((strcmp(this->name,emf.name))==0))
                {
                        if((dt_arrivo->giorno==emf.dt_arrivo->giorno)&&(dt_arrivo->mese==emf.dt_arrivo->mese)&&(dt_arrivo->anno==emf.dt_arrivo->anno))
                        {
                                if((or_arrivo->ore==emf.or_arrivo->ore)&&(or_arrivo->minuti==emf.or_arrivo->minuti)&&(or_arrivo->secondi==emf.or_arrivo->secondi))
                                        return true;
                        }
                }
                return false;
        }

        bool operator> (const eMuleFILE &emf)
        {
                if((dt_arrivo->anno<=emf.dt_arrivo->anno))
                {
                        if(dt_arrivo->mese<=emf.dt_arrivo->mese)
                        {
                                if(dt_arrivo->giorno<=emf.dt_arrivo->giorno)
                                {
                                        if(or_arrivo->ore<=emf.or_arrivo->ore)
                                        {
                                                if(or_arrivo->minuti<=emf.or_arrivo->minuti)
                                                {
                                                        if(or_arrivo->secondi<=emf.or_arrivo->secondi)
                                                                return false;
                                                }
                                        }
                                }
                        }
                }
                return true;
        }
};

typedef struct list_element
{
        eMuleFILE *value;
        struct list_element  *next;
}item;
typedef item* list;


//Classe Lista
//-----------------------------------------------------------------------------
class DBListMESSAGE
{
private:
        list root, l, end;

//-----------------------------------------------------------------------------
//Primitive
//-----------------------------------------------------------------------------
public:
        DBListMESSAGE(void)
        {
                root=NULL;
                end=NULL;
                l=root;
        };

        void Add(eMuleFILE e)
        {

                list t;
                t=(list)malloc(sizeof(item));
                t->value=e;
                t->next=NULL;
                t->index=0;
                if(root==NULL)
                        root=t;
                else
                {
                        if(root->next==NULL)
                        {

                                if(t->value->dt_arrivo>root->value->dt_arrivo)
                                {
                                        root->next=t;
                                        end=t;
                                }
                                else
                                {
                                        if(t->value->or_arrivo>root->value->or_arrivo)
                                        {
                                                root->next=t;
                                                end=t;
                                        }
                                        else
                                        {
                                                end=root;
                                                t->next=root;
                                                root=t;
                                        }
                                }
                        }
                        else
                        {
                                list prev;
                                prev=root;
                                l=prev->next;
                                if((strcmp(t->value.name,l->value.name))<0)
                                {
                                        t->next=prev;
                                        root=t;
                                }
                                else
                                {
                                        while((l!=NULL) && ((strcmp(t->value.name,l->value.name))>=0))
                                        {
                                                prev=l;
                                                l=l->next;
                                        }
                                        if(l==NULL)
                                        {
                                                end->next=t;
                                                end=t;
                                        }
                                        else
                                        {
                                                prev->next=t;
                                                t->next=l;
                                        }
                                }
                        }
                        l=root;
                        int i=0;
                        while(l!=NULL)
                        {
                                l->index=i;
                                l=l->next;
                                i++;
                        }
                }
        };

        void Add(char *p)
        {
                FILE_MM dato;
                dato=p;
                Add(dato);
        };

        void Destroy()
        {
                if(root==NULL)
                        return;
                while(l!=NULL)
                {
                        free(root);
                        root=l;
                        l=root->next;
                }
                free(root);
        };

        int Count()
        {
                l=root;
                int counter=1;
                if(l==NULL)
                        return 0;
                while(l->next!=NULL)
                {
                        counter++;
                        l=l->next;
                }
                return counter;
        };

        void Clear()
        {
                if(root==NULL)
                        return;
                l=root->next;
                while(l!=NULL)
                {
                        free(root);
                        root=l;
                        l=root->next;
                }
                free(root);
                root=NULL;
                end=NULL;
                l=root;
        };

        bool IsEmpty()
        {
                if(root==NULL)
                        return true;
                return false;
        };
//-----------------------------------------------------------------------------
//Metodi
//-----------------------------------------------------------------------------
        void Delete(int el)
        {
                l=root;
                list l2;
                list_element *app;
                bool trovato=false;
                if(root->index==el)
                {
                        root=root->next;
                        free(l);
                }
                else
                {
                        while(l!=NULL)
                        {
                                l2=l;
                                l=l->next;
                                if(l->index==el)
                                {
                                        trovato=true;
                                        break;
                                }
                        }
                        if(trovato)
                        {
                            app=l->next;
                            free(l);
                            l2->next=app;
                        }
                }
        };
//-----------------------------------------------------------------------------
        void PrintQuerySearchPath(char *p)
        {
                int ind=1;
                l=root;
                while(l!=NULL)
                {
                        if((strcmp(l->value.path,p))==0)
                        {
                                frmMain->stgLista->Cells[0][ind]=l->index+1;
                                frmMain->stgLista->Cells[1][ind]=l->value.name;
                                frmMain->stgLista->RowCount++;
                                ind++;
                        }
                        l=l->next;
                }
                frmMain->stgLista->RowCount--;
        }
//-----------------------------------------------------------------------------
        void Print()
        {
                int ind=1;
                l=root;
                while(l!=NULL)
                {
                        frmMain->stgLista->Cells[0][ind]=l->index+1;
                        frmMain->stgLista->Cells[1][ind]=l->value.name;
                        frmMain->stgLista->RowCount++;
                        ind++;
                        l=l->next;
                }
                frmMain->stgLista->RowCount--;
        }
//-----------------------------------------------------------------------------
        AnsiString GetFileName(int ind)
        {
                AnsiString nm;
                l=root;
                nm="";
                while(l!=NULL)
                {
                        if(l->index==ind)
                        {
                                nm.Insert(l->value.path,0);
                                nm.Insert("\\",nm.Length()+1);
                                nm.Insert(l->value.name,nm.Length()+1);
                                break;
                        }
                        l=l->next;
                }
                return nm;
        }
//-----------------------------------------------------------------------------
        void Change(char *oldName, char *newName)
        {
                while(l!=NULL)
                {
                        if((strcmp(oldName,l->value.name))==0)
                        {
                                strcpy(l->value.name,newName);
                                break;
                        }
                        l=l->next;
                }
        }
//-----------------------------------------------------------------------------
        bool SearchForMatch(AnsiString match)
        {
                int ind=1;
                bool findMatch=false;
                l=root;
                while(l!=NULL)
                {
                        if((strstr(LowerCase(l->value.name).c_str(),match.LowerCase().c_str()))!=NULL)
                        {
                                findMatch=true;
                                frmMain->stgLista->Cells[0][ind]=l->index+1;
                                frmMain->stgLista->Cells[1][ind]=l->value.name;
                                frmMain->stgLista->RowCount++;
                                ind++;
                        }
                        l=l->next;
                }
                frmMain->stgLista->RowCount--;
                return findMatch;
        }
//-----------------------------------------------------------------------------
        float GetFileSizeMb(int ind)
        {
                FileOnSystem fileMp3;
                float size=-1;
                bool trovato=false;
                l=root;
                while(l!=NULL)
                {
                        if(l->index==ind)
                        {
                                trovato=true;
                                break;
                        }
                        l=l->next;
                }
                if(trovato)
                {
                        fileMp3.name=l->value.name;
                        fileMp3.path=l->value.path;
                        fileMp3.FullPath=fileMp3.path+"\\"+fileMp3.name;
                        size=fileMp3.FileSize(); 
                }
                return size;
        }
}; **/
