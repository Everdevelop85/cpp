object frmEmuleDW: TfrmEmuleDW
  Left = 306
  Top = 241
  Width = 555
  Height = 316
  Caption = 'eMule Download'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object lblTimeInfo: TStaticText
    Left = 0
    Top = 0
    Width = 547
    Height = 33
    Align = alTop
    Alignment = taCenter
    AutoSize = False
    BorderStyle = sbsSunken
    Caption = 'lblTimeInfo'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Arial Black'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
  end
  object lstView: TListView
    Left = 0
    Top = 33
    Width = 547
    Height = 216
    Columns = <>
    TabOrder = 1
  end
  object stbPOP: TStatusBar
    Left = 0
    Top = 264
    Width = 547
    Height = 18
    Panels = <
      item
        Width = 250
      end>
    SimplePanel = False
  end
  object tmrControl: TTimer
    Interval = 60000
    OnTimer = tmrControlTimer
    Left = 400
    Top = 8
  end
  object pop3: TNMPOP3
    Port = 110
    ReportLevel = 0
    OnDisconnect = pop3Disconnect
    OnConnect = pop3Connect
    OnInvalidHost = pop3InvalidHost
    OnConnectionFailed = pop3ConnectionFailed
    OnConnectionRequired = pop3ConnectionRequired
    OnPacketRecvd = pop3PacketRecvd
    Parse = False
    DeleteOnRead = False
    OnAuthenticationNeeded = pop3AuthenticationNeeded
    OnAuthenticationFailed = pop3AuthenticationFailed
    OnFailure = pop3Failure
    OnDecodeStart = pop3DecodeStart
    OnDecodeEnd = pop3DecodeEnd
    Left = 48
    Top = 48
  end
end
