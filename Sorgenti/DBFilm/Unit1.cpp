//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TfrmMain *frmMain;
//---------------------------------------------------------------------------
__fastcall TfrmMain::TfrmMain(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::FormCreate(TObject *Sender)
{
        ADOCon->Connected=true;
}
//---------------------------------------------------------------------------
void __fastcall TfrmMain::ADOConConnectComplete(TADOConnection *Connection,
      const Error *Error, TEventStatus &EventStatus)
{
        stsBar->Panels->Items[0]->Text="Connection database "+ADOCon->DefaultDatabase+": Connessione completata";
        ADODataSetFILMS->Active=true;
        DBGrid1->Columns->Items[0]->Width=200;
//        ADODataSetFILMS->Refresh();
//        DBGrid1->Refresh();
}
//---------------------------------------------------------------------------
void __fastcall TfrmMain::ADOConDisconnect(TADOConnection *Connection,
      TEventStatus &EventStatus)
{
        stsBar->Panels->Items[0]->Text="Connection database "+ADOCon->DefaultDatabase+": Disconnesso";
}
//---------------------------------------------------------------------------
void __fastcall TfrmMain::ADOConExecuteComplete(TADOConnection *Connection,
      int RecordsAffected, const Error *Error, TEventStatus &EventStatus,
      const _Command *Command, const _Recordset *Recordset)
{
        stsBar->Panels->Items[0]->Text="Connection database "+ADOCon->DefaultDatabase+": Esecuzione completata";        
}
//---------------------------------------------------------------------------
