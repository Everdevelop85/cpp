//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ADODB.hpp>
#include <DB.hpp>
#include <ComCtrls.hpp>
#include <DBGrids.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
//---------------------------------------------------------------------------
class TfrmMain : public TForm
{
__published:	// IDE-managed Components
        TADOConnection *ADOCon;
        TStatusBar *stsBar;
        TPanel *Panel1;
        TADODataSet *ADODataSetFILMS;
        TDataSource *DataSourceFILMS;
        TDBGrid *DBGrid1;
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall ADOConConnectComplete(TADOConnection *Connection,
          const Error *Error, TEventStatus &EventStatus);
        void __fastcall ADOConDisconnect(TADOConnection *Connection,
          TEventStatus &EventStatus);
        void __fastcall ADOConExecuteComplete(TADOConnection *Connection,
          int RecordsAffected, const Error *Error,
          TEventStatus &EventStatus, const _Command *Command,
          const _Recordset *Recordset);
private:	// User declarations
public:		// User declarations
        __fastcall TfrmMain(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfrmMain *frmMain;
//---------------------------------------------------------------------------
#endif
 