object frmMain: TfrmMain
  Left = 345
  Top = 105
  Width = 791
  Height = 495
  Caption = 'I miei film'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object stsBar: TStatusBar
    Left = 0
    Top = 442
    Width = 783
    Height = 19
    Panels = <
      item
        Text = 'Connection DB'
        Width = 50
      end>
    SimplePanel = False
  end
  object Panel1: TPanel
    Left = 80
    Top = 40
    Width = 457
    Height = 329
    BevelOuter = bvNone
    Caption = 'Panel1'
    TabOrder = 1
    object DBGrid1: TDBGrid
      Left = 0
      Top = 0
      Width = 457
      Height = 329
      Align = alClient
      DataSource = DataSourceFILMS
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
    end
  end
  object ADOCon: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=michele;Persist Security Info=True;' +
      'User ID=sa;Initial Catalog=DBFilm;Data Source=ZEUS\SQL2005;Use P' +
      'rocedure for Prepare=1;Auto Translate=True;Packet Size=4096;Work' +
      'station ID=APOLLO;Use Encryption for Data=False;Tag with column ' +
      'collation when possible=False'
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    OnDisconnect = ADOConDisconnect
    OnConnectComplete = ADOConConnectComplete
    OnExecuteComplete = ADOConExecuteComplete
  end
  object ADODataSetFILMS: TADODataSet
    Connection = ADOCon
    CursorType = ctStatic
    LockType = ltReadOnly
    CommandText = 'select * from films'
    Parameters = <>
    Left = 32
  end
  object DataSourceFILMS: TDataSource
    DataSet = ADODataSetFILMS
    Left = 32
    Top = 32
  end
end
