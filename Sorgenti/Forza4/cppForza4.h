//---------------------------------------------------------------------------
#ifndef cppForza4H
#define cppForza4H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Menus.hpp>
#include <ScktComp.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TfrmMain : public TForm
{
__published:	// IDE-managed Components
        TLabel *Label1;
        TLabel *Label2;
        TLabel *Label3;
        TLabel *Label4;
        TLabel *Label5;
        TLabel *Label6;
        TLabel *Label7;
        TMainMenu *MainMenu;
        TMenuItem *mnuConnection;
        TMenuItem *mnuConnectionItem1;
        TMenuItem *mnuConnectionItem2;
        TClientSocket *Client;
        TServerSocket *Server;
        TLabel *lblMsg;
        TMenuItem *mnuPartita;
        TMenuItem *mnuPartitaItem1;
        TLabel *lblPlayer1;
        TLabel *lblPlayer2;
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall Label1Click(TObject *Sender);
        void __fastcall Label2Click(TObject *Sender);
        void __fastcall mnuConnectionItem1Click(TObject *Sender);
        void __fastcall mnuConnectionItem2Click(TObject *Sender);
        void __fastcall ClientConnect(TObject *Sender,
          TCustomWinSocket *Socket);
        void __fastcall ClientConnecting(TObject *Sender,
          TCustomWinSocket *Socket);
        void __fastcall ClientDisconnect(TObject *Sender,
          TCustomWinSocket *Socket);
        void __fastcall ClientError(TObject *Sender,
          TCustomWinSocket *Socket, TErrorEvent ErrorEvent,
          int &ErrorCode);
        void __fastcall ClientRead(TObject *Sender,
          TCustomWinSocket *Socket);
        void __fastcall ClientWrite(TObject *Sender,
          TCustomWinSocket *Socket);
        void __fastcall ServerAccept(TObject *Sender,
          TCustomWinSocket *Socket);
        void __fastcall ServerClientConnect(TObject *Sender,
          TCustomWinSocket *Socket);
        void __fastcall ServerClientDisconnect(TObject *Sender,
          TCustomWinSocket *Socket);
        void __fastcall ServerClientError(TObject *Sender,
          TCustomWinSocket *Socket, TErrorEvent ErrorEvent,
          int &ErrorCode);
        void __fastcall ServerClientRead(TObject *Sender,
          TCustomWinSocket *Socket);
        void __fastcall ServerClientWrite(TObject *Sender,
          TCustomWinSocket *Socket);
        void __fastcall ServerListen(TObject *Sender,
          TCustomWinSocket *Socket);
        void __fastcall mnuPartitaItem1Click(TObject *Sender);
        void __fastcall Label3Click(TObject *Sender);
        void __fastcall Label4Click(TObject *Sender);
        void __fastcall Label5Click(TObject *Sender);
        void __fastcall Label6Click(TObject *Sender);
        void __fastcall Label7Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
        int matrix[8][9];
        __fastcall TfrmMain(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfrmMain *frmMain;
//---------------------------------------------------------------------------
#endif
