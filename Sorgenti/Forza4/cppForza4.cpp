//---------------------------------------------------------------------------
#include <vcl.h>

#pragma hdrstop

#include "cppForza4.h"
//#include <srting.h>
void GetMatrix(int v[8][9], TShape *s[6][7]);
int ins_pedina(int v[8][9], int y);
int controllo(int v[8][9], int x1, int y1, int *vittoria);
void reset(int v[8][9], TShape *s[6][7], int *riga, int*x, int *y, bool *turno, int *vittoria, int *perso);

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TfrmMain *frmMain;
TShape *f4[6][7];
TColor colore;
AnsiString IPAdress, messaggio, dati, ServerPlayer, ClientPlayer;
char xy[2];
bool turno;
int riga, x, y, punteggio1=0, punteggio2=0, vittoria=0, NumCl, perso;
//---------------------------------------------------------------------------
__fastcall TfrmMain::TfrmMain(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::FormCreate(TObject *Sender)
{
        frmMain->Color=0x8b0000;
        int i, j, top=33, left=-40;
        for(i=0;i<6;i++)
        {
          for(j=0;j<7;j++)
          {
            f4[i][j]= new TShape(this);
            f4[i][j]->Parent=this;
            f4[i][j]->Height=33;
            f4[i][j]->Width=33;
            left=left+33+15;
            f4[i][j]->Top=top;
            f4[i][j]->Left=left;
            f4[i][j]->Shape=stEllipse;
            f4[i][j]->Brush->Color=clBlack;
          }
          left=-40;
          top=top+33+7;
        }
        reset(matrix,f4,&riga,&x,&y,&turno,&vittoria,&perso);
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
//FUNZIONI
//---------------------------------------------------------------------------
void GetMatrix(int v[8][9], TShape *s[6][7])
{
  int i,j;
  for(i=1;i<7;i++)
  {
    for(j=1;j<8;j++)
    {
      if(s[i-1][j-1]->Brush->Color==clRed)
        v[i][j]=2;
      if(s[i-1][j-1]->Brush->Color==clYellow)
        v[i][j]=1;
    }
  }
}

int ins_pedina(int v[8][9], int y)
{
  int i=7;
  do
  {
    i--;
  }
  while((v[i][y+1]!=0) && (i>1));
  return i-1;
}

int controllo(int v[8][9], int x1, int y1, int *vittoria)
{
   int i,j;
   *vittoria=0;
   x1++;
   y1++;
     //CASO |
     if(v[x1-1][y1]==v[x1][y1])
     {
        i=x1;
       while(v[i-1][y1]==v[x1][y1])
          i--;
       while((v[i][y1]==v[x1][y1])&&(*vittoria<4))
       {
          i++;
          *vittoria=*vittoria+1;
       }
       if(*vittoria==4) return v[x1][y1]; else return 0;
     }

     //CASO /
     if(v[x1-1][y1+1]==v[x1][y1])
     {
       i=x1;
       j=y1;
       while(v[i-1][j+1]==v[x1][y1])
       {
          i--;
          j++;
       }
       while((v[i][j]==v[x1][y1])&&(*vittoria<4))
       {
          i++;
          j--;
          *vittoria=*vittoria+1;
       }
       if(*vittoria==4) return v[x1][y1]; else return 0;
     }

     //CASO -
     if(v[x1][y1+1]==v[x1][y1])
     {
       i=y1;
       while(v[x1][i+1]==v[x1][y1])
          i++;
       while((v[x1][i]==v[x1][y1])&&(*vittoria<4))
       {
          i--;
          *vittoria=*vittoria+1;
       }
       if(*vittoria==4) return v[x1][y1]; else return 0;
     }

     //CASO "\"
     if(v[x1+1][y1+1]==v[x1][y1])
     {
       i=x1;
       j=y1;
       while(v[i+1][j+1]==v[x1][y1])
       {
          i++;
          j++;
       }
       while((v[i][j]==v[x1][y1])&&(*vittoria<4))
       {
          i--;
          j--;
          *vittoria=*vittoria+1;
       }
       if(*vittoria==4) return v[x1][y1]; else return 0;
     }

     //CASO |
     if(v[x1+1][y1]==v[x1][y1])
     {
       i=x1;
       while(v[i+1][y1]==v[x1][y1])
          i++;
       while((v[i][y1]==v[x1][y1])&&(*vittoria<4))
       {
          i--;
          *vittoria=*vittoria+1;
       }
       if(*vittoria==4) return v[x1][y1]; else return 0;
     }

     //CASO /
     if(v[x1+1][y1-1]==v[x1][y1])
     {
       i=x1;
       j=y1;
       while(v[i+1][j-1]==v[x1][y1])
       {
          i++;
          j--;
       }
       while((v[i][j]==v[x1][y1])&&(*vittoria<4))
       {
          i--;
          j++;
          *vittoria=*vittoria+1;
       }
       if(*vittoria==4) return v[x1][y1]; else return 0;
     }

     //CASO -
     if(v[x1][y1-1]==v[x1][y1])
     {
       i=y1;
       while(v[x1][i-1]==v[x1][y1])
          i--;
       while((v[x1][i]==v[x1][y1])&&(*vittoria<4))
       {
          i++;
          *vittoria=*vittoria+1;
       }
       if(*vittoria==4) return v[x1][y1]; else return 0;
     }

     //CASO "\"
     if(v[x1-1][y1-1]==v[x1][y1])
     {
       i=x1;
       j=y1;
       while(v[i-1][j-1]==v[x1][y1])
       {
          i--;
          j--;
       }
       while((v[i][j]==v[x1][y1])&&(*vittoria<4))
       {
          i++;
          j++;
          *vittoria=*vittoria+1;
       }
       if(*vittoria==4) return v[x1][y1]; else return 0;
     }
     else return 0;
}

void PutMatrix(int v[8][9], TShape *s[6][7])
{
  int i,j;
  for(i=0;i<6;i++)
  {
    for(j=0;j<7;j++)
    {
      if(v[i][j]==2)
        s[i][j]->Brush->Color=clRed;
      if(v[i][j]==1)
        s[i][j]->Brush->Color=clYellow;
    }
  }
}

void reset(int v[8][9], TShape *s[6][7], int *riga, int*x, int *y, bool *turno, int *vittoria, int *perso)
{
   int i,j;
   for(i=0;i<8;i++)
   {
     for(j=0;j<9;j++)
     {
        v[i][j]=0;
        if(i<6 && j<7)
           s[i][j]->Brush->Color=clBlack;
     }
   }
   *riga=*x=*y=*vittoria=*perso=0;
   *turno=true;
}

AnsiString ControlloVittoria(AnsiString dat, int *perso)
{
   char s[50], app[50];
   int i=0;
   strcpy(s, dat.c_str());
   *perso=StrToInt(s[0]);
   while(s[i]!='\0')
   {
     app[i]=s[i+2];
     i++;
   }
   dat=app;
   return dat;
}

//---------------------------------------------------------------------------
//INSERIMENTO PEDINE
//---------------------------------------------------------------------------
void __fastcall TfrmMain::Label1Click(TObject *Sender)
{
  if(turno==true)
  {
    GetMatrix(matrix,f4);
    riga=ins_pedina(matrix,0);
    f4[riga][0]->Brush->Color=colore;
    if(colore==clRed) matrix[riga+1][1]=2; else matrix[riga+1][1]=1;
    messaggio=IntToStr(riga)+"0";
    if(Server->Active==true)
        Server->Socket->Connections[0]->SendText(messaggio);
    else
        Client->Socket->SendText(messaggio);
    turno=false;
    NumCl=controllo(matrix,riga,0,&vittoria);
    if(vittoria==4)
    {
        if(NumCl==2)
        {
          punteggio1++;
          lblPlayer1->Caption=ServerPlayer+":  "+IntToStr(punteggio1);
          Server->Socket->Connections[0]->SendText(IntToStr(vittoria)+";"+lblPlayer1->Caption);
        }
        else
        {
          punteggio2++;
          lblPlayer2->Caption=ClientPlayer+":  "+IntToStr(punteggio2);
          Client->Socket->SendText(IntToStr(vittoria)+";"+lblPlayer2->Caption);
        }
        if((MessageBox(frmMain->Handle,"HAI VINTO!!! Vuoi fare un'altra partita?", "VITTORIA",MB_ICONINFORMATION+MB_YESNO))==IDNO)
          frmMain->Close();
        else
          reset(matrix,f4,&riga,&x,&y,&turno,&vittoria,&perso);
    }
  }
}
//---------------------------------------------------------------------------
void __fastcall TfrmMain::Label2Click(TObject *Sender)
{
  if(turno==true)
  {
    GetMatrix(matrix,f4);
    riga=ins_pedina(matrix,1);
    f4[riga][1]->Brush->Color=colore;
    if(colore==clRed) matrix[riga+1][2]=2; else matrix[riga+1][2]=1;
    messaggio=IntToStr(riga)+"1";
    if(Server->Active==true)
        Server->Socket->Connections[0]->SendText(messaggio);
    else
        Client->Socket->SendText(messaggio);
    turno=false;
    NumCl=controllo(matrix,riga,1,&vittoria);
    if(vittoria==4)
    {
        if(NumCl==2)
        {
          punteggio1++;
          lblPlayer1->Caption=ServerPlayer+":  "+IntToStr(punteggio1);
          Server->Socket->Connections[0]->SendText(IntToStr(vittoria)+";"+lblPlayer1->Caption);
        }
        else
        {
          punteggio2++;
          lblPlayer2->Caption=ClientPlayer+":  "+IntToStr(punteggio2);
          Client->Socket->SendText(IntToStr(vittoria)+";"+lblPlayer2->Caption);
        }
        if((MessageBox(frmMain->Handle,"HAI VINTO!!! Vuoi fare un'altra partita?", "VITTORIA",MB_ICONINFORMATION+MB_YESNO))==IDNO)
          frmMain->Close();
        else
          reset(matrix,f4,&riga,&x,&y,&turno,&vittoria,&perso);
    }
  }
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::mnuConnectionItem1Click(TObject *Sender)
{
  colore=clRed;
  ServerPlayer=InputBox("Inserisci il tuo Nick", "NickName", "");
  if(ServerPlayer!="")
  {
    lblPlayer1->Caption=ServerPlayer+":  0";
    Server->Port=1024;
    Server->Active=true;
    Client->Active=false;
    mnuConnectionItem1->Checked=true;
    mnuConnectionItem2->Checked=false;
  }
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::mnuConnectionItem2Click(TObject *Sender)
{
  colore=clYellow;
  IPAdress=InputBox("IPAdress", "Indirizzo IP del server:", "");
  ClientPlayer=InputBox("Inserisci il tuo Nick", "NickName", "");
  if((IPAdress!="") && (ClientPlayer!=""))
  {
    lblPlayer2->Caption=ClientPlayer+":  0";
    Client->Port=1024;
    Client->Host=IPAdress;
    Client->Active=true;
    Server->Active=false;
    mnuConnectionItem1->Checked=false;
    mnuConnectionItem2->Checked=true;
    Client->Socket->SendText(IntToStr(vittoria)+";"+lblPlayer2->Caption);
  }
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::ClientConnect(TObject *Sender,
      TCustomWinSocket *Socket)
{
   lblMsg->Caption="Connesso";
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::ClientConnecting(TObject *Sender,
      TCustomWinSocket *Socket)
{
   lblMsg->Caption="In connessione...";
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::ClientDisconnect(TObject *Sender,
      TCustomWinSocket *Socket)
{
  lblMsg->Caption="Disconnesso";
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::ClientError(TObject *Sender,
      TCustomWinSocket *Socket, TErrorEvent ErrorEvent, int &ErrorCode)
{
  lblMsg->Caption="Errore";
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::ClientRead(TObject *Sender,
      TCustomWinSocket *Socket)
{
   dati=Socket->ReceiveText();
   if((strlen(dati.c_str()))<=2)
   {
     strcpy(xy,dati.c_str());
     x=StrToInt(xy[0]);
     y=StrToInt(xy[1]);
     f4[x][y]->Brush->Color=clRed;
     lblMsg->Caption="Dati ricevuti";
     turno=true;
   }
   else
   {
      lblPlayer1->Caption=ControlloVittoria(dati,&perso);
      if(perso==4)
      {
          MessageBox(frmMain->Handle,"H A I     P E R S O ! ! ! ", "LESS",MB_ICONEXCLAMATION+MB_OK);
          reset(matrix,f4,&riga,&x,&y,&turno,&vittoria,&perso);
      }
   }
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::ClientWrite(TObject *Sender,
      TCustomWinSocket *Socket)
{

   lblMsg->Caption="Dati Inviati";
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::ServerAccept(TObject *Sender,
      TCustomWinSocket *Socket)
{
   lblMsg->Caption="Connessione accettata";
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::ServerClientConnect(TObject *Sender,
      TCustomWinSocket *Socket)
{
   lblMsg->Caption="Client connesso";
   Server->Socket->Connections[0]->SendText(IntToStr(vittoria)+";"+lblPlayer1->Caption);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::ServerClientDisconnect(TObject *Sender,
      TCustomWinSocket *Socket)
{
   lblMsg->Caption="Client disconnnesso";
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::ServerClientError(TObject *Sender,
      TCustomWinSocket *Socket, TErrorEvent ErrorEvent, int &ErrorCode)
{
   lblMsg->Caption="Client error";
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::ServerClientRead(TObject *Sender,
      TCustomWinSocket *Socket)
{
   dati=Socket->ReceiveText();
   if((strlen(dati.c_str()))<=2)
   {
     strcpy(xy,dati.c_str());
     x=StrToInt(xy[0]);
     y=StrToInt(xy[1]);
     f4[x][y]->Brush->Color=clYellow;
     lblMsg->Caption="Dati ricevuti";
     turno=true;
   }
   else
   {
      lblPlayer2->Caption=ControlloVittoria(dati,&perso);
      if(perso==4)
      {
          MessageBox(frmMain->Handle,"H A I     P E R S O ! ! ! ", "LESS",MB_ICONEXCLAMATION+MB_OK);
          reset(matrix,f4,&riga,&x,&y,&turno,&vittoria,&perso);
      }
   }
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::ServerClientWrite(TObject *Sender,
      TCustomWinSocket *Socket)
{
   lblMsg->Caption="Dati inviati";
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::ServerListen(TObject *Sender,
      TCustomWinSocket *Socket)
{
   lblMsg->Caption="In attesa...";
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::mnuPartitaItem1Click(TObject *Sender)
{
   reset(matrix,f4,&riga,&x,&y,&turno,&vittoria,&perso);
}
//---------------------------------------------------------------------------


void __fastcall TfrmMain::Label3Click(TObject *Sender)
{
  if(turno==true)
  {
    GetMatrix(matrix,f4);
    riga=ins_pedina(matrix,2);
    f4[riga][2]->Brush->Color=colore;
    if(colore==clRed) matrix[riga+1][3]=2; else matrix[riga+1][3]=1;
    messaggio=IntToStr(riga)+"2";
    if(Server->Active==true)
        Server->Socket->Connections[0]->SendText(messaggio);
    else
        Client->Socket->SendText(messaggio);
    turno=false;
    NumCl=controllo(matrix,riga,2,&vittoria);
    if(vittoria==4)
    {
        if(NumCl==2)
        {
          punteggio1++;
          lblPlayer1->Caption=ServerPlayer+":  "+IntToStr(punteggio1);
          Server->Socket->Connections[0]->SendText(IntToStr(vittoria)+";"+lblPlayer1->Caption);
        }
        else
        {
          punteggio2++;
          lblPlayer2->Caption=ClientPlayer+":  "+IntToStr(punteggio2);
          Client->Socket->SendText(IntToStr(vittoria)+";"+lblPlayer2->Caption);
        }
        if((MessageBox(frmMain->Handle,"HAI VINTO!!! Vuoi fare un'altra partita?", "VITTORIA",MB_ICONINFORMATION+MB_YESNO))==IDNO)
          frmMain->Close();
        else
          reset(matrix,f4,&riga,&x,&y,&turno,&vittoria,&perso);
    }
  }
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::Label4Click(TObject *Sender)
{
  if(turno==true)
  {
    GetMatrix(matrix,f4);
    riga=ins_pedina(matrix,3);
    f4[riga][3]->Brush->Color=colore;
    if(colore==clRed) matrix[riga+1][4]=2; else matrix[riga+1][4]=1;
    messaggio=IntToStr(riga)+"3";
    if(Server->Active==true)
        Server->Socket->Connections[0]->SendText(messaggio);
    else
        Client->Socket->SendText(messaggio);
    turno=false;
    NumCl=controllo(matrix,riga,3,&vittoria);
    if(vittoria==4)
    {
        if(NumCl==2)
        {
          punteggio1++;
          lblPlayer1->Caption=ServerPlayer+":  "+IntToStr(punteggio1);
          Server->Socket->Connections[0]->SendText(IntToStr(vittoria)+";"+lblPlayer1->Caption);
        }
        else
        {
          punteggio2++;
          lblPlayer2->Caption=ClientPlayer+":  "+IntToStr(punteggio2);
          Client->Socket->SendText(IntToStr(vittoria)+";"+lblPlayer2->Caption);
        }
        if((MessageBox(frmMain->Handle,"HAI VINTO!!! Vuoi fare un'altra partita?", "VITTORIA",MB_ICONINFORMATION+MB_YESNO))==IDNO)
          frmMain->Close();
        else
          reset(matrix,f4,&riga,&x,&y,&turno,&vittoria,&perso);
    }
  }
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::Label5Click(TObject *Sender)
{
  if(turno==true)
  {
    GetMatrix(matrix,f4);
    riga=ins_pedina(matrix,4);
    f4[riga][4]->Brush->Color=colore;
    if(colore==clRed) matrix[riga+1][5]=2; else matrix[riga+1][5]=1;
    messaggio=IntToStr(riga)+"4";
    if(Server->Active==true)
        Server->Socket->Connections[0]->SendText(messaggio);
    else
        Client->Socket->SendText(messaggio);
    turno=false;
    NumCl=controllo(matrix,riga,4,&vittoria);
    if(vittoria==4)
    {
        if(NumCl==2)
        {
          punteggio1++;
          lblPlayer1->Caption=ServerPlayer+":  "+IntToStr(punteggio1);
          Server->Socket->Connections[0]->SendText(IntToStr(vittoria)+";"+lblPlayer1->Caption);
        }
        else
        {
          punteggio2++;
          lblPlayer2->Caption=ClientPlayer+":  "+IntToStr(punteggio2);
          Client->Socket->SendText(IntToStr(vittoria)+";"+lblPlayer2->Caption);
        }
        if((MessageBox(frmMain->Handle,"HAI VINTO!!! Vuoi fare un'altra partita?", "VITTORIA",MB_ICONINFORMATION+MB_YESNO))==IDNO)
          frmMain->Close();
        else
          reset(matrix,f4,&riga,&x,&y,&turno,&vittoria,&perso);
    }
  }
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::Label6Click(TObject *Sender)
{
  if(turno==true)
  {
    GetMatrix(matrix,f4);
    riga=ins_pedina(matrix,5);
    f4[riga][5]->Brush->Color=colore;
    if(colore==clRed) matrix[riga+1][6]=2; else matrix[riga+1][6]=1;
    messaggio=IntToStr(riga)+"5";
    if(Server->Active==true)
        Server->Socket->Connections[0]->SendText(messaggio);
    else
        Client->Socket->SendText(messaggio);
    turno=false;
    NumCl=controllo(matrix,riga,5,&vittoria);
    if(vittoria==4)
    {
        if(NumCl==2)
        {
          punteggio1++;
          lblPlayer1->Caption=ServerPlayer+":  "+IntToStr(punteggio1);
          Server->Socket->Connections[0]->SendText(IntToStr(vittoria)+";"+lblPlayer1->Caption);
        }
        else
        {
          punteggio2++;
          lblPlayer2->Caption=ClientPlayer+":  "+IntToStr(punteggio2);
          Client->Socket->SendText(IntToStr(vittoria)+";"+lblPlayer2->Caption);
        }
        if((MessageBox(frmMain->Handle,"HAI VINTO!!! Vuoi fare un'altra partita?", "VITTORIA",MB_ICONINFORMATION+MB_YESNO))==IDNO)
          frmMain->Close();
        else
          reset(matrix,f4,&riga,&x,&y,&turno,&vittoria,&perso);
    }
  }
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::Label7Click(TObject *Sender)
{
  if(turno==true)
  {
    GetMatrix(matrix,f4);
    riga=ins_pedina(matrix,6);
    f4[riga][6]->Brush->Color=colore;
    if(colore==clRed) matrix[riga+1][7]=2; else matrix[riga+1][7]=1;
    messaggio=IntToStr(riga)+"6";
    if(Server->Active==true)
        Server->Socket->Connections[0]->SendText(messaggio);
    else
        Client->Socket->SendText(messaggio);
    turno=false;
    NumCl=controllo(matrix,riga,6,&vittoria);
    if(vittoria==4)
    {
        if(NumCl==2)
        {
          punteggio1++;
          lblPlayer1->Caption=ServerPlayer+":  "+IntToStr(punteggio1);
          Server->Socket->Connections[0]->SendText(IntToStr(vittoria)+";"+lblPlayer1->Caption);
        }
        else
        {
          punteggio2++;
          lblPlayer2->Caption=ClientPlayer+":  "+IntToStr(punteggio2);
          Client->Socket->SendText(IntToStr(vittoria)+";"+lblPlayer2->Caption);
        }
        if((MessageBox(frmMain->Handle,"HAI VINTO!!! Vuoi fare un'altra partita?", "VITTORIA",MB_ICONINFORMATION+MB_YESNO))==IDNO)
          frmMain->Close();
        else
          reset(matrix,f4,&riga,&x,&y,&turno,&vittoria,&perso);
    }
  }
}
//---------------------------------------------------------------------------

