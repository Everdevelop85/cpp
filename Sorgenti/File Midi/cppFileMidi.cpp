//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "cppFileMidi.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TfrmMain *frmMain;
//---------------------------------------------------------------------------
__fastcall TfrmMain::TfrmMain(TComponent* Owner)
        : TForm(Owner)
{
}
AnsiString NomeFile;
long int t, a, NowPos;
//---------------------------------------------------------------------------
void __fastcall TfrmMain::cboDriveChange(TObject *Sender)
{
        lstDir->Drive = cboDrive->Drive;
}
//---------------------------------------------------------------------------
void __fastcall TfrmMain::lstDirChange(TObject *Sender)
{
        lstFile->Clear();
        lstFile->Directory = lstDir->Directory;
}
//---------------------------------------------------------------------------
void __fastcall TfrmMain::FormCreate(TObject *Sender)
{
        lstFile->Mask = "*.mid;*.midi";
        lstFile->Clear();
        lstDir->Directory="C:\\Programmi\\Visual S25\\mid";
        lstFile->Directory="C:\\Programmi\\Visual S25\\mid";
        mciMidi->DeviceType = dtSequencer;
        mciMidi->TimeFormat = tfMilliseconds;
}
//---------------------------------------------------------------------------
void __fastcall TfrmMain::lstFileClick(TObject *Sender)
{
        NomeFile=lstFile->Items->Strings[lstFile->ItemIndex];
        mciMidi->FileName=lstFile->Directory+"\\"+NomeFile;
        mciMidi->Open();
}
//---------------------------------------------------------------------------
