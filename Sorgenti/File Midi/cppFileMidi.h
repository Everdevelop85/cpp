//---------------------------------------------------------------------------
#ifndef cppFileMidiH
#define cppFileMidiH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <FileCtrl.hpp>
#include <MPlayer.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TfrmMain : public TForm
{
__published:	// IDE-managed Components
        TDirectoryListBox *lstDir;
        TFileListBox *lstFile;
        TDriveComboBox *cboDrive;
        TMediaPlayer *mciMidi;
        void __fastcall cboDriveChange(TObject *Sender);
        void __fastcall lstDirChange(TObject *Sender);
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall lstFileClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TfrmMain(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfrmMain *frmMain;
//---------------------------------------------------------------------------
#endif
