object frmMain: TfrmMain
  Left = 89
  Top = 182
  Width = 898
  Height = 592
  Caption = 'frmMain'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 890
    Height = 41
    Align = alTop
    AutoSize = True
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    OnConstrainedResize = Panel1ConstrainedResize
    object Panel2: TPanel
      Left = 2
      Top = 2
      Width = 89
      Height = 37
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 8
        Width = 53
        Height = 13
        Caption = 'Equazione:'
      end
    end
    object txtEquation: TEdit
      Left = 72
      Top = 8
      Width = 577
      Height = 21
      TabOrder = 1
    end
    object rdbMax: TRadioButton
      Left = 656
      Top = 8
      Width = 81
      Height = 17
      Caption = 'Massimizza'
      TabOrder = 2
      OnClick = rdbMaxClick
    end
    object rdbMin: TRadioButton
      Left = 752
      Top = 8
      Width = 73
      Height = 17
      Caption = 'Minimizza'
      Checked = True
      TabOrder = 3
      TabStop = True
      OnClick = rdbMinClick
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 41
    Width = 890
    Height = 505
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Splitter: TSplitter
      Left = 0
      Top = 185
      Width = 890
      Height = 9
      Cursor = crVSplit
      Align = alTop
      Beveled = True
    end
    object Panel4: TPanel
      Left = 0
      Top = 0
      Width = 890
      Height = 185
      Align = alTop
      Caption = 'Panel4'
      TabOrder = 0
      object memTies: TMemo
        Left = 1
        Top = 1
        Width = 888
        Height = 183
        Align = alClient
        TabOrder = 0
      end
    end
    object Panel5: TPanel
      Left = 0
      Top = 194
      Width = 890
      Height = 311
      Align = alClient
      Caption = 'Panel5'
      TabOrder = 1
      object stgSimplex: TStringGrid
        Left = 1
        Top = 1
        Width = 888
        Height = 309
        Align = alClient
        FixedCols = 0
        FixedRows = 0
        TabOrder = 0
      end
    end
  end
  object MainMenu: TMainMenu
    Left = 136
    Top = 80
    object MainMenuFile: TMenuItem
      Caption = 'File'
      object MainMenuFileItem1: TMenuItem
        Caption = 'Apri...'
      end
      object MainMenuFileItem2: TMenuItem
        Caption = 'Salva'
      end
      object MainMenuFileItem3: TMenuItem
        Caption = 'Salva con nome...'
      end
      object MainMenuFileItem4: TMenuItem
        Caption = '-'
      end
      object MainMenuFileItem5: TMenuItem
        Caption = 'Stampa'
      end
      object MainMenuFileItem6: TMenuItem
        Caption = '-'
      end
      object MainMenuFileItem7: TMenuItem
        Caption = 'Esci'
      end
    end
    object MainMenuSimplex: TMenuItem
      Caption = 'Simplesso'
      object MainMenuSimplexItem2: TMenuItem
        Caption = 'Inserisci parametri...'
        OnClick = MainMenuSimplexItem2Click
      end
      object MainMenuSimplexItem3: TMenuItem
        Caption = 'Azzera'
      end
      object MainMenuSimplexItem4: TMenuItem
        Caption = '-'
      end
      object MainMenuSimplexItem1: TMenuItem
        Caption = 'AVVIA'
      end
    end
    object MainMenuHelp: TMenuItem
      Caption = '?'
      object MainMenuHelpItem1: TMenuItem
        Caption = 'Help'
      end
      object MainMenuHelpItem2: TMenuItem
        Caption = 'About'
      end
    end
  end
end
