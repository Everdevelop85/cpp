//---------------------------------------------------------------------------

#ifndef FunctionsH
#define FunctionsH
//---------------------------------------------------------------------------
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <fcntl.h>
#include <Dialogs.hpp>
#include <sys\stat.h>
#include <io.h>

#define MINIMIZE 0
#define MAXIMIZE 1
class FRACTAL
{
        public:
                int num;
                int den;

//COSTRUCTOR
        FRACTAL(void)
        {
                num=0;
                den=1;
        }

        FRACTAL(const FRACTAL &fr)
        {
                num=fr.num;
                den=fr.den;
                Semplifica();
        }

        FRACTAL(int n, int d)
        {
                if(den<0)
                {
                        num*=-1;
                        den=abs(den);
                }
                Semplifica();
        }

        FRACTAL(char *fr)
        {
                int i=0,l;
                char n[5],d[5];
                if((strchr(fr,'/'))==NULL)
                {
                        num=atoi(fr);
                        den=1;
                        return;
                }
                while(fr[i]!='/')
                {
                        n[i]=fr[i];
                        i++;
                }
                n[i]='\0';
                i++;
                l=i;
                while(fr[i]!='\0')
                {
                        d[i-l]=fr[i];
                        i++;
                }
                d[i-l]='\0';
                num=atoi(n);
                if(atoi(d)<0)
                {
                        num*=-1;
                        den=abs(atoi(d));
                }
                else
                        den=atoi(d);
                Semplifica();
        }

//METHODS
        public: void Semplifica()
        {
                int *n,divisor,sign=1;
                if(num>den)
                        n=&den;
                else
                        n=&num;
                if(*n<0)
                {
                        sign=-1;
                        *n=(*n)*-1;
                }
                for(divisor=*n;divisor>1;divisor--)
                {
                        while((((den%divisor)==0)&&((num%divisor)==0))&&(divisor>=2))
                        {
                                den/=divisor;
                                num/=divisor;
                                divisor=*n;
                        }
                }
                num*=sign;
        }

        public: void Print()
        {
                if((den==1)||(num==0))
                        printf("%d",num);
                else
                        printf("%d/%d",num,den);
        }

        float toFloat()
        {
                return (float) num/den;
        }
//OPERATOR SAME TYPE
        void operator=(const FRACTAL &fr)
        {
                num=fr.num;
                den=fr.den;
        }

        void operator=(const int &n)
        {
                num=n;
                den=1;
        }

        void operator=(const char *fr)
        {
                int i=0,l;
                char n[5],d[5];
                if((strchr(fr,'/'))==NULL)
                {
                        num=atoi(fr);
                        den=1;
                        return;
                }
                else
                while(fr[i]!='/')
                {
                        n[i]=fr[i];
                        i++;
                }
                n[i]='\0';
                i++;
                l=i;
                while(fr[i]!='\0')
                {
                        d[i-l]=fr[i];
                        i++;
                }
                d[i-l]='\0';
                num=atoi(n);
                if(atoi(d)<0)
                {
                        num*=-1;
                        den=abs(atoi(d));
                }
                else
                        den=atoi(d);
                Semplifica();
        }

        friend bool operator==(const FRACTAL &f1, const FRACTAL &f2)
        {
                FRACTAL p1(f1),p2(f2);
                if((p1.num==p2.num)&&(p1.den==p2.den))
                        return true;
                return false;
        }

        friend bool operator<(const FRACTAL &f1, const FRACTAL &f2)
        {
                float fr1=0.00, fr2=0.00;
                fr1=(float) (f1.num/f1.den);
                fr2=(float) (f2.num/f2.den);
                if(fr1<fr2)
                        return true;
                return false;
        }

        friend bool operator==(const FRACTAL &f1, const int &n)
        {
                FRACTAL app(n,1), p1(f1);
                if(p1==app)
                        return true;
                return false;
        }


        friend FRACTAL operator+(const FRACTAL &f1, const FRACTAL &f2)
        {
                FRACTAL ris;
                ris.den=f1.den*f2.den;
                ris.num=((ris.den/f1.den)*f1.num)+((ris.den/f2.den)*f2.num);
                ris.Semplifica();
                return ris;
        }

        friend FRACTAL operator+=(const FRACTAL &f1, const FRACTAL &f2)
        {
                FRACTAL ris;
                ris.den=f1.den*f2.den;
                ris.num=((ris.den/f1.den)*f1.num)+((ris.den/f2.den)*f2.num);
                ris.Semplifica();
                return ris;
        }

        friend FRACTAL operator-(const FRACTAL &f1, const FRACTAL &f2)
        {
                FRACTAL ris;
                ris.den=f1.num*f2.den;
                ris.num=(((ris.den/f1.den)*f1.num)-((ris.den/f2.den)*f2.num));
                ris.Semplifica();
                return ris;
        };

        friend FRACTAL operator*(const FRACTAL &f1, const FRACTAL &f2)
        {
                FRACTAL ris;
                ris.num=f1.num*f2.num;
                ris.den=f1.den*f2.den;
                ris.Semplifica();
                return ris;
        };

        friend FRACTAL operator/(const FRACTAL &f1, const FRACTAL &f2)
        {
                FRACTAL ris;
                ris.num=f1.num*f2.den;
                ris.den=f1.den*f2.num;
                ris.Semplifica();
                return ris;
        };

        bool operator!=(const FRACTAL &fr)
        {
                if((num!=fr.num)&&((den!=fr.den)))
                        return true;
                return false;
        };

//OPERTORS AND CONSTANT
        bool operator!=(const int &n)
        {
                if(den!=1)
                        return true;
                else
                {
                        if(num!=n)
                                return true;
                        return false;
                }
        };

        bool operator>(const int &n)
        {
                float fr,nm;
                nm=(float) n;
                fr=(float) num/den;
                if(fr>nm)
                        return true;
                return false;
        };

        friend FRACTAL operator^(const FRACTAL &f1, const int &n)
        {
                FRACTAL ris(1,1),Rfr(f1);
                int app,exp=n;
                if(exp<0)
                {
                        app=Rfr.den;
                        Rfr.den=Rfr.num;
                        Rfr.num=app;
                        exp=abs(exp);
                }
                if(exp>0)
                {
                        ris.num=pow(Rfr.num,exp);
                        ris.den=pow(Rfr.den,exp);
                        ris.Semplifica();
                }
                return ris;
        };

        friend FRACTAL operator+(const FRACTAL &f1, const int &n)
        {
                FRACTAL ris;
                ris.den=f1.den;
                ris.num=(ris.den*n)+f1.num;
                ris.Semplifica();
                return ris;
        }

        friend FRACTAL operator-(const FRACTAL &f1, const int &n)
        {
                FRACTAL ris;
                ris.den=f1.den;
                ris.num=(f1.num-(f1.den*n));
                ris.Semplifica();
                return ris;
        };

        friend FRACTAL operator*(const FRACTAL &f1, const int &n)
        {
                FRACTAL ris;
                ris.num=f1.num*n;
                ris.den=f1.den;
                ris.Semplifica();
                return ris;
        };

        friend FRACTAL operator/(const FRACTAL &f1, const int &n)
        {
                FRACTAL ris;
                ris.num=f1.num;
                ris.den=f1.den*n;
                ris.Semplifica();
                return ris;
        };

        friend bool operator>=(const FRACTAL &f1, const int &n)
        {
                FRACTAL app;
                app=f1;
                if(app.toFloat()>=n)
                        return true;
                return false;
        };

        friend bool operator<=(const FRACTAL &f1, const int &n)
        {
                FRACTAL app;
                app=f1;
                if(app.toFloat()<=n)
                        return true;
                return false;
        };

        AnsiString toString()
        {
                this->Semplifica();
                if(this->den==1)
                        return IntToStr(this->num);
                return IntToStr(this->num)+"/"+IntToStr(this->den);
        }
};

class MATRIX_SMP
{
        private:
                int m;
                int n;
                FRACTAL **MatrSmp;
                int lineCi;
                int columnBi;

public:
        /*MATRIX_SMP(int neq, int minc, FRACTAL **matrCoeff, FRACTAL VettC[], FRACTAL VettB[])
        {
                m=neq;
                n=minc;
                lineCi=m;
                columnBi=n;
                int i,j;
                MatrSmp=(FRACTAL **)malloc(sizeof(FRACTAL)*(m+1));
                for(i=0;i<m;i++)
                        MatrSmp[i]=(FRACTAL *)malloc(sizeof(FRACTAL)*(n+1));
                for(i=0;i<m;i++)
                {
                        for(j=0;j<n;j++)
                                MatrSmp[i][j]=matrCoeff[i][j];
                }
                for(j=0;i<m;j++)
                        MatrSmp[j][columnBi]=VettB[j];
                for(i=0;i<n;i++)
                {
                        if(i<m)
                                MatrSmp[lineCi][i]=0;
                        else
                                MatrSmp[lineCi][i]=VettC[j];
                }
        }*/
        MATRIX_SMP(int meq, int ninc, FRACTAL **matrCoeff)
        {
                m=meq;
                n=ninc;
                lineCi=m;
                columnBi=n+m;
                int i,j;
                MatrSmp=matrCoeff;
        }

        float *getReduxCostCoeff()
        {
                float *CiZi,Zi=0.0;
                CiZi=(float *)malloc(n);
                int i,j;
                for(j=0;j<n;j++)
                {
                        for(i=0;i<m;i++)
                                Zi+=(MatrSmp[lineCi][j]*MatrSmp[i][j]).toFloat();
                        CiZi[j]=MatrSmp[lineCi][j].toFloat()-Zi;
                }
                return CiZi;
        }

        FRACTAL *Solution()
        {
                FRACTAL *SL, modulo;
                SL=(FRACTAL *)malloc((sizeof(FRACTAL))*n);
                int i,j;
                float norma;
                for(j=0;j<n;j++)
                {
                        for(i=0;i<m;i++)
                                modulo=modulo+(MatrSmp[i][j]^2);
                        norma=sqrt(modulo.toFloat());
                        if(norma==1)
                        {
                                int l=0;
                                while(MatrSmp[l][j]!=1)
                                        l++;
                                SL[j]=MatrSmp[l][columnBi];
                        }
                }
                return SL;
        }

        int ColumnPivot()
        {
                float min=MatrSmp[lineCi][0].toFloat();
                int i;
                for(i=0;i<n;i++)
                {
                        if(min>MatrSmp[lineCi][i].toFloat())
                        {
                                min=MatrSmp[lineCi][n].toFloat();
                                i=0;
                        }
                }
                i=0;
                while(MatrSmp[lineCi][i].toFloat()!=min)
                        i++;
                return i;
        }

        int RowPivot()
        {
                float *vett, min;
                int cp=ColumnPivot(), i;
                vett=(float *)malloc(m);
                for(i=0;i<m;i++)
                {
                        if(MatrSmp[i][cp].toFloat()>0)
                                vett[i]=MatrSmp[i][columnBi].toFloat()/MatrSmp[i][cp].toFloat();
                        else
                                vett[i]=-1;
                }
                min=vett[0];
                for(i=0;i<m;i++)
                {
                        if((min>vett[i])&&(vett[i]>0))
                        {
                                min=vett[i];
                                i=0;
                        }
                }
                i=0;
                while(vett[i]!=min)
                        i++;
                return i;
        }

        FRACTAL GetPivot()
        {
                return MatrSmp[RowPivot()][ColumnPivot()];
        }

        bool EndAlgorithm(const int op)
        {
                bool ret=false;
                if(op==MINIMIZE)
                {
                        int count=0;
                        for(int i=0;i<n;i++)
                        {
                                if(MatrSmp[lineCi][i]>=0)
                                count++;
                        }
                        if(count==n)
                                ret=true;
                }
                else
                {
                        int count=0;
                        for(int i=0;i<n;i++)
                        {
                                if(MatrSmp[lineCi][i]<=0)
                                count++;
                        }
                        if(count==n)
                                ret=true;
                }
                return ret;
        }

        int getRows(){int m;}
        int getColumns(){int n;}

};

