//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "cppSimplex.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TfrmMain *frmMain;
ListSIMPLEX *ListMatricies;
MATRIX_SMP *MatriceSMP;
int NumeroC, NumeroEq, NumeroInC;
//---------------------------------------------------------------------------
__fastcall TfrmMain::TfrmMain(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------

int CountVariables(char *s)
{
        int i=0, nc=0;
        while(s[i]!='\0')
        {
                if(((s[i]>='a')&&(s[i]<='z'))||((s[i]>='A')&&(s[i]<='Z')))
                        nc++;
                i++;
        }
        return nc;
}

FRACTAL *getCoeffVett(char *vett)
{
        int i, nC, ind=0, indC=0;
        char buffer[20];
        FRACTAL *VettCoeff;
        VettCoeff=(FRACTAL *)malloc((sizeof(FRACTAL))*(CountVariables(vett)+1));
        while((vett[i]!='\0')&&(vett[i]!='>')&&(vett[i]!='<'))
        {
                if(((vett[0]=='+')||(vett[0]=='-'))&&(i>0))
                {
                        buffer[ind]='\0';
                        VettCoeff[indC]=buffer;
                        ind=0;
                        indC++;
                }
                buffer[ind]=vett[i];
                ind++;
                i++;
        }
        return VettCoeff;
}

FRACTAL KnowedValue(char *vett)
{
        char *p;
        FRACTAL tn(0,1);
        if((p=strstr(vett,">"))==NULL)
        {
                if((p=strstr(vett,"<"))==NULL)
                        return tn;
        }
        tn=p;
        return tn;
}

FRACTAL **emptyMatrixSimplex(int &r, int &c)
{
        int n_rows, n_columns;
        FRACTAL **matrixCoeff, *vettCoeff;
        n_rows=frmMain->memTies->Lines->Count+1;
        n_columns=(CountVariables(frmMain->memTies->Lines->Strings[0].c_str()))+n_rows+1;
//Matrice coefficenti
        matrixCoeff=(FRACTAL **)malloc(sizeof(FRACTAL)*n_rows);
        for(int i=0;i<n_rows;i++)
                matrixCoeff[i]=(FRACTAL *)malloc(sizeof(FRACTAL)*(n_columns+1));
        for(int i=0;i<n_rows;i++)
        {
                vettCoeff=getCoeffVett(frmMain->memTies->Lines->Strings[i].c_str());
                for(int j=frmMain->memTies->Lines->Count;j<n_columns-1;j++)
                        matrixCoeff[i][j]=vettCoeff[i];
        }
        for(int i=0;i<frmMain->memTies->Lines->Count;i++)
        {
                for(int j=0;j<frmMain->memTies->Lines->Count;j++)
                {
                        matrixCoeff[i][j]=0;
                        if(i==j)
                                matrixCoeff[i][j]=1;
                }
        }
//vettore termini noti
        for(int i=0;i<n_rows-1;i++)
                matrixCoeff[i][n_columns-1]=KnowedValue(frmMain->memTies->Lines->Strings[i].c_str());
//vettore funzione
        for(int i=0;i<n_columns;i++)
                matrixCoeff[n_rows-1][i]=0;
        for(int i=frmMain->memTies->Lines->Count;i<CountVariables(frmMain->txtEquation->Text.c_str());i++)
                matrixCoeff[n_rows-1][i]=vettCoeff[i-frmMain->memTies->Lines->Count];

        r=n_rows;
        c=n_columns;
        free(vettCoeff);

        return matrixCoeff;
}

FRACTAL **createNewMatrix(FRACTAL **matrixA)
{
        FRACTAL **matrixB;

}

//---------------------------------------------------------------------------
void __fastcall TfrmMain::FormCreate(TObject *Sender)
{
//        MatriceSMP = new MATRIX_SMP();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::rdbMinClick(TObject *Sender)
{
        rdbMax->Checked=false;
        rdbMin->Checked=true;
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::rdbMaxClick(TObject *Sender)
{
        rdbMax->Checked=true;
        rdbMin->Checked=false;
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::Panel1ConstrainedResize(TObject *Sender,
      int &MinWidth, int &MinHeight, int &MaxWidth, int &MaxHeight)
{
        rdbMin->Left=Panel1->Width-rdbMin->Width-8;
        rdbMax->Left=rdbMin->Left-rdbMax->Width-8;
        txtEquation->Width=rdbMax->Left-Label1->Width-32;
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::MainMenuSimplexItem2Click(TObject *Sender)
{
        int meq=0, ninc=0;
        MATRIX_SMP *MatriceSMP = new MATRIX_SMP(meq, ninc,emptyMatrixSimplex(&meq, &ninc));
        ListMatricies = new ListSIMPLEX();
        ListMatricies->Add(MatriceSMP);

}
//---------------------------------------------------------------------------

