//---------------------------------------------------------------------------

#ifndef cppSimplexH
#define cppSimplexH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "Functions.h"
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <Menus.hpp>
//---------------------------------------------------------------------------
typedef struct list_element
{
        MATRIX_SMP *value;
        list_element *next;
}item;

typedef item*  list;

class TfrmMain : public TForm
{
__published:	// IDE-managed Components
        TPanel *Panel1;
        TPanel *Panel2;
        TEdit *txtEquation;
        TLabel *Label1;
        TPanel *Panel3;
        TPanel *Panel4;
        TPanel *Panel5;
        TSplitter *Splitter;
        TMemo *memTies;
        TStringGrid *stgSimplex;
        TMainMenu *MainMenu;
        TMenuItem *MainMenuFile;
        TMenuItem *MainMenuFileItem1;
        TMenuItem *MainMenuFileItem2;
        TMenuItem *MainMenuFileItem3;
        TMenuItem *MainMenuFileItem4;
        TMenuItem *MainMenuFileItem5;
        TMenuItem *MainMenuFileItem6;
        TMenuItem *MainMenuFileItem7;
        TMenuItem *MainMenuSimplex;
        TMenuItem *MainMenuSimplexItem1;
        TMenuItem *MainMenuHelp;
        TMenuItem *MainMenuHelpItem1;
        TMenuItem *MainMenuHelpItem2;
        TMenuItem *MainMenuSimplexItem2;
        TRadioButton *rdbMax;
        TRadioButton *rdbMin;
        TMenuItem *MainMenuSimplexItem3;
        TMenuItem *MainMenuSimplexItem4;
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall rdbMinClick(TObject *Sender);
        void __fastcall rdbMaxClick(TObject *Sender);
        void __fastcall Panel1ConstrainedResize(TObject *Sender,
          int &MinWidth, int &MinHeight, int &MaxWidth, int &MaxHeight);
        void __fastcall MainMenuSimplexItem2Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TfrmMain(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfrmMain *frmMain;
//---------------------------------------------------------------------------
#endif

class ListSIMPLEX
{
private:
        list root;

public:
        ListSIMPLEX()
        {
                root=NULL;
        }

        void Add(MATRIX_SMP *e)
        {
                list t;
                t=(list)malloc(sizeof(item));
                t->value=e;
                t->next=root;
                root=t;
        }

        MATRIX_SMP *GenerateNewMatrix()
        {
                MATRIX_SMP *newMatrix = ;

                newMatrix = new MATRIX_SMP();
        }

};
