//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
USERES("bprInterface.res");
USEFORM("cppInterface.cpp", frmMain);
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->CreateForm(__classid(TfrmMain), &frmMain);
                 Application->Run();
        }
        catch (Exception &exception)
        {
                if(exception.ClassNameIs("ESocketError"))
                {
                        MessageBox(Application->Handle,"S� � verificato un errore nella connessione.\r\nLa connessione verr� interrotta.","Errore",MB_ICONERROR+MB_OK);
                        Application->Terminate(); 
                }
                else
                        Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
