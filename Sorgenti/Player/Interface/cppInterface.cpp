//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "cppInterface.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TfrmMain *frmMain;
AnsiString NomeFileMP3, buff;
//---------------------------------------------------------------------------
__fastcall TfrmMain::TfrmMain(TComponent* Owner)
        : TForm(Owner)
{

}
//---------------------------------------------------------------------------


AnsiString DateTimeMS(int sec)
{
   int m;
   AnsiString ms, ss;
   m=sec/60;
   sec=sec%60;
   if(m<10)
     ms="0"+IntToStr(m);
   else
     ms=IntToStr(m);
   if(sec<10)
     ss="0"+IntToStr(sec);
   else
     ss=IntToStr(sec);
   if(sec<0)
     return "00:00";
   else
     return ms+":"+ss;
}

void GetInfoFile(char *s)
{
        int i=0,l;
        char tt[5],ta[5];
        while(s[i]!=';')
        {
                tt[i]=s[i];
                i++;
        }
        tt[i]='\0';
        i++;
        l=i;
        while(s[i]!='\0')
        {
                ta[i-l]=s[i];
                i++;
        }
        ta[i-l]='\0';
        frmMain->lblFullTime->Caption=DateTimeMS(atoi(tt));
        frmMain->lblTime->Caption=DateTimeMS(atoi(ta));
}

int GetTime(char *s)
{
        int i=1;
        char t[5];
        while(s[i]!='\0')
        {
                t[i-1]=s[i];
                i++;
        }
        t[i-1]='\0';
        return atoi(t);
}
void __fastcall TfrmMain::ClientSocketConnect(TObject *Sender,
      TCustomWinSocket *Socket)
{
        stsBar->Panels->Items[0]->Text="Connessione: Attiva";
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::ClientSocketConnecting(TObject *Sender,
      TCustomWinSocket *Socket)
{
        stsBar->Panels->Items[0]->Text="Connessione: In corso...";
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::ClientSocketDisconnect(TObject *Sender,
      TCustomWinSocket *Socket)
{
        stsBar->Panels->Items[0]->Text="Connessione: Disattivata";
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::ClientSocketError(TObject *Sender,
      TCustomWinSocket *Socket, TErrorEvent ErrorEvent, int &ErrorCode)
{
        if(ErrorEvent==eeGeneral)
                stsBar->Panels->Items[0]->Text="Connessione: Errore sconosciuto";
        if(ErrorEvent==eeSend)
                stsBar->Panels->Items[0]->Text="Connessione: Errore nell'invio";
        if(ErrorEvent==eeReceive)
                stsBar->Panels->Items[0]->Text="Connessione: Errore nella ricezione";
        if(ErrorEvent==eeConnect)
                stsBar->Panels->Items[0]->Text="Connessione: Errore di connessione";
        if(ErrorEvent==eeDisconnect)
                stsBar->Panels->Items[0]->Text="Connessione: Errore di disconnessione";
        if(ErrorEvent==eeAccept)
                stsBar->Panels->Items[0]->Text="Connessione: Errore connessione non valida";
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::ClientSocketRead(TObject *Sender,
      TCustomWinSocket *Socket)
{
        //STRINGA1 --> "@@[<nome file>]"
        //STRINGA2 --> "@@@[<durata in secondi>],[<tempo attuale>]"
        stsBar->Panels->Items[1]->Text="Stato: Ricezione dati";
        buff=Socket->ReceiveText();
        if((buff[1]=='@')&&(buff[2]=='@')&&(buff[3]=='@'))
        {
                buff=buff.Delete(1,3);
                GetInfoFile(buff.c_str());
        }
        else
        {
                if((buff[1]=='@')&&(buff[2]=='@'))
                {
                        buff=buff.Delete(1,2);
                        lblFileName->Caption=buff;
                }
                else
                {
                        buff=buff.Delete(1,1);
                        lblTime->Caption=DateTimeMS(GetTime(buff.c_str()));
                }
        }
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::ClientSocketWrite(TObject *Sender,
      TCustomWinSocket *Socket)
{
        stsBar->Panels->Items[1]->Text="Stato: Invio dati";
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::FormCreate(TObject *Sender)
{
        ClientSocket->Port=1024;
        ClientSocket->Host="127.0.0.1";
        try
        {
                ClientSocket->Active=true;
        }
        catch (Exception &E)
        {
                MessageBox(frmMain->Handle,"sdsdsdsd","ssdsdsd",0);
                //E.ClassName
        }
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cmdPlayClick(TObject *Sender)
{
        ClientSocket->Socket->SendText(action[0]);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cmdPauseClick(TObject *Sender)
{
        ClientSocket->Socket->SendText(action[1]);        
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cmdNextClick(TObject *Sender)
{
        ClientSocket->Socket->SendText(action[3]);        
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cmdPrevClick(TObject *Sender)
{
        ClientSocket->Socket->SendText(action[4]);        
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cmdStopClick(TObject *Sender)
{
        ClientSocket->Socket->SendText(action[2]);        
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::cekRepeatClick(TObject *Sender)
{
        if(cekRepeat->Checked)
                ClientSocket->Socket->SendText(action[6]);
        else
                ClientSocket->Socket->SendText(action[7]);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::FormClose(TObject *Sender, TCloseAction &Action)
{
        if((MessageBox(frmMain->Handle,"L'applicazione verr� chiusa, terminare anche l'applicazione in remoto ?","Attenzione",MB_ICONEXCLAMATION+MB_YESNO))==IDYES)
                ClientSocket->Socket->SendText(action[8]);
}
//---------------------------------------------------------------------------

