//---------------------------------------------------------------------------
#ifndef cppInterfaceH
#define cppInterfaceH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <ScktComp.hpp>
//---------------------------------------------------------------------------

const char action[9][14]={"PLAY",
                    "PAUSE",
                    "STOP",
                    "NEXT",
                    "PREV",
                    "MOVE",
                    "REPEAT-TRUE",
                    "REPEAT-FALSE",
                    "CLOSE"
                    };
                    
class TfrmMain : public TForm
{
__published:	// IDE-managed Components
        TStaticText *lblFileName;
        TStatusBar *stsBar;
        TButton *cmdPlay;
        TButton *cmdPause;
        TButton *cmdNext;
        TButton *cmdPrev;
        TButton *cmdStop;
        TLabel *lblTime;
        TLabel *lblFullTime;
        TCheckBox *cekRepeat;
        TClientSocket *ClientSocket;
        TProgressBar *trbTempo;
        void __fastcall ClientSocketConnect(TObject *Sender,
          TCustomWinSocket *Socket);
        void __fastcall ClientSocketConnecting(TObject *Sender,
          TCustomWinSocket *Socket);
        void __fastcall ClientSocketDisconnect(TObject *Sender,
          TCustomWinSocket *Socket);
        void __fastcall ClientSocketError(TObject *Sender,
          TCustomWinSocket *Socket, TErrorEvent ErrorEvent,
          int &ErrorCode);
        void __fastcall ClientSocketRead(TObject *Sender,
          TCustomWinSocket *Socket);
        void __fastcall ClientSocketWrite(TObject *Sender,
          TCustomWinSocket *Socket);
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall cmdPlayClick(TObject *Sender);
        void __fastcall cmdPauseClick(TObject *Sender);
        void __fastcall cmdNextClick(TObject *Sender);
        void __fastcall cmdPrevClick(TObject *Sender);
        void __fastcall cmdStopClick(TObject *Sender);
        void __fastcall cekRepeatClick(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
private:	// User declarations
public:		// User declarations
        __fastcall TfrmMain(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfrmMain *frmMain;
//---------------------------------------------------------------------------
#endif
