�
 TFRMMAIN 0
  TPF0TfrmMainfrmMainLeft� Top	BorderStylebsToolWindowCaptionInterfacciaClientHeightqClientWidth�Color	clBtnFaceFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style OldCreateOrderOnClose	FormCloseOnCreate
FormCreatePixelsPerInch`
TextHeight TLabellblTimeLeft:Top:Width3HeightCaption00:00Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTransparent	  TLabellblFullTimeLeftxTop?Width%HeightCaption00:00Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTransparent	  TStaticTextlblFileNameLeftTopWidth�HeightAutoSizeBorderStyle	sbsSunkenCaptionNome file.mp3ColorclBlackFont.CharsetANSI_CHARSET
Font.ColorclSilverFont.Height�	Font.NameArial
Font.Style ParentColor
ParentFontTabOrder   
TStatusBarstsBarLeft Top`Width�HeightPanelsTextConnessione:Width�  TextStato:Width�   SimplePanel  TButtoncmdPlayLeftTop9Width9HeightCaption4Font.CharsetSYMBOL_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameWebdings
Font.Style 
ParentFontTabOrderOnClickcmdPlayClick  TButtoncmdPauseLeft@Top9Width!HeightCaption;Font.CharsetSYMBOL_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameWebdings
Font.Style 
ParentFontTabOrderOnClickcmdPauseClick  TButtoncmdNextLeft`Top9Width!HeightCaption:Font.CharsetSYMBOL_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameWebdings
Font.Style 
ParentFontTabOrderOnClickcmdNextClick  TButtoncmdPrevLeft� Top9Width!HeightCaption9Font.CharsetSYMBOL_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameWebdings
Font.Style 
ParentFontTabOrderOnClickcmdPrevClick  TButtoncmdStopLeft� Top9Width!HeightCaption<Font.CharsetSYMBOL_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameWebdings
Font.Style 
ParentFontTabOrderOnClickcmdStopClick  	TCheckBox	cekRepeatLeft� Top9Width9HeightCaptionRepeatTabOrderOnClickcekRepeatClick  TProgressBartrbTempoLeftTop Width�HeightMin MaxdTabOrder  TClientSocketClientSocketActive
ClientTypectNonBlockingPort OnConnectingClientSocketConnecting	OnConnectClientSocketConnectOnDisconnectClientSocketDisconnectOnReadClientSocketReadOnWriteClientSocketWriteOnErrorClientSocketErrorLeftTop8   