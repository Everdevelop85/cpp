//---------------------------------------------------------------------------
#ifndef cppPlayerH
#define cppPlayerH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <ComCtrls.hpp>
#include <Controls.hpp>
#include <Dialogs.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <Menus.hpp>
#include <MPlayer.hpp>
#include <StdCtrls.hpp>
#include <inifiles.hpp>
//-----------------------------------------------------------------------------
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <dir.h>
#include <dos.h>
#include <math.h>
#include <io.h>
#include <string.h>
//#include "trayicon.h"
#define MAX 255
#define PERM 0644
//-----------------------------------------------------------------------------
#include "FileOnSystem.h"
//-----------------------------------------------------------------------------
//Nodi
//-----------------------------------------------------------------------------
struct FILE_MM
{
   char path[MAX];
   char name[150];

        void operator =(FILE_MM &dato)
        {
                strcpy(path,dato.path);
                strcpy(name,dato.name);
        }

        void operator=(char *s)
        {
                int pos=(strlen(s))-1,i=0;
                while(s[pos]!='\\')
                        pos--;
                strcpy(path,s);
                path[pos]='\0';
                while(s[i+(pos+1)]!='\0')
                {
                        name[i]=s[i+(pos+1)];
                        i++;
                }
                name[i]='\0';
        }
};
typedef struct list_element
{
        FILE_MM value;
        int index;
        struct list_element  *next;
}item;
typedef item*  list;
//-----------------------------------------------------------------------------
class TfrmMain : public TForm
{
__published:	// IDE-managed Components
        TMediaPlayer *wmpPlayer;
        TTimer *tmrTime;
        TSaveDialog *dlgSavePL;
        TOpenDialog *dlgOpenPL;
        TPopupMenu *PopupMenu;
        TMenuItem *Aggiorna1;
        TMenuItem *Reset1;
        TMenuItem *N1;
        TMenuItem *OpenPlayList;
        TMenuItem *MakePlayList;
        TMenuItem *N2;
        TMenuItem *Esplora1;
        TMenuItem *Eliminafile1;
        TMenuItem *Rename1;
        TMenuItem *N3;
        TMenuItem *OpenDir;
        TMenuItem *Copyfile;
        TMenuItem *N4;
        TMenuItem *Propiety;
        TPanel *Panel1;
        TPanel *Panel3;
        TProgressBar *trbTempo;
        TPanel *Panel4;
        TButton *cmdPlay;
        TButton *cmdPause;
        TButton *cmdNext;
        TButton *cmdPrev;
        TButton *cmdStop;
        TPanel *Panel5;
        TEdit *txtMatch;
        TPanel *Panel2;
        TCheckBox *cekRepeat;
        TButton *cmdExpDir;
        TLabel *lblFullTime;
        TLabel *lblTime;
        TPanel *panList;
        TListBox *lstDir;
        TPanel *Panel7;
        TSplitter *Splitter;
        TStringGrid *stgLista;
        TGroupBox *fraCopy;
        TPanel *Panel6;
        TLabel *lblFile;
        TProgressBar *prgBar;
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall Aggiorna1Click(TObject *Sender);
        void __fastcall Reset1Click(TObject *Sender);
        void __fastcall FormConstrainedResize(TObject *Sender,
          int &MinWidth, int &MinHeight, int &MaxWidth, int &MaxHeight);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall cmdExpDirClick(TObject *Sender);
        void __fastcall lstDirDblClick(TObject *Sender);
        void __fastcall stgListaDblClick(TObject *Sender);
        void __fastcall stgListaClick(TObject *Sender);
        void __fastcall tmrTimeTimer(TObject *Sender);
        void __fastcall trbTempoMouseDown(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
        void __fastcall cmdPlayClick(TObject *Sender);
        void __fastcall cmdNextClick(TObject *Sender);
        void __fastcall cmdPrevClick(TObject *Sender);
        void __fastcall cmdPauseClick(TObject *Sender);
        void __fastcall cmdStopClick(TObject *Sender);
        void __fastcall MakePlayListClick(TObject *Sender);
        void __fastcall OpenPlayListClick(TObject *Sender);
        void __fastcall Eliminafile1Click(TObject *Sender);
        void __fastcall Rename1Click(TObject *Sender);
        void __fastcall CopyfileClick(TObject *Sender);
        void __fastcall txtMatchChange(TObject *Sender);
        void __fastcall PropietyClick(TObject *Sender);
        void __fastcall Esplora1Click(TObject *Sender);
        void __fastcall OpenDirClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
        char pathDataDir[MAX], pathRootDir[MAX], pathIniFile[MAX];
        int RecordCount, IndiceSel;
        AnsiString pathBrowse;

        __fastcall TfrmMain(TComponent* Owner);

};
//-----------------------------------------------------------------------------
extern PACKAGE TfrmMain *frmMain;
//-----------------------------------------------------------------------------
#endif
class TBRowseThread : public TThread
{
        private:
        protected:
                void __fastcall Execute();
        public:
                __fastcall TBRowseThread(bool CreateSuspended);
};

//Classe Database
//-----------------------------------------------------------------------------
class DBListMP3
{
private:
        list root, l, end;

//-----------------------------------------------------------------------------
//Primitive
//-----------------------------------------------------------------------------
public:
        DBListMP3()
        {
                root=NULL;
                end=NULL;
                l=root;
        };

        void Add(FILE_MM e)
        {

                list t;
                t=(list)malloc(sizeof(item));
                t->value=e;
                t->next=NULL;
                t->index=0;
                if(root==NULL)
                        root=t;
                else
                {
                        if(root->next==NULL)
                        {
                                if((strcmp(t->value.name,root->value.name))>0)
                                {
                                        root->next=t;
                                        end=t;
                                }
                                else
                                {
                                        end=root;
                                        t->next=root;
                                        root=t;
                                }
                        }
                        else
                        {
                                list prev;
                                prev=root;
                                l=prev->next;
                                if((strcmp(t->value.name,l->value.name))<0)
                                {
                                        t->next=prev;
                                        root=t;
                                }
                                else
                                {
                                        while((l!=NULL) && ((strcmp(t->value.name,l->value.name))>=0))
                                        {
                                                prev=l;
                                                l=l->next;
                                        }
                                        if(l==NULL)
                                        {
                                                end->next=t;
                                                end=t;
                                        }
                                        else
                                        {
                                                prev->next=t;
                                                t->next=l;
                                        }
                                }
                        }
                        l=root;
                        int i=0;
                        while(l!=NULL)
                        {
                                l->index=i;
                                l=l->next;
                                i++;
                        }
                }
        };

        void Add(char *p)
        {
                FILE_MM dato;
                dato=p;
                Add(dato);
        };

        void Destroy()
        {
                if(root==NULL)
                        return;
                while(l!=NULL)
                {
                        free(root);
                        root=l;
                        l=root->next;
                }
                free(root);
        };

        int Count()
        {
                l=root;
                int counter=1;
                if(l==NULL)
                        return 0;
                while(l->next!=NULL)
                {
                        counter++;
                        l=l->next;
                }
                return counter;
        };

        void Clear()
        {
                if(root==NULL)
                        return;
                l=root->next;
                while(l!=NULL)
                {
                        free(root);
                        root=l;
                        l=root->next;
                }
                free(root);
                root=NULL;
                end=NULL;
                l=root;
        };

        bool IsEmpty()
        {
                if(root==NULL)
                        return true;
                return false;
        };
//-----------------------------------------------------------------------------
//Metodi
//-----------------------------------------------------------------------------
        void Delete(int el)
        {
                l=root;
                list l2;
                list_element *app;
                bool trovato=false;
                if(root->index==el)
                {
                        root=root->next;
                        free(l);
                }
                else
                {
                        while(l!=NULL)
                        {
                                l2=l;
                                l=l->next;
                                if(l->index==el)
                                {
                                        trovato=true;
                                        break;
                                }
                        }
                        if(trovato)
                        {
                            app=l->next;
                            free(l);
                            l2->next=app;
                        }
                }
        };
//-----------------------------------------------------------------------------
        void PrintQuerySearchPath(char *p)
        {
                int ind=1;
                l=root;
                while(l!=NULL)
                {
                        if((strcmp(l->value.path,p))==0)
                        {
                                frmMain->stgLista->Cells[0][ind]=l->index+1;
                                frmMain->stgLista->Cells[1][ind]=l->value.name;
                                frmMain->stgLista->RowCount++;
                                ind++;
                        }
                        l=l->next;
                }
                frmMain->stgLista->RowCount--;
        }
//-----------------------------------------------------------------------------
        void Print()
        {
                int ind=1;
                l=root;
                while(l!=NULL)
                {
                        frmMain->stgLista->Cells[0][ind]=l->index+1;
                        frmMain->stgLista->Cells[1][ind]=l->value.name;
                        frmMain->stgLista->RowCount++;
                        ind++;
                        l=l->next;
                }
                frmMain->stgLista->RowCount--;
        }
//-----------------------------------------------------------------------------
        AnsiString GetFileName(int ind)
        {
                AnsiString nm;
                l=root;
                nm="";
                while(l!=NULL)
                {
                        if(l->index==ind)
                        {
                                nm.Insert(l->value.path,0);
                                nm.Insert("\\",nm.Length()+1);
                                nm.Insert(l->value.name,nm.Length()+1);
                                break;
                        }
                        l=l->next;
                }
                return nm;
        }
//-----------------------------------------------------------------------------
        void Change(char *oldName, char *newName)
        {
                while(l!=NULL)
                {
                        if((strcmp(oldName,l->value.name))==0)
                        {
                                strcpy(l->value.name,newName);
                                break;
                        }
                        l=l->next;
                }
        }
//-----------------------------------------------------------------------------
        bool SearchForMatch(AnsiString match)
        {
                int ind=1;
                bool findMatch=false;
                l=root;
                while(l!=NULL)
                {
                        if((strstr(LowerCase(l->value.name).c_str(),match.LowerCase().c_str()))!=NULL)
                        {
                                findMatch=true;
                                frmMain->stgLista->Cells[0][ind]=l->index+1;
                                frmMain->stgLista->Cells[1][ind]=l->value.name;
                                frmMain->stgLista->RowCount++;
                                ind++;
                        }
                        l=l->next;
                }
                frmMain->stgLista->RowCount--;
                return findMatch;
        }
//-----------------------------------------------------------------------------
        float GetFileSizeMb(int ind)
        {
                FileOnSystem fileMp3;
                float size=-1;
                bool trovato=false;
                l=root;
                while(l!=NULL)
                {
                        if(l->index==ind)
                        {
                                trovato=true;
                                break;
                        }
                        l=l->next;
                }
                if(trovato)
                {
                        fileMp3.name=l->value.name;
                        fileMp3.path=l->value.path;
                        fileMp3.FullPath=fileMp3.path+"\\"+fileMp3.name;
                        size=fileMp3.FileSize(MBYTE_MODE);
                }
                return size;
        }
};
