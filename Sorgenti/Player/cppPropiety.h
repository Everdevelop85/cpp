//---------------------------------------------------------------------------
#ifndef cppPropietyH
#define cppPropietyH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <Graphics.hpp>
//---------------------------------------------------------------------------
class TfrmPropiety : public TForm
{
__published:	// IDE-managed Components
        TImage *imgIcon;
        TLabel *lblMSG;
private:	// User declarations
public:		// User declarations
        __fastcall TfrmPropiety(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfrmPropiety *frmPropiety;
//---------------------------------------------------------------------------
#endif
