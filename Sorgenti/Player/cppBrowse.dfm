object dlgBrowse: TdlgBrowse
  Left = 451
  Top = 245
  BorderStyle = bsDialog
  Caption = 'Esplora'
  ClientHeight = 321
  ClientWidth = 297
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object lstDir: TDirectoryListBox
    Left = 8
    Top = 64
    Width = 281
    Height = 217
    ItemHeight = 16
    TabOrder = 0
  end
  object cboDrive: TDriveComboBox
    Left = 8
    Top = 40
    Width = 281
    Height = 19
    TabOrder = 1
    OnChange = cboDriveChange
  end
  object cmdCancel: TButton
    Left = 216
    Top = 288
    Width = 73
    Height = 25
    Caption = 'Annulla'
    TabOrder = 2
    OnClick = cmdCancelClick
  end
  object cmdOk: TButton
    Left = 136
    Top = 288
    Width = 73
    Height = 25
    Caption = 'OK'
    TabOrder = 3
    OnClick = cmdOkClick
  end
end
