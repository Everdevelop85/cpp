//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
USEFORM("cppPlayer.cpp", frmMain);
USEFORM("cppPropiety.cpp", frmPropiety);
USEFORM("cppBrowse.cpp", dlgBrowse);
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->CreateForm(__classid(TfrmMain), &frmMain);
                 Application->CreateForm(__classid(TfrmPropiety), &frmPropiety);
                 Application->CreateForm(__classid(TdlgBrowse), &dlgBrowse);
                 Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
