//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "cppBrowse.h"
#include "cppPlayer.h"
#include "cppPropiety.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TdlgBrowse *dlgBrowse;
//---------------------------------------------------------------------------
__fastcall TdlgBrowse::TdlgBrowse(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TdlgBrowse::cboDriveChange(TObject *Sender)
{
        lstDir->Directory="";
        lstDir->Directory.Insert(cboDrive->Drive,0);
        lstDir->Directory+=":\\";
}
//---------------------------------------------------------------------------
void __fastcall TdlgBrowse::cmdOkClick(TObject *Sender)
{
        frmMain->pathBrowse=lstDir->Directory;
}
//---------------------------------------------------------------------------
void __fastcall TdlgBrowse::cmdCancelClick(TObject *Sender)
{
        frmMain->pathBrowse="";
        dlgBrowse->Close();
}
//---------------------------------------------------------------------------
void __fastcall TdlgBrowse::FormClose(TObject *Sender,
      TCloseAction &Action)
{
        frmMain->Enabled=true;
}
//---------------------------------------------------------------------------
