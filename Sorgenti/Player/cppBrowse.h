//---------------------------------------------------------------------------

#ifndef cppBrowseH
#define cppBrowseH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <FileCtrl.hpp>
//---------------------------------------------------------------------------
class TdlgBrowse : public TForm
{
__published:	// IDE-managed Components
        TDirectoryListBox *lstDir;
        TDriveComboBox *cboDrive;
        TButton *cmdCancel;
        TButton *cmdOk;
        void __fastcall cboDriveChange(TObject *Sender);
        void __fastcall cmdOkClick(TObject *Sender);
        void __fastcall cmdCancelClick(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
private:	// User declarations
public:		// User declarations
        __fastcall TdlgBrowse(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TdlgBrowse *dlgBrowse;
//---------------------------------------------------------------------------
#endif
