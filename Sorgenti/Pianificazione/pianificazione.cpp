//---------------------------------------------------------------------------
#include <vcl.h>
#include <string.h>
#include <iostream.h>
#include <stdlib.h>
#include <stdio.h>
#include <conio.h>
#pragma hdrstop
#include <inifiles.hpp>

#include "pianificazione.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CGRID"
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------

TLabel *Indice[24];
TShape *Ore[24];
//Struttura INTERVALLI: indica l'ora dipartenza
//di una pianificazione e quante ore interessa
struct INTERVALLI
{
  int ore, partenza;
};

//Rende invisibili i quadrati rossi del grafico
//di pianificazione
void RESET()
{
        int i;
        for(i=0;i<24;i++)
                Ore[i]->Visible=false;
}

INTERVALLI PIAN;
int secondi;
void __fastcall TForm1::Button1Click(TObject *Sender)
{
 RESET();

 TIniFile *ore = new TIniFile("Pianificazione.ini");
 int orapart, numero;

        n_Pian=0;
        //scorre la ListBox
        for(int i=0; i<24; i++)
        {
                //Se l'elemento di indice i �
                //selezionato
                if (lstOraIN->Selected[i])
                {
                   numero=0;
                   orapart=i;
                   //controlla se anche gli elementi successivi
                   //sono selezionati.
                   do
                   {
                       Ore[i]->Visible=true;
                       numero++;
                       i++;
                   }
                   while (lstOraIN->Selected[i] && i<24);
                   n_Pian++;
                   ore->WriteInteger("Canale0","Numero_Ore"+IntToStr(n_Pian),numero);
                   ore->WriteInteger("Canale0","Ora_Inizio"+IntToStr(n_Pian),orapart);
                 }
        }
        Button4->Enabled = false;
        Button3->Enabled = true;
        Button2->Enabled = true;
        Button1->Enabled = false;

}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormCreate(TObject *Sender)
{
        //Eseguo un ciclo che riempie la ListBox con le ore da 0 a 23
        for(int i=0; i<24; i++)
        {
                //Creo il vettore di oggetti Label
                Indice[i]= new TLabel(this);
                Indice[i]->Parent=this;
                //Creo il vettore di oggetti Shape
                Ore[i]= new TShape(this);
                Ore[i]->Parent=this;
                Indice[i]->Caption=IntToStr(i);
                Indice[i]->Top=96;
                Ore[i]->Brush->Color=clRed;
                Ore[i]->Pen->Style=psClear;
                Ore[i]->Top=(Indice[i]->Height+Indice[i]->Top)+10;
                if (i!=0)
                        if (i<=10)
                                Indice[i]->Left=(Indice[i-1]->Left+Indice[i-1]->Width)+15;
                        else
                                Indice[i]->Left=(Indice[i-1]->Left+Indice[i-1]->Width)+9;
                else
                        Indice[i]->Left=296;

                Ore[i]->Left=Indice[i]->Left;
                Ore[i]->Width=22;
                Ore[i]->Height=20;
                //assegna le ore alla ListBox
                if(i<10)
                {
                        if(i<13)
                                lstOraIN->Items->Add("Ora: 0"+IntToStr(i)+":00 h AM" );
                        else
                                lstOraIN->Items->Add("Ora: 0"+IntToStr(i)+":00 h PM" );
                }
                else
                {
                        if(i<13)
                                lstOraIN->Items->Add("Ora: "+IntToStr(i)+":00 h AM" );
                        else
                                lstOraIN->Items->Add("Ora: "+IntToStr(i)+":00 h PM" );
                }

        }

        //Apro il file INI
        TIniFile *ore = new TIniFile("Pianificazione.ini");

        //Acuisisto i primi due dati
        PIAN.ore=ore->ReadInteger("Canale0","Numero_Ore1",-1);
        PIAN.partenza=ore->ReadInteger("Canale0","Ora_Inizio1",-1);

        //Se sono entrambi diversi -1
        if ((PIAN.ore!=-1)&&(PIAN.partenza!=-1))
        {
                Button4->Enabled = false;
                Button3->Enabled = true;
                Button2->Enabled = true;
                Button1->Enabled = false;
                RESET();

                for(int i=0; i<24; i++)
                {
                        PIAN.ore=ore->ReadInteger("Canale0","Numero_Ore"+IntToStr(i+1),-1);
                        PIAN.partenza=ore->ReadInteger("Canale0","Ora_Inizio"+IntToStr(i+1),-1);
                        //Disegno il grafico dei dati ricevuti dal file ini
                        if (PIAN.ore>1)
                        {
                                for(int j=PIAN.partenza;j<(PIAN.ore+PIAN.partenza);j++)
                                        Ore[j]->Visible=true;
                        }
                        else
                        {
                                if(PIAN.ore==1)
                                        Ore[PIAN.partenza]->Visible=true;
                        }

                }
        }
        else
        {
                Button4->Enabled = false;
                Button3->Enabled = false;
                Button2->Enabled = false;
                Button1->Enabled = true;
        }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::tmrPianificazioneTimer(TObject *Sender)
{
//-----------------------------PER OGNI MINUTO------------------------------

        //acquisisco l'ora di sistema corrente
        TDateTime tempo = Now();
        //Ottengo ore, minuti, secondi separate
        DecodeTime(tempo, h, m, s, ms);
        //Se l'ora di partenza � = all'ora del sistema
        if (PIAN.partenza==h)
        {
                //fai partire la registrazione
                lblStat->Caption = "Registrazione Attivata";
                tmrPianificazione->Enabled = false;
                tmrRec->Enabled = true;
        }


}
//---------------------------------------------------------------------------

void __fastcall TForm1::tmrRecTimer(TObject *Sender)
{
//-----------------------------PER OGNI MINUTO------------------------------

        //acquisisco l'ora di sistema corrente
        TDateTime tempo = Now();
        //Ottengo ore, minuti, secondi separate
        DecodeTime(tempo, h, m, s, ms);

        //se l'ora di sistema � = all'ora di fine-registrazione
        if (h==(PIAN.partenza+PIAN.ore))
        {
                //Si ferma la registrazione
                lblStat->Caption = "Registrazione Terminata";
        }
        //Incrementa la vasriabile per la sezione
        n_Pian=n_Pian+1;
        //apro il file ini e leggo le sezioni
        TIniFile *ore = new TIniFile("Pianificazione.ini");
        PIAN.ore=ore->ReadInteger("Canale0"/* + variabile del canale selezionato*/,"Numero_Ore"+IntToStr(n_Pian),-1);
        PIAN.partenza=ore->ReadInteger("Canale0"/* + variabile del canale selezionato*/,"Ora_Inizio"+IntToStr(n_Pian),-1);
        //Riabilito la pianificazione
        tmrPianificazione->Enabled=true;
        tmrRec->Enabled=false;

}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button2Click(TObject *Sender)
{
        tmrPianificazione->Enabled = false;
        tmrRec->Enabled = false;
        TIniFile *ore = new TIniFile("Pianificazione.ini");
        RESET();
        for(int i=0; i<16; i++)
                ore->EraseSection("Canale"+IntToStr(i));
        Button4->Enabled = false;
        Button3->Enabled = false;
        Button2->Enabled = false;
        Button1->Enabled = true;


}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button3Click(TObject *Sender)
{
        Button4->Enabled = true;
        Button3->Enabled = false;
        Button2->Enabled = false;
        Button1->Enabled = false;
        n_Pian=0;
        //Acquisisco i primi dati dal file ini
        TIniFile *ore = new TIniFile("Pianificazione.ini");
        PIAN.ore=ore->ReadInteger("Canale0","Numero_Ore1",-1);
        PIAN.partenza=ore->ReadInteger("Canale0","Ora_Inizio1",-1);
        //Attivo il timer per il controllo sul'ora
        tmrPianificazione->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button4Click(TObject *Sender)
{
        Button4->Enabled = false;
        Button3->Enabled = true;
        Button2->Enabled = true;
        Button1->Enabled = false;
        tmrPianificazione->Enabled = false;
        tmrRec->Enabled = false;
}
//---------------------------------------------------------------------------


