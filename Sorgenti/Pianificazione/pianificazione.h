//---------------------------------------------------------------------------
#ifndef pianificazioneH
#define pianificazioneH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Menus.hpp>
#include "CGRID.h"
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
        TGroupBox *GroupBox1;
        TListBox *lstOraIN;
        TButton *Button1;
        TTimer *tmrPianificazione;
        TTimer *tmrRec;
        TButton *Button2;
        TButton *Button3;
        TButton *Button4;
        TLabel *Label1;
        TLabel *lblStat;
        void __fastcall Button1Click(TObject *Sender);
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall tmrPianificazioneTimer(TObject *Sender);
        void __fastcall tmrRecTimer(TObject *Sender);
        void __fastcall Button2Click(TObject *Sender);
        void __fastcall Button3Click(TObject *Sender);
        void __fastcall Button4Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
        int n_Pian;
        Word h, m, s, ms;
        __fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
