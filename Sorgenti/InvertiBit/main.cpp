#include <cstdlib>
#include <iostream>
#include <stdio.h>
#include <conio.h>
#include <fcntl.h>
#include <io.h>
#include <string.h>
#include <sys\stat.h>

#define MAX 255

void CreateFileName(char *f1,char *f2)
{
        strcpy(f2,f1);
        int i=(strlen(f2)-1);
        char *ext;
        if((ext=strstr(f1,"."))==NULL)
                strcpy(ext,".txt");
        while(f2[i]!='.')
                i--;
        f2[i]='\0';
        strcat(f2,"_cpy");
        strcat(f2,ext);
};

int ChangeBit(char *srg, char *dest)
{
        if(access(srg,06|00)!=0)
                return 1;
        int s,d;
        char *page, c;
        long int off=0;
        if((s=open(srg,O_RDONLY,S_IREAD|S_IWRITE))>0)
        {
                if((d=open(dest,O_CREAT|O_TRUNC|O_WRONLY,S_IREAD|S_IWRITE))<0)
                {
                        close(s);
                        return 3;
                }
        }
        else
                return 2;

        while(!eof(s))
        {
                read(s,&c,1);
                if(c=='{')
                {
                        while((!eof(s))&&(c!='}'))
                        {
                                read(s,&c,1);
                                switch(c)
                                {
                                        case '0':
                                                c='1';
                                        break;

                                        case '1':
                                                c='0';
                                        break;
                                };
                                if(c!='}')
                                        write(d,&c,1);
                        }
                }
                else
                        write(d,&c,1);
        }
        close(s);
        close(d);
        return 0;
};

int main(int argc, char* argv[])
{
        char FileName[MAX], newFileName[MAX], maker[MAX];
        int  mode, ret;

        printf("File name: ");
        scanf("%s",FileName);
        CreateFileName(FileName,newFileName);
        ret=ChangeBit(FileName,newFileName);
        switch(ret)
        {
                case 0:
                        printf("Modifica completata\n\n");
                break;

                case 1:
                        printf("Accesso negato\n\n");
                break;

                case 2:
                        printf("Errore apertura sorgente\n\n");
                break;
                case 3:
                        printf("Errore apertura destinazione\n\n");
                break;
        };
        system("PAUSE");
        return 0;
}

