        PROCESSOR       16F628A
        RADIX           DEC
        INCLUDE         "16F628A.INC"
        ERRORLEVEL      -302
        __CONFIG        0x3FF1

	ORG	020h
count	RES	2
ncicl	RES	1
num	RES	1

	ORG	0h
	CALL	preset
	MOWLW	09h
	MOVWF	num
	
attesa
	CALL	DISPLAY
	CALL	delay
	BTFSC	PORTA,1
	GOTO	attesa
	GOTO	countdown

countd
	DECSFZ	num
	GOTO	attesa
	GOTO	stop
stop
	CALL	DISPLAY
	CALL	delay	
	GOTO	stop

	
preset
	BSF	STATUS,RP0
        MOVLW   B'00000001'
        MOVWF   TRISA
        MOVLW   B'00000000'
        MOVWF   TRISB
        BCF     STATUS,RP0



;Ritardo FirstCicle
cnt
	MOVLW	0FFh
	MOVWF	count
	MOVWF	count+1
ciclo	
	DECFSZ	count
	GOTO	ciclo
	
	DECFSZ	count+1
	GOTO	ciclo

	RETURN

;Delay finished
delay
	MOVLW	02h
	MOVWF	ncicl
clcnt
	CALL	cnt
	DECFSZ	ncicl
	GOTO	clcnt
	RETURN


DISPLAY     CALL       TABDISP      ;Chiama subroutine conversione
            MOVWF      PORTB        ;Scrive valore dei LED su PORTB
            RETURN
;-----------------------------------------------------
TABDISP	    ADDWF      PCL,F        ;Conversione esa->display
{		RETLW	11000000B
		RETLW	11111001B
		RETLW	10100100B
		RETLW	10110000B
		RETLW	10011001B
		RETLW	10010010B
		RETLW	10000010B
		RETLW	11111000B
		RETLW	10000000B
		RETLW	10010000B
}
	END