        PROCESSOR       16F628A
        RADIX           DEC
        INCLUDE         "16F628A.INC"
        ERRORLEVEL      -302
        __CONFIG        0x3FF1

	ORG	020h
count	RES	2
ncicl	RES	1
num	RES	1

	ORG	0h
	CALL	preset
	MOWLW	09h
	MOVWF	num
	
attesa
	CALL	DISPLAY
	CALL	delay
	BTFSC	PORTA,1
	GOTO	attesa
	GOTO	countdown

countd
	DECSFZ	num
	GOTO	attesa
	GOTO	stop
stop
	CALL	DISPLAY
	CALL	delay	
	GOTO	stop

	
preset
	BSF	STATUS,RP0
        MOVLW   B'00000001'
        MOVWF   TRISA
        MOVLW   B'00000000'
        MOVWF   TRISB
        BCF     STATUS,RP0



;Ritardo FirstCicle
cnt
	MOVLW	0FFh
	MOVWF	count
	MOVWF	count+1
ciclo	
	DECFSZ	count
	GOTO	ciclo
	
	DECFSZ	count+1
	GOTO	ciclo

	RETURN

;Delay finished
delay
	MOVLW	02h
	MOVWF	ncicl
clcnt
	CALL	cnt
	DECFSZ	ncicl
	GOTO	clcnt
	RETURN


DISPLAY     CALL       TABDISP      ;Chiama subroutine conversione
            MOVWF      PORTB        ;Scrive valore dei LED su PORTB
            RETURN
;-----------------------------------------------------
TABDISP	    ADDWF      PCL,F        ;Conversione esa->display
		RETLW	00111111B
		RETLW	00000110B
		RETLW	01011011B
		RETLW	01001111B
		RETLW	01100110B
		RETLW	01101101B
		RETLW	01111101B
		RETLW	00000111B
		RETLW	01111111B
		RETLW	01101111B

	END