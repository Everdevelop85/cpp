//---------------------------------------------------------------------------

#ifndef cppDeleteFilesH
#define cppDeleteFilesH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class TfrmDeleteFiles : public TForm
{
__published:	// IDE-managed Components
        TButton *cmdDelete;
        TListBox *lstFiles;
        void __fastcall cmdDeleteClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TfrmDeleteFiles(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfrmDeleteFiles *frmDeleteFiles;
//---------------------------------------------------------------------------
#endif
