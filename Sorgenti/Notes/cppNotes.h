//---------------------------------------------------------------------------

#ifndef cppNotesH
#define cppNotesH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <ToolWin.hpp>

#include <stdlib.h>
#include <stdio.h>
#include <conio.h>
#include <dos.h>
#include <dir.h>
#include <fcntl.h>
#include <Dialogs.hpp>
#include <ExtCtrls.hpp>
#include <Buttons.hpp>
#include <sys/stat.h>
#include <io.h>

#define MAX 255

void OrderList(char **l, int count);
int SaveOnFile(char *file);
int OpenfromFile(char *file);
TDateTime *OpenNotes();
//---------------------------------------------------------------------------
class TfrmNotes : public TForm
{
__published:	// IDE-managed Components
        TMemo *txtText;
        TOpenDialog *OpenDialog;
        TPanel *Panel1;
        TSpeedButton *cmdNew;
        TSpeedButton *cmdOpen;
        TSpeedButton *cmdSave;
        TSpeedButton *cmdPaste;
        TSpeedButton *cmdDeleteFile;
        TSpeedButton *cmdAlarm;
        TTimer *tmrControllo;
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall cmdOpenClick(TObject *Sender);
        void __fastcall cmdSaveClick(TObject *Sender);
        void __fastcall cmdNewClick(TObject *Sender);
        void __fastcall cmdPasteClick(TObject *Sender);
        void __fastcall cmdDeleteFileClick(TObject *Sender);
        void __fastcall cmdAlarmClick(TObject *Sender);
        void __fastcall tmrControlloTimer(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TfrmNotes(TComponent* Owner);

        char directoryForSaving[MAX];
        int Num_AL;
        AnsiString FileOpened;

};
//---------------------------------------------------------------------------
extern PACKAGE TfrmNotes *frmNotes;
//---------------------------------------------------------------------------
#endif
