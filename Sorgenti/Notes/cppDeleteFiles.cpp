//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "cppDeleteFiles.h"
#include "cppNotes.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TfrmDeleteFiles *frmDeleteFiles;
//---------------------------------------------------------------------------
__fastcall TfrmDeleteFiles::TfrmDeleteFiles(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TfrmDeleteFiles::cmdDeleteClick(TObject *Sender)
{
        char fileName[255];
        for(int i=0;i<lstFiles->Count;i++)
        {
                if(lstFiles->Selected[i])
                {
                        strcpy(fileName,frmNotes->directoryForSaving);
                        strcat(fileName,"\\");
                        strcat(fileName,lstFiles->Items->Strings[lstFiles->ItemIndex].c_str());
                        if((remove(fileName))==0)
                                lstFiles->Items->Delete(i);
                }
        }
        frmNotes->FormCreate(Sender);
}
//---------------------------------------------------------------------------
