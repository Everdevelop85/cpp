//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "cppInsertAlarm.h"
#include "cppNotes.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TfrmInsertAlarm *frmInsertAlarm;
//---------------------------------------------------------------------------
__fastcall TfrmInsertAlarm::TfrmInsertAlarm(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TfrmInsertAlarm::FormCreate(TObject *Sender)
{
        txtData->Text=FormatDateTime("gg/mm/yyyy",Now());
        txtOra->Text=FormatDateTime("hh:nn",Now());
        txtText->Lines->Clear();
}
//---------------------------------------------------------------------------


