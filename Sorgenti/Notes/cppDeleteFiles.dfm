object frmDeleteFiles: TfrmDeleteFiles
  Left = 432
  Top = 324
  BorderStyle = bsDialog
  Caption = 'frmDeleteFiles'
  ClientHeight = 281
  ClientWidth = 291
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object cmdDelete: TButton
    Left = 176
    Top = 248
    Width = 113
    Height = 25
    Caption = 'Elimina'
    TabOrder = 0
    OnClick = cmdDeleteClick
  end
  object lstFiles: TListBox
    Left = 0
    Top = 0
    Width = 289
    Height = 241
    ItemHeight = 13
    MultiSelect = True
    TabOrder = 1
  end
end
