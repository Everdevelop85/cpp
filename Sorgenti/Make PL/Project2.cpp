#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <string.h>
#include <fcntl.h>
#include <io.h>
#include <dir.h>
#include <dos.h>

struct FILE_AVI
{
        char pathName[255];
        int index;

        void operator=(FILE_AVI &fa)
        {
                strcpy(pathName,fa.pathName);
                index=fa.index;
        }
};

int FileCount(char *p)
{
        int i=0;
        struct ffblk ff;
        if(findfirst(p,&ff,0)==0)
                i=1;
        while(findnext(&ff)==0)
                i++;
        return i;
}

int getIndex(char *name)
{
        int i;
        bool trovato=false;
        i=0;
        char number[5];
        number[0]='-';
        number[1]='1';
        while(name[i+1]!='\0')
        {
                if(((name[i]>='0')&&(name[i]<='9'))&&((name[i+1]>='0')&&(name[i+1]<='9')))
                {
                        trovato=true;
                        break;
                }
                i++;
        }
        if(trovato)
        {
                int j=i;
                while((name[i]>='0')&&(name[i]<='9'))
                {
                        number[i-j]=name[i];
                        i++;
                }
        }
        return atoi(number);
}

void MakeVector(char *p, FILE_AVI *v, int dim)
{
        int i=0;
        struct ffblk ff;
        char directory[255];
        strcpy(directory,p);
        directory[strlen(directory)-5]='\0';
        if(findfirst(p,&ff,FA_ARCH)==0)
        {
                strcpy(v[i].pathName,directory);
                strcat(v[i].pathName,ff.ff_name);
                v[i].index=getIndex(v[i].pathName);
                i++;
        }
        while((findnext(&ff)==0)&&(i<dim))
        {
                strcpy(v[i].pathName,directory);
                strcat(v[i].pathName,ff.ff_name);
                v[i].index=getIndex(v[i].pathName);
                i++;
        }
}

void ordina(FILE_AVI *v, int dim)
{
        int i=0,j=i+1;
        FILE_AVI app;
        while(j<dim)
        {
                if(v[i].index>v[j].index)
                {
                        app=v[i];
                        v[i]=v[j];
                        v[j]=app;
                        i=-1;
                }
                i++;
                j=i+1;
        }
}
int SaveOnFile(char *fn, FILE_AVI *v, int dim)
{
        int f;
        if((f=open(fn,O_CREAT|O_TRUNC|O_WRONLY))<0)
                return -1;
        for(int i=0;i<dim;i++)
        {
                if((write(f,v[i].pathName,strlen(v[i].pathName)))<0)
                        return -1;
                write(f,"\n",1);
        }
        close(f);
        return 0;
}

int main(int argc, char* argv[])
{
        if(argc!=2)
              return 1;
        char path[255],FileName[255]="C:\\Documents and Settings\\All Users\\Desktop\\",fileN[100];
        strcpy(path,argv[1]);
        if(path[strlen(path)-1]!='\\')
              strcat(path,"\\");
        strcat(path,"*.avi");
        FILE_AVI *vector;
        int dimV=FileCount(path);
        vector=(FILE_AVI *) malloc(sizeof(FILE_AVI)*dimV);
        MakeVector(path,vector,dimV);
        ordina(vector,dimV);
        system("CLS");
        printf("Inserire il nome della playlist: ");
        scanf("%s",fileN);
        strcat(FileName,fileN);
        if(SaveOnFile(FileName,vector,dimV)<0)
        {
                system("CLS");
                printf("Errore nella creazione della playlist\n\n");
                system("PAUSE");
        }
        return 0;
}


